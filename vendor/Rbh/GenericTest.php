<?php
//%LICENCE_HEADER%

namespace Rbh;
use Rbh\Material\UsedMaterial;
use Rbh\Material\Material;
use Rbh\Material\Recipe;
use Rbh\Dao\Pg\DaoList As DaoList;
use Rbh\Dao\Pg\Loader AS Loader;
use Rbh\Dao\Factory AS DaoFactory;

use Rbh\Pdm;
use Rbh\Build\Product;

use Rbh\Parameter;
use Rbh\Parameter\Formula;

/**
 * @brief Example of use
 *
 */
class GenericTest extends \Rbh\Test\Test
{
	public static function recursiveSave($Any){
		foreach($Any->links as $lnk){
			if($lnk->rule==Link::RULE_PARENT){
				continue;
			}
			if($lnk->child){
				if($lnk->child->links){
					self::recursiveSave($lnk->child);
				}
				$Dao=DaoFactory::getDao($lnk->child)->save($lnk->child);
			}
		}
	}

	/**
	 *
	 */
	public static function recursifChildren($obj){
		//echo str_repeat('.', $obj->level);
		echo $obj->name  .' - '. get_class($obj);
		echo ' -- ID:'.$obj->id . '--';
		if(is_a($obj,'\Rbh\Material\Recipe')){
			//echo ' volume='.$obj->getInheritedVolume().'m3';
			echo ' density='.$obj->density.'Kg/m3';
			echo ' weight='.$obj->weight.'Kg';
		}
		elseif(is_a($obj,'\Rbh\Material\Material')){
			//echo ' volume='.$obj->getInheritedVolume().'m3';
			echo ' density='.$obj->density.'Kg/m3';
			//echo ' weight='.$obj->getWeight().'Kg';
		}
		elseif(is_a($obj,'\Rbh\Build\Surface')){
			echo ' Volume='.$obj->volume.'m3';
			echo ' Surface='.$obj->surface.'m2';
			echo ' Thickness='.$obj->thickness.'m';
		}
		echo  CRLF;

		if(is_array($obj->links)){
			foreach($obj->links as $lnk){
				self::recursifChildren($lnk->child);
			}
		}
	}

	/**
	 *
	 */
	public static function recursifParent($obj){
	}


	public function Test_Parameters()
	{
		$Material1 = \Rbh\Material\Material::init('gt_material1');

		$WindowFrame = Product::init();
		$WindowFrame->addParam('H',new Parameter\Length(5000));
		$WindowFrame->addParam('L',new Parameter\Length(5000));

		$Joint=Product::init();
		$Joint->setMaterial($Material1);
		$Joint->addParam( 'Long',new Parameter\Length(0), 'Longueur');

		$Cadre=Product::init();
		$Cadre->addParam( 'H',new Parameter\Length(1500), 'Hauteur');
		$Cadre->addParam( 'L',new Parameter\Length(950), 'Largeur');
		$Cadre->addParam( 'P',new Parameter\Length(400), 'Profondeur');
		
		$Appui=Product::init('Appui');
		$Appui->addParam('Ep',new Parameter\Length(0));
		$Appui->addParam('Larg',new Parameter\Length(0));
		$Appui->addParam('Long',new Parameter\Length(0));
		$Appui->setMaterial($Material1);

		$BandeauD=Product::init('BandeauD');
		$BandeauD->addParam('Ep',new Parameter\Length(0));
		$BandeauD->addParam('Larg',new Parameter\Length(0));
		$BandeauD->addParam('Long',new Parameter\Length(0));
		$BandeauD->setMaterial($Material1);

		$BandeauG=Product::init('BandeauG');
		$BandeauG->addParam('Ep',new Parameter\Length(0));
		$BandeauG->addParam('Larg',new Parameter\Length(0));
		$BandeauG->addParam('Long',new Parameter\Length(0));
		$BandeauG->setMaterial($Material1);

		$BandeauH=Product::init('BandeauH');
		$BandeauH->addParam('Ep',new Parameter\Length(20));
		$BandeauH->addParam('Larg',new Parameter\Length(20));
		$BandeauH->addParam('Long',new Parameter\Length(0));
		$BandeauH->setMaterial($Material1);

		$Cadre->addComponent('Appui', $Appui);
		$Cadre->addComponent('BandeauD', $BandeauD);
		$Cadre->addComponent('BandeauG', $BandeauG);
		$Cadre->addComponent('BandeauH', $BandeauH);

		$WindowFrame->addComponent('Joint', $Joint);
		$WindowFrame->addComponent('Cadre', $Cadre);
		
		$WindowFrame->addFormula('Cadre.H=H');
		$WindowFrame->addFormula('Cadre.L=L');
		$WindowFrame->addFormula('Joint.Long=2*H+2*L');

		$Cadre->addFormula('Appui.Long=L');
		$Cadre->addFormula('Appui.Larg=P');
		$Cadre->addFormula('Appui.Ep=0.040');
		
		$Cadre->addFormula('BandeauD.Long=L+Appui.Ep');
		$Cadre->addFormula('BandeauD.Larg=P');
		$Cadre->addFormula('BandeauD.Ep=0.027');

		$Cadre->addFormula('BandeauG.Long=Appui.Ep+L');
	 	$Cadre->addFormula('BandeauG.Larg=P');
		$Cadre->addFormula('BandeauG.Ep=0.027');
		
		$Cadre->addFormula('BandeauH.Long=L');
		$Cadre->addFormula('BandeauH.Larg=P');
		$Cadre->addFormula('BandeauH.Ep=0.027');
		
		//var_dump(in_array('nimportequoi', array('untruc',0,'autretruc'),true));die; //tjrs vrai si 3eme arg est omis
		
		//$Formula = new Formula('BandeauH.Ep=10+sin(45+45)', $Cadre);
		//echo $Formula->evaluate() . CRLF;
		//$Formula = new Formula('Appui.Ep=BandeauH.Ep+BandeauH.Long', $Cadre);
		//echo $Formula->evaluate() . CRLF;
		//$BandeauHEp = $Cadre->getComponent('BandeauH')->getParam('Ep')->getValue();
		//echo $BandeauHEp.CRLF;

		$WindowFrame->evaluate();
		//$Cadre->evaluate();
		
		echo $Cadre->getParam('L')->getValue() . CRLF;
		echo $Cadre->getComponent('BandeauH')->getParam('Long')->getValue() . CRLF;
		echo $Cadre->getComponent('BandeauH')->getParam('Larg')->getValue() . CRLF;
		echo $Cadre->getComponent('BandeauH')->getParam('Ep')->getValue() . CRLF;
		
		//https://github.com/ircmaxell/php-math-parser
		//http://codehackit.blogspot.fr/2011/08/expression-parser-in-php.html
		
		die;
	}


	public function Test_Registry()
	{
		$registry=\Rbh\Dao\Registry::singleton();
		$Obj1 = \Rbh\AnyObject::init('obj1');
		$Obj2 = \Rbh\AnyObject::init('obj2');
		$Obj3 = \Rbh\AnyObject::init('obj3');

		$Obj1->id = 1;
		$Obj2->id = 2;
		$Obj3->id = 3;

		$registry->add($Obj1);
		$registry->add($Obj2);
		$registry->add($Obj3);

		$B1=$registry->get(1);
		assert($B1===$Obj1);
		//die;
	}


	/**
	 *
	 * @access public
	 */
	public function _Test_Phpfunc()
	{
		$a1=array('a','b','c');
		array_walk($a1, function (&$val,$key){
			$val=':'.$val;
		});
		var_dump($a1);

		$a2 = array('a:'=>'AA','b:'=>'BB');

		$aI = array_intersect_key($a1, array_keys($a2));

		var_dump($aI);

		$bind = array(
			':uid'=>100,
			':name'=>'myname',
			':label'=>'monlabel'
		);
		$select = array('uid', 'name');

		array_filter($bind, function($val){
			var_dump($val);
		});

		array_walk($select, function (&$val,$key){
			$val=':'.$val;
		});
		$select = array_flip($select);

		var_dump($bind,$select);
		$bind2 = array_intersect_key($bind, $select);

		var_dump($bind2);
	}

	public function Test_Serialize()
	{
		$Parent = \Rbh\AnyObject::init('parent');
		$Parent->id = $parentId;
		$Child1 = \Rbh\AnyObject::init('child1');
		$Child1->id = $child1Id;
		$Child2 = \Rbh\AnyObject::init('child2');
		$Child2->id = $Child2Id;

		$link1 = $Parent->addLink($Child1);
		$link2 = $Parent->addLink($Child2);

		$serializedStr=serialize($Parent);
		var_dump($serializedStr);

		$unserialized=unserialize($serializedStr);
		//var_dump($unserialized->links);
		assert(array_pop($unserialized->links)->child->name == 'child2');
		assert(array_pop($unserialized->links)->child->name == 'child1');
	}



	/**
	 * Un object a des liens vers d'autres objets.
	 * Un objet liée, peut-être utilisé par son parent au travers d'une instance
	 * de l'objet composite AnyUsed.
	 * Le lien ne contient pas d'objet, seulement des references explicit au ID.
	 * L'objet AnyUsed contient un objet Link et un objet AnyObject.
	 * @todo : archiver model UML :  <path to uml file a compléter>
	 *
	 */
	public function Test_AnyLinkUsed()
	{
		$parentId = 10000;
		$child1Id = 10001;
		$child2Id = 10002;
		$Parent = \Rbh\AnyObject::init('parent');
		$Parent->id = $parentId;
		$Child1 = \Rbh\AnyObject::init('child1');
		$Child1->id = $child1Id;
		$Child2 = \Rbh\AnyObject::init('child2');
		$Child2->id = $Child2Id;

		//---------First way ----------------
		$link1 = $Parent->addLink($Child1, \Rbh\Link::RULE_NONE);
		$link1->data['volume'] = 10;
		$usedChild1 = new \Rbh\AnyUsed($Child1, $link1);
		assert($usedChild1->getData('volume') == 10);
		assert($link1->childId == $Child1->id);
		assert($usedChild1->childId == $Child1->id);
		assert($usedChild1->parentId == $Parent->id);
		assert($link1->child == $Child1);
		assert($link1->parent == $Parent);

		//-------OR second way ---------------
		$Parent->links = null;
		$link1 = \Rbh\Link::init();
		$link1->data['volume'] = 10;
		$link1->childId = $Child1->id;
		$link1->child = $Child1;
		//Parent is set in Link by addChildren method :
		$usedChild1 = new \Rbh\AnyUsed($Child1, $link1);
		$Parent->addChildren($usedChild1);
		assert($link1->child == $Child1);
		assert($link1->parent == $Parent);
		//Test of proxy to link
		assert($usedChild1->getData('volume') == 10);
		assert($link1->childId == $Child1->id);
		assert($link1->parentId == $Parent->id);
		assert($usedChild1->childId == $Child1->id);
		assert($usedChild1->parentId == $Parent->id);

		//var_dump($Parent->links);
		$Child1->parent = null;
		$link1->parent = 'TITI';
		assert($usedChild1->parent === 'TITI');
		$Child1->parent = 'TOTO';
		assert($usedChild1->parent === 'TITI');
		$link1->parent = false;
		assert($usedChild1->parent === false);
		$link1->parent = null;
		assert($usedChild1->parent === 'TOTO');
		var_dump($usedChild1->parent);
	}

	public function Test_DaoCRDULinkUsed()
	{
	}

	/**
	 *
	 * @access public
	 * @see \Rbh\Build\SurfaceTest
	 */
	public function Test_Generic()
	{
		$monProjet = \Rbh\Project\Project::init('gt_monprojet');

		$surface1 = \Rbh\Build\Surface::init('gt_surface1');
		$monProjet->addLink($surface1);
		$surface1->surface = 20; //m2

		$surfaceDao = DaoFactory::getDao($surface1);
		$surfaceDao->save($surface1);

		$material1 = \Rbh\Material\Material::init('gt_material1');
		$material1->thLambda = 0.1;
		$material1->density =1000;
		$material2 = \Rbh\Material\Material::init('gt_material2');
		$material2->thLambda = 1;
		$material2->density =2000;
		$material3 = \Rbh\Material\Material::init('gt_material3');
		$material3->thLambda = 10; //Sapin
		$material3->density =4000;

		$monProjet->addLink($material1);
		$monProjet->addLink($material2);
		$monProjet->addLink($material3);

		//--------- Create a beton ---------------------
		$ingredient1 = \Rbh\Material\Material::init('gt_ciment');
		$ingredient1->density = 1200;
		$ingredient2 = \Rbh\Material\Material::init('gt_sablesec');
		$ingredient2->density = 1400;
		$ingredient3 = \Rbh\Material\Material::init('gt_gravier');
		$ingredient3->density = 2000;

		$monProjet->addLink($ingredient1);
		$monProjet->addLink($ingredient2);
		$monProjet->addLink($ingredient3);

		$projectDao = DaoFactory::getDao($monProjet);
		$materialDao = DaoFactory::getDao($material1);
		$materialDao->save($material1);
		$materialDao->save($material2);
		$materialDao->save($material3);
		$materialDao->save($ingredient1);
		$materialDao->save($ingredient2);
		$materialDao->save($ingredient3);
		$projectDao->save($monProjet);

		//$usedMat1=UsedMaterial::newUsed($material1);
		//$usedMat2=UsedMaterial::newUsed($material2);
		//$usedMat3=UsedMaterial::newUsed($material3);
		//$usedMat4=UsedMaterial::used($ingredient1);
		//$usedMat5=UsedMaterial::used($ingredient2);
		//$usedMat6=UsedMaterial::used($ingredient3);

		//$linkDao = DaoFactory::getDao('\Rbh\Link');
		//$linkDao->insert($usedMat1->useLink);
		//$linkDao->insert($usedMat2->useLink);
		//$linkDao->insert($usedMat3->useLink);

		$recipe1 = Recipe::init('gt_beton1Recipe');
		$recipe1->volume = 1;
		$recipe1->addIngredient($ingredient1, 'gt_beton1Recipe/liant', 250, 'kg');
		$recipe1->addIngredient($ingredient2, 'gt_beton1Recipe/granulat04', 0.5, 'm3');
		$recipe1->addIngredient($ingredient3, 'gt_beton1Recipe/granulat30', 0.5, 'm3');
		$recipe1->prepare(1);
		$materialDao->save($recipe1);

		/*
		 $link = \Rbh\Link::init(array('childId'=>$Material1->id, 'name'=>'material'));
		$Layer1->addLink($link);
		$link->name = 'test';
		var_dump($Layer1->links[$link->id]);
		*/

		//Create layer and set a material
		$layer1 = \Rbh\Build\Surface::init('gt_layer1');
		$layer1->thickness = 0.01; //m
		$layer1->addLink($material1, \Rbh\Link::RULE_NONE);

		$layer2 = \Rbh\Build\Surface::init('gt_layer2');
		$layer2->thickness = 0.01; //m
		$layer2->addLink($material2, \Rbh\Link::RULE_NONE);

		$layer3 = \Rbh\Build\Surface::init('gt_layer3');
		$layer3->thickness = 0.01; //m
		$layer3->addLink($material3, \Rbh\Link::RULE_NONE);

		$layer4 = \Rbh\Build\Surface::init('gt_layer4');
		$layer4->thickness = 0.01; //m
		$layer4->addLink($recipe1, \Rbh\Link::RULE_NONE);

		$surfaceDao->save($layer1);
		$surfaceDao->save($layer2);
		$surfaceDao->save($layer3);
		$surfaceDao->save($layer4);

		//attach layers to surface1
		$link1 = \Rbh\Link::init(array(
			'parentId'=>$surface1->id,
			'childId'=>$layer1->id,
			'name'=>'layer1',
			'lindex'=>2,
			'data'=>'',
		));
		$link2 = \Rbh\Link::init(array(
			'parentId'=>$surface1->id,
			'childId'=>$layer2->id,
			'name'=>'layer2',
			'lindex'=>1,
			'data'=>'',
		));
		$link3 = \Rbh\Link::init(array(
			'parentId'=>$surface1->id,
			'childId'=>$layer3->id,
			'name'=>'layer3',
			'lindex'=>0,
			'data'=>'',
		));
		$link4 = \Rbh\Link::init(array(
			'parentId'=>$surface1->id,
			'childId'=>$layer4->id,
			'name'=>'layer4',
			'lindex'=>0,
			'data'=>'',
		));

		$LinkDao = DaoFactory::getDao($link1);
		$LinkDao->insert(array($link1, $link2, $link3, $link4));

		$monProjetId = $monProjet->id;
		$surface1Id  = $surface1->id;
		$material1Id = $material1->id;
		$material2Id = $material2->id;
		$material3Id = $material3->id;
		$layer1Id = $layer1->id;
		$layer2Id = $layer2->id;
		$layer3Id = $layer3->id;
		$layer4Id = $layer4->id;

		//-------------------Reload all -----------------
		$monProjet 	= null;
		$surface1 	= null;
		$material1 	= null;
		$material2 	= null;
		$material3 	= null;
		$layer1 	= null;
		$layer2 	= null;
		$layer3 	= null;
		$layer4 	= null;

		$layer1 = $surfaceDao->load(new \Rbh\Build\Surface(), 'id='.$layer1Id);
		$layer1 = $surfaceDao->load(new \Rbh\Build\Surface(), 'id='.$layer1Id, array('withlink'=>true));
		$monProjet = $projectDao->load(new \Rbh\Project\Project(), 'id='.$monProjetId, array('withlink'=>true));
		$surface1 = $surfaceDao->load(new \Rbh\Build\Surface(), 'id='.$surface1Id, array('withlink'=>true));

		//var_dump($layer1);
		//var_dump($monProjet);

		//surface1 and material1, material2, material3 are in project:
		/*
		foreach($monProjet->links as $l){
		assert(in_array($l->childId, array($surface1Id,$material1Id, $material2Id, $material3Id )));
		}
		*/

		//material1 as no children
		assert($material1->links == null);

		//$surface1 has 3 layers, ordered by lindex
		//var_dump($surface1->links, $material1Id, $material2Id, $material3Id);
		//assert(count($surface1->links) == 3);
		//assert($surface1->links[0]->lindex == 0);
		//assert($surface1->links[1]->lindex == 1);
		//assert($surface1->links[2]->lindex == 2);

		//assert($surface1->links[0]->childId == $layer3Id);
		//assert($surface1->links[1]->childId == $layer2Id);
		//assert($surface1->links[2]->childId == $layer1Id);

		$layers = array();
		$materials = array();
		foreach($surface1->links as $l){
			$layers[] = $surfaceDao->load(new \Rbh\Build\Surface(), 'id='.$l->childId, array('withlink'=>true));
			//var_dump(end($layers)->name);
			//each layer has material
			$materialId = end($layers)->links[0]->childId;
			$materials[] = $materialDao->load(new \Rbh\Material\Material(), 'id='.$materialId, array('withlink'=>true));
			//var_dump(end($materials)->name);
		}

		//------------ get list of materials used by surface1 --------------------

		/*------------First method------------------------------------------------
		 $List = new DaoList();

		$List->table = 'build_surface';
		$List->loadTree("tg.lpath <@ '$surface1Id'");
		$registry = array();
		echo '-------------------------------'.CRLF;
		foreach($List as $item){
		echo str_repeat('.', $item['level']) . $item['name']  .' - '. $item['classname'] . CRLF;
		//var_dump($item);
		$obj = $surfaceDao->loadFromArray(new $item['classname'](), $item);
		$obj->parentId = $item['lparent'];
		$obj->path = $item['lpath'];
		$obj->level = $item['level'];
		//$obj->addLink($LinkDao->loadFromArray(new \Rbh\Link(), $item));
		//$obj = new $item['classname']($item);
		$registry[$obj->id]=$obj;
		}

		$List->table = 'material';
		$List->loadTree("tg.lpath <@ '$surface1Id'");
		echo '-------------------------------'.CRLF;
		foreach($List as $item){
		echo str_repeat('.', $item['level']) . $item['name']  .' - '. $item['classname'] . CRLF;
		$obj = $materialDao->loadFromArray(new $item['classname'](), $item);
		$obj->parentId = $item['lparent'];
		$obj->path = $item['lpath'];
		$obj->level = $item['level'];
		//$obj = new $item['classname']($item);
		$registry[$obj->id]=$obj;
		}

		$List->table = 'anyobject';
		$List->loadTree("tg.lpath <@ '$surface1Id'");
		echo '-------------------------------'.CRLF;
		foreach($List as $item){
		echo str_repeat('.', $item['level']) . $item['name']  .' - '. $item['classname'] . CRLF;
		}

		$surface1 = $surfaceDao->load(new \Rbh\Build\Surface(), 'id='.$surface1Id, array('withlink'=>false));

		//create bi-direction links parent/child
		foreach($registry as $obj){
		$parent = $registry[$obj->parentId];
		$obj->parent = $parent;
		$parent->children[] = $obj;
		if($obj->level == 1){
		$surface1->children[] = $obj;
		$obj->parent = $surface1;
		}
		}
		//unset($registry);
		$this->recursifChildren($surface1);
		*/

		echo '--------------------------------- Recursive tree ----------------------'.CRLF;

		Loader::init();
		$surface1 = $surfaceDao->load(new \Rbh\Build\Surface(), 'id='.$surface1Id);
		Loader::loadChildren($surface1);

		//parcours haut->bas pour set des héritages :
		$this->recursifChildren($surface1);

		//parcours bas->haut pour cumul des masses et volumes
		//$this->recursifParent($surface1);
	}

	/**
	 *
	 * @access public
	 */
	public function _Spec001()
	{
		$Monprojet = \Rbh\Project\Project::init('monprojet', \Rbh\Org\Root::singleton());
		$projetDao = DaoFactory::getDao($Monprojet);
		$projetDao->save($Monprojet);
			
		//$Materials = \Rbh\Material\Collection::init('materials', \Rbh\Org\Root::singleton());
		$Systems = \Rbh\Org\Unit::init('systems', \Rbh\Org\Root::singleton());
			
		//$Ciment = new Rbh\Material\Bulk('ciment', $Materials);
		$Ciment = $Materials->newMaterial('ciment', Rbh\Material\Bulk);
		$Ciment->density=1400;
		$Ciment->getPackaging()->setQty(35, 'kg');
		$Ciment->getPackaging()->setPrice(10);
		$Sable = Rbh\Material\Bulk::init('sable', $Materials);
		$Beton = Rbh\Material\Composite::init('beton', $Materials);
		$Beton->add($Ciment, 3, 'vol');
		$Beton->add($Sable, 1, 'vol');
		$Beton->qty(5, 'm3');
		echo $Beton->getByName('ciment')->getMasse();
		echo $Beton->getByName('ciment')->getVolume();
		echo $Beton->getByName('ciment')->getUnit();

		$SemelleFilante = buildSystem::init('semelleFilante', $Systems);
		$SemelleFilante->addMaterial($Beton, 1, 'm3', 'm3'); //1m3 par m3
		$SemelleFilante->addMaterial($Fer, 1, 'm', 'ml'); //1m par metre lineaire
			
		$Semmelle = buildSurface::init('semelle', $Monprojet);
		$Semmelle->setBuildSystem($SemelleFilante);
		$Semelle->length=5; //m
		$Semelle->width=0.4; //m
		$Semelle->height=500; //m
		//ou
		$Semelle->volume = (5*0.4*0.5); //m3
		//ou
		$Semelle->surface = (5*0.4); //m2
		$Semelle->height = 0.5; //m

		echo $Semelle->getMaterial('beton')->getQty();
		echo $Semelle->getMaterial('beton')->getMaterial('ciment')->getQty();
		$Semelle->getPrice();

		//Definition d'un systeme avec parametre et regles de conceptions liants les composants
		$SemelleFilante = buildSystem::init('semelleFilante');
		$SemelleFilante->addParameter('width');
		$SemelleFilante->addParameter('height');
		$SemelleFilante->addParameter('length');
		$SemelleFilante->addParameter('volume');
		$SemelleFilante->addConstraint('$/semelleFilante/volume', '=/$semelleFilante/width$/ * /$semelleFilante/height$/ */$semelleFilante/length$/');
		$SemelleFilante->addMaterial($Beton, 'beton');
		$SemelleFilante->addMaterial($FerDeFondDeFouille, 'fer');
		$SemelleFilante->addConstraint($FerDeFondDeFouille->getParameter('length'), '=$/semelleFilante/length$/');
		// ou $SemelleFilante->addConstraint('$/semelleFilante/fer/length', '=/$semelleFilante/length$/');
		$SemelleFilante->addConstraint('$/semelleFilante/beton/volume$/', '=$/semelleFilante/volume$/');

		$Rule1 = ConceptCheckRule::init();
		$Rule2 = ConceptCheckRule::init();
		$Rule1->set('/$semelleFilante/beton/width$/ > 1.2 * /$semelleFilante/ferFondDeFouille/width$/');
		$Rule2->set('/$semelleFilante/beton/width$/ < 2 * /$semelleFilante/ferFondDeFouille/width$/');
		$SemelleFilante->setConceptRule($Rule1);
		$SemelleFilante->setConceptRule($Rule2);

		//reste plus qu'a definir les qtes:
		$SemelleFilante->setParameter('width', 500);
		$SemelleFilante->setParameter('height', 300);
		$SemelleFilante->setParameter('length', 10000);

		//Definition de famille de component
		$FondDeFouille = MaterialFamily::init('fondDeFouille');
		$FondDeFouille->addParameter('width');
		$FondDeFouille->addParameter('height');
		$FondDeFouille->addParameter('length');
		$FondDeFouille->addConstraint($FondDeFouille->getParameter('width'), '=1.2 * /$fondDeFouille/height$/');
			
		$df1 = $FondDeFouille->new(array('height'=>100, 'length'=>6000), 'fondDeFouille100x6');
		$df2 = $FondDeFouille->new(array('height'=>200, 'length'=>6000), 'fondDeFouille200x6');
		$df3 = $FondDeFouille->new(array('height'=>100, 'length'=>3000), 'fondDeFouille100x3');
		$df4 = $FondDeFouille->new(array('height'=>200, 'length'=>3000), 'fondDeFouille200x3');
			
		$df1 = $FondDeFouille->getMember('fondDeFouille100x6');
			
		//Exemple de constitution d'un sol
		$Gravier10 = Material::init('gravier10lave');
		$Gravier10->setVolumeWeight(1500);
		$Gravier5 = Material::init('gravier5lave');
		$Geotextile = Material::init('geotextile');
		$Roofmat30 = Material::init('roofmat30');
		$Beton = CompositeMaterial::init('beton');
		//..
		$Carelage = Material::init('carelage');
		$Colle = Material::init('colle');
			
		$SolSurTerrePlein = buildSystem::init('SolSurTerrePlein');
		$SemelleFilante->addParameter('gravierEp');
		$SemelleFilante->addParameter('isolantEp');
		$SemelleFilante->addParameter('dalleEp');
		$SemelleFilante->addParameter('chapeEp');
		$SolSurTerrePlein->addMaterial($Gravier10, 'gravier10');
		$SolSurTerrePlein->addMaterial($Gravier5, 'gravier5');
		$SolSurTerrePlein->addMaterial($Geotextile, 'geotxt');
		$SolSurTerrePlein->addMaterial($Roofmat30, 'roofmat');
		$SolSurTerrePlein->addMaterial($Beton, 'dalle');
		$SolSurTerrePlein->addMaterial($Beton, 'chape');
		$FondDeFouille->addConstraint();
	}

}
