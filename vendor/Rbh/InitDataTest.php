<?php
//%LICENCE_HEADER%

namespace Rbh;
use Rbh\Material\UsedMaterial;
use Rbh\Material\Material;
use Rbh\Material\Recipe;
use Rbh\Dao\Pg\DaoList As DaoList;
use Rbh\Dao\Pg\Loader AS Loader;
use Rbh\Dao\Factory AS DaoFactory;

use Rbh\Pdm;
use Rbh\Build\Product;

use Rbh\Parameter as Parameter;

/**
 * @brief Example of use
 *
 */
class InitDataTest extends \Rbh\Test\Test
{

	/**
	 *
	 * @access public
	 * @see \Rbh\Build\SurfaceTest
	 */
	public function Test_InitMaterials()
	{
		$monProjet = \Rbh\Project\Project::init('gt_monprojet');
		$projectDao = DaoFactory::getDao($monProjet);
		$projectDao->save($monProjet);
		
		$material1 = \Rbh\Material\Material::init('bois_peuplier');
		$material1->thLambda = 0.1;
		$material1->density =450;
		
		$material2 = \Rbh\Material\Material::init('bois_chene');
		$material2->thLambda = 0.1;
		$material2->density =800;
		
		$material3 = \Rbh\Material\Material::init('bois_sapin');
		$material3->thLambda = 0.2; //Sapin
		$material3->density =500;
		
		$material4 = \Rbh\Material\Material::init('bois_douglas');
		$material3->thLambda = 0.1; //Sapin
		$material3->density =750;
		
		//--------- Create a beton ---------------------
		$ingredient1 = \Rbh\Material\Bulk::init('gt_ciment');
		$ingredient1->density = 1200;
		$ingredient2 = \Rbh\Material\Bulk::init('gt_sablesec');
		$ingredient2->density = 1400;
		$ingredient3 = \Rbh\Material\Bulk::init('gt_gravier');
		$ingredient3->density = 2000;

		$materialDao = DaoFactory::getDao($material1);
		$materialDao->save($material1);
		$materialDao->save($material2);
		$materialDao->save($material3);
		$materialDao->save($material4);
		$materialDao->save($ingredient1);
		$materialDao->save($ingredient2);
		$materialDao->save($ingredient3);

		$recipe1 = Recipe::init('gt_beton1Recipe');
		$recipe1->volume = 1;
		$recipe1->addIngredient($ingredient1, 'gt_beton1Recipe/liant', 250, 'kg');
		$recipe1->addIngredient($ingredient2, 'gt_beton1Recipe/granulat04', 0.5, 'm3');
		$recipe1->addIngredient($ingredient3, 'gt_beton1Recipe/granulat30', 0.5, 'm3');
		$recipe1->prepare(1);
		$materialDao->save($recipe1);
	}

}
