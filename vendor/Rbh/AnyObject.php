<?php
//%LICENCE_HEADER%

namespace Rbh;

use Rbh\Link as Link;
use Rbh\Dao\MappedInterface;
use Rbh\Any;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class AnyObject extends Any implements MappedInterface
{
	/**
	 * Implement design pattern COMPOSITE OBJECT.
	 * 
	 * @var array of Rbh\AnyUsed
	 * @cardinal 0,n
	 */
	public $children = array();
	
	/**
	 * @var array of Rbh\Link
	 * @cardinal 0,n
	 */
	public $links = array();
	
	/**
	 * Label is use to construct path. It must be composed of only characters [0-9A-Za-z] and -.
	 * @var string
	 */
	public $label;
	
	/**
	 * @var integer
	 */
	public $ownerId;
	
	/**
	 *
	 * @var integer
	 */
	public $classId;
	
	/**
	 * Implements Rbh\Dao\MappedInterface.
	 * True if object is loaded from db.
	 *
	 * @var boolean
	 */
	protected $isLoaded = false;
	
	/**
	 * @var boolean
	 */
	protected $isLeaf = true;
	
	/**
	 * @param string $name
	 * @return \Rbh\AnyObject
	 */
	public static function init($name='')
	{
		$class = get_called_class();
		$obj = new $class();
		$obj->uid = \Rbh\Uuid::newUid();
		$obj->name = $name;
		$obj->ownerId = \Rbh\People\User::SUPER_USER_ID;
		return $obj;
	}
	
	/**
	 * @param \Rbh\AnyObject
	 * @return \Rbh\Link
	 */
	public function addLink(\Rbh\AnyObject $any, $rule=0)
	{
		if($rule==\Rbh\Link::RULE_PARENT){
			$link = \Rbh\Link::init(array(
				'parentId'=>$any->id, 
				'childId'=>$this->id,
				'parent'=>$any,
				'child'=>$this,
				'rule'=>$rule));
		}
		else{
			$link = \Rbh\Link::init(array(
				'parentId'=>$this->id, 
				'childId'=>$any->id,
				'parent'=>$this,
				'child'=>$any,
				'rule'=>$rule));
		}
		$this->links[$any->uid] = $link;
		return $link;
	} //End of method
	
	/**
	 * @param \Rbh\AnyUsed
	 * @return void
	 */
	public function addChildren(\Rbh\AnyUsed $used)
	{
		$this->children[$used->link->uid] = $used;
		$used->useLink->parentId = $this->id;
		$used->useLink->parent = $this;
	} //End of method
	
	/**
	 * Implements Rbh\Dao\MappedInterface.
	 * 
	 * @see library/Rbh/Dao/MappedInterface#isLoaded($bool)
	 * 
	 * Setter/Getter
	 * Return true if the object is loaded from database
	 *
	 * @param boolean	$bool	Set load state
	 * @return boolean
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->isLoaded;
		}
		else{
			return $this->isLoaded = (boolean) $bool;
		}
	}
	
}
