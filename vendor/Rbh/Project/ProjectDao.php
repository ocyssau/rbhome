<?php
//%LICENCE_HEADER%

/** SQL_SCRIPT>>
 CREATE TABLE project(
 description varchar(255)
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (1020, 'Rbh\Project\Project', 'project');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE project ADD PRIMARY KEY (id);
 ALTER TABLE project ADD UNIQUE (uid);
 ALTER TABLE project ADD UNIQUE (path);
 ALTER TABLE project ALTER COLUMN cid SET DEFAULT 1020;
 CREATE INDEX INDEX_project_owner ON project USING btree (owner);
 CREATE INDEX INDEX_project_uid ON project USING btree (uid);
 CREATE INDEX INDEX_project_name ON project USING btree (name);
 CREATE INDEX INDEX_project_label ON project USING btree (label);
 CREATE INDEX INDEX_project_path ON project USING btree (path);
 <<*/

/** SQL_FKEY>>
ALTER TABLE project ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_project AFTER INSERT OR UPDATE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_project AFTER DELETE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

namespace Rbh\Project;
use \Rbh\Org\UnitDao As Dao;

/**
 * @brief Dao class for Rbplm_Org_Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Org_UnitTest
 *
 */
class ProjectDao extends Dao
{
	/**
	 *
	 * @var string
	 */
	var $table = 'project';

	/**
	 *
	 * @var integer
	 */
	var $classId = 1020;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array('description'=>'description');
} //End of class
