<?php
//%LICENCE_HEADER%

namespace Rbh;

/**
 * @brief This class enables you to get real uuids using the OSSP library.
 * 
 * Note you need php-uuid installed.
 * @code
 * //On ubuntu, do simply
 * sudo apt-get install php5-uuid
 * @endcode
 *
 * @see http://fr.wikipedia.org/wiki/Universal_Unique_Identifier
 * @author Marius Karthaus
 * @author Eric COSNARD
 * @author Olivier CYSSAU
 * 
 *
 *
 */
class Uuid{

	protected $uuidobject;

	/**
	 * Generate a uniq identifier
	 *
	 * @return String
	 */
	public static function newUid()
	{
		if (function_exists('com_create_guid')){
			return self::format(com_create_guid());
		}
		else if(function_exists('uuid_create')){
			$uuid = new self();
			return $uuid->v4();
		}
		else{
		    $chars = md5(uniqid(mt_rand(), true));
		    $uuid  = substr($chars,0,8) . '-';
		    $uuid .= substr($chars,8,4) . '-';
		    $uuid .= substr($chars,12,4) . '-';
		    $uuid .= substr($chars,16,4) . '-';
		    $uuid .= substr($chars,20,12);
		    return $uuid;
		}
	} // End of method


	/**
	 * Format a uuid in accordance to Rbplm requierement.
	 * 
	 * if $format = 1 [DEFAULT]
	 * A Rbplm Uuid is in lower case without starting'{' and ending '}'.
	 * 
	 * if $format = 2
	 * A Rbplm Uuid is in upper case with starting'{' and ending '}'.
	 *
	 * @param string	uuid	$uuid
	 * @param string	integer	$format	php-uuid|com
	 * @return string	uuid
	 */
	public static function format($uuid, $format = 'php-uuid')
	{
		if($format==='php-uuid'){
			$uuid = strtolower( trim($uuid, '{}') );
		}
		else{
			$uuid = chr(123) . strtoupper( trim($uuid, '{}') ) . chr(125);
		}
		return $uuid;
	}
	
	
	/**
	 * Compare 2 uuid and return true if same value.
	 * 
	 * @param string	uuid	$uuid1
	 * @param string	uuid	$uuid2
	 * @return boolean
	 */
	public static function compare($uuid1, $uuid2)
	{
		if( strtolower(trim($uuid1, '{}')) == strtolower(trim($uuid2, '{}')) ){
			return true;
		}
		else{
			return false;
		}
	}
	

	/**
	 * On long running deamons i've seen a lost resource. This checks the resource and creates it if needed.
	 *
	 */
	protected function create() {
		if (! is_resource ( $this->uuidobject )) {
			uuid_create ( $this->uuidobject );
		}
	}
	
	
	/**
	 * Return a type 1 (MAC address and time based) uuid
	 *
	 * @return String
	 */
	public function v1() {
		$this->create ();
		uuid_make ( $this->uuidobject, UUID_MAKE_V1 );
		uuid_export ( $this->uuidobject, UUID_FMT_STR, $uuidstring );
		return trim ( $uuidstring );
	}
		
	/**
	 * Return a type 4 (random) uuid
	 *
	 * @return String
	 */
	public function v4() {
		$this->create ();
		uuid_make ( $this->uuidobject, UUID_MAKE_V4 );
		uuid_export ( $this->uuidobject, UUID_FMT_STR, $uuidstring );
		return trim ( $uuidstring );
	}
		
	/**
	 * Return a type 5 (SHA-1 hash) uuid
	 *
	 * @return String
	 */
	public function v5() {
		$this->create ();
		uuid_make ( $this->uuidobject, UUID_MAKE_V5 );
		uuid_export ( $this->uuidobject, UUID_FMT_STR, $uuidstring );
		return trim ( $uuidstring );
	}
}
