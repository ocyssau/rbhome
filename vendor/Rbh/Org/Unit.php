<?php
//%LICENCE_HEADER%

namespace Rbh\Org;

use Rbh\AnyObject As AnyObject;


/**
 * @brief Organizational unit.
 * 
 * Xtract from wikipedia:
 * 
 * OU provides a way of classifying objects located in directories, or names in a digital certificate hierarchy,
 * typically used either to differentiate between objects with the same name
 * (John Doe in OU "marketing" versus John Doe in OU "customer service"),
 * or to parcel out authority to create and manage objects (for example: to give rights for user-creation to local
 * technicians instead of having to manage all accounts from a single central group).
 * 
 * Example and tests: Rbplm/Org/UnitTest.php
 *
 */
class Unit extends AnyObject
{
	
    /**
     * @var Rbh\People\User
     */
    protected $owner = null;
    public $ownerId = null;
    
	/**
	 *
	 * @var string
	 */
	public $description;
	
    /**
     * @param Rbh\People\User $Owner
     * @return void
     */
    public function setOwner(Rbh\People\User $Owner)
    {
        $this->owner = $Owner;
        $this->ownerId = $this->owner->getUid();
        $this->getLinks()->add($this->owner);
        return $this;
    }
	
    /**
     * @return Rbh\People\User
     */
    public function getOwner()
    {
        if( !$this->owner ){
			try{
				$this->owner = Rbh\Dao\Loader::load( $this->ownerId, 'Rbh\People\User' );
			}
			catch(Exception $e){
        		throw new Rbplm_Sys_Exception('PROPERTY_%0%_IS_NOT_SET', Rbh\Sys\Error::WARNING, array('_owner'));
			}
        }
        return $this->owner;
    }
    
}//End of class
