<?php
//%LICENCE_HEADER%

namespace Rbh\Org;

use \Rbh\Test\Test;
use \Rbh\Org\Unit as Unit;


/**
 * @brief Test class for Unit.
 * 
 * @include Rbplm/Org/UnitTest.php
 * 
 */
class UnitTest extends Test
{

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
}


