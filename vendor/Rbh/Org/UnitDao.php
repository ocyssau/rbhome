<?php
//%LICENCE_HEADER%

/** SQL_SCRIPT>>
 CREATE TABLE org_ou(
 description varchar(255)
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (20, 'Rbh\Org\Unit', 'org_ou');
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description)
	VALUES (89, 'fd5666ea-7be0-f414-1211-ee692b14de91', 20, 'Rbh', 'Rbh', NULL, 10, 'Rbh', NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description) 
	VALUES (90, '2767cbdd-1089-5b62-5c4c-7e6ebd2aa71f', 20, 'Groups', 'Groups', 89, 10, 'Rbh.Groups', NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description) 
	VALUES (91, 'd6bd8247-fc5d-c699-9b5c-23fc204d6313', 20, 'Users', 'Users', 89, 10, 'Rbh.Users', NULL);
 <<*/

/** SQL_ALTER>>
 ALTER TABLE org_ou ADD PRIMARY KEY (id);
 ALTER TABLE org_ou ADD UNIQUE (uid);
 ALTER TABLE org_ou ADD UNIQUE (path);
 ALTER TABLE org_ou ALTER COLUMN cid SET DEFAULT 20;
 CREATE INDEX INDEX_org_ou_owner ON org_ou USING btree (owner);
 CREATE INDEX INDEX_org_ou_uid ON org_ou USING btree (uid);
 CREATE INDEX INDEX_org_ou_name ON org_ou USING btree (name);
 CREATE INDEX INDEX_org_ou_label ON org_ou USING btree (label);
 CREATE INDEX INDEX_org_ou_path ON org_ou USING btree (path);
 <<*/

/** SQL_FKEY>>
ALTER TABLE org_ou ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
ON org_ou FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
CREATE TRIGGER trig02_org_ou AFTER DELETE
ON org_ou FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

namespace Rbh\Org;
use \Rbh\Dao\Pg\Dao As Dao;


/**
 * @brief Dao class for Rbplm_Org_Unit
 *
 * See the examples: Rbplm/Org/UnitTest.php
 *
 * @see Rbplm_Dao_Pg
 * @see Rbplm_Org_UnitTest
 *
 */
class UnitDao extends Dao
{

	/**
	 *
	 * @var string
	 */
	var $table = 'org_ou';

	/**
	 *
	 * @var integer
	 */
	var $classId = 20;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array('description'=>'description');


	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbh\Org\Unit			$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbh\Org\Unit
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		$mapped->description = $row['description'];
		return $mapped;
	} //End of function

	/**
	 * @param Rbplm_Org_Unit   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
				':description'=>$mapped->description,
		);
		if( $mapped->id > 0 ){
			$this->_update($mapped, $bind);
		}
		else{
			$this->_insert($mapped, $bind);
		}
	}

} //End of class
