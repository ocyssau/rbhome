<?php
//%LICENCE_HEADER%


namespace Rbh\Org;

use Rbh\AnyObject;
use Rbh\Org\Unit As Unit;
use Rbh\Sys\Error As Error;
use Rbh\Sys\Exception As Exception;

/**
 * @brief Rbplm is the root node of the COMPOSITE OBJECTS hierarchy.
 *
 * @verbatim
 * Rbplm_Org_Root
 *	 |OrgamizationUnit
 *	 	|Document
 *	 	|Document_Version
 * 			|Docfile_Version
 *				|Data
 * @endverbatim
 * 
 * Example and tests: Rbplm/Org/UnitTest.php
 * 
 */
class Root extends Unit
{
	
	/**
	 * 
	 * Singleton instance
	 * @var Rbplm_Org_Root
	 */
	static $instance;
	
	
    /**
     * Uid for the root component
     * @var String	uuid
     */
    const UUID = '01234567-0123-0123-0123-0123456789ab';
    const ID = 89;
	
	
	/**
	 * Use singleton
	 * 
	 * @return void
	 */
	function __construct(){
		$this->name = 'Rbh';
		$this->label = 'Rbh';
		$this->uid = self::UUID;
		$this->id = self::ID;
		$this->basePath = null;
		$this->parent = null;
	}
	
	/**
	 * Singleton method
	 * 
	 * @return \Rbh\Org\Root
	 */
	static public function singleton(){
		if( !self::$instance ){
			self::$instance = new Root();
		}
		return self::$instance;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see Rbh.AnyObject::setParent()
	 */
	function setParent($object){
		throw new Exception('NOT_ON_ROOT_OBJECT', Error::ERROR);
	}
	
}

