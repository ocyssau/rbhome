<?php
//%LICENCE_HEADER%

namespace Rbh;


/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
use Rbh\Sys\Exception;
use Rbh\Sys\Error;


final class Link extends Any
{
	const RULE_NONE=0;
	const RULE_PARENT=2;
	const RULE_ING=4;
	
	public $uid = null;
	public $parentId = null;
	public $childId = null;
	public $name = null;
	public $lindex = 0;
	public $data = null;
	
	public $level = false;
	public $path = false;
	
	public $isLeaf = false;
	public $isSuppressed = false;
	public $isLoaded = false;
	
	/**
	 * @var \Rbh\AnyObject
	 */
	public $parent=null;
	
	/**
	 * @var \Rbh\AnyObject
	 */
	public $child=null;
	
	/**
	 * 
	 */
	public static function init($properties=null)
	{
		$class = get_called_class();
		$link = new $class($properties);
		$link->uid = \Rbh\Uuid::newUid();
		return $link;
	}
	
	/**
	 * 
	 * @throws \Rbh\Sys\Exception
	 */
	public function bind()
	{
		if($this->parentId == 0){
			throw new Exception(Error::OBJECT_IS_NOT_SAVED, Error::ERROR, 'parent');
		}
		if($this->childId == 0){
			$this->childId = $this->child->id;
		}
		return array(
			':uid'=>$this->uid,
			':parentId'=>(int)$this->parentId,
			':childId'=>(int)$this->childId,
			':name'=>$this->name,
			':lindex'=>(int)$this->lindex,
			':data'=>\json_encode($this->data)
		);
	}
	
	/*
	public static function factory($parent, $child)
	{
		$link = new self();
		$link->parentId = $parent->id;
		$link->parent = $parent;
		$link->childId = $child->id;
		$link->child = $child;
	}
	*/
}
