<?php
//%LICENCE_HEADER%

namespace Rbh\Link;

use Rbh\Sys\Collection as BaseCollection;
use Rbh\AnyObject;

/**
 * @brief A collection of \Rbh\LinkRelatedInterface.
 * Link collection implements RecursiveIterator with \Rbh\AnyObject::getLinks.
 * 
 * Example and tests: Rbh/Model/ComponentTest.php
 * 
 * 
 */
class Collection extends BaseCollection
{
	
	/**
	 * 
	 * Implements RecursiveIterator
	 * @return LinkCollection 
	 */
	public function getChildren ()
	{
		$current = $this->current();
		if( $current != false ){
			if( $current instanceof AnyObject){
				return $current->getLinks();
			}
			if( $current instanceof BaseCollection ){
				return $current;
			}
			else{
				return $current->getChildren();
			}
		}
		else{
			return false;
		}
	}
	
	
	/**
	 * 
	 * Implements RecursiveIterator
	 * 
	 * @return boolean
	 */
	public function hasChildren ()
	{
		$current = $this->current();
		if( $current != false ){
			if( $current instanceof AnyObject){
				return $current->hasLinks();
			}
		    if( $current instanceof BaseCollection ){
				return (boolean) $current->count();
			}
			else{
				return $current->hasChildren();
			}
		}
		else{
			return false;
		}
	}
    
}
