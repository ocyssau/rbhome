<?php

namespace Rbh;

/**
 * Classe de gestion des signaux.
 *
 * Copyright (C) 2011 Nicolas Joseph
 *
 * LICENSE:
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    NanoMvc
 * @author     Nicolas Joseph <gege2061@homecomputing.fr>
 * @copyright  2011 Nicolas Joseph
 * @license    http://www.opensource.org/licenses/agpl-3.0.html AGPL v3
 * @filesource
 * 
 * Modified by Olivier CYSSAU.
 * 
 */
class Signal
{
	const SIGNAL_POST_SETNAME = 'postsetname';
	const SIGNAL_POST_SETNUMBER = 'postsetnumber';
	const SIGNAL_POST_SETPARENT = 'postsetparent';
	const SIGNAL_POST_SETSTATE = 'postsetstate';
	
	const SIGNAL_PRE_SUPPRESS = 'presuppress';
	const SIGNAL_POST_SUPPRESS = 'postsuppress';
	
	const SIGNAL_POST_NEWVERSION = 'postnewversion';
	
	const SIGNAL_PRE_CHECKOUT = 'precheckout';
	const SIGNAL_POST_CHECKOUT = 'precheckout';
	
	const SIGNAL_PRE_CHECKIN = 'precheckin';
	const SIGNAL_POST_CHECKIN = 'precheckin';
	
	const SIGNAL_PRE_SAVE = 'presave';
	const SIGNAL_POST_SAVE = 'postsave';
	
	const SIGNAL_PRE_LOAD = 'preload';
	const SIGNAL_POST_LOAD = 'postload';
	
	const SIGNAL_PRE_LOCK = 'lock';
	const SIGNAL_PRE_UNLOCK = 'unlock';
	
	/**
	 * @var array
	 */
	protected static $callback_list;
	
	
	/**
	 * 
	 * @param $object
	 * @param $signal_name
	 * @return unknown_type
	 */
	private static function _get_signal_hash ($object, $signal_name)
	{
		return spl_object_hash ($object) . $signal_name;
	} //End of method


	/**
	 * Connecte une fonction de rappel au signal $name de l'objet $object.
	 *
	 * @param StdClass $object Object à connecter
	 * @param string $name Nom du signal
	 * @param function $callback Fonction de rappel
	 *
	 * @return int L'identifiant unique de la connexion. Voir Signal::disconnect
	 */
	public static function connect ($object, $name, $callback)
	{
		$id = -1;
		
		$hash = self::_get_signal_hash ($object, $name);
		
		if( !isset( self::$callback_list[$hash] ) ){
			self::$callback_list[$hash] = array ();
		}
		
		$id = uniqid ();
		self::$callback_list[$hash][$id] = $callback;
		
		return $id;
	} //End of method

	/**
	 * Déconnecte le signal portant l'identifiant $id de l'object $object.
	 *
	 * @param StdClass $object Objet connecté au signal
	 * @param string $name Nom du signal
	 * @param int L'indentifiant du signal
	 */
	public static function disconnect ($object, $name, $id)
	{
		if (self::has ($object, $name))
		{
			$hash = self::_get_signal_hash ($object, $name);
			unset (self::$callback_list[$hash][$id]);
		}
		else
		{
			throw new Rbplm_Sys_Exception ('UNKNOW_SIGNAL', Rbplm_Sys_Error::WARNING, $name);
		}
	} //End of method

	/**
	 * Émet le signal $name sur l'object $object.
	 *
	 * @param StdClass $object Objet envoyant le signal
	 * @param string $name Le nom du signal à envoyé
	 * @param mixed ... Liste variable d'arguments passée à la fonction de rappel
	 */
	public static function emit ($object, $name /*, ...*/)
	{
		$hash = self::_get_signal_hash ($object, $name);
		
		if( isset( self::$callback_list[$hash] ) ){
			$callbacks = self::$callback_list[$hash];
			if (!empty ($callbacks))
			{
				$args = func_get_args ();
				unset ($args[1]);
				
				foreach ($callbacks as $callback)
				{
					call_user_func_array ($callback, $args);
				}
			}
		}
	} //End of method
	
} //End of class

