<?php 
namespace Rbh\Build;

use Rbh\Pdm;
use Rbh\Build\Product as Product;

use Rbh\Parameter\Parameter as Parameter;
use Rbh\Parameter\Formula;

use Rbh\Sys\Exception;
use Rbh\Sys\Error;

class Product extends Pdm\Product\Version
{
	protected $_params = array();
	protected $_products = array();
	protected $_formulas = array();
	
	/**
	 * 
	 * @param string $name
	 * @param Parameter $param
	 */
	public function addParam($name, Parameter $param)
	{
		$this->_params[$name]=$param;
	}
	
	/**
	 *
	 * @param string $name
	 * @return Parameter
	 */
	public function getParam($name)
	{
	    return $this->_params[$name];
	}
	
	
	/**
	 * @param string $name
	 * @param Product $param
	 */
	public function addComponent($name, Product $product)
	{
	    $this->_products[$name]=$product;
	}
	
	/**
	 *
	 * @param string $name
	 * @return Product
	 */
	public function getComponent($name)
	{
	    return $this->_products[$name];
	}
	
	/**
	 * 
	 * @param string $formula
	 */
	public function addFormula($formula)
	{
		$this->_formulas[] = new Formula($formula, $this);
	}
	
	public function evaluate()
	{
		foreach($this->_formulas as $f){
			$f->evaluate();
		}
		foreach($this->_products as $f){
		    $f->evaluate();
		}
	}
	
	
}
