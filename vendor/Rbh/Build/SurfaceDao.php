<?php
//%LICENCE_HEADER%

/** SQL_SCRIPT>>
 CREATE TABLE build_surface(
 description varchar(64),
 surface real,
 volume real,
 material integer,
 thickness real,
 density real,
 weight real,
 perimeter real,
 thlambda real,
 azimut real,
 inclinaison real,
 layoffset integer
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (2000, 'Rbh\Build\Surface', 'build_surface');
 INSERT INTO classes (id, name, tablename) VALUES (2001, 'Rbh\Build\SurfaceStack', 'build_surface');
 INSERT INTO classes (id, name, tablename) VALUES (2002, 'Rbh\Build\SurfaceComplex', 'build_surface');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE build_surface ADD PRIMARY KEY (id);
 ALTER TABLE build_surface ALTER COLUMN cid SET DEFAULT 2000;
 CREATE INDEX INDEX_build_surface_owner ON build_surface USING btree (owner);
 CREATE INDEX INDEX_build_surface_uid ON build_surface USING btree (uid);
CREATE INDEX INDEX_build_surface_name ON build_surface USING btree (name);
CREATE INDEX INDEX_build_surface_label ON build_surface USING btree (label);
CREATE INDEX INDEX_build_surface_path ON build_surface USING btree (path);
CREATE INDEX INDEX_build_surface_material ON build_surface USING btree (material);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_build_surface AFTER INSERT OR UPDATE
ON build_surface FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

CREATE TRIGGER trig02_build_surface AFTER DELETE
ON build_surface FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

namespace Rbh\Build;

use \Rbh\AnyObject;
use \Rbh\Dao\Pg\Dao As Dao;

/**
 * @brief Dao class
 *
 * @see Rb\Dao\Pg
 *
 */
class SurfaceDao extends Dao
{

	/**
	 *
	 * @var string
	 */
	var $table = 'build_surface';

	/**
	 * @var integer
	 */
	var $classId = 2000;

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'description'=>'description',
		'surface'=>'surface',
		'volume'=>'volume',
		'thickness'=>'thickness',
		'density'=>'density',
		'weight'=>'weight',
		'perimeter'=>'perimeter',
		'thlambda'=>'thLambda',
		'azimut'=>'azimut',
		'inclinaison'=>'inclinaison',
		'layoffset'=>'layOffset',
	);

	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbh\Build\Surface	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbh\Build\Surface
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		$mapped->description = $row['description'];
		$mapped->surface = $row['surface'];
		$mapped->volume = $row['volume'];
		$mapped->thickness = $row['thickness'];
		$mapped->density = $row['density'];
		$mapped->weight = $row['weight'];
		$mapped->perimeter = $row['perimeter'];
		$mapped->thLambda = $row['thlambda'];
		$mapped->azimut = $row['azimut'];
		$mapped->inclinaison = $row['inclinaison'];
		$mapped->layOffset = (int)$row['layoffset'];
		return $mapped;
	} //End of function

	/**
	 * @param Rbplm_Org_Unit   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped, $select=null)
	{
		//$mapped->classId = $this->classId;
		
		$bind = array(
			':description'=>$mapped->description,
			':surface'=>(float)$mapped->surface,
			':volume'=>(float)$mapped->volume,
			':thickness'=>(float)$mapped->thickness,
			':density'=>(float)$mapped->density,
			':weight'=>(float)$mapped->weight,
			':perimeter'=>(float)$mapped->perimeter,
			':thLambda'=>(float)$mapped->thLambda,
			':azimut'=>(float)$mapped->azimut,
			':inclinaison'=>(float)$mapped->inclinaison,
			':layOffset'=>(int)$mapped->layOffset,
		);

		//create link fo material
		/*
		if($mapped->materialId){
			$mapped->links['material'] = array(
				':id'=>\Rbh\Uuid::newUid(),
				':parentId'=>$mapped->parentId,
				':childId'=>$mapped->materialId,
				':lname'=>'material',
				':lindex'=>0,
				':data'=>null
			);
		}
		*/

		if( $mapped->id > 0 ){
			$this->_update($mapped, $bind, $select);
		}
		else{
			$this->_insert($mapped, $bind, $select);
		}
	}
} //End of class
