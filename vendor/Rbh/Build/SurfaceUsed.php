<?php
//%LICENCE_HEADER%

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rbh\Build;

use Rbh\Link As Link;
use Rbh\AnyUsed As AnyUsed;

/**
 * @brief
 *
 * @see Rbh/Build/SurfaceTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class UsedMaterial extends AnyUsed
{
	
	
	
	/**
	 * 
	 */
	public function getInheritedVolume()
	{
		$parent = $this->useLink->parent;
		if( is_a($parent, '\Rbh\Build\Surface')
			&& $this->volume == -1
		){
			$this->getInheritedSurface();
			$this->getInheritedThickness();
		}
		$this->volume = $this->surface * $this->thickness;
		if($this->volume < 0){
			$this->volume=-1;
		}
		return $this->volume;
	}

	/**
	 * 
	 */
	public function getInheritedSurface()
	{
		$parent = $this->useLink->parent;
		if( is_a($parent, '\Rbh\Build\Surface')
				&& $this->surface == -1
		){
			$this->surface = $parent->getInheritedSurface();
		}
		return $this->surface;
	}

	/**
	 * 
	 */
	public function getInheritedThickness()
	{
		$parent = $this->useLink->parent;
		if( is_a($parent, '\Rbh\Build\Surface')
				&& $this->thickness == -1
		){
			$this->thickness = $parent->getInheritedThickness();
		}
		return $this->thickness;
	}
}
