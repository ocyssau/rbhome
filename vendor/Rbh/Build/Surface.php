<?php
//%LICENCE_HEADER%

namespace Rbh\Build;

use Rbh\AnyObject;
use Rbh\Sys\Exception As Exception;
use Rbh\Sys\Error As Error;

/**
 * @brief
 *
 * @see
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Surface extends AnyObject
{
	/**@var literals**/
	public $description="";
	public $surface=-1; //m2, if=-1, value is inherited
	public $length; //m2;
	public $width; //m2;
	public $height; //m2;
	public $perimeter; //m
	public $thickness; //m
	public $volume; //m3
	public $azimut; //degree inherit
	public $inclinaison; //degree inherit
	public $density; //Kg/m3
	public $weight; //kg
	public $thLambda;
	public $thR;
	public $layOffset=0;
	public $inheritedSurface;
	public $classId = 2000;

	/**
	 * Niveaux de la couche. Plusieurs couches peuvent être au même niveaux.
	 * Epaisseur du niveau = ep la plus grande de ses composants.
	 * Les composants sont les enfants de la couche.
	 */
	public $layer=0;
	
	public function build()
	{
		if(!$this->links){
			return;
		}
		($this->surface == -1) ? $surface = $this->inheritedSurface : $surface = $this->surface;
		$this->weight=0;
		$this->volume=0;
		foreach($this->links as $lnk){
			$child=$lnk->child;
			$this->density = $child->density;
			$this->thLambda = $child->thLambda;
			$this->thSpecificHeat = $child->thSpecificHeat;
			
			$this->volume = $surface*$this->thickness;
			$this->weight = $this->volume*$this->density;
		}
	}
}
