<?php
//%LICENCE_HEADER%

namespace Rbh\Build;

use Rbh\Build\Surface;
use Rbh\Sys\Exception As Exception;
use Rbh\Sys\Error As Error;

/**
 * @brief
 *
 * @see
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class SurfaceComplex extends Surface
{
	public $classId = 2002;
	
	public function build()
	{
		$Rsum=0;
		$this->surface = 0;
		$this->weight  = 0;
		$this->volume  = 0;
		$this->thickness = 0;
		foreach($this->links as $lnk){
			$child=$lnk->child;
			if(method_exists($child, 'build')){
				$child->build();
			}
			($child->thickness > $this->thickness) ? $this->thickness = $child->thickness : NULL;
			
			$this->surface += $child->surface;
			$this->weight += $child->weight;
			$this->volume  += $child->volume;
				
			$Rsum = $Rsum+($child->surface*(1/$child->thLambda));
		}
		$this->thLambda = $this->surface/$Rsum; //Lambda=e/R
		$this->thR = $this->thickness/$this->thLambda; //R=e/Lambda
	}
}
