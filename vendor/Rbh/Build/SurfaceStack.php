<?php
//%LICENCE_HEADER%

namespace Rbh\Build;

use Rbh\Build\Surface;
use Rbh\Sys\Exception As Exception;
use Rbh\Sys\Error As Error;

/**
 * @brief
 *
 * @see
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class SurfaceStack extends Surface
{
	public $classId = 2001;
	
	public function build()
	{
		if(!$this->links){
			return;
		}
		($this->surface == -1) ? $surface = $this->inheritedSurface : $surface = $this->surface;
		$this->weight=0;
		$this->volume=0;
		$this->thickness=0;
		foreach($this->links as $lnk){
			$child=$lnk->child;
			($child->surface == -1) ? $child->inheritedSurface = $surface : $child->inheritedSurface = null;
			$child->build();
			$this->thickness += $child->thickness;
			$this->weight    += $child->weight;
			$this->volume    += $child->volume;
			
			$Rsum += ($child->surface*(1/$child->thLambda));
		}
		$this->density=$this->weight/$this->volume;
		if($Rsum!=0){
			$this->thLambda = $surface/$Rsum; //Lambda=e/R
			$this->thR = $this->thickness/$this->thLambda; //R=e/Lambda
		}
	}
}
