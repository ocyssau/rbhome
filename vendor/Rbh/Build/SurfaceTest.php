<?php
//%LICENCE_HEADER%

namespace Rbh\Build;

use \Rbh\Test\Test;
use \Rbh\Build\Surface As Surface;
use \Rbh\Build\SurfaceStack As SurfaceStack;
use \Rbh\Build\SurfaceComplex As SurfaceComplex;
use \Rbh\Material\Material As Material;
use \Rbh\Material\MaterialDao As MaterialDao;
use \Rbh\Link As Link;
use \Rbh\Dao\Factory As DaoFactory;
use \Rbh\Dao\Pg\Loader As Loader;


/**
 * @brief Test class for Rbh_Surface.
 * @include Rbh/SurfaceTest.php
 * 
 */
class SurfaceTest extends Test
{
    protected function tearDown()
    {
    }
    
    protected function setUp()
    {
		$SurfaceDao=new SurfaceDao();
		$MaterialDao=new MaterialDao();
		$SurfaceDao->newList()->suppress('name LIKE \'surfacetest%\'');
		$MaterialDao->newList()->suppress('name LIKE \'surfacetest_material%\'');
    }
	
	/**
	 * 
	 */	
	public static function recursiveSave($Any){
		foreach($Any->links as $lnk){
			if($lnk->rule==Link::RULE_PARENT){
				continue;
			}
			if($lnk->child){
				if($lnk->child->links){
					self::recursiveSave($lnk->child);
				}
				if(!$lnk->child->id){
					$Dao=DaoFactory::getDao($lnk->child)->save($lnk->child);
				}
			}
		}
	}
	
	/**
	 *
	 * @param unknown_type $Parent
	 * @param unknown_type $LnkName
	 * @return unknown
	 */
	private static function getLinkByChildName($Parent, $LnkChildName){
		foreach($Parent->links as $lnk){
			if ($lnk->child->name == $LnkChildName){
				return $lnk->child;
			}
		}
	}
	
	/**
	 *
	 */
	private function getDefinition()
	{
		$monProjet = \Rbh\Org\Root::singleton();
		$Surface = SurfaceStack::init('surfacetest');
		$Surface->inclinaison=10;
		$Surface->azimut=90;
		$Surface->surface=100;
		$Layer1 = Surface::init('surfacetest_layer1');
		$Layer2 = Surface::init('surfacetest_layer2');
		$Stack1 = SurfaceStack::init('surfacetest_stack1');
		$Layer3 = Surface::init('surfacetest_layer3');
		$Layer4 = Surface::init('surfacetest_layer4');
		
		$inc=$Layer1->inclinaison;
		$vol=$Layer1->volume;
		$sur=$Layer1->surface;
		
		$Material1 = Material::init('surfacetest_material1');
		$Material1->thLambda = 0.1; //isolant
		$Material1->density = 50;
		$Material2 = Material::init('surfacetest_material2');
		$Material2->thLambda = 1; //beton
		$Material2->density = 1000;
		$Material3 = Material::init('surfacetest_material3');
		$Material3->thLambda = 10; //Sapin
		$Material3->density = 500;
		
		$Material1->addLink($monProjet, \Rbh\Link::RULE_PARENT);
		$Material2->addLink($monProjet, \Rbh\Link::RULE_PARENT);
		$Material3->addLink($monProjet, \Rbh\Link::RULE_PARENT);
		
		$Layer1->addLink($Material1, \Rbh\Link::RULE_ING);
		$Layer2->addLink($Material2, \Rbh\Link::RULE_ING);
		$Layer3->addLink($Material3, \Rbh\Link::RULE_ING);
		$Layer4->addLink($Material1, \Rbh\Link::RULE_ING);
		
		$Layer1->thickness=1;
		$Layer1->layOffset=1;
		
		$Layer2->thickness=2;
		$Layer2->layOffset=2;
		
		$Layer3->thickness=4;
		$Layer3->layOffset=3;
	
		$Layer4->thickness=8;
	
		$Surface->addLink($Layer1);
		$Surface->addLink($Layer2);
		$Stack1->addLink($Layer3);
		$Stack1->addLink($Layer4);
		$Surface->addLink($Stack1);
		
		$ComplexLayer1 = SurfaceComplex::init('surfacetest_ComplexLayer1');
		$Layer5 = Surface::init('surfacetest_ComplexLayer1_Lay5');
		$Layer5->addLink($Material1);
		$Layer5->surface = 2;
		$Layer5->thickness = 16;
		$Layer6 = Surface::init('surfacetest_ComplexLayer1_Lay6');
		$Layer6->addLink($Material2);
		$Layer6->surface = 5;
		$Layer6->thickness = 32;
		$ComplexLayer1->addLink($Layer5);
		$ComplexLayer1->addLink($Layer6);
		//$ComplexLayer1->build();
		$Surface->addLink($ComplexLayer1);
		$Surface->build();
		
		return $Surface;
	}
	
	
	/**
	 *
	 */
	public function Test_Constructor()
	{
		$Surface = $this->getDefinition();
		$this->displayTestResult($Surface);
	}
	
	
	/**
	 * 
	 */
	private function displayTestResult($Surface)
	{
		$Layer1 = self::getLinkByChildName($Surface, 'surfacetest_layer1');
		$Layer2 = self::getLinkByChildName($Surface, 'surfacetest_layer2');
		$Stack1 = self::getLinkByChildName($Surface, 'surfacetest_stack1');
		$Layer3 = self::getLinkByChildName($Stack1, 'surfacetest_layer3');
		$Layer4 = self::getLinkByChildName($Stack1, 'surfacetest_layer4');
		$ComplexLayer1 = self::getLinkByChildName($Surface, 'surfacetest_ComplexLayer1');
		$Layer5 = self::getLinkByChildName($ComplexLayer1, 'surfacetest_ComplexLayer1_Lay5');
		$Layer6 = self::getLinkByChildName($ComplexLayer1, 'surfacetest_ComplexLayer1_Lay6');
		
		echo 'Ep totale: '.$Surface->thickness.CRLF;
		assert($Surface->thickness ==47);
		echo 'S totale: '.$Surface->surface .CRLF;
		assert($Surface->surface==100);
		echo 'thLambda Totale: '.$Surface->thLambda .CRLF;
		echo 'thR Totale: '.$Surface->thR .CRLF;
		echo 'weight Totale: '.$Surface->weight .CRLF;
		echo 'density Totale: '.$Surface->density .CRLF;
		
		echo 'Surface layer1: '.$Layer1->surface .CRLF;
		assert($Layer1->surface==-1);
		assert($Layer2->surface==-1);
		assert($Layer3->surface==-1);
		
		echo 'Masse layer1: '.$Layer1->weight .CRLF;
		assert($Layer1->weight==100*1*50);
		echo 'Masse layer2: '.$Layer2->weight .CRLF;
		echo 'Masse layer3: '.$Layer3->weight .CRLF;
		echo 'Masse layer5: '.$Layer5->weight .CRLF;
		assert($Layer5->weight==2*16*50);
		echo 'volume layer5: '.$Layer5->volume .CRLF;
		assert($Layer5->volume==2*16);
		echo 'Masse layer6: '.$Layer6->weight .CRLF;
		assert($Layer6->weight==5*32*1000);
		echo 'volume layer6: '.$Layer6->volume .CRLF;
		assert($Layer6->volume==5*32);
		
		echo 'density $Layer1: '.$Layer1->density .CRLF;
		echo 'density $Layer2: '.$Layer2->density .CRLF;
		echo 'density $Layer3: '.$Layer3->density .CRLF;
		
		echo 'Ep $ComplexLayer1: '.$ComplexLayer1->thickness. CRLF;
		assert($ComplexLayer1->thickness == 32);
		echo 'surface $ComplexLayer1: '.$ComplexLayer1->surface. CRLF;
		assert($ComplexLayer1->surface == 7);
		echo 'volume $ComplexLayer1: '.$ComplexLayer1->volume. CRLF;
		assert($ComplexLayer1->volume == $Layer5->volume+$Layer6->volume); //2*16+5*32=192
		echo 'weight $ComplexLayer1: '.$ComplexLayer1->weight. CRLF;
		assert($ComplexLayer1->weight ==(2*16*50+5*32*1000)); //2*16*50+5*32*1000
		assert($ComplexLayer1->weight == $Layer5->weight+$Layer6->weight);
		
		assert($Stack1->thickness == $Layer3->thickness + $Layer4->thickness);
	}
	

	/**
	 * 
	 */
	public function Test_DaoSave()
	{
		$Surface = $this->getDefinition();
		self::recursiveSave($Surface);
		$Dao=DaoFactory::getDao($Surface)->save($Surface);
	}
	
	
	/**
	 * 
	 */
	public function Test_DaoReadUpdateLoad()
	{
		$SurfaceDao=new SurfaceDao();
		$Surface=$SurfaceDao->load(new SurfaceStack(), 'name=\'surfacetest\'');
		assert($Surface->name == 'surfacetest');
		assert(is_integer($Surface->id));
		Loader::init();
		Loader::loadChildren($Surface);
		$this->displayTestResult($Surface);
		
		$Surface->surface=1000;
		$Surface->build();
		
		echo 'Ep totale: '.$Surface->thickness.CRLF;
		echo 'S totale: '.$Surface->surface .CRLF;
		echo 'thLambda Totale: '.$Surface->thLambda .CRLF;
		echo 'thR Totale: '.$Surface->thR .CRLF;
		echo 'weight Totale: '.$Surface->weight .CRLF;
		echo 'density Totale: '.$Surface->density .CRLF;
		
		$Layer1 = self::getLinkByChildName($Surface, 'surfacetest_layer1');
		$ComplexLayer1 = self::getLinkByChildName($Surface, 'surfacetest_ComplexLayer1');
		echo 'Class of $ComplexLayer1 :'.get_class($ComplexLayer1).CRLF;
		assert(is_a($ComplexLayer1, '\Rbh\Build\SurfaceComplex'));
		
		echo 'Surface layer1: '.$Layer1->surface .CRLF;
		echo 'Inherited surface layer1: '.$Layer1->inheritedSurface .CRLF;
		echo 'Masse layer1: '.$Layer1->weight .CRLF;
		assert($Layer1->weight==1000*1*50);
	}
}
