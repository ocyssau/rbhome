<?php 
//%LICENCE_HEADER%

/**
 * $Id: Pg.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */


/** SQL_SCRIPT>>
 -- SEQUENCE
 -- anyobject_id_seq
 --
 CREATE SEQUENCE anyobject_id_seq
 START WITH 100
 INCREMENT BY 1
 NO MAXVALUE
 NO MINVALUE
 CACHE 1;

 -------------------------------------------------------------------------------------------------------------------
 -- anyobject
 --
 CREATE TABLE anyobject (
 id bigint NOT NULL,
 uid uuid NOT NULL,
 cid integer NOT NULL,
 name varchar(256),
 label varchar(256),
 owner bigint NOT NULL,
 parent bigint,
 path ltree
 );

 -------------------------------------------------------------------------------------------------------------------
 -- anyobject_links
 --
 CREATE TABLE anyobject_links (
 	luid uuid NOT NULL,
 	lparent bigint NOT NULL,
 	lchild bigint NOT NULL,
 	lname varchar(256),
 	ldata text NULL,
 	lindex integer NULL,
 	level int DEFAULT 0,
 	lpath ltree,
 	is_leaf boolean NOT NULL DEFAULT false
 );

<<*/

/** SQL_ALTER>>
 -------------------------------------------------------------------------------------------------------------------
-- anyobject
--
ALTER TABLE anyobject ALTER COLUMN id SET DEFAULT nextval('anyobject_id_seq'::regclass);
ALTER TABLE anyobject ADD PRIMARY KEY (id);
ALTER TABLE anyobject ADD UNIQUE (path);
ALTER TABLE anyobject ADD FOREIGN KEY (cid) REFERENCES classes (id);

CREATE INDEX INDEX_anyobject_name ON anyobject USING btree (name);
CREATE INDEX INDEX_anyobject_label ON anyobject USING btree (label);
CREATE INDEX INDEX_anyobject_uid ON anyobject USING btree (uid);
CREATE INDEX INDEX_anyobject_class_id ON anyobject USING btree (cid);
CREATE UNIQUE INDEX INDEX_anyobject_path_btree ON anyobject USING btree(path);
CREATE INDEX INDEX_anyobject_path_gist ON anyobject USING gist(path);

-------------------------------------------------------------------------------------------------------------------
-- anyobject_links
--
ALTER TABLE anyobject_links ADD UNIQUE (lparent, lchild);
CREATE INDEX INDEX_anyobject_links_parent ON anyobject_links USING btree (lparent);
CREATE INDEX INDEX_anyobject_links_child ON anyobject_links USING btree (lchild);
<<*/

/** SQL_VIEW>>
 CREATE OR REPLACE VIEW view_anyobject_parentlinks AS
SELECT
l.luid,l.lname,l.lindex,l.ldata,
p.id AS p_id, p.uid AS p_uid, p.name AS p_name, p.cid AS p_cid,
c.id AS c_id, c.uid AS c_uid, c.name AS c_name, c.cid AS c_cid
FROM anyobject_links AS l
LEFT OUTER JOIN anyobject AS c ON l.lchild  = c.id
LEFT OUTER JOIN anyobject AS p ON l.lparent = p.id
ORDER BY p_id ASC, lindex ASC;

-- TODO : Comment concatener les ltree

-- All graph from Root node :
CREATE OR REPLACE VIEW view_allgraphe_001 AS
WITH RECURSIVE graphe(lparent, lchild, lname, lindex, ldata, level, path) AS (
	SELECT l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, (l.lparent||'.'||l.lchild)::text::ltree
	FROM anyobject_links l
	UNION ALL
	SELECT l.lparent, l.lchild, l.lname, l.lindex, l.ldata, g.level + 1, (path::text || '.'|| l.lchild)::text::ltree
	FROM anyobject_links l, graphe g
	WHERE g.lchild = l.lparent
)
SELECT
l.path AS path, l.level AS level, l.lname AS lname, l.lindex, l.ldata AS ldata,
p.id AS p_id, p.uid AS p_uid, p.name AS p_name, p.cid AS p_cid,
c.id AS c_id, c.uid AS c_uid, c.name AS c_name, c.cid AS c_cid
FROM graphe AS l
LEFT OUTER JOIN anyobject AS p ON l.lparent = p.id
LEFT OUTER JOIN anyobject AS c ON l.lchild = c.id
WHERE l.path <@ '89'
ORDER BY path ASC, lindex ASC;
<<*/


/** SQL_PROC>>
-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_get_object_path
--
CREATE OR REPLACE FUNCTION rb_get_object_path(param_node_id bigint) RETURNS ltree AS
$$
SELECT
CASE WHEN tobj.lparent IS NULL
THEN
tobj.id::text::ltree
ELSE
rb_get_object_path(tobj.lparent)  || tobj.id::text
END
FROM anyobject_links As tobj
WHERE tobj.luid=$1;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_set_object_path
--
CREATE OR REPLACE FUNCTION rb_set_object_path() RETURNS VOID AS
$$
UPDATE
anyobject_links As tobj
SET
lpath = rb_get_object_path(tobj.parent) || tobj.uid::text,
level = nlevel(path);
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_replace_links
--
CREATE OR REPLACE FUNCTION rb_replace_links(in_uuid uuid,in_parent bigint,in_child bigint,in_data TEXT,in_lindex INT,in_name TEXT) RETURNS VOID AS
$$
BEGIN
LOOP
UPDATE anyobject_links SET ldata = in_data, lindex = in_lindex, lname = in_name WHERE lparent = in_parent AND lchild = in_child;
IF found THEN
RETURN;
END IF;
BEGIN
--loc_lui = uuid();
INSERT INTO anyobject_links (luid,lparent,lchild,ldata,lindex,lname) VALUES (in_uuid,in_parent,in_child,in_data,in_lindex,in_name);
RETURN;
EXCEPTION WHEN unique_violation THEN
-- do nothing
END;
END LOOP;
END;
$$
LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Proc rb_trig_anyobject_update_path
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_update_path() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'UPDATE' THEN
IF (OLD.lpath IS NULL) THEN
UPDATE anyobject_links AS lt SET
lpath = rb_get_object_path(anyobject.id),
level = nlevel(NEW.lpath)
WHERE
lt.lparent = NEW.lparent AND lt.lchild = NEW.lchild
OR
lt.parent = NEW.id
;
END IF;
IF (OLD.parent != NEW.parent  OR  NEW.id != OLD.id) THEN
-- update all nodes that are children of this one including this one
UPDATE anyobject SET
path = rb_get_object_path(anyobject.id),
depth = nlevel(NEW.path)
WHERE
OLD.path  @> anyobject.path;
END IF;

ELSIF TG_OP = 'INSERT' THEN
UPDATE anyobject SET
path = rb_get_object_path(NEW.id),
depth = nlevel(NEW.path)
WHERE
anyobject.id = NEW.id;
END IF;

RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_anyobject_delete
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_delete() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'DELETE' THEN
DELETE FROM anyobject_links WHERE parent = OLD.id OR child = OLD.id;
END IF;
RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

<<*/


/** SQL_TRIGGER>>
-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig01_anyobject_update_path
--
DROP TRIGGER IF EXISTS rb_trig01_anyobject_update_path ON anyobject;
CREATE TRIGGER rb_trig01_anyobject_update_path AFTER INSERT OR UPDATE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig02_anyobject_delete
--
DROP TRIGGER IF EXISTS rb_trig02_anyobject_delete ON anyobject;
CREATE TRIGGER rb_trig02_anyobject_delete AFTER DELETE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
<<*/

/** SQL_DROP>>
 DROP SEQUENCE IF EXISTS anyobject_id_seq;
DROP TABLE IF EXISTS anyobject CASCADE;
DROP TABLE IF EXISTS anyobject_links CASCADE;
<<*/


/**
 * @brief Abstract Dao for pgsql database with shemas using ltree to manage trees.
*
* NEED TO INSTALL LTREE.
* ON UBUNTU:
* apt-get install postgresql-contrib
*
* psql -d bdd_name -f SHAREDIR/contrib/ltree.sql
* where SHAREDIR = /usr/share/postgresql/8.4 (example)
*
*
* Extract from http://docs.postgresql.fr/8.4/contrib.html:
* Le fichier .sql doit être exécuté dans chaque base de données où le module doit être disponible.
* Il peut également être exécuté dans la base template1 pour que le module soit automatiquement copié dans toute
* nouvelle base de données créée.
* La première commande du fichier .sql peut être modifiée pour déterminer le schéma de la base où sont créés les objets.
* Par défaut, ils sont placés dans public.
* Après une mise à jour majeure de PostgreSQL™, le script d'installation doit être réexécuté,
* même si les objets du module sont éventuellement créés par une sauvegarde de l'ancienne installation.
* Cela assure que toute nouvelle fonction est disponible et tout correction nécessaire appliquée.
*
*
*  To create converter between sys attributs and application properties you may add static methods with name [appPropertyName]'ToSys'($value)
*  and  [sysAttributName]'ToApp'($value). This methods return values in appropriate format.
*  You may see examples as methods pathToApp and pathToSys.
*
* @link http://docs.postgresql.fr/8.4/contrib.html
* @link http://docs.postgresql.fr/8.4/ltree.html
*
*/

namespace Rbh\Dao\Pg;

use Rbh\Dao\DaoInterface;
use Rbh\Dao\MappedInterface;
use Rbh\Dao\Connexion;
use Rbh\Sys\Exception As Exception;
use Rbh\Sys\Error As Error;
use Rbh\Signal As Signal;
use Rbh\Rbh As Rbh;

abstract class Dao implements DaoInterface
{
	/**
	 * View table to query for read
	 *
	 * @var string
	 */
	public $vtable;

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public $table = 'anyobject';

	/**
	 * Table name where write links definition.
	 * @var string
	 */
	public $ltable = 'anyobject_links';

	/**
	 * Table name where read links definition.
	 * @var string
	 */
	public $lvtable = 'view_anyobject_links';

	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 *
	 * @var array
	 */
	public static $sysToApp = array(
		'id'=>'id',
		'uid'=>'uid',
		'cid'=>'classId',
		'name'=>'name',
		'label'=>'label',
		'owner'=>'ownerId',
	);

	/**
	 * @var array
	 */
	protected $config;

	/**
	 * Class id use by postgresql dao to map rbplm class to entry in db.
	 * @var integer
	 */
	protected $classId;

	/**
	 *
	 * @var string
	 */
	public $sequence_name = 'anyobject_id_seq';

	/**
	 *
	 * @var unknown_type
	 */
	private $insertStmt;
	private $updateStmt;


	/**
	 * Constructor
	 *
	 * @param \PDO
	 */
	public function __construct( $conn = null )
	{
		/*
		 if( $conn ){
		$this->connexion = $conn;
		}
		else{
		$this->connexion = Connexion::get();
		}
		*/
		$this->connexion = Connexion::get();
	} //End of function


	/**
	 * To recreate connexion after unserialize
	 */
	public function __wakeup()
	{
		$this->connexion = Connexion::get();
		return $this;
	} //End of function


	/**
	 * Clean connexion before serialize.
	 * @return array
	 */
	public function __sleep()
	{
		$this->connexion = null;
		return get_class_vars($this);
	} //End of function


	/**
	 * @see library/Rbplm/Dao/Rbplm_Dao_Interface#save($mapped, $unlocks)
	 */
	public function save(MappedInterface $mapped, array $select = null )
	{
		$this->connexion->beginTransaction();
		try{
			Signal::emit($this, Signal::SIGNAL_PRE_SAVE);
			$this->_saveObject($mapped, $select);
			$this->connexion->commit();
			$mapped->isLoaded(true);
			Signal::emit($this, Signal::SIGNAL_POST_SAVE);
		}
		catch(Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	} //End of function

	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbh\AnyObject		$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbh\Dao\Pg\Dao
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		$mapped->id = (int)$row['id'];
		$mapped->uid = $row['uid'];
		$mapped->name = $row['name'];
		$mapped->label = $row['label'];
		$mapped->classId = (int)$row['cid'];
		$mapped->ownerId = (int)$row['owner'];
		//$mapped->parentId = (int)$row['parent'];
		//$mapped->path = $row['path'];
		//$mapped->isLeaf($row['is_leaf']);
		return $mapped;
	} //End of function


	/**
	 * @see Rbplm_Dao_Interface::load()
	 * @return \Rbh\AnyObject
	 */
	public function load( MappedInterface $mapped, $filter = null, array $options = array() )
	{
		if ( $mapped->isLoaded() == true && $force == false ){
			Rbh::log('Object ' . $mapped->getUid() . ' is yet loaded');
			return $mapped;
		}

		Signal::emit($this, Signal::SIGNAL_PRE_LOAD);
		
		//$select = implode( ',', array_keys( array_merge(self::$sysToApp, static::$sysToApp) ) );
		$sql = "SELECT * FROM $this->table AS a";
		
		if($filter){
			$sql .= " WHERE $filter";
		}
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute();
		$row = $stmt->fetch();
		
		if($row){
			$this->loadFromArray($mapped, $row);
		}
		else{
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING, array($sql) );
		}
		$mapped->isLoaded(true);
		Signal::emit($this, Signal::SIGNAL_POST_LOAD);
		
		return $mapped;
	} //End of method

	/**
	 * Count line from a filter
	 *
	 * @return integer
	 */
	public function count($filter=null)
	{
		$sql = "SELECT COUNT(*) AS C FROM $this->vtable";

		if(is_a($filter, '\Rbh\Dao\FilterInterface')){
			$filter = $filter->__toString();
		}

		if($filter){
			$sql .= " WHERE $filter";
		}

		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_COLUMN, 0);
		$stmt->execute();
		$count = $stmt->fetch();

		if(!$count){
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING, array($sql) );
		}
		else{
			return $count;
		}
	}


	/**
	 *
	 * @param \Rbh\AnyObject		$mapped
	 * @return void
	 * @throws \Rbh\Sys\Exception
	 */
	abstract protected function _saveObject( $mapped );


	/**
	 * @param \Rbh\AnyObject   	$mapped
	 * @param array				$bind		Bind definition to add to generic bind construct from $sysToApp
	 * @param array				$select	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 * @throws \Rbh\Sys\Exception
	 *
	 */
	protected function _insert($mapped, $bind, $select=null)
	{
		/*
		 if( !is_integer($mapped->parentId) AND !is_null($mapped->parentId)){
		$mapped->parentId = $mapped->parent->id;
		}
		if($mapped->parentId == 0){
		$mapped->parentId = NULL;
		}
		*/

		//Init statement
		if(!$this->insertStmt){
			$sysToApp = array_merge(static::$sysToApp, self::$sysToApp);
			unset($sysToApp['id']);
			foreach($sysToApp as $asSys=>$asApp){
				$sysSet[] = $asSys;
				$appSet[] = ':'.$asApp;
			}
			$sql = 'INSERT INTO ' . $this->table . '(' . implode(',', $sysSet) . ') VALUES (' . implode(',', $appSet) . ') RETURNING id;';
			$this->insertStmt = $this->connexion->prepare($sql);

			$lsql = 'INSERT INTO '.$this->ltable.' (luid, lparent, lchild, lname, lindex, ldata)
			VALUES (:uid, :parentId, :childId, :name, :lindex, :data)';
			$this->linsertStmt = $this->connexion->prepare($lsql);
		}

		//insert object definition
		$bind = array_merge( array(
			':uid'=>$mapped->uid,
			':name'=>$mapped->name,
			':label'=>$mapped->label,
			':classId'=>$mapped->classId,
			':ownerId'=>(integer) $mapped->ownerId,
		), $bind);

		$this->insertStmt->execute($bind);
		$mapped->id = $this->insertStmt->fetch(\PDO::FETCH_COLUMN);

		if(is_array($mapped->links)){
			foreach($mapped->links as $link){
				if($link->rule == \Rbh\Link::RULE_PARENT){
					$link->childId = $mapped->id;
				}
				else{
					$link->parentId = $mapped->id;
				}
				$this->linsertStmt->execute($link->bind());
			}
		}
	}


	/**
	 * @param Rbh\AnyObject   $mapped
	 * @param array				  $bind		Bind definition to add to generic bind construct from $sysToApp
	 * @param array				  $select	Array of system properties define in $sysToApp array than must be save. If empty, save all.
	 * @return void
	 * @throws \Rbh\Sys\Exception
	 *
	 */
	protected function _update($mapped, $bind, $select=null)
	{
		if(!$this->updateStmt){
			$sysToApp = array_merge(static::$sysToApp, self::$sysToApp);
			unset($sysToApp['path']);
			unset($sysToApp['depth']);
			unset($sysToApp['id']);

			if(is_array($select)){
				$sysToApp = array_intersect($sysToApp, $select);
			}
			foreach($sysToApp as $asSys=>$asApp){
				$set[] = $asSys . '=:' . $asApp;
			}
			$sql = 'UPDATE '.$this->table.' SET ' . implode(',', $set) . ' WHERE id=:id;';
			$this->updateStmt = $this->connexion->prepare($sql);

			$sql = "SELECT rb_replace_links(:uid, :parentId, :childId, :data, :lindex, :name)";
			$this->lupdateStmt = $this->connexion->prepare($sql);
		}

		//insert object definition
		$bind = array_merge( array(
			':id'=>$mapped->id,
			':uid'=>$mapped->uid,
			':name'=>$mapped->name,
			':label'=>$mapped->label,
			':classId'=>$mapped->classId,
			':ownerId'=>(integer) $mapped->ownerId,
		),$bind);

		if(is_array($select)){
			array_walk($select, function (&$val,$key){
				$val=':'.$val;
			});
			$bind = array_intersect_key($bind, array_flip($select));
			$bind[':id'] = $mapped->id;
		}
		$this->updateStmt->execute($bind);

		//suppress links
		if(isset($this->linksToSuppress)){
			$sql = "DELETE FROM $this->ltable WHERE lparent = :parentId AND lchild = :childId";
			$stmt = $this->connexion->prepare($sql);
			foreach($this->linksToSuppress as $bind){
				$stmt->execute($bind);
			}
			$this->linksToSuppress = array();
		}

		if(is_array($mapped->links)){
			foreach($mapped->links as $lnk){
				$this->lupdateStmt->execute($lnk->bind);
			}
		}

		/*
		 $sql = 'SELECT pg_advisory_unlock(' . $this->classId . ') FROM ' . $table . ' WHERE id=\''.$mapped->id.'\'';
		$this->connexion->exec($sql);
		*/
	}

	/**
	 * @see Rbh\Dao\Interface::loadFromUid()
	 */
	public function loadFromUid($mapped, $uid, $options = array() )
	{
		$filter = "uid='$uid'";
		return $this->load($mapped, $filter, $options );
	} //End of function

	/**
	 */
	public function loadFromId($mapped, $id)
	{
		if(!isset($this->loadFromIdStmt)){
			$sql = "SELECT * FROM $this->table WHERE id=:id";
			$this->loadFromIdStmt = $this->connexion->prepare($sql);
			$this->loadFromIdStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}
		$this->loadFromIdStmt->execute(array(':id'=>$id));
		$row = $this->loadFromIdStmt->fetch();

		if($row){
			$this->loadFromArray($mapped, $row);
		}
		else{
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING, array($sql) );
		}
		$mapped->isLoaded(true);
		return $mapped;
	} //End of function

	/**
	 */
	public function loadFromName($mapped, $name)
	{
		if(!$this->loadFromNameStmt){
			$sql = "SELECT * FROM $this->table WHERE name=:name";
			$this->loadFromNameStmt = $this->connexion->prepare($sql);
			$this->loadFromNameStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}
		$this->loadFromNameStmt->execute(array(':name'=>$name));
		$row = $this->loadFromNameStmt->fetch();

		if($row){
			$this->loadFromArray($mapped, $row);
		}
		else{
			throw new Exception(Error::CAN_NOT_BE_LOAD_FROM, Error::WARNING, array($sql) );
		}
		$mapped->isLoaded(true);
		return $mapped;
	} //End of function


	/**
	 * @see Rbh\Dao\Interface::loadFromPath()
	 * @return Rbplm_Dao_Pg
	 */
	public function loadFromPath($mapped, $path, $options = array() )
	{
		//$path = self::pathToSys($path);
		$filter = "path='$path'";
		return $this->load($mapped, $filter, $options );
	} //End of function

	/**
	 * @see library/Rbplm/Dao/Rbplm_Dao_Interface#suppress($mapped)
	 * @return \Rbh\Dao\Pg\Dao
	 *
	 */
	public function suppress(MappedInterface $mapped, $withChilds = false )
	{
		$this->connexion->beginTransaction();

		if( $withChilds ){
			$path = $mapped->getPath();
			$path = self::pathToSys($path);
			$sql = "DELETE FROM anyobject WHERE path::ltree ~ '$path.*{0,}'";
			$this->connexion->exec($sql);
		}
		else{
			$id = $mapped->id;
			$parentId = $mapped->parentId;
			if ($parentId != ""){
				$sql = "UPDATE anyobject SET parent=$parentId WHERE parent=$id";
				$this->connexion->exec($sql);
			}
		}

		/*
		 if( !$this->connexion->exec($sql) ){
		throw new \Rbh\Sys\Exception( Error::SUPPRESSION_FAILED, Error::ERROR, array($bind, $sql) );
		}
		*/

		$sql = 'DELETE FROM anyobject WHERE id=:id';
		$bind = array( ':id'=>$mapped->id );
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute($bind);
		$this->connexion->commit();

		return $this;

		/*
		 if( ! ){
		throw new \Rbh\Sys\Exception( Error::SUPPRESSION_FAILED, Error::ERROR, array($bind, $sql));
		}
		else{
		$this->connexion->commit();
		}
		*/
	} //End of method

	/**
	 * Convert a array in a postgres array synthax.
	 * The keys names are stored in the second array strucuture:
	 *
	 * {{value,...}{key,...}}
	 *
	 * @link http://docs.postgresqlfr.org/8.4/arrays.html
	 *
	 * @param array	$phpArray
	 * @return string
	 */
	protected static function _toPgArray( $phpArray )
	{
		$strValues = '{';
		$strKeys = '{';
		$delim = '';
		foreach($phpArray as $key=>$value){
			if(is_array($value)){
				$value = self::_toPgArray($value);
			}
			$strValues .= $delim . '"' . $value . '"';
			$strKeys .= $delim . '"' . $key . '"';
			$delim = ',';
		}
		$strValues .= '}';
		$strKeys .= '}';

		return '{' . $strValues . ',' . $strKeys . '}';
	}

	/**
	 * Convert a postgres array strucutre synthax to php array.
	 *
	 * @link http://docs.postgresqlfr.org/8.4/arrays.html
	 *
	 * From veggivore at yahoo dot com on : @link http://php.net/manual/en/ref.pgsql.php
	 *
	 * @param string	$pgArray
	 * @return array
	 */
	protected static function _toPhpArray( $pgArray )
	{
		$s = $pgArray;
		$s = str_replace("{", "array(", $s);
		$s = str_replace("}", ")", $s);
		$s = "\$retval = $s;";
		eval( $s );
		return $retval;
	}

	/**
	 * Convert a array in a postgres xml structure.
	 *
	 * @param array		$phpArray
	 * @param boolean	$asString	If true, return a string, else return a SimpleXMLElement object
	 * @return string XML | SimpleXMLElement
	 */
	protected static function _arrayToXml( $phpArray, $asString = true )
	{
		$xml = new SimpleXMLElement("<array></array>");
		array_walk_recursive($phpArray, array ($xml, 'addChild'));
		if($asString){
			return $xml->asXML();
		}
		else{
			return $xml;
		}
	}

	/**
	 * Convert a xml structure synthax to php array.
	 *
	 * @param string	$xml
	 * @return array
	 */
	protected static function _xmlToArray( $xml )
	{
		$f = function($iter) {
			foreach($iter as $key=>$val)
				$arr[$key][] = ($iter->hasChildren())?
				call_user_func (__FUNCTION__, $val)
				: strval($val);
			return $arr;
		};
		return $f( new SimpleXMLElement($xml) );
	}

	/**
	 * Encode multi-value property to inject in postgresql
	 * Use json encoding, but may be other like xml or postgres array.
	 *
	 * @param array		$phpArray
	 * @return string
	 */
	public static function multivalToPg( array $phpArray )
	{
		return json_encode($phpArray);
	}

	/**
	 * Decode multi-value property to inject in application.
	 * Ensure that method multivalToPg is coherent with this converter method.
	 *
	 * @param string	$json
	 * @return array
	 */
	public static function multivalToApp( $json )
	{
		return json_decode($json, true);
	}

	/**
	 * Factory method for instanciate List object.
	 *
	 * @throws \Rbh\Sys\Exception
	 * @return \Rbh\Dao\Pg\DaoList
	 */
	public function newList()
	{
		$list=new \Rbh\Dao\Pg\DaoList();
		$list->table = $this->table;
		return $list;
	} //End of method
} //End of class
