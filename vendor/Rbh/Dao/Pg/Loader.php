<?php
//%LICENCE_HEADER%

/**
 * $Id: Pgsql.php 487 2011-06-23 21:45:21Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Loader/Pgsql.php $
 * $LastChangedDate: 2011-06-23 23:45:21 +0200 (Thu, 23 Jun 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 487 $
 */

/** SQL_SCRIPT>>
 CREATE TABLE index (
 uid uuid NOT NULL,
 dao varchar(256) DEFAULT NULL,
 connexion varchar(256) DEFAULT NULL
 );
 ALTER TABLE index ADD PRIMARY KEY (uid);
 <<*/


/** SQL//CANCEL//_VIEW>>
 CREATE OR REPLACE VIEW view_anyobject_index AS
 SELECT
 anyobject.id,
 anyobject.uid,
 anyobject.name,
 anyobject.class_id,
 anyobject.label,
 anyobject.parent,
 anyobject.path,
 classes.name AS class_name,
 index.dao,
 index.connexion
 FROM
 anyobject
 JOIN classes ON (anyobject.class_id = classes.id)
 LEFT OUTER JOIN index ON (anyobject.uid = index.uid);

 CREATE OR REPLACE VIEW view_anyobject_class AS
 SELECT
anyobject.id,
anyobject.uid,
anyobject.name,
anyobject.label,
anyobject.parent,
anyobject.path,
classes.id AS class_id,
classes.name AS class_name
FROM
anyobject
JOIN classes ON (anyobject.class_id = classes.id);

<<*/

namespace Rbh\Dao\Pg;

use \Rbh\Dao\DaoInterface;
use \Rbh\Dao\MappedInterface;
use \Rbh\Dao\Connexion;
use \Rbh\Sys\Exception As Exception;
use \Rbh\Sys\Error As Error;
use \Rbh\Signal As Signal;

/**
 * @brief Instanciate a anyobject and load properties in from the given Uuid.
 *
 * It is a builder class to instanciate the Rbplm objects from a Uuid. instanciate the corresponding class
 * and load properties from the pg database.
 *
 * @see \Rbh\Dao\LoaderInterface
 *
 * @example
 * @code
 * $myanyobject = Loader::load($uid);
 * @endcode
 *
 *
 *
 */
class Loader
{
	/**
	 * The table to query for get the class name from uid.
	 * @var string
	 */
	static $_master_index_table = 'view_anyobject_index';

	/**
	 * Connexion to pgsql server where is the master index table
	 * @var PDO
	 */
	public static $connexion = null;

	/**
	 * If true, force to query db to reload links.
	 * @var boolean
	 */
	public static $force;

	/**
	 * If true, lock the db records in accordance to the use db system.
	 * @var boolean
	 */
	public static $lock;

	/**
	 * If true, get object from registry, default is true
	 * @var \Rbh\Dao\Registry
	 */
	public static $registry;

	/**
	 * Statment for get class table
	 * @var \PDOStatement
	 */
	public static $getClassStmt;

	/**
	 * Set the connexion to Postgresql db where are store the index.
	 *
	 * @param PDO
	 * @return void
	 */
	public static function init()
	{
		self::$connexion=\Rbh\Dao\Connexion::get();
		self::$registry=\Rbh\Dao\Registry::singleton();
		self::$getClassStmt=null;
	}

	/**
	 * @param string				$uid	Uuid of the object to load.
	 * @param array		$options = array(
	 * 						string	'classname'	  [OPTIONAL]	Name of class of object to load
	 * )
	 * @return \Rbh\Anyobject
	 */
	public static function load( $id, $className )
	{
		$Object = self::$registry->get($id);
		if($Object){
			return $Object;
		}
		else{
			$Object = new $className();
			$Dao = \Rbh\Dao\Factory::getDao( $className );
			$Dao->loadFromId( $Object, $id );
			self::$registry->add($Object);
			return $Object;
		}
	}

	/**
	 * @param string				$uid	Uuid of the object to load.
	 * @return string
	 */
	public static function getClassFromId( $id )
	{
		if(!self::$getClassStmt){
			$sql = "SELECT c.name as cname FROM anyobject AS a JOIN classes AS c ON a.cid=c.id WHERE a.id=:id";
			self::$getClassStmt = self::$connexion->prepare($sql);
			self::$getClassStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}
		self::$getClassStmt->execute(array(':id'=>$id));
		$row = self::$getClassStmt->fetch();
		return $row['cname'];
	}



	/**
	 *
	 * @param \Rbh\Anyobject		$mapped
	 * @param array 				$expectedProperties
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParent( $mapped, $level=100, $force = false, $locks = true )
	{
		$sql="
		WITH RECURSIVE treegraph(luid, lparent, lchild, lname, lindex, ldata, level, lpath) AS (
		SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, (l.lparent||'.'||l.lchild)::text::ltree
		FROM anyobject_links l
		WHERE l.lchild = '$mapped->id'
		UNION ALL
		SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, tg.level + 1, (tg.lpath::text || '.'|| l.lchild)::text::ltree
		FROM anyobject_links l, treegraph tg
		WHERE tg.lparent = l.lchild AND tg.level < $level
		)
		SELECT
		tg.luid, tg.lparent, tg.lchild, tg.lname, tg.lindex, tg.ldata, tg.level, tg.lpath,
		class.name AS classname, class.tablename AS dbtablename
		FROM treegraph AS tg
		JOIN anyobject AS parent ON parent.id = tg.lparent
		JOIN classes AS class ON parent.cid = class.id
		ORDER BY lpath ASC, lindex ASC;
		";

		self::$registry->add($mapped);

		$stmt = self::$connexion->query($sql);
		
		while($row=$stmt->fetch()){
			$parent = self::load($row['lparent'], $row['classname']);
			$child = self::$registry->get($row['lchild']);
			$lnk = \Rbh\LinkDao::loadFromArray(new \Rbh\Link(), $row);
			$lnk->parent = $parent;
			$lnk->child = $child;
			$parent->links[$lnk->uid] = $lnk;
		}
	} //End of method


	/**
	 *
	 * @param \Rbh\Anyobject		$mapped
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadParentRecursively( $mapped, $force = false, $locks = true )
	{
		if($mapped->parentId){
			$parent = Loader::load( $mapped->parentId, $force, $locks );
			if($parent){
				self::loadParentRecursively( $parent, $force, $locks );
				$mapped->parent=$parent;
			}
		}
	} //End of method


	/**
	 *
	 * @param \Rbh\Anyobject	$mapped
	 * @param array 				$expectedProperties
	 * @param integer				$level
	 * @param boolean				$force
	 * @param boolean				$locks
	 * @return void
	 */
	public static function loadChildren( $mapped, $level=100, $force = false, $locks = true )
	{
		$sql="
		WITH RECURSIVE treegraph(luid, lparent, lchild, lname, lindex, ldata, level, lpath) AS (
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, (l.lparent||'.'||l.lchild)
			FROM anyobject_links l
			WHERE l.lparent = '$mapped->id'
			UNION ALL
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, tg.level + 1, (tg.lpath || '.'|| l.lchild)
			FROM anyobject_links l, treegraph tg
			WHERE tg.lchild = l.lparent AND tg.level < $level
		)
		SELECT
		tg.*,class.name AS classname
		FROM treegraph AS tg
		JOIN anyobject AS child ON child.id = tg.lchild
		JOIN classes AS class ON child.cid = class.id
		ORDER BY lpath ASC, lindex ASC;
		";
		
		self::$registry->add($mapped);

		$stmt = self::$connexion->query($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);

		while($row=$stmt->fetch()){
			$child = self::load($row['lchild'], $row['classname']);
			$parent = self::$registry->get($row['lparent']);
			$lnk = \Rbh\LinkDao::loadFromArray(new \Rbh\Link(), $row);
			$lnk->parent = $parent;
			$lnk->child = $child;
			$parent->links[$lnk->uid] = $lnk;
		}
	} //End of method

	/*
	 *
	item0
	item1.1
	item1.2
	item2.1
	item2.2
	*
	*/

} //End of class
