<?php
//%LICENCE_HEADER%

/**
 * $Id: ListPgTest.php 630 2011-09-15 20:36:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/ListTest.php $
 * $LastChangedDate: 2011-09-15 22:36:49 +0200 (jeu., 15 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 630 $
 */

namespace Rbh\Dao\Pg;

use \Rbh\Test\Test;
use \Rbh\Dao\Pg\Loader;
use \Rbh\Dao\Connexion;

/**
 * @brief Test class for \Rbh\Dao\Pg\Loader
 * 
 */
class LoaderTest extends Test
{
	protected function setUp()
	{
		Loader::init();
	}
	
	
	/**
	 * 
	 */
	function Test_getClassFromId()
	{
		$R = Loader::getClassFromId(89);
		assert($R=='Rbh\Org\Unit');
	}
	
	/**
	 *
	 */
	function Test_load()
	{
		$R = Loader::load(89, 'Rbh\Org\Unit');
		echo $R->id.CRLF;
		echo $R->name.CRLF;
		echo $R->uid.CRLF;
		assert(get_class($R)=='Rbh\Org\Unit');
	}
	
	
	/**
	 * @See \Rbh\Build\SurfaceTest
	 */
	function Test_loadChildren()
	{
		echo '@See \Rbh\Build\SurfaceTest';
	}
	
	/**
	 * @TODO
	 */
	function Test_loadParent()
	{
		echo '@TODO';
	}
	
	/**
	 * @TODO
	 */
	function Test_loadParentRecursively()
	{
		echo '@TODO';
	}
	
	
}
