<?php
//%LICENCE_HEADER%

/**
 * $Id: ListPgTest.php 630 2011-09-15 20:36:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/ListTest.php $
 * $LastChangedDate: 2011-09-15 22:36:49 +0200 (jeu., 15 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 630 $
 */

namespace Rbh\Dao\Pg;
use \Rbh\Test\Test As Test;
use \Rbh\Dao\Connexion As Connexion;


/**
 * @brief Test class for DaoList
 * @include Rbplm/Dao/Pg/ListTest.php
 *
 */
class DaoListTest extends Test
{
	/**
	 * @var    DaoList
	 * @access protected
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		//\Rbh\Dao\Registry::singleton()->reset();
	}


	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	/*
	 INSERT INTO anyobject (id,name,parent,uid,cid,owner)
	VALUES
	(500, 'A', 89,'00000000-1111-2222-3333-00000000000a',20,10),
	(501, 'B', 89,'00000000-1111-2222-3333-00000000000b',20,10),
	(502, 'C', 89,'00000000-1111-2222-3333-00000000000c',20,10),
	(503, 'D', 89,'00000000-1111-2222-3333-00000000000d',20,10),
	(504, 'E', 89,'00000000-1111-2222-3333-00000000000e',20,10),
	(505, 'F', 89,'00000000-1111-2222-3333-00000000000f',20,10);

	INSERT INTO anyobject_tree (parent, child, name)
	VALUES
	(500, 501, 'B.1'),
	(500, 502, 'C.1'),
	(502, 503, 'D.1'),
	(502, 504, 'E.1'),
	(502, 505, 'F.1'),
	(501, 505, 'F.2');

	WITH RECURSIVE parcourt_graphe(parent, child, donnee, depth, name) AS (
		SELECT g.parent, g.child, g.data, 1, g.name
		FROM anyobject_tree g
		UNION ALL
		SELECT g.parent, g.child, g.data, sg.depth + 1, g.name
		FROM anyobject_tree g, parcourt_graphe sg
		WHERE g.parent = sg.child
	)
	SELECT * FROM parcourt_graphe;
	*/
	function Test_DaoInitData()
	{
		$List = new DaoList();
		$List->table='anyobject';
		 
		/* Construct a list with 10 entries.
		 * Each entries must have same set of keys.
		*/
		echo 'Construct a list.' . CRLF;
		$alist = array();
		$i = 0;
		$j = 1000;
		while($i < 10){
			$alist[] = array(
				'id'=>$j++,
				'cid'=>20,
				'uid'=>\Rbh\Uuid::newUid(),
				'name'=>'A'.$i,
				'label'=>uniqid(),
				'owner'=>\Rbh\People\User::SUPER_USER_ID,
			);
			$i++;
		}
		foreach($alist as $aitem){
			$i=0;
			while($i < 10){
				$blist[] = array(
					'id'=>$j++,
					'cid'=>20,
					'uid'=>\Rbh\Uuid::newUid(),
					'name'=>$aitem['name'].'.'.'B'.$i,
					'label'=>uniqid(),
					'owner'=>\Rbh\People\User::SUPER_USER_ID,
					'parent'=>$aitem['id'],
				);
				$i++;
			}
		}
		foreach($blist as $aitem){
			$i=0;
			while($i < 10){
				$clist[] = array(
					'id'=>$j++,
					'cid'=>20,
					'uid'=>\Rbh\Uuid::newUid(),
					'name'=>$aitem['name'].'.'.'C'.$i,
					'label'=>uniqid(),
					'owner'=>\Rbh\People\User::SUPER_USER_ID,
					'parent'=>$aitem['id'],
				);
				$i++;
			}
		}
		
		$merged = array_merge($alist,$blist,$clist);
		
		/* SAVE the list in Db*/
		echo 'SAVE the list in Db.' . CRLF;
		//var_dump($merged);die;
		//$List->save( $merged, array('pkey'=>array('id'), 'priority'=>'insert' ) );
		
		//Create links items
		$i=0;
		foreach($blist as $bitem){
			$bclist[] = array(
					'luid'=>\Rbh\Uuid::newUid(),
					'lparent'=>$clist[0]['id'],
					'lchild'=>$bitem['id'],
					'lname'=>$clist[0]['name'].'.'.'CB.'.$i,
			);
			$bclist[] = array(
					'luid'=>\Rbh\Uuid::newUid(),
				    'lparent'=>$clist[1]['id'],
					'lchild'=>$bitem['id'],
					'lname'=>$clist[1]['name'].'.'.'CB.'.$i,
			);
			$bclist[] = array(
					'luid'=>\Rbh\Uuid::newUid(),
					'lparent'=>$clist[2]['id'],
					'lchild'=>$bitem['id'],
					'lname'=>$clist[2]['name'].'.'.'CB.'.$i,
			);
			$bclist[] = array(
					'luid'=>\Rbh\Uuid::newUid(),
					'lparent'=>$alist[0]['id'],
					'lchild'=>$bitem['id'],
					'lname'=>$alist[0]['name'].'.'.'AB.'.$i,
			);
			$bclist[] = array(
					'luid'=>\Rbh\Uuid::newUid(),
					'lparent'=>$alist[1]['id'],
					'lchild'=>$bitem['id'],
					'lname'=>$alist[1]['name'].'.'.'AB.'.$i,
			);
			$bclist[] = array(
					'luid'=>\Rbh\Uuid::newUid(),
					'lparent'=>$alist[2]['id'],
					'lchild'=>$bitem['id'],
					'lname'=>$alist[2]['name'].'.'.'AB.'.$i,
			);
			$i++;
		}
		$List = new DaoList();
		$List->table='anyobject_links';
		$List->suppress("lname LIKE 'A%'");
		$List->insert( $bclist, array('pkey'=>array('id'), 'priority'=>'insert' ) );
	}

	/**
	 * Assume that tests datas are loaded in db with this structure:
	 * Init with comand
	 * $ php tests.php -i
	 *
	 */
	function _Test_DaoLoadTree(){
		$List = new DaoList();
		$List->table = 'anyobject';

		/*
		 * Note:
		* Postgres ltree is use to search.
		* ltree cast function text2ltree must be call to convert string in ltree type.
		* To use text2ltree, string dont must contains characters like [,;:-].
		*
		* Ltree documentation in http://www.sai.msu.su/~megera/postgres/gist/ltree/ say that
		* label must contains only [a-zA-Z0-9] and - characters.
		*
		* This limitation require the property label on class Component.
		*
		*/
		echo 'Get all tree under the OU RanchbePlm.Top:' . CRLF;
		$filter = 'path::ltree <@ \'RanchbePlm.Top\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;


		echo 'Get parents of RanchbePlm.Top.Collection.Astronomy.Stars' . CRLF;
		$filter = 'path::ltree @> \'RanchbePlm.Top.Collections.Astronomy.Stars\'::ltree';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;


		echo 'Get one level childs of RanchbePlm.Top' . CRLF;
		$filter = 'path::ltree ~ \'RanchbePlm.Top.*{0,1}\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;

		echo 'Get exactly and only the second level after RanchbePlm.Top' . CRLF;
		$filter = 'path::ltree ~ \'RanchbePlm.Top.*{2}\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;

		echo 'Get 2 levels childs of RanchbePlm.Top' . CRLF;
		$filter = 'path::ltree ~ \'RanchbePlm.Top.*{0,2}\'';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;

		echo 'Get node RanchbePlm.Top.Collection.Astronomy.Stars' . CRLF;
		$filter = 'path::ltree = \'RanchbePlm.Top.Collections.Astronomy.Stars\'::ltree';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;

		echo 'Get all nodes except RanchbePlm.Top.Collection.Astronomy.Stars' . CRLF;
		$filter = 'path::ltree <> \'RanchbePlm.Top.Collections.Astronomy.Stars\'::ltree';
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
			var_dump( $current['fullpathname'] );
		}
		echo '=====================================' . CRLF;

		/*@todo :*/
		echo 'With array: TODO...' . CRLF;
		echo '=====================================' . CRLF;
	}


	/**
	 *
	 */
	function _Test_DaoLoadInCollection(){
		$config = array(
			'table'=>'view_people_group_links'
		);
		$List = new DaoList($config);
		$filter = null;
		$List->load( $filter, array('force'=>true) );
		foreach($List as $current){
		}

		$collection = new Rbplm_Model_Collection();
		$List->loadInCollection($collection);
		foreach($collection as $current){
		}
	}


	/**
	 *
	 */
	function _Test_DaoSaveUpdateDelete(){
		/*Config table or other parameters need by Dao*/
		$config = array(
			'table'=>'org_ou'
		);
		$List = new DaoList($config, Connexion::get());

		/* Construct a list with 10 entries.
		 * Each entries must have same set of keys.
		*/
		echo 'Construct a list with 10 entries.' . CRLF;
		$alist = array();
		$i = 0;
		while($i < 10){
			$alist[] = array(
				'uid'=>\Rbh\Uuid::newUid(),
				'name'=>uniqid('ListPgSaveTest_Ou_') . '_' . $i,
				'label'=>uniqid(),
				'owner'=>\Rbh\People\User::getCurrentUser()->uid,
				'parent'=>\Rbh\Org\Root::singleton()->uid,
				'description'=>__CLASS__ . '::' . __FUNCTION__,
			);
			$i++;
		}

		/* SAVE the list in Db*/
		echo 'SAVE the list in Db.' . CRLF;
		$List->save( $alist, array('pkey'=>array('uid'), 'priority'=>'insert' ) );

		$List = null;
		$List = new DaoList($config, Connexion::get());

		/*GET a list from DB*/
		echo 'GET a list from DB.' . CRLF;
		$filter = "name LIKE 'ListPgSaveTest_Ou_%'";
		$List->load($filter);

		$alist = array();
		foreach($List as $current){
			$current['name'] = $current['name'] . '_Update';
			$alist[] = $current;
		}

		/*UPDATE a list from DB*/
		echo 'UPDATE a list from DB.' . CRLF;
		//var_dump($alist);
		$List->save( $alist, array('pkey'=>array('uid'), 'priority'=>'update' ) );

		$List = null;
		$List = new DaoList($config, Connexion::get());
		$List->load($filter);
		foreach($List as $current){
			assert( strstr($current['name'], '_Update') );
		}

		/**
		 * If loop is call twice, db is request twice. No cache in List.
		 */
		foreach($List as $current){
			assert( strstr($current['name'], '_Update') );
		}

		/* DELETE*/
		echo 'DELETE.' . CRLF;
		$List->suppress($filter);

		/* After delete datas are no more available in list */
		echo 'After delete datas are no more available in list.' . CRLF;
		foreach($List as $current){
			assert( $current == false );
		}

		//Suppress add words to names
		echo 'Suppress add words to names.' . CRLF;
		$Pdo = $List->getConnexion();
		$c = $List->getConfig();
		$table = $c['table'];
		$Sql = "UPDATE $table AS t SET name = REPLACE(t.name, '_Update', '')";
		$Pdo->exec($Sql);
	}
}
