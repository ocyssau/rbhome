<?php
//%LICENCE_HEADER%


/**
 * $Id: ListPg.php 680 2011-11-04 11:16:39Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/List.php $
 * $LastChangedDate: 2011-11-04 12:16:39 +0100 (ven., 04 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 680 $
 */


/** SQL//CANCEL//_VIEW>>
CREATE OR REPLACE VIEW view_fullpathname_component AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.label FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathname
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathname;

CREATE OR REPLACE VIEW view_fullpathuid_component AS
SELECT o.id, o.name, o.label, o.class_id, o.parent, o.path, array_to_string(
	ARRAY(SELECT a.uid FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;


CREATE OR REPLACE VIEW view_fullpathuid_component_2 AS
SELECT o.*, array_to_string(
	ARRAY(SELECT a.uid FROM component AS a WHERE a.path @> o.path ORDER BY a.path),
    '.') As fullpathuid
FROM component As o 
WHERE path IS NOT NULL
ORDER BY fullpathuid;
<<*/

namespace Rbh\Dao\Pg;

use \Rbh\Dao\Connexion As Connexion;
use \Rbh\Sys\Exception As Exception;
use \Rbh\Sys\Error As Error;
use \Rbh\Signal as Signal;
use \Rbh\Rbh As Rbh;


/**
 * A list of records in db.
 * This class implements a iterator.
 * Query postgresql database to create a list of scalar values in a iterator class.
 * Example and tests: Rbplm/Dao/Pg/ListTest.php
 * 
 * @todo Check and improve update list procedures.
 * 
 */
class DaoList implements \Iterator, \Countable
{
	
	/**
	 * Name of table to query for write
	 * @var string
	 */
	public $table = 'anyobject';
	
	/**
	 * View table to query for read
	 *
	 * @var string
	 */
	public $vtable = null;
	
	/**
	 * @var \PDO
	 */
	public $connexion;

	/**
	 * If true, the datas are loaded from db, else its a new record not yet save
	 *
	 * @var boolean
	 */
	protected $isLoaded = false;
	
	/**
	 * @var \PDOStatement
	 */
	public $stmt;

	/**
	 * @var integer
	 */
	protected $count = 0;

	/** 
	 * Position of the iterator
	 *
	 * @var string
	 */
	protected $key = 0;

	/**
	 * Current iterator item
	 */
	protected $current;

	/**
	 * Flag to mark the key of $current
	 *
	 * @var integer
	 */
	protected $loadedKey;
	
	/**
	 * The used filter to get datas
	 * 
	 * @var string
	 */
	protected $filter;
	
	/**
	 * Fetch mode.
	 * @var integer
	 */
	public $mode = \PDO::FETCH_ASSOC;
	
	public $withLinks = false;

	/**
	 * Constructor
	 *
	 * @param \PDO
	 *
	 */
	public function __construct( $conn=null )
	{
		/*
		if( $conn ){
		$this->connexion = $conn;
		}
		else{
		$this->connexion = \Rbh\Dao\Connexion::get();
		}
		*/
		$this->connexion = \Rbh\Dao\Connexion::get();
	} //End of function

	
	/**
	 * Reinit properties
	 *
	 * @return void
	 */
	protected function _reset()
	{
		$this->stmt = null;
		$this->isLoaded == false;
		$this->propsToSuppress = array();
		$this->linksToSuppress = array();
		
		$this->current = null;
		$this->key = 0;
		$this->count = null;
		$this->level = null;
		$this->loadedKey = null;
	} //End of function
	
	/**
	 * Factory method for instanciate List object.
	 * 
	 * @return \Rbh\Dao\Pg\List
	 */
	public static function newList($Dao)
	{
		$list=new self();
		$list->table = $Dao->table;
		return $list;
	} //End of method
	
	
	/**
	 * @see Rbplm_Dao_Interface::suppress()
	 * @param string $filter	sql filter.
	 * 
	 */
	public function suppress( $filter )
	{
		if ( !$filter ){
			throw new Exception('NONE_FILTER_SET', Error::ERROR, $filter);
		}
		$sql = "DELETE FROM $this->table WHERE $filter";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute();
		$this->lastSql = $sql;
	}
	
	
	/**
	 * 
	 * @param unknown_type $filter
	 * @param unknown_type $options
	 */
	public function loadTree($parentId, $level=100, $filter=null, $options = array()){
		$options = array_merge( array('force'=>false,'lock'=>false, 'select'=>false), $options );
		$this->_reset();
		$table = $this->table;
		$select = '*';
		if($filter){
			$filter = ' WHERE '.$filter;
		}
		$sql="
		WITH RECURSIVE treegraph(luid, lparent, lchild, lname, lindex, ldata, level, lpath) AS (
				SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, (l.lparent||'.'||l.lchild)
				FROM anyobject_links l
				WHERE l.lparent = '$parentId'
				UNION ALL
				SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, tg.level + 1, (tg.lpath || '.'|| l.lchild)
				FROM anyobject_links l, treegraph tg
				WHERE tg.lchild = l.lparent AND tg.level < $level
		)
		SELECT
		tg.*,
		class.name AS classname, class.tablename AS dbtablename,
		child.*
		FROM treegraph AS tg
		LEFT OUTER JOIN $table AS child ON tg.lchild = child.id
		JOIN classes AS class ON child.cid = class.id
		$filter
		ORDER BY lpath ASC, lindex ASC;
		";
		$this->lastSql = $sql;
		$this->stmt = $this->connexion->query($sql);
		$this->isLoaded = true;
		$this->filter = $filter;
		return $this;
	}
	
	
	/**
	 * @param string		$filter
	 * @param array			$options	Is array of options.
	 * 						options are force, lock
	 * @return Rbplm_Model_List
	 */
	public function load( $filter = null, array $options = array() )
	{
		$options = array_merge( array('force'=>false,'lock'=>false, 'select'=>false), $options );
		$select = '*';
		
		if ( $this->isLoaded == true && $options['force'] == false ){
			return $this;
		}
		
		if($options['lock'] == true){
			$select = "pg_advisory_lock(id), "  . $select;
		}
		
		$this->_reset();
		
		if($this->withLinks){
			$sql = "SELECT $select FROM $this->table AS a LEFT OUTER JOIN anyobject_links AS l ON a.id = l.lparent";
		}
		else{
			$sql = "SELECT $select FROM $this->table";
		}
		if($filter != ""){
			$sql = $sql . " WHERE $filter";
		}
		$this->stmt = $this->connexion->prepare($sql);
		$this->stmt->setFetchMode($this->mode);
		$this->stmt->execute();
		$this->isLoaded = true;
		$this->filter = $filter;
		$this->lastSql = $sql;
		return $this;
	} //End of function
	
	/**
	 * Populate list from sql request.
	 * The request must return all property of object with system name.
	 * 
	 * NOT IN INTERFACES.
	 *
	 * @param string			Sql request
	 * @param array			$options	Is array of options.
	 * 							'lock'	=>boolean 	If true, lock records, default is false
	 * 							'force'	=>boolean	If true, force to reload, default is false
	 * @return Rbplm_Model_List
	 */
	public function loadFromSql( $sql, $options = array() )
	{
		$options = array_merge( array('force'=>false,'lock'=>false), $options );
		if ( $this->isLoaded == false || $options['force'] == true ){
			$this->_reset();
			$this->stmt = $this->connexion->prepare($sql);
			$this->stmt->setFetchMode(\PDO::FETCH_ASSOC);
			$this->stmt->execute();
			$this->isLoaded = true;
			$this->filter = '';
			$this->lastSql = $sql;
		}
		return $this;
	} //End of function
	
	
	/**
	 * Save content of array $list in db.
	 * 
	 * $list is array where key are name of properties and values, values to save.
	 * Each entry of array must respect same number and name of properties.
	 * 
	 * $pkey is array where values are name of key involved in primary key definition.
	 * The values of this key must be presents in $list entry.
	 * 
	 * Example:
	 * <code>
	 * $list = array();
	 * $list[] = array('uid'=>$uuid1, 'name'=>$name1, 'description'=>$desc1);
	 * $list[] = array('uid'=>$uuid2, 'name'=>$name2, 'description'=>$desc2);
	 * $oList->save($list, false, array('uid'));
	 * </code>
	 * 
	 * 
	 * @see Rbplm_Dao_Interface::save()
	 * @param array		$list
	 * @param array		$options	Is array of options.
	 * 								'unlock'=>boolean 	If true, unlock records, default is true.
	 * 								'pkey'	=>array 	name of keys to use as primary keys. Default is uid.
	 * 								'priority'=>string	save | update, if priority is give to update, update is try first, else insert is try first. Default is update.
	 * @return void
	 */
	public function save( $list, array $options = array() )
	{
		$table = $this->table;
		$options = array_merge( array('unlock'=>false,'pkey'=>false,'priority'=>'update'), $options );
		$pkeys = $options['pkey'];
		
		$select = array_keys($list[0]); //array('pname')
		$bind = array();
		foreach($select as $p){
			$bind[] = ':' . $p;  //array(':pname')
		}
		
		/* INSERT */
		$sKeys = implode(',', $select);
		$sValues = implode(',', $bind);
		$insertSql = "INSERT INTO $table ($sKeys) VALUES ($sValues)";
		
		/* UPDATE */
		$select2 = $select;
		$bind2 = $bind;
		if( $pkeys ){
			$aWhere = array();
			foreach(array_intersect_key( array_combine($select, $bind), array_flip($pkeys) ) as $keyname=>$keybind){ //array('pkey'=>':pkey')
				$aWhere[] = $keyname . '=' . $keybind; //string 'pkey'=':pkey'
				unset($select2[array_search($keyname, $select)]); //suppress pkey from $select
				unset($bind2[array_search($keybind, $bind)]); //suppress pkey from $bind
			}
			if($aWhere){
				$sWhere = ' WHERE '.implode(' AND ', $aWhere);
			}
		}
		else{
			$sWhere = " WHERE uid=':uid'";
			unset($select2[array_search('uid', $select)]); //suppress uid from $select
			unset($bind2[array_search(':uid', $bind)]); //suppress :uid from $bind
		}
		$sKeys 	 = implode(',', $select2);
		$sValues = implode(',', $bind2);
		$updateSql = "UPDATE $table SET ($sKeys) = ($sValues)" . $sWhere;
		
		/*
		$sql = "BEGIN;
				SAVEPOINT sp1;
				$insertSql;
				ROLLBACK TO sp1;
				$updateSql;
				COMMIT;";
		$sql = $this->connexion->quote($sql);
		$this->connexion->exec( $sql );
		*/
		
		/* EXECUTE */
		$insertStmt = $this->connexion->prepare($insertSql);
		$updateStmt = $this->connexion->prepare($updateSql);
		if($options['priority'] == 'update'){
			foreach($list as $entry){
				//Convert boolean to integer! \PDO dont do that! Grr..
				foreach($entry as $k=>$att){
					if( is_bool($att) ){
						$entry[$k] = (integer) $att;
					}
				}
				$b = array_combine( $bind, array_values($entry) );
				try{
					$updateStmt->execute( $b );
				}catch(\PDOException $e){
					$insertStmt->execute( $b );
				}
			}
		}
		else{
			foreach($list as $entry){
				$b = array_combine( $bind, array_values($entry) );
				//var_dump($bind,$entry,$b);
				try{
					$insertStmt->execute( $b );
				}catch(\PDOException $e){
						echo $e->getMessage().CRLF;
					try{
						$updateStmt->execute( $b );
					}catch(\PDOException $e){
						echo $e->getMessage().CRLF;
					}
				}
			}
		}
	} //End of function
	
	
	public function insert( $list, array $options = array() )
	{
		$table = $this->table;
		$options = array_merge( array('unlock'=>false,'pkey'=>false,'priority'=>'update'), $options );
		$pkeys = $options['pkey'];
	
		$select = array_keys($list[0]); //array('pname')
		$bind = array();
		foreach($select as $p){
			$bind[] = ':' . $p;  //array(':pname')
		}
	
		/* INSERT */
		$sKeys = implode(',', $select);
		$sValues = implode(',', $bind);
		$insertSql = "INSERT INTO $table ($sKeys) VALUES ($sValues)";
		
		/* EXECUTE */
		$insertStmt = $this->connexion->prepare($insertSql);
		foreach($list as $entry){
			$b = array_combine( $bind, array_values($entry) );
			try{
				$insertStmt->execute( $b );
			}catch(\PDOException $e){
				echo $e->getMessage().CRLF;
			}
		}
	} //End of function
	
	
	/**
	 * Getter to \PDOStatement composed object.
	 * 
	 * NOT IN INTERFACES.
	 * 
	 * @return \PDOStatement
	 */
	public function getStmt()
	{
		return $this->stmt;
	}
	
	/**
	 * Translate current row to Rbplm object.
	 * If $useRegistry = true, get object from registry if exist and add it if not.
	 * 
	 * @param bool $useRegistry
	 * @return Rbplm_Model_Component
	 * 
	 */
	public function toObject($useRegistry = true)
	{
		$current = $this->current();
		
		if($useRegistry){
			$Mapped = Rbh\Dao\Registry::singleton()->get($current['uid']);
		}
		if( !isset($current['class_id']) ){
			throw new Exception('CLASS_ID_IS_NOT_REACHABLE', Error::ERROR, $current);
		}
		if( !$Mapped ){
			$className = Rbh\Dao\Pg\ClassDao::singleton()->toName( $current['class_id'] );
			$Dao = Rbh\Dao\Factory::getDao($className);
			$Mapped = new $className();
			$Dao->loadFromArray($Mapped, $current, false);
			if($useRegistry){
				Rbh\Dao\Registry::singleton()->add($Mapped);
			}
		}
		return $Mapped;
	}
	
	
	/**
	 * Translate current row from system notation to application notation.
	 * 
	 * If $dao is a string, assume that is a class with a method toApp().
	 * If $dao is a object, assume that is a type with a method toApp().
	 * If $dao is null, get the appropriate class to use from the factory Rbplm_Dao_Factory::getDaoClass().
	 * In this last case, require an attribut class_id in current statement.
	 * 
	 * @param $dao	[OPTIONAL]	Set a Dao class or any other class with a method toApp(), to use for convert attributs.
	 * @return array
	 */
	public function toApp( $dao=null )
	{
		$current = $this->current();
		$class = Rbh\Dao\Pg\ClassDao::singleton()->toName($current['cid']);
		$DaoClass = Rbh\Dao\Factory::getDaoClass($class);
		//throw new Exception('CLASS_ID_IS_NOT_SET', Error::WARNING, $current);
		return call_user_func(array($DaoClass, 'toApp'), $current);
	}
	
	
	/**
	 * Translate current row from system notation to application notation.
	 * 
	 * @return array
	 */
	public function toSys()
	{
		$current = $this->current();
		$class = Rbh\Dao\Pg\ClassDao::singleton()->toName($current['cid']);
		$DaoClass = Rbh\Dao\Factory::getDaoClass($class);
		return call_user_func( array($DaoClass, 'toSys'), array($current) );
	}
	
	/**
	 * Translate current stmt into array.
	 * @return array
	 */
	public function toArray()
	{
		return $this->stmt->fetchAll();
	}
	
	
	/**
	 * @see Iterator::valid()
	 * @return void
	 */
	public function valid()
	{
		if( $this->key < $this->count() ){
			return true;
		}
		else{
			$this->stmt->closeCursor();
			return false;
		}
	}


	/**
	 * Gets the current iterator item
	 * 
	 * @see Iterator::current()
	 * @return array
	 */
	public function current()
	{
		if( $this->loadedKey !== $this->key ){
			$this->current = $this->stmt->fetch();
			$this->loadedKey = $this->key;
		}
		return $this->current;
	}


	/**
	 * Value of the iterator key for the current pointer
	 * (translated by the metamodel)
	 * 
	 * @see Iterator::key()
	 * @return string
	 */
	public function key()
	{
		return $this->key;
	}


	/**
	 * @see Iterator::next()
	 * @return void
	 */
	public function next()
	{
		$this->key++;
	}

	
	/**
	 * @see Iterator::rewind()
	 * @return void
	 */
	public function rewind()
	{
		if( $this->key > 0 ){
			$this->stmt->execute();
			$this->key = 0;
		}
	}

	/**
	 * @see Countable::count()
	 * @return integer
	 */
	public function count()
	{
		if( !$this->count ){
			$this->count = $this->stmt->rowCount();
		}
		return $this->count;
	}
	
	/**
	 * Test if values in array $list of the property $propNameToTest are existing.
	 * Return a array of values for each properties defined by $returnSelect.
	 * 
	 * @param string	$propNameToTest	Name of property to test
	 * @param array		$list			List of value relatives to $propNameToTest to test
	 * @param array		$returnSelect	List of properties to pu in return for each tested element
	 * @return array	(Value of property $propNameToTest => array( 'selected property'=>'selected property value' ))
	 */
	public function areExisting( $propNameToTest, $list, $returnSelect=null )
	{
		if(is_array($returnSelect)){
			$select = implode(',', $returnSelect);
		}
		else{
			$select = $propNameToTest;
		}
		$sql = "SELECT $select FROM $this->table WHERE $propNameToTest=:testedValue";
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		foreach ($list as $testedValue){
			$bind = array(':testedValue'=>$testedValue);
			$stmt->execute($bind);
			$out[$testedValue] = $stmt->fetch();
		}
		return $out;
	}
	
	/**
	 * Count result from a sql request.
	 * 
	 * Is different of method count wich necessit to get the real query and receive the real datas.
	 * Here, select only count().
	 * 
	 * @param string|Rbplm_Dao_Pg_Filter	$filter
	 * @return integer
	 * 
	 */
	public function countAll($filter)
	{
		/*
		if(is_a($filter, '\Rbh\Dao\FilterInterface')){
			$filter = $filter->__toString();
		}
		*/
		$sql = "SELECT COUNT(*) FROM $this->table";
		if($filter){
			$sql .= " WHERE $filter";
		}
		$stmt = $this->connexion->query($sql);
		$count = $stmt->fetch();
		return $count[0];
	}
	
} //End of class
