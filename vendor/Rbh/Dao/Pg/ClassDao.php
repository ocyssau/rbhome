<?php
//%LICENCE_HEADER%

/**
 * $Id: ClassPg.php 491 2011-06-24 13:50:12Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/ClassPg.php $
 * $LastChangedDate: 2011-06-24 15:50:12 +0200 (ven., 24 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 491 $
 */

namespace Rbh\Dao\Pg;

use Rbh\Sys\Exception As Exception;
use Rbh\Sys\Error as Error;

/** SQL_SCRIPT>>
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
<<*/



/**
 * Dao to access to class identification in db.
 * Use by Postgres Dao class only.
 * 
 * This class load class definition from a table in db, to map class name to a integer id.
 * 
 * To use it, first, you must set a connexion. When setConnexion is call, class definition are loaded,
 * so, just do in bootstrap of application:
 * @code
 * Rbplm_Dao_Pg_Class::singleton()->setConnexion( $myConnexion );
 * //when set connexion is set, class is ready to use:
 * $myClassName = Rbplm_Dao_Pg_Class::singleton()->toName(100);
 * $myClassId = Rbplm_Dao_Pg_Class::singleton()->toId($myClassName);
 * @endcode
 * 
 */
class ClassDao{
	
	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	protected $table = 'classes';
	
	/**
	 * 
	 * @var array
	 */
	protected $classes = null;
	
	/**
	 * @var array
	 */
	protected $idIndex = null;
	
	/**
	 * @var array
	 */
	protected $classIndex = null;
	
	/**
	 * 
	 * Singleton pattern
	 * @var ClassDao
	 */
	protected static $instance = null;
	
	/**
	 * 
	 * @var boolean
	 */
	protected $isLoaded = false;
	
	/**
	 * Constructor
	 */
	private function __construct()
	{
	} //End of function
	
	
	/**
	 * Singleton pattern
	 * @return Rbplm_Dao_Pg_Class
	 */
	public static function singleton()
	{
		if( !self::$instance ){
			self::$instance = new ClassDao();
		}
		return self::$instance;
	} //End of function
	
	
	/**
	 * Load the properties in the mapped object.
	 * 
	 * @param \Rbh\Dao\Connexion	$mapped
	 * @return Rbh\Dao\Pg\ClassDao
	 */
	public function load($conn=null)
	{
		if($conn==null){
			$conn = \Rbh\Dao\Connexion::get();
		}
		$sql = "SELECT id, name, tablename FROM $this->table";
		$stmt = $conn->query($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$this->classes = $stmt->fetchAll();

		//$this->idIndex = new SplFixedArray( count($this->classes) );
		//$this->classIndex = new SplFixedArray( count($this->classes) );
		
		$this->idIndex 	= array();
		$this->classIndex 	= array();
		
		foreach($this->classes as $k=>$def){
			$this->idIndex[$k] = $def['id'];
			$this->classIndex[$k] = $def['name'];
		}
		$this->isLoaded = true;
		return $this;
	}
	
	/**
	 * Return the id of a class from his name.
	 * 
	 * @param string $name
	 * @return integer
	 * @throws \Rbh\Sys\Exception
	 */
	public function toId($name)
	{
		$k = array_search($name, $this->classIndex);
		if( $k === false ){
			throw new Exception('CLASSNAME_NOT_FOUND_%0%',Error::WARNING, array($name));
		}
		return $this->classes[$k]['id'];
	}
	
	/**
	 * Return name of a class from his id.
	 * 
	 * @param integer 		$id
	 * @return string
	 * @throws \Rbh\Sys\Exception
	 */
	public function toName($id)
	{
		$k = array_search($id, $this->idIndex);
		if( $k === false ){
			throw new \Rbh\Sys\Exception('CLASSID_NOT_FOUND_%0%', \Rbh\Sys\Error::WARNING, array($id));
		}
		return $this->classes[$k]['name'];
	}
	
	/**
	 * Return table used to store objects of class $id
	 * 
	 * @param integer|string		$id
	 * @return string
	 * @throws \Rbh\Sys\Exception
	 */
	public function getTable($id)
	{
		if(is_integer($id)){
			$k = array_search($id, $this->idIndex);
			if( $k === false ){
				throw new \Rbh\Sys\Exception('CLASSID_NOT_FOUND_%0%', \Rbh\Sys\Error::WARNING, array($id));
			}
		}
		else{
			$k = array_search($id, $this->classIndex);
			if( $k === false ){
				throw new \Rbh\Sys\Exception('CLASSNAME_NOT_FOUND_%0%', \Rbh\Sys\Error::WARNING, array($id));
			}
		}
		return $this->classes[$k]['tablename'];
	}
	
	
} //End of class
