<?php
//%LICENCE_HEADER%

/**
 * $Id: ListPgTest.php 630 2011-09-15 20:36:49Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/ListTest.php $
 * $LastChangedDate: 2011-09-15 22:36:49 +0200 (jeu., 15 sept. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 630 $
 */

namespace Rbh\Dao\Pg;

use \Rbh\Test\Test;
use \Rbh\Dao\Pg\ClassDao;
use \Rbh\Dao\Connexion;

/**
 * @brief Test class for Rbplm_Dao_Pg_Class
 * 
 */
class ClassDaoTest extends Test
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     *
     * @access protected
     */
    protected function setUp()
    {
    }
	
    
    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     *
     * @access protected
     */
    protected function tearDown()
    {
    }
	
	/**
	 * 
	 */
	function Test_Dao()
	{
		$Class = ClassDao::singleton()->load(Connexion::get());
		
		var_dump( $Class->toName(20) );
		var_dump( $Class->toId('Rbh\Org\Unit') );
		var_dump( $Class->getTable('Rbh\Org\Unit') );
		var_dump( $Class->getTable(20) );
		var_dump( $Class );
		
		assert( $Class->toName(20) == 'Rbh\Org\Unit');
		assert( $Class->toId('Rbh\Org\Unit') == 20);
		assert( $Class->getTable('Rbh\Org\Unit') == 'org_ou');
		assert( $Class->getTable(20) == 'org_ou');
	}
}
