<?php
//%LICENCE_HEADER%

/**
 * $Id: User.php 520 2011-07-18 22:32:13Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/User.php $
 * $LastChangedDate: 2011-07-19 00:32:13 +0200 (mar., 19 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 520 $
 */

namespace Rbh\Dao;

use \Rbh\Dao\Filter\FilterInterface;
use \Rbh\Dao\Filter\Op;
use \Rbh\Sys\Exception As Exception;
use \Rbh\Sys\Error As Error;
use \Rbh\Signal As Signal;

/**
 * Create params to search in database.
 *
 */
class Filter implements FilterInterface
{
	/**
	 * Specify type of input. May be true if input is in application semantic, or false if input is in database semantic.
	 * @var boolean
	 */
	private $_asApp = true;
	
	/**
	 * Name of dao class to use for map application model<->system model
	 * @var string
	 */
	private $_daoClass = null;
	
	/**
	 * Limit the number of records in the result
	 * @var integer
	 */
	protected $_limit = null;

	/**
	 * Pagination page to display
	 * @var integer
	 */
	protected $_offset = null;

	/**
	 * field use for sort sql option
	 * @var string
	 */
	protected $_sortBy = '';
	
	/**
	 * field use for sort sql option
	 * @var string
	 */
	protected $_sortOrder = '';

	/**
	 * Input special where close. Exemple : $params[where] = array('field1 > 1' , 'field2 = 2')
	 * @var array
	 */
	protected $_where = array();

	/**
	 * any query to add to end
	 * @var string
	 */
	protected $_extra;
	
	/**
	 * Array of attributs to select
	 * @var string|array
	 */
	protected $_select = null;
	
	/**
	 * 
	 * @var boolean
	 */
	protected $_paginationIsActive = true;
	
	/**
	 * @param string	Dao class name.
	 * @param boolean	If true, assume that inputs attributs are defined as in application.
	 * @return void
	 */
	public function __construct($daoClass, $asApp = true)
	{
		$this->_asApp = $asApp;
		$this->_daoClass = $daoClass;
		$this->_paginationIsActive = true;
	}

	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Rbplm_Search_Op::OP_*
	 * @return Filter
	 */
	protected function _find( $what, $where, $op=Op::OP_CONTAINS, $boolOp='AND' )
	{
		if($this->_asApp){
			$where = call_user_func(array($this->_daoClass, 'toSys'), $where);
		}
		
		if(!$what || !$where){
			return;
		}
		
		switch($op){
			case Op::OP_BEGIN:
				$op = '=';
				$what = '\''.$what.'%\'';
				break;
			case Op::OP_END:
				$op = '=';
				$what = '\'%'.$what.'\'';
				break;
			case Op::OP_CONTAINS:
				$op = '=';
				$what = '\'%'.$what.'%\'';
				break;
			case Op::OP_NOTCONTAINS:
				$op = '<>';
				$what = '\'%'.$what.'%\'';
				break;
			case Op::OP_NOTEND:
				$op = '<>';
				$what = '\'%'.$what.'\'';
				break;
			case Op::OP_NOTBEGIN:
				$op = '<>';
				$what = '\''.$what.'%\'';
				break;
			case Op::OP_SUP:
				$op = '>';
				$what = '\''.$what.'\'';
				break;
			case Op::OP_EQUAL:
				if(is_string($what)){
					$op = 'LIKE';
				}
				else{
					$op = '=';
				}
				$what = '\''.$what.'\'';
				break;
			case Op::OP_EQUALSUP:
				$op = '>=';
				$what = '\''.$what.'\'';
				break;
			case Op::OP_INF:
				$op = '<';
				$what = '\''.$what.'\'';
				break;
			case Op::OP_EQUALINF:
				$op = '<=';
				$what = '\''.$what.'\'';
				break;
			default:
				throw( new Rbplm_Sys_Exception('UNKNOW_OPERATOR', Rbplm_Sys_Error::WARNING, array($op)) );
		}
		$sql = $where .' '. $op .' '. $what;
		$this->_where[] = array($sql, $boolOp);
		return $this;
	}
	
	/**
	 * @param FilterInterface
	 * @return Filter
	 */
	public function suband( $subFilter )
	{
		$this->_where[] = array('(' . $subFilter->__toString() . ')', 'AND');
		return $this;
	}
	
	/**
	 * @param FilterInterface
	 * @return Filter
	 */
	public function subor( $subFilter )
	{
		$this->_where[] = array('(' . $subFilter->__toString() . ')', 'AND');
		return $this;
	}
	
	/**
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Op::OP_*
	 * @return Filter
	 */
	public function andfind( $what, $where, $op=Op::OP_CONTAINS )
	{
		return $this->_find($what, $where, $op, 'AND');
	}
	
	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Op::OP_*
	 * @return Filter
	 */
	public function orfind( $what, $where, $op=Op::OP_CONTAINS )
	{
		return $this->_find($what, $where, $op, 'OR');
	}

	/**
	 * 
	 * @param integer	$int
	 * @return Filter
	 */
	public function page( $page, $pageLimit )
	{
		$this->_offset = ($page - 1) * $pageLimit;
		$this->_limit = $offset + $pageLimit;
		return $this;
	}

	/**
	 * Set or return status of pagination activation.
	 * If unactive, remove offset, limits and order.
	 * 
	 * @param boolean
	 * @return boolean
	 */
	public function paginationIsActive( $bool=null )
	{
		if($bool === true || $bool === false){
			$this->_paginationIsActive = $bool;
		}
		return $this->_paginationIsActive;
	}
	
	
	/**
	 * @param string|array	$by
	 * @param string		$dir
	 * @return Filter
	 */
	public function sort( $by , $dir='ASC' )
	{
		if($this->_asApp){
			if(is_array($by)){
				$by = call_user_func(array($this->_daoClass, 'toSys'), array_flip($by));
				$by = array_flip($by);
			}
			else{
				$by = call_user_func(array($this->_daoClass, 'toSys'), $by);
			}
		}
		if(is_array($by)){
			$this->_sortBy = implode( ', ', $by );
		}
		else{
			$this->_sortBy = $by;
		}
		$this->_sortOrder = $dir;
		return $this;
	}
	
	/**
	 * Get children of node
	 * 
	 * @param $path		string	Path as define in application
	 * @param $option	array	Options
	 * Options may be :
	 * 	level:	integer	The level after current node to query
	 * 	depth:	Match "depth" levels from level "level"
	 * 	pathAttributeName: string	Name of system attribute for path, default is 'path'.
	 * 	boolOp: string		Boolean operator, may be 'AND' or 'OR', default is AND
	 * 
	 * @example
	 * Examples:
	 * @code
	 * level = 2
	 * depth = null
	 * path = 'Root'
	 * return nodes:
	 * 	Root.Node0-1.Node1-1
	 * 	Root.Node0-1.Node1-2
	 * 	Root.Node0-1.Node1-3
	 * 
	 * level = 2
	 * depth = 1
	 * path = 'Root.Node1'
	 * return nodes:
	 * 	Root.Node0-1.Node1-1
	 * 	Root.Node0-1.Node1-1.Node1-1-1
	 * 	Root.Node0-1.Node1-1.Node1-1-2
	 * 	Root.Node0-1.Node1-2
	 * 	Root.Node0-1.Node1-2.Node1-2-1
	 * 	Root.Node0-1.Node1-2.Node1-2-2
	 * 	Root.Node0-1.Node1-3
	 * 	Root.Node0-1.Node1-3.Node1-3-1
	 * 	Root.Node0-1.Node1-3.Node1-3-2
	 * 
	 * level = null
	 * depth = 1
	 * path = 'Root.Node1'
	 * return nodes:
	 * 	Root.Node0-1
	 * 	Root.Node0-1.Node1-1
	 * 	Root.Node0-1.Node1-1.Node1-1-1
	 * 	Root.Node0-1.Node1-1.Node1-1-2
	 * 	Root.Node0-1.Node1-2
	 * 	Root.Node0-1.Node1-2.Node1-2-1
	 * 	Root.Node0-1.Node1-2.Node1-2-2
	 * 	Root.Node0-1.Node1-3
	 * 	Root.Node0-1.Node1-3.Node1-3-1
	 * 	Root.Node0-1.Node1-3.Node1-3-2
	 * @endcode
	 * 
	 * @see FilterInterface::children()
	 * 
	 * @return Filter
	 */
	public function children( $path, $options=array() )
	{
		/*
		 * ltree request @see http://www.sai.msu.su/~megera/postgres/gist/ltree/
		 * 
		 The following quantifiers are recognized for '*' (like in Perl):
		 {n}    Match exactly the level n
		 {n,}   Match at the level n and next
		 {n,m}  Match m levels from level n
		 {,m}   Match at maximum m levels (eq. to {0,m})
		 
		 It is possible to use several modifiers at the end of a label:
		 @     Do case-insensitive label matching
		 *     Do prefix matching for a label
		 %     Don't account word separator '_' in label matching, that is 'Russian%' would match 'Russian_nations', but not 'Russian'
		 
		 lquery could contains logical '!' (NOT) at the beginning of the label and '|' (OR) to specify possible alternatives for label matching.
		 
		 Example of lquery:
		 Top.*{0,2}.sport*@.!football|tennis.Russ*|Spain
		 a)  b)     c)      d)               e)
		 A label path should
			 a) begins from a node with label 'Top'
			 b) and following zero or 2 labels until
			 c) a node with label beginning from case-insensitive prefix 'sport'
			 d) following node with label not matched 'football' or 'tennis' and
			 e) ends on node with label beginning from 'Russ' or strictly matched 'Spain'.      *
		 */
		
		//SELECT path FROM component WHERE path::ltree ~ 'RanchbePlm.Top.*{2,2}';
		
		if($this->_asApp){
			$path = Rbplm_Dao_Pg::pathToSys($path);
		}
		
		$o = array_merge( array('level'=>null, 'depth'=>0, 'pathAttributeName'=>'path', 'boolOp'=>'and'), $options );
		$path = trim($path, '/');
		$level = $o['level'];
		$depth = $o['depth'];
		$maxLevel = $level + $depth;
		$pathAttName = $o['pathAttributeName'];
		$boolOp = $o['boolOp'];
		
		if($level && $depth){
			$scope = '{'.$level.','.$maxLevel.'}';
		}
		else if($depth){
			$scope = '{,'.$depth.'}';
		}
		else if($level){
			$scope = '{'.$level.'}';
		}
		$sql = "$pathAttName::ltree ~ '$path.*$scope'";
		$this->_where[] = array($sql, $boolOp);
		return $this;
	}
	
	/**
	 * Get the ancestors of a node.
	 * 
	 * @param $path		string	Path as define in application
	 * @param $option	array	Options
	 * 
	 * Options may be :
	 * 	minLevel:	integer	Min level of ancestors to query. If level is negatif, calcul level from current node, else calcul from root node. Default is 0.
	 * 	maxLevel:	Max level to query. May be negatif. Default is level of current node.
	 * 	pathAttributeName: string	Name of system attribute for path, default is 'path'.
	 * 	boolOp: string		Boolean operator, may be 'AND' or 'OR', default is AND
	 * 
	 * If maxLevel = 0, max is unactivate.
	 * 
	 * Examples:
	 * minLevel = 1
	 * maxLevel = 1
	 * path = 'Root.Node1.Node2.Node3'
	 * return node Root.Node1
	 * 
	 * minLevel = -1
	 * maxLevel = -1
	 * path = 'Root.Node1.Node2.Node3'
	 * return node Root.Node1.Node2
	 * 
	 * maxLevel = -1
	 * minLevel = 0
	 * path = 'Root.Node1.Node2.Node3'
	 * return nodes :
	 * 		Root.Node1.Node2
	 * 		Root.Node1
	 * 		Root
	 * 
	 * maxLevel = 0
	 * minLevel = 0
	 * path = 'Root.Node1.Node2.Node3'
	 * return nodes :
	 * 		Root.Node1.Node2.Node3
	 * 		Root.Node1.Node2
	 * 		Root.Node1
	 * 		Root
	 * 
	 * maxLevel = -1
	 * minLevel = 0
	 * path = 'Root.Node1.Node2.Node3'
	 * return nodes :
	 * 		Root.Node1.Node2
	 * 		Root.Node1
	 * 		Root
	 * 
	 * @return Filter
	 */
	public function ancestors( $path, $options=array() )
	{
		//SELECT path FROM component WHERE path::ltree ~ subpath('RanchbePlm.Top.Science.Astronomy', 0, 2)::text::lquery;
		//SELECT path FROM component WHERE path::ltree @> subpath('RanchbePlm.Top.Science.Astronomy', 0, 5) AND nlevel(path) >= 2;
		
		if($this->_asApp){
			$path = Rbplm_Dao_Pg::pathToSys($path);
		}
		
		$o = array_merge( array('maxLevel'=>0, 'minLevel'=>0, 'pathAttributeName'=>'path', 'boolOp'=>'and'), $options );
		$path = trim($path, '/');
		$maxLevel = $o['maxLevel'];
		$minLevel = $o['minLevel'];
		$pathAttName = $o['pathAttributeName'];
		$boolOp = $o['boolOp'];
		
		if($minLevel === 0 && $maxLevel > 0){
			$sql = "$pathAttName::ltree @> subpath('$path', 0, $maxLevel)";
		}
		else if($minLevel === 0 && $maxLevel < 0){
			$sql = "$pathAttName::ltree @> subpath('$path', 0, nlevel('$path')+$maxLevel)";
		}
		else if($minLevel === $maxLevel && $minLevel > 0){
			$sql = "$pathAttName::ltree ~ subpath('$path', 0, $minLevel)::text::lquery";
		}
		else if($minLevel === $maxLevel && $minLevel < 0){
			$sql = "$pathAttName::ltree ~ subpath('$path', 0, nlevel('$path')-$minLevel)::text::lquery";
		}
		else{
			$sql = "$pathAttName::ltree @> '$path'";
			if($minLevel > 0){
				$sql .= " AND nlevel('$path') >= $minLevel";
			}
			else if($minLevel < 0){
				$sql .= " AND nlevel('$path') >= nlevel('$path')+$minLevel";
			}
		}
		$this->_where[] = array($sql, $boolOp);
		return $this;
	}
	
	/**
	 * @param array	$select
	 * @return Filter
	 */
	public function select( $select )
	{
		if($this->_asApp){
			$dao = $this->_daoClass;
			$select = array_flip( $dao::toSys( array_flip($select) ) );
		}
		$this->_select = $select;
		return $this;
	}
	
	/**
	 * @return array|string
	 */
	public function selectToString()
	{
		if( is_array($this->_select) ){
			//add class_id and id to select if not yet selected
			return implode( ',', array_merge( $this->_select, array('id', 'class_id') ) );
		}
		else{
			return '*';
		}
	}
	
	/**
	 * @return string
	 */
	public function __toString()
	{
		$filter = '';
		if( $this->_where ){
			$i=0;
			foreach($this->_where as $where){
				if($i>0){
					$Awhere[] = $where[1] .' '. $where[0];
				}
				else{
					$Awhere[] = $where[0];
				}
				$i++;
			}
			$Swhere = implode(' ', $Awhere);
			$filter .= ' ' . $Swhere;
		}
		else{
			$filter .= ' 1=1';
		}
		
		if( $this->_extra ){
			$filter .= ' ' . $this->_extra;
		}
		
		if($this->_paginationIsActive == true){
			if( $this->_sortBy ){
				$Sorder = ' ORDER BY ' . $this->_sortBy . ' ' . $this->_sortOrder;
				$filter .= ' ' . $Sorder;
			}
			if( $this->_limit ){
				$filter .= ' LIMIT ' . $this->_limit;
			}
			if( $this->_offset ){
				$filter .= ' OFFSET ' . $this->_offset;
			}
		}
		
		return $filter;
	}//End of method
	
}//End of class
