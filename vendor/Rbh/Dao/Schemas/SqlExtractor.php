<?php
/**
 * $Id: Pg.php 822 2013-10-11 20:14:18Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg.php $
 * $LastChangedDate: 2013-10-11 22:14:18 +0200 (ven., 11 oct. 2013) $
 * $LastChangedBy: olivierc $
 * $Rev: 822 $
 */

namespace Rbh\Dao\Schemas;

class SqlExtractor
{
	public $resultPath = null;

	public function __construct($resultPath)
	{
		$this->resultPath = $resultPath;
	} //End of method

	/**
	 * Return a list of dao class definition files.
	 *
	 * @param string $basePath
	 * @return array
	 */
	private static function _parseDirectoryTree($basePath)
	{
		$files = array($basePath . '/Dao/Pg/DaoList.php', $basePath . '/Dao/Pg/Loader.php');
		$directoryIt = new \RecursiveIteratorIterator( new \RecursiveDirectoryIterator( $basePath ) );
		foreach($directoryIt as $file){
			if( substr($file->getFilename(), -7) == 'Dao.php' ){
				$files[] = $file->getRealPath();
			}
		}
		return $files;
	} //End of method


	/**
	 *
	 */
	private static function _parseSql(\SplFileObject $file, $openFlag = 'SQL_SCRIPT>>')
	{
		$lines = array();
		$file->rewind();

		$lines[] = '-- ' .str_repeat('=', 70) . "\n";
		$lines[] = '-- From file ' . $file->getFilename() . "\n";
		$lines[] = '-- ' .str_repeat('=', 70) . "\n";

		while( strstr($file->current(), $openFlag) === false && !$file->eof() ){
			$file->next();
		}
		while( strstr($file->current(), '<<*/') === false  && !$file->eof() ){
			$lines[] = $file->current();
			$file->next();
		}
		/*Suppress flag from the lines list*/
		unset( $lines[3] );

		return $lines;
	} //End of method


	/**
	 *
	 */
	private static function _compile($fileName, array $content)
	{
		/*
		 $oriFileName = $fileName;
		 while( is_file($fileName) ){
			$fileName = $oriFileName . '_' . $i++;
			}
			*/

		$resultFile = new \SplFileObject($fileName, 'w+');
		foreach($content as $sql){
			$resultFile->fwrite( implode("", $sql) );
		}
	} //End of method


	/** SQL_SCRIPT>>
	 <<*/

	/** SQL_INSERT>>
	 <<*/

	/** SQL_ALTER>>
	 <<*/

	/** SQL_FKEY>>
	 <<*/

	/** SQL_TRIGGER>>
	 <<*/

	/** SQL_PROC>>
	 <<*/
	
	/** SQL_VIEW>>
	 <<*/

	/** SQL_DROP>>
	 <<*/
	/**
	 *
	 * @param $SPLfile
	 * @return void
	 */
	public function parse($path)
	{
		$create 	= array();
		$alter 		= array();
		$fkey 		= array();
		$trigger 	= array();
		$proc 	    = array();
		$view 		= array();
		$insert 	= array();
		$drop 		= array();

		$resultPath = $this->resultPath;

		foreach( self::_parseDirectoryTree($path) as $path ){
			$file = new \SplFileObject($path);
			$create[] 	= self::_parseSql($file, 'SQL_SCRIPT>>');
			$alter[] 	= self::_parseSql($file, 'SQL_ALTER>>');
			$fkey[] 	= self::_parseSql($file, 'SQL_FKEY>>');
			$trigger[] 	= self::_parseSql($file, 'SQL_TRIGGER>>');
			$proc[] 	= self::_parseSql($file, 'SQL_PROC>>');
			$view[] 	= self::_parseSql($file, 'SQL_VIEW>>');
			$insert[] 	= self::_parseSql($file, 'SQL_INSERT>>');
			$drop[] 	= self::_parseSql($file, 'SQL_DROP>>');
		}
		
		$seqences = $this->_parseCreateSequenceCommands($create);
		$create = $this->_parseCreateSqlCommands($create);

		self::_compile($resultPath . '/create.sql', $create );
		self::_compile($resultPath . '/sequence.sql', $seqences );
		self::_compile($resultPath . '/alter.sql', $alter);
		self::_compile($resultPath . '/foreignKey.sql', $fkey);
		self::_compile($resultPath . '/proc.sql', $proc);
		self::_compile($resultPath . '/trigger.sql', $trigger);
		self::_compile($resultPath . '/view.sql', $view);
		self::_compile($resultPath . '/insert.sql', $insert);
		self::_compile($resultPath . '/drop.sql', $drop);
	} //End of method
	
	/**
	 * 
	 * @param array	$create
	 * @param boolean	$v	If true, verbose mode
	 * @return array
	 */
	public function _parseCreateSqlCommands($create, $v=false)
	{
		//var_dump($create);
		$createIter = new \ArrayIterator($create);
		$tableIndex = array();
		$bloc = array();
		$lines = array();
		$blocs = array();

		//extract inherits rules:
		while( $createIter->valid() )
		{
			$table = '';
			$parentTable = '';
			$current = $createIter->current();
			
			foreach($current as $line){
				if(!$table){
					//current line begin a create table command
					$ok = preg_match('/(CREATE TABLE){1} ([a-z_]*){1}/', $line, $match);
					if($ok){
						$table = trim($match[2]);
						if($v) echo "Table $table" . CRLF;
					}
				}
				if($table){
					$lines[] = $line;
					
					$ok = preg_match('/(INHERITS){1} (\([a-z_]*\)){1}/', $line, $match);
					if($ok){
						$parentTable = trim( $match[2], '() ' );
						if($v) echo "Parent table $parentTable" . CRLF;
					}
					//if contain ;, and of CREATE TABLE command
					//write result in index
					//ATTENTION @todo: ';' symbole not must be found on a comment line
					$ok = preg_match('/;/', $line);
					if($ok){
						if($table && $parentTable){
							$tableIndex[$table] = $parentTable;
						}
						if($table){
							$tableIndex[$table] = null;
						}
						$bloc['table'] = $table;
						$bloc['ptable'] = $parentTable;
						$bloc['sql'] = $lines;
						$blocs[] = $bloc;
						$table = '';
						$parentTable = '';
						$bloc = array();
						$lines = array();
					}
				}
			}
			$createIter->next();
		} //End of while

		//var_dump($blocs);die;
		
		$allOk = false;
		$outIndex = array();
		$create = array();
		
		/* ALTER TABLE INHERIT is not same that CREATE TABLE ... INHERITS ...*/
		//Reordonne la creation des table dans l'ordre des heritages
		while( $allOk == false ){
			foreach($blocs as $bloc){
				$table = $bloc['table'];
				$parentTable = $bloc['ptable'];
				$sql = $bloc['sql'];
					
				if( isset($outIndex[$table]) ){
					continue;
				}
					
				if($parentTable){ //a parent table is found, check if is in pile
					if($v) echo "A parent table $parentTable is found".CRLF;
					if( isset($outIndex[$parentTable]) ){ //move creation to out pile
						if($v) echo "Add table $table to out".CRLF;
						$create[] = $sql;
						$outIndex[$table] = $parentTable;
					}
					else{ //else try later
						if($v) echo "Parent table $parentTable is not found in out".CRLF;
					}
				}
				else{ //if no parent, move to outPile
					$create[] = $sql;
					$outIndex[$table] = 'none';
				}
			} //End foreach

			if(count($create) == count($blocs)){
				$allOk = true;
			}
		} //End while
		
		return $create;
	} //End of method
	
	/**
	 * 
	 * @param array	$in
	 * @param boolean	$v	If true, verbose mode
	 * @return array
	 */
	public function _parseCreateSequenceCommands($in, $v=false)
	{
		$Iter = new \ArrayIterator($in);
		$lines = array();
		$out = array();

		//extract inherits rules:
		while( $Iter->valid() )
		{
			$name = '';
			$current = $Iter->current();
			
			foreach($current as $line){
				if(!$name){
					//current line begin a create table command
					$ok = preg_match('/(CREATE SEQUENCE){1} ([a-z_]*){1}/', $line, $match);
					if($ok){
						$name = trim($match[2]);
						if($v) echo "Name $name" . CRLF;
					}
				}
				if($name){
					$lines[] = $line;
					//ATTENTION @todo: ';' symbole not must be found on a comment line
					$ok = preg_match('/;/', $line);
					if($ok){
						$out[] = $lines;
						$lines = array();
						$name = '';
					}
				}
			}
			$Iter->next();
		} //End of while

		return $out;
	} //End of method

} //End of class
