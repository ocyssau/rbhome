 CREATE SEQUENCE anyobject_id_seq
 START WITH 100
 INCREMENT BY 1
 NO MAXVALUE
 NO MINVALUE
 CACHE 1;
 CREATE TABLE index (
 uid uuid NOT NULL,
 dao varchar(256) DEFAULT NULL,
 connexion varchar(256) DEFAULT NULL
 );
CREATE TABLE people_user_preference(
	owner uuid, 
	class_id integer, 
	preferences text, 
	enable boolean
);
 CREATE TABLE anyobject (
 id bigint NOT NULL,
 uid uuid NOT NULL,
 cid integer NOT NULL,
 name varchar(256),
 label varchar(256),
 owner bigint NOT NULL,
 parent bigint,
 path ltree
 );
 CREATE TABLE anyobject_links (
 	luid uuid NOT NULL,
 	lparent bigint NOT NULL,
 	lchild bigint NOT NULL,
 	lname varchar(256),
 	ldata text NULL,
 	lindex integer NULL,
 	level int DEFAULT 0,
 	lpath ltree,
 	is_leaf boolean NOT NULL DEFAULT false
 );
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
 CREATE TABLE build_surface(
 description varchar(64),
 surface real,
 volume real,
 material integer,
 thickness real,
 density real,
 weight real,
 perimeter real,
 thlambda real,
 azimut real,
 inclinaison real,
 layoffset integer
 ) INHERITS (anyobject);
 CREATE TABLE project(
 description varchar(255)
 ) INHERITS (anyobject);
 CREATE TABLE people_user(
 is_active boolean,
 last_login integer,
 login varchar(255) NOT NULL,
 firstname varchar(255),
 lastname varchar(255),
 password varchar(255),
 mail varchar(255),
 wildspace varchar(255)
 ) INHERITS (anyobject);
 CREATE TABLE people_group(
 is_active boolean,
 description varchar(255)
 ) INHERITS (anyobject);
 CREATE TABLE usedmaterial(
 volume real,
 weight real,
 length real,
 quantity real,
 surface real,
 inputunit character(2),
 cid integer
 ) INHERITS (anyobject_links);
 CREATE TABLE material(
 density real,
 volume real,
 weight real,
 thlambda real,
 thSpecificHeat real,
 material bigint,
 inputunit character(2)
 ) INHERITS (anyobject);
 CREATE TABLE materialrecipe(
 ) INHERITS (material);
 CREATE TABLE org_ou(
 description varchar(255)
 ) INHERITS (anyobject);
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (111, 'Rbplm_People_User_Preference', 'people_user_preference');
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (110, 'Rbh\People\User', 'people_user');
 INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, is_active, last_login, login, firstname, lastname, password, mail, wildspace)
 VALUES (10, '99999999-9999-9999-9999-00000000abcd', 110, 'admin', 'admin', 91, 10, 'Rbh.Users.admin', true, NULL, 'admin', 'rbh administrator', 'administrator', NULL, NULL, NULL);
 INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, is_active, last_login, login, firstname, lastname, password, mail, wildspace)
 VALUES (11, '99999999-9999-9999-9999-99999999ffff', 110, 'anybody', 'anybody', 91, 10, 'Rbh.Users.anybody', true, NULL, 'anybody', 'anonymous user', 'anybody', NULL, NULL, NULL);
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (120, 'Rbh\People\Group', 'people_group');
 INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, is_active, description)
 VALUES (15, '99999999-9999-9999-9999-00000000cdef', 120, 'admins', 'admins', 90, 10, 'Rbh.Groups.admins', true, 'rbh administrators');
 INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, is_active, description)
 VALUES (16, '99999999-9999-9999-9999-00000000ffff', 120, 'anonymous', 'anonymous', 90, 10, 'Rbh.Groups.anonymous', true, 'anonymous users');
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (5010, 'Rbh\Material\UsedMaterial', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5011, 'Rbh\Material\UsedBulk', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5013, 'Rbh\Material\UsedDiscrete', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5014, 'Rbh\Material\UsedLineic', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5015, 'Rbh\Material\UsedSurfacic', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5060, 'Rbh\Material\UsedRecipe', 'usedmaterial');
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (5000, 'Rbh\Material\Material', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5001, 'Rbh\Material\Bulk', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5002, 'Rbh\Material\Composite', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5003, 'Rbh\Material\Discrete', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5004, 'Rbh\Material\Lineic', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5005, 'Rbh\Material\Surfacic', 'material');
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (5050, 'Rbh\Material\Recipe', 'materialrecipe');
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (20, 'Rbh\Org\Unit', 'org_ou');
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description)
	VALUES (89, 'fd5666ea-7be0-f414-1211-ee692b14de91', 20, 'Rbh', 'Rbh', NULL, 10, 'Rbh', NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description) 
	VALUES (90, '2767cbdd-1089-5b62-5c4c-7e6ebd2aa71f', 20, 'Groups', 'Groups', 89, 10, 'Rbh.Groups', NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description) 
	VALUES (91, 'd6bd8247-fc5d-c699-9b5c-23fc204d6313', 20, 'Users', 'Users', 89, 10, 'Rbh.Users', NULL);
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (2000, 'Rbh\Build\Surface', 'build_surface');
 INSERT INTO classes (id, name, tablename) VALUES (2001, 'Rbh\Build\SurfaceStack', 'build_surface');
 INSERT INTO classes (id, name, tablename) VALUES (2002, 'Rbh\Build\SurfaceComplex', 'build_surface');
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (1020, 'Rbh\Project\Project', 'project');
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
ALTER TABLE people_user_preference ADD PRIMARY KEY (owner);
ALTER TABLE people_user_preference ALTER COLUMN class_id SET DEFAULT 111;
ALTER TABLE people_user_preference ALTER COLUMN enable SET DEFAULT true;
CREATE INDEX INDEX_people_user_preference_class_id ON people_user_preference USING btree (class_id);
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
 ALTER TABLE people_user ADD PRIMARY KEY (id);
 ALTER TABLE people_user ADD UNIQUE (login);
 ALTER TABLE people_user ADD UNIQUE (uid);
 ALTER TABLE people_user ADD UNIQUE (path);
 ALTER TABLE people_user ALTER COLUMN cid SET DEFAULT 110;
 CREATE INDEX INDEX_people_user_login ON people_user USING btree (login);
 CREATE INDEX INDEX_people_user_uid ON people_user USING btree (uid);
 CREATE INDEX INDEX_people_user_name ON people_user USING btree (name);
 CREATE INDEX INDEX_people_user_label ON people_user USING btree (label);
 CREATE INDEX INDEX_people_user_class_id ON people_user USING btree (cid);
 CREATE INDEX INDEX_people_user_path ON people_user USING btree (path);
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
 ALTER TABLE people_group ADD PRIMARY KEY (id);
ALTER TABLE people_group ADD UNIQUE (uid);
ALTER TABLE people_group ADD UNIQUE (path);
ALTER TABLE people_group ALTER COLUMN cid SET DEFAULT 120;
CREATE INDEX INDEX_people_group_uid ON people_group USING btree (uid);
CREATE INDEX INDEX_people_group_name ON people_group USING btree (name);
CREATE INDEX INDEX_people_group_label ON people_group USING btree (label);
CREATE INDEX INDEX_people_group_class_id ON people_user USING btree (cid);
CREATE INDEX INDEX_people_group_path ON people_group USING btree (path);
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
ALTER TABLE usedmaterial ADD UNIQUE (lparent, lchild);
ALTER TABLE usedmaterial ALTER COLUMN cid SET DEFAULT 5010;
CREATE INDEX INDEX_usedmaterial_parent ON usedmaterial USING btree (lparent);
CREATE INDEX INDEX_usedmaterial_child ON usedmaterial USING btree (lchild);
ALTER TABLE usedmaterial ADD PRIMARY KEY (luid);
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
 ALTER TABLE material ADD PRIMARY KEY (id);
 ALTER TABLE material ALTER COLUMN cid SET DEFAULT 5000;
 CREATE INDEX INDEX_material_owner ON material USING btree (owner);
 CREATE INDEX INDEX_material_uid ON material USING btree (uid);
 CREATE INDEX INDEX_material_name ON material USING btree (name);
 CREATE INDEX INDEX_material_label ON material USING btree (label);
 CREATE INDEX INDEX_material_path ON material USING btree (path);
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
 ALTER TABLE materialrecipe ADD PRIMARY KEY (id);
 ALTER TABLE materialrecipe ALTER COLUMN cid SET DEFAULT 5050;
 CREATE INDEX INDEX_materialrecipe_owner ON materialrecipe USING btree (owner);
 CREATE INDEX INDEX_materialrecipe_uid ON materialrecipe USING btree (uid);
 CREATE INDEX INDEX_materialrecipe_name ON materialrecipe USING btree (name);
 CREATE INDEX INDEX_materialrecipe_label ON materialrecipe USING btree (label);
 CREATE INDEX INDEX_materialrecipe_path ON materialrecipe USING btree (path);
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
 ALTER TABLE org_ou ADD PRIMARY KEY (id);
 ALTER TABLE org_ou ADD UNIQUE (uid);
 ALTER TABLE org_ou ADD UNIQUE (path);
 ALTER TABLE org_ou ALTER COLUMN cid SET DEFAULT 20;
 CREATE INDEX INDEX_org_ou_owner ON org_ou USING btree (owner);
 CREATE INDEX INDEX_org_ou_uid ON org_ou USING btree (uid);
 CREATE INDEX INDEX_org_ou_name ON org_ou USING btree (name);
 CREATE INDEX INDEX_org_ou_label ON org_ou USING btree (label);
 CREATE INDEX INDEX_org_ou_path ON org_ou USING btree (path);
-- ======================================================================
-- From file Dao.php
-- ======================================================================
 -------------------------------------------------------------------------------------------------------------------
-- anyobject
--
ALTER TABLE anyobject ALTER COLUMN id SET DEFAULT nextval('anyobject_id_seq'::regclass);
ALTER TABLE anyobject ADD PRIMARY KEY (id);
ALTER TABLE anyobject ADD UNIQUE (path);
ALTER TABLE anyobject ADD FOREIGN KEY (cid) REFERENCES classes (id);

CREATE INDEX INDEX_anyobject_name ON anyobject USING btree (name);
CREATE INDEX INDEX_anyobject_label ON anyobject USING btree (label);
CREATE INDEX INDEX_anyobject_uid ON anyobject USING btree (uid);
CREATE INDEX INDEX_anyobject_class_id ON anyobject USING btree (cid);
CREATE UNIQUE INDEX INDEX_anyobject_path_btree ON anyobject USING btree(path);
CREATE INDEX INDEX_anyobject_path_gist ON anyobject USING gist(path);

-------------------------------------------------------------------------------------------------------------------
-- anyobject_links
--
ALTER TABLE anyobject_links ADD UNIQUE (lparent, lchild);
CREATE INDEX INDEX_anyobject_links_parent ON anyobject_links USING btree (lparent);
CREATE INDEX INDEX_anyobject_links_child ON anyobject_links USING btree (lchild);
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
 ALTER TABLE build_surface ADD PRIMARY KEY (id);
 ALTER TABLE build_surface ALTER COLUMN cid SET DEFAULT 2000;
 CREATE INDEX INDEX_build_surface_owner ON build_surface USING btree (owner);
 CREATE INDEX INDEX_build_surface_uid ON build_surface USING btree (uid);
CREATE INDEX INDEX_build_surface_name ON build_surface USING btree (name);
CREATE INDEX INDEX_build_surface_label ON build_surface USING btree (label);
CREATE INDEX INDEX_build_surface_path ON build_surface USING btree (path);
CREATE INDEX INDEX_build_surface_material ON build_surface USING btree (material);
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
 ALTER TABLE project ADD PRIMARY KEY (id);
 ALTER TABLE project ADD UNIQUE (uid);
 ALTER TABLE project ADD UNIQUE (path);
 ALTER TABLE project ALTER COLUMN cid SET DEFAULT 1020;
 CREATE INDEX INDEX_project_owner ON project USING btree (owner);
 CREATE INDEX INDEX_project_uid ON project USING btree (uid);
 CREATE INDEX INDEX_project_name ON project USING btree (name);
 CREATE INDEX INDEX_project_label ON project USING btree (label);
 CREATE INDEX INDEX_project_path ON project USING btree (path);
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
ALTER TABLE people_user_preference ADD FOREIGN KEY (owner) REFERENCES people_user (uid) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
 ALTER TABLE material ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
 ALTER TABLE materialrecipe ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
ALTER TABLE org_ou ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
ALTER TABLE project ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
-- ======================================================================
-- From file Dao.php
-- ======================================================================
 CREATE OR REPLACE VIEW view_anyobject_parentlinks AS
SELECT
l.luid,l.lname,l.lindex,l.ldata,
p.id AS p_id, p.uid AS p_uid, p.name AS p_name, p.cid AS p_cid,
c.id AS c_id, c.uid AS c_uid, c.name AS c_name, c.cid AS c_cid
FROM anyobject_links AS l
LEFT OUTER JOIN anyobject AS c ON l.lchild  = c.id
LEFT OUTER JOIN anyobject AS p ON l.lparent = p.id
ORDER BY p_id ASC, lindex ASC;

-- TODO : Comment concatener les ltree

-- All graph from Root node :
CREATE OR REPLACE VIEW view_allgraphe_001 AS
WITH RECURSIVE graphe(lparent, lchild, lname, lindex, ldata, level, path) AS (
	SELECT l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, (l.lparent||'.'||l.lchild)::text::ltree
	FROM anyobject_links l
	UNION ALL
	SELECT l.lparent, l.lchild, l.lname, l.lindex, l.ldata, g.level + 1, (path::text || '.'|| l.lchild)::text::ltree
	FROM anyobject_links l, graphe g
	WHERE g.lchild = l.lparent
)
SELECT
l.path AS path, l.level AS level, l.lname AS lname, l.lindex, l.ldata AS ldata,
p.id AS p_id, p.uid AS p_uid, p.name AS p_name, p.cid AS p_cid,
c.id AS c_id, c.uid AS c_uid, c.name AS c_name, c.cid AS c_cid
FROM graphe AS l
LEFT OUTER JOIN anyobject AS p ON l.lparent = p.id
LEFT OUTER JOIN anyobject AS c ON l.lchild = c.id
WHERE l.path <@ '89'
ORDER BY path ASC, lindex ASC;
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_get_object_path
--
CREATE OR REPLACE FUNCTION rb_get_object_path(param_node_id bigint) RETURNS ltree AS
$$
SELECT
CASE WHEN tobj.parent IS NULL
THEN
tobj.id::text::ltree
ELSE
rb_get_object_path(tobj.parent)  || tobj.id::text
END
FROM anyobject As tobj
WHERE tobj.id=$1;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_set_object_path
--
CREATE OR REPLACE FUNCTION rb_set_object_path() RETURNS VOID AS
$$
UPDATE
anyobject_links As tobj
SET
lpath = rb_get_object_path(tobj.parent) || tobj.id::text,
level = nlevel(path);
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_replace_links
--
CREATE OR REPLACE FUNCTION rb_replace_links(in_uuid uuid,in_parent bigint,in_child bigint,in_data TEXT,in_lindex INT,in_name TEXT) RETURNS VOID AS
$$
BEGIN
LOOP
UPDATE anyobject_links SET ldata = in_data, lindex = in_lindex, lname = in_name WHERE lparent = in_parent AND lchild = in_child;
IF found THEN
RETURN;
END IF;
BEGIN
--loc_lui = uuid();
INSERT INTO anyobject_links (luid,lparent,lchild,ldata,lindex,lname) VALUES (in_uuid,in_parent,in_child,in_data,in_lindex,in_name);
RETURN;
EXCEPTION WHEN unique_violation THEN
-- do nothing
END;
END LOOP;
END;
$$
LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Proc rb_trig_anyobject_update_path
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_update_path() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'UPDATE' THEN
IF (OLD.lpath IS NULL) THEN
UPDATE anyobject_links AS lt SET
lpath = rb_get_object_path(anyobject.id),
level = nlevel(NEW.lpath)
WHERE
lt.lparent = NEW.lparent AND lt.lchild = NEW.lchild
OR
lt.parent = NEW.id
;
END IF;
IF (OLD.parent != NEW.parent  OR  NEW.id != OLD.id) THEN
-- update all nodes that are children of this one including this one
UPDATE anyobject SET
path = rb_get_object_path(anyobject.id),
depth = nlevel(NEW.path)
WHERE
OLD.path  @> anyobject.path;
END IF;

ELSIF TG_OP = 'INSERT' THEN
UPDATE anyobject SET
path = rb_get_object_path(NEW.id),
depth = nlevel(NEW.path)
WHERE
anyobject.id = NEW.id;
END IF;

RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_anyobject_delete
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_delete() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'DELETE' THEN
DELETE FROM anyobject_links WHERE parent = OLD.id OR child = OLD.id;
END IF;
RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
 CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE
 ON people_user FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_people_user AFTER DELETE
 ON people_user FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
 CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE
ON people_group FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE
ON people_group FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();

-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
 CREATE TRIGGER trig01_material AFTER INSERT OR UPDATE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_material AFTER DELETE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
 CREATE TRIGGER trig01_materialrecipe AFTER INSERT OR UPDATE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_materialrecipe AFTER DELETE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
ON org_ou FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
CREATE TRIGGER trig02_org_ou AFTER DELETE
ON org_ou FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig01_anyobject_update_path
--
DROP TRIGGER IF EXISTS rb_trig01_anyobject_update_path ON anyobject;
CREATE TRIGGER rb_trig01_anyobject_update_path AFTER INSERT OR UPDATE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig02_anyobject_delete
--
DROP TRIGGER IF EXISTS rb_trig02_anyobject_delete ON anyobject;
CREATE TRIGGER rb_trig02_anyobject_delete AFTER DELETE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
 CREATE TRIGGER trig01_build_surface AFTER INSERT OR UPDATE
ON build_surface FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

CREATE TRIGGER trig02_build_surface AFTER DELETE
ON build_surface FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
 CREATE TRIGGER trig01_project AFTER INSERT OR UPDATE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_project AFTER DELETE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
