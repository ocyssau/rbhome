-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
ALTER TABLE people_user_preference ADD PRIMARY KEY (owner);
ALTER TABLE people_user_preference ALTER COLUMN class_id SET DEFAULT 111;
ALTER TABLE people_user_preference ALTER COLUMN enable SET DEFAULT true;
CREATE INDEX INDEX_people_user_preference_class_id ON people_user_preference USING btree (class_id);
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
 ALTER TABLE people_user ADD PRIMARY KEY (id);
 ALTER TABLE people_user ADD UNIQUE (login);
 ALTER TABLE people_user ADD UNIQUE (uid);
 ALTER TABLE people_user ADD UNIQUE (path);
 ALTER TABLE people_user ALTER COLUMN cid SET DEFAULT 110;
 CREATE INDEX INDEX_people_user_login ON people_user USING btree (login);
 CREATE INDEX INDEX_people_user_uid ON people_user USING btree (uid);
 CREATE INDEX INDEX_people_user_name ON people_user USING btree (name);
 CREATE INDEX INDEX_people_user_label ON people_user USING btree (label);
 CREATE INDEX INDEX_people_user_class_id ON people_user USING btree (cid);
 CREATE INDEX INDEX_people_user_path ON people_user USING btree (path);
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
 ALTER TABLE people_group ADD PRIMARY KEY (id);
ALTER TABLE people_group ADD UNIQUE (uid);
ALTER TABLE people_group ADD UNIQUE (path);
ALTER TABLE people_group ALTER COLUMN cid SET DEFAULT 120;
CREATE INDEX INDEX_people_group_uid ON people_group USING btree (uid);
CREATE INDEX INDEX_people_group_name ON people_group USING btree (name);
CREATE INDEX INDEX_people_group_label ON people_group USING btree (label);
CREATE INDEX INDEX_people_group_class_id ON people_user USING btree (cid);
CREATE INDEX INDEX_people_group_path ON people_group USING btree (path);
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
ALTER TABLE usedmaterial ADD UNIQUE (lparent, lchild);
ALTER TABLE usedmaterial ALTER COLUMN cid SET DEFAULT 5010;
CREATE INDEX INDEX_usedmaterial_parent ON usedmaterial USING btree (lparent);
CREATE INDEX INDEX_usedmaterial_child ON usedmaterial USING btree (lchild);
ALTER TABLE usedmaterial ADD PRIMARY KEY (luid);
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
 ALTER TABLE material ADD PRIMARY KEY (id);
 ALTER TABLE material ALTER COLUMN cid SET DEFAULT 5000;
 CREATE INDEX INDEX_material_owner ON material USING btree (owner);
 CREATE INDEX INDEX_material_uid ON material USING btree (uid);
 CREATE INDEX INDEX_material_name ON material USING btree (name);
 CREATE INDEX INDEX_material_label ON material USING btree (label);
 CREATE INDEX INDEX_material_path ON material USING btree (path);
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
 ALTER TABLE materialrecipe ADD PRIMARY KEY (id);
 ALTER TABLE materialrecipe ALTER COLUMN cid SET DEFAULT 5050;
 CREATE INDEX INDEX_materialrecipe_owner ON materialrecipe USING btree (owner);
 CREATE INDEX INDEX_materialrecipe_uid ON materialrecipe USING btree (uid);
 CREATE INDEX INDEX_materialrecipe_name ON materialrecipe USING btree (name);
 CREATE INDEX INDEX_materialrecipe_label ON materialrecipe USING btree (label);
 CREATE INDEX INDEX_materialrecipe_path ON materialrecipe USING btree (path);
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
 ALTER TABLE org_ou ADD PRIMARY KEY (id);
 ALTER TABLE org_ou ADD UNIQUE (uid);
 ALTER TABLE org_ou ADD UNIQUE (path);
 ALTER TABLE org_ou ALTER COLUMN cid SET DEFAULT 20;
 CREATE INDEX INDEX_org_ou_owner ON org_ou USING btree (owner);
 CREATE INDEX INDEX_org_ou_uid ON org_ou USING btree (uid);
 CREATE INDEX INDEX_org_ou_name ON org_ou USING btree (name);
 CREATE INDEX INDEX_org_ou_label ON org_ou USING btree (label);
 CREATE INDEX INDEX_org_ou_path ON org_ou USING btree (path);
-- ======================================================================
-- From file Dao.php
-- ======================================================================
 -------------------------------------------------------------------------------------------------------------------
-- anyobject
--
ALTER TABLE anyobject ALTER COLUMN id SET DEFAULT nextval('anyobject_id_seq'::regclass);
ALTER TABLE anyobject ADD PRIMARY KEY (id);
ALTER TABLE anyobject ADD UNIQUE (path);
ALTER TABLE anyobject ADD FOREIGN KEY (cid) REFERENCES classes (id);

CREATE INDEX INDEX_anyobject_name ON anyobject USING btree (name);
CREATE INDEX INDEX_anyobject_label ON anyobject USING btree (label);
CREATE INDEX INDEX_anyobject_uid ON anyobject USING btree (uid);
CREATE INDEX INDEX_anyobject_class_id ON anyobject USING btree (cid);
CREATE UNIQUE INDEX INDEX_anyobject_path_btree ON anyobject USING btree(path);
CREATE INDEX INDEX_anyobject_path_gist ON anyobject USING gist(path);

-------------------------------------------------------------------------------------------------------------------
-- anyobject_links
--
ALTER TABLE anyobject_links ADD UNIQUE (lparent, lchild);
CREATE INDEX INDEX_anyobject_links_parent ON anyobject_links USING btree (lparent);
CREATE INDEX INDEX_anyobject_links_child ON anyobject_links USING btree (lchild);
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
 ALTER TABLE build_surface ADD PRIMARY KEY (id);
 ALTER TABLE build_surface ALTER COLUMN cid SET DEFAULT 2000;
 CREATE INDEX INDEX_build_surface_owner ON build_surface USING btree (owner);
 CREATE INDEX INDEX_build_surface_uid ON build_surface USING btree (uid);
CREATE INDEX INDEX_build_surface_name ON build_surface USING btree (name);
CREATE INDEX INDEX_build_surface_label ON build_surface USING btree (label);
CREATE INDEX INDEX_build_surface_path ON build_surface USING btree (path);
CREATE INDEX INDEX_build_surface_material ON build_surface USING btree (material);
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
 ALTER TABLE project ADD PRIMARY KEY (id);
 ALTER TABLE project ADD UNIQUE (uid);
 ALTER TABLE project ADD UNIQUE (path);
 ALTER TABLE project ALTER COLUMN cid SET DEFAULT 1020;
 CREATE INDEX INDEX_project_owner ON project USING btree (owner);
 CREATE INDEX INDEX_project_uid ON project USING btree (uid);
 CREATE INDEX INDEX_project_name ON project USING btree (name);
 CREATE INDEX INDEX_project_label ON project USING btree (label);
 CREATE INDEX INDEX_project_path ON project USING btree (path);
