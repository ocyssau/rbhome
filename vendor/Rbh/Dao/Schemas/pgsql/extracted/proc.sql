-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_get_object_path
--
CREATE OR REPLACE FUNCTION rb_get_object_path(param_node_id bigint) RETURNS ltree AS
$$
SELECT
CASE WHEN tobj.parent IS NULL
THEN
tobj.id::text::ltree
ELSE
rb_get_object_path(tobj.parent)  || tobj.id::text
END
FROM anyobject As tobj
WHERE tobj.id=$1;
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_set_object_path
--
CREATE OR REPLACE FUNCTION rb_set_object_path() RETURNS VOID AS
$$
UPDATE
anyobject_links As tobj
SET
lpath = rb_get_object_path(tobj.parent) || tobj.id::text,
level = nlevel(path);
$$
LANGUAGE sql;

-------------------------------------------------------------------------------------------------------------------
-- Procedure rb_replace_links
--
CREATE OR REPLACE FUNCTION rb_replace_links(in_uuid uuid,in_parent bigint,in_child bigint,in_data TEXT,in_lindex INT,in_name TEXT) RETURNS VOID AS
$$
BEGIN
LOOP
UPDATE anyobject_links SET ldata = in_data, lindex = in_lindex, lname = in_name WHERE lparent = in_parent AND lchild = in_child;
IF found THEN
RETURN;
END IF;
BEGIN
--loc_lui = uuid();
INSERT INTO anyobject_links (luid,lparent,lchild,ldata,lindex,lname) VALUES (in_uuid,in_parent,in_child,in_data,in_lindex,in_name);
RETURN;
EXCEPTION WHEN unique_violation THEN
-- do nothing
END;
END LOOP;
END;
$$
LANGUAGE plpgsql;

-------------------------------------------------------------------------------------------------------------------
-- Proc rb_trig_anyobject_update_path
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_update_path() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'UPDATE' THEN
IF (OLD.lpath IS NULL) THEN
UPDATE anyobject_links AS lt SET
lpath = rb_get_object_path(anyobject.id),
level = nlevel(NEW.lpath)
WHERE
lt.lparent = NEW.lparent AND lt.lchild = NEW.lchild
OR
lt.parent = NEW.id
;
END IF;
IF (OLD.parent != NEW.parent  OR  NEW.id != OLD.id) THEN
-- update all nodes that are children of this one including this one
UPDATE anyobject SET
path = rb_get_object_path(anyobject.id),
depth = nlevel(NEW.path)
WHERE
OLD.path  @> anyobject.path;
END IF;

ELSIF TG_OP = 'INSERT' THEN
UPDATE anyobject SET
path = rb_get_object_path(NEW.id),
depth = nlevel(NEW.path)
WHERE
anyobject.id = NEW.id;
END IF;

RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig_anyobject_delete
--
-- Trigger to maintain node when parent or item id changes or record is added
-- We revised this since the first version because the order of the short-circuiting is not predictable so TG_OP needs a nested IF.
-- Also some other minor simplifications
--
CREATE OR REPLACE FUNCTION rb_trig_anyobject_delete() RETURNS trigger AS
$$
BEGIN
IF TG_OP = 'DELETE' THEN
DELETE FROM anyobject_links WHERE parent = OLD.id OR child = OLD.id;
END IF;
RETURN NEW;
END
$$
LANGUAGE 'plpgsql' VOLATILE;

-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
