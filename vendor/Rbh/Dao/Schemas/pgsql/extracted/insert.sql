-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (111, 'Rbplm_People_User_Preference', 'people_user_preference');
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (110, 'Rbh\People\User', 'people_user');
 INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, is_active, last_login, login, firstname, lastname, password, mail, wildspace)
 VALUES (10, '99999999-9999-9999-9999-00000000abcd', 110, 'admin', 'admin', 91, 10, 'Rbh.Users.admin', true, NULL, 'admin', 'rbh administrator', 'administrator', NULL, NULL, NULL);
 INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, is_active, last_login, login, firstname, lastname, password, mail, wildspace)
 VALUES (11, '99999999-9999-9999-9999-99999999ffff', 110, 'anybody', 'anybody', 91, 10, 'Rbh.Users.anybody', true, NULL, 'anybody', 'anonymous user', 'anybody', NULL, NULL, NULL);
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (120, 'Rbh\People\Group', 'people_group');
 INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, is_active, description)
 VALUES (15, '99999999-9999-9999-9999-00000000cdef', 120, 'admins', 'admins', 90, 10, 'Rbh.Groups.admins', true, 'rbh administrators');
 INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, is_active, description)
 VALUES (16, '99999999-9999-9999-9999-00000000ffff', 120, 'anonymous', 'anonymous', 90, 10, 'Rbh.Groups.anonymous', true, 'anonymous users');
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (5010, 'Rbh\Material\UsedMaterial', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5011, 'Rbh\Material\UsedBulk', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5013, 'Rbh\Material\UsedDiscrete', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5014, 'Rbh\Material\UsedLineic', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5015, 'Rbh\Material\UsedSurfacic', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5060, 'Rbh\Material\UsedRecipe', 'usedmaterial');
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (5000, 'Rbh\Material\Material', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5001, 'Rbh\Material\Bulk', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5002, 'Rbh\Material\Composite', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5003, 'Rbh\Material\Discrete', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5004, 'Rbh\Material\Lineic', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5005, 'Rbh\Material\Surfacic', 'material');
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (5050, 'Rbh\Material\Recipe', 'materialrecipe');
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (20, 'Rbh\Org\Unit', 'org_ou');
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description)
	VALUES (89, 'fd5666ea-7be0-f414-1211-ee692b14de91', 20, 'Rbh', 'Rbh', NULL, 10, 'Rbh', NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description) 
	VALUES (90, '2767cbdd-1089-5b62-5c4c-7e6ebd2aa71f', 20, 'Groups', 'Groups', 89, 10, 'Rbh.Groups', NULL);
INSERT INTO org_ou (id, uid, cid, name, label, parent, owner, path, description) 
	VALUES (91, 'd6bd8247-fc5d-c699-9b5c-23fc204d6313', 20, 'Users', 'Users', 89, 10, 'Rbh.Users', NULL);
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
 INSERT INTO classes (id, name, tablename) VALUES (2000, 'Rbh\Build\Surface', 'build_surface');
 INSERT INTO classes (id, name, tablename) VALUES (2001, 'Rbh\Build\SurfaceStack', 'build_surface');
 INSERT INTO classes (id, name, tablename) VALUES (2002, 'Rbh\Build\SurfaceComplex', 'build_surface');
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
INSERT INTO classes (id, name, tablename) VALUES (1020, 'Rbh\Project\Project', 'project');
