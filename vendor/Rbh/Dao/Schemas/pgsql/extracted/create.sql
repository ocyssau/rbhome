 CREATE TABLE index (
 uid uuid NOT NULL,
 dao varchar(256) DEFAULT NULL,
 connexion varchar(256) DEFAULT NULL
 );
CREATE TABLE people_user_preference(
	owner uuid, 
	class_id integer, 
	preferences text, 
	enable boolean
);
 CREATE TABLE anyobject (
 id bigint NOT NULL,
 uid uuid NOT NULL,
 cid integer NOT NULL,
 name varchar(256),
 label varchar(256),
 owner bigint NOT NULL,
 parent bigint,
 path ltree
 );
 CREATE TABLE anyobject_links (
 	luid uuid NOT NULL,
 	lparent bigint NOT NULL,
 	lchild bigint NOT NULL,
 	lname varchar(256),
 	ldata text NULL,
 	lindex integer NULL,
 	level int DEFAULT 0,
 	lpath ltree,
 	is_leaf boolean NOT NULL DEFAULT false
 );
CREATE TABLE classes (
  id integer NOT NULL PRIMARY KEY,
  name VARCHAR(64) NOT NULL,
  tablename VARCHAR(64) NOT NULL
);
 CREATE TABLE build_surface(
 description varchar(64),
 surface real,
 volume real,
 material integer,
 thickness real,
 density real,
 weight real,
 perimeter real,
 thlambda real,
 azimut real,
 inclinaison real,
 layoffset integer
 ) INHERITS (anyobject);
 CREATE TABLE project(
 description varchar(255)
 ) INHERITS (anyobject);
 CREATE TABLE people_user(
 is_active boolean,
 last_login integer,
 login varchar(255) NOT NULL,
 firstname varchar(255),
 lastname varchar(255),
 password varchar(255),
 mail varchar(255),
 wildspace varchar(255)
 ) INHERITS (anyobject);
 CREATE TABLE people_group(
 is_active boolean,
 description varchar(255)
 ) INHERITS (anyobject);
 CREATE TABLE usedmaterial(
 volume real,
 weight real,
 length real,
 quantity real,
 surface real,
 inputunit character(2),
 cid integer
 ) INHERITS (anyobject_links);
 CREATE TABLE material(
 density real,
 volume real,
 weight real,
 thlambda real,
 thSpecificHeat real,
 material bigint,
 inputunit character(2)
 ) INHERITS (anyobject);
 CREATE TABLE materialrecipe(
 ) INHERITS (material);
 CREATE TABLE org_ou(
 description varchar(255)
 ) INHERITS (anyobject);
