-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
-- ======================================================================
-- From file Dao.php
-- ======================================================================
 CREATE OR REPLACE VIEW view_anyobject_parentlinks AS
SELECT
l.luid,l.lname,l.lindex,l.ldata,
p.id AS p_id, p.uid AS p_uid, p.name AS p_name, p.cid AS p_cid,
c.id AS c_id, c.uid AS c_uid, c.name AS c_name, c.cid AS c_cid
FROM anyobject_links AS l
LEFT OUTER JOIN anyobject AS c ON l.lchild  = c.id
LEFT OUTER JOIN anyobject AS p ON l.lparent = p.id
ORDER BY p_id ASC, lindex ASC;

-- TODO : Comment concatener les ltree

-- All graph from Root node :
CREATE OR REPLACE VIEW view_allgraphe_001 AS
WITH RECURSIVE graphe(lparent, lchild, lname, lindex, ldata, level, path) AS (
	SELECT l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, (l.lparent||'.'||l.lchild)::text::ltree
	FROM anyobject_links l
	UNION ALL
	SELECT l.lparent, l.lchild, l.lname, l.lindex, l.ldata, g.level + 1, (path::text || '.'|| l.lchild)::text::ltree
	FROM anyobject_links l, graphe g
	WHERE g.lchild = l.lparent
)
SELECT
l.path AS path, l.level AS level, l.lname AS lname, l.lindex, l.ldata AS ldata,
p.id AS p_id, p.uid AS p_uid, p.name AS p_name, p.cid AS p_cid,
c.id AS c_id, c.uid AS c_uid, c.name AS c_name, c.cid AS c_cid
FROM graphe AS l
LEFT OUTER JOIN anyobject AS p ON l.lparent = p.id
LEFT OUTER JOIN anyobject AS c ON l.lchild = c.id
WHERE l.path <@ '89'
ORDER BY path ASC, lindex ASC;
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
