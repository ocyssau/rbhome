-- ======================================================================
-- From file DaoList.php
-- ======================================================================
-- ======================================================================
-- From file Loader.php
-- ======================================================================
-- ======================================================================
-- From file PreferenceDao.php
-- ======================================================================
-- ======================================================================
-- From file UserDao.php
-- ======================================================================
 CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE
 ON people_user FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_people_user AFTER DELETE
 ON people_user FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file GroupDao.php
-- ======================================================================
 CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE
ON people_group FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE
ON people_group FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();

-- ======================================================================
-- From file MaterialUsedDao.php
-- ======================================================================
-- ======================================================================
-- From file MaterialDao.php
-- ======================================================================
 CREATE TRIGGER trig01_material AFTER INSERT OR UPDATE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_material AFTER DELETE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file RecipeDao.php
-- ======================================================================
 CREATE TRIGGER trig01_materialrecipe AFTER INSERT OR UPDATE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_materialrecipe AFTER DELETE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file LinkDao.php
-- ======================================================================
-- ======================================================================
-- From file UnitDao.php
-- ======================================================================
CREATE TRIGGER trig01_org_ou AFTER INSERT OR UPDATE
ON org_ou FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
CREATE TRIGGER trig02_org_ou AFTER DELETE
ON org_ou FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file Dao.php
-- ======================================================================
-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig01_anyobject_update_path
--
DROP TRIGGER IF EXISTS rb_trig01_anyobject_update_path ON anyobject;
CREATE TRIGGER rb_trig01_anyobject_update_path AFTER INSERT OR UPDATE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

-------------------------------------------------------------------------------------------------------------------
-- Trigger rb_trig02_anyobject_delete
--
DROP TRIGGER IF EXISTS rb_trig02_anyobject_delete ON anyobject;
CREATE TRIGGER rb_trig02_anyobject_delete AFTER DELETE
ON anyobject FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ClassDao.php
-- ======================================================================
-- ======================================================================
-- From file SurfaceDao.php
-- ======================================================================
 CREATE TRIGGER trig01_build_surface AFTER INSERT OR UPDATE
ON build_surface FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();

CREATE TRIGGER trig02_build_surface AFTER DELETE
ON build_surface FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();
-- ======================================================================
-- From file ProjectDao.php
-- ======================================================================
 CREATE TRIGGER trig01_project AFTER INSERT OR UPDATE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_project AFTER DELETE
 ON project FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
