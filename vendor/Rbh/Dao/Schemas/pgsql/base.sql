-- 
-- Id: $Id: base.sql 801 2012-04-18 21:08:27Z olivierc $
-- File name: $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Schemas/pgsql/base.sql $
-- Last modified: $LastChangedDate: 2012-04-18 23:08:27 +0200 (mer., 18 avr. 2012) $
-- Last modified by: $LastChangedBy: olivierc $
-- Revision: $Rev: 801 $
-- 

-- CREATE DATABASE rbh DEFAULT CHARACTER SET latin1 COLLATE latin1_general_ci;

-- Shema informations
-- 
CREATE TABLE schema (
  id CHAR(255) NOT NULL,
  rev CHAR(255) NOT NULL
);
INSERT INTO schema (id, rev) VALUES ('$LastChangedDate: 2012-04-18 23:08:27 +0200 (mer., 18 avr. 2012) $', '$Rev: 801 $');

