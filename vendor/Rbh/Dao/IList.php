<?php
//%LICENCE_HEADER%


/**
 * $Id: ListPg.php 680 2011-11-04 11:16:39Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Pg/List.php $
 * $LastChangedDate: 2011-11-04 12:16:39 +0100 (ven., 04 nov. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 680 $
 */

namespace Rbh\Dao;

use \Rbh\Sys\Exception As Exception;
use \Rbh\Sys\Error As Error;
use \Rbh\Signal as Signal;
use \Rbh\Rbh As Rbh;


/**
 * A list of records in db.
 * This class implements a iterator.
 * Query postgresql database to create a list of scalar values in a iterator class.
 * Example and tests: Rbplm/Dao/Pg/ListTest.php
 *
 * @todo Check and improve update list procedures.
 *
 */
class IList implements \Iterator, \Countable
{

	/**
	 * @var \PDOStatement
	 */
	public $stmt;

	/**
	 * @var integer
	 */
	protected $count = 0;

	/**
	 * Position of the iterator
	 *
	 * @var string
	 */
	protected $key = 0;

	/**
	 * Current iterator item
	 */
	protected $current;

	/**
	 * Constructor
	 *
	 * @param \PDO
	 *
	 */
	public function __construct($stmt)
	{
		$this->stmt = $stmt;
	} //End of function


	/**
	 * Reinit properties
	 *
	 * @return void
	 */
	protected function _reset()
	{
		$this->stmt = null;
		$this->current = null;
		$this->key = 0;
		$this->count = null;
		$this->level = null;
	} //End of function

	/**
	 * @see Iterator::valid()
	 * @return void
	 */
	public function valid()
	{
		if( $this->key < $this->count() ){
			return true;
		}
		else{
			$this->stmt->closeCursor();
			return false;
		}
	}


	/**
	 * Gets the current iterator item
	 *
	 * @see Iterator::current()
	 * @return array
	 */
	public function current()
	{
		if( $this->loadedKey !== $this->key ){
			$this->current = $this->stmt->fetch();
			$this->loadedKey = $this->key;
		}
		return $this->current;
	}


	/**
	 * Value of the iterator key for the current pointer
	 * (translated by the metamodel)
	 *
	 * @see Iterator::key()
	 * @return string
	 */
	public function key()
	{
		return $this->key;
	}

	/**
	 * @see Iterator::next()
	 * @return void
	 */
	public function next()
	{
		$this->key++;
	}

	/**
	 * @see Iterator::rewind()
	 * @return void
	 */
	public function rewind()
	{
		if( $this->key > 0 ){
			$this->stmt->execute();
			$this->key = 0;
		}
	}

	/**
	 * @see Countable::count()
	 * @return integer
	 */
	public function count()
	{
		if( !$this->count ){
			$this->count = $this->stmt->rowCount();
		}
		return $this->count;
	}

} //End of class
