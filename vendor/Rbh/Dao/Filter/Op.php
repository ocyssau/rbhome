<?php
//%LICENCE_HEADER%

/**
 * $Id: User.php 520 2011-07-18 22:32:13Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/User.php $
 * $LastChangedDate: 2011-07-19 00:32:13 +0200 (mar., 19 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 520 $
 */

namespace Rbh\Dao\Filter;

/**
 * 
 * Enum for operator
 *
 */
class Op
{
	 const OP_CONTAINS = 'contains'; 		// ~
	 const OP_NOTCONTAINS = 'notcontains'; 	// !~
	 const OP_BEGIN = 'begin'; 				// .*
	 const OP_NOTBEGIN = 'notbegin';		// !.*
	 const OP_END = 'end';					// *
	 const OP_NOTEND = 'notend';			// !*
	 const OP_EQUAL = 'equal';				// =
	 const OP_NOTEQUAL = 'notequal';		// !=
	 const OP_IN = 'in';					// 
	 const OP_NOTIN = 'notin';				// 
	 const OP_SUP = 'sup';					// >
	 const OP_EQUALSUP = 'equalsup';		// =>
	 const OP_INF = 'inf';					// <
	 const OP_EQUALINF = 'equalinf';		// =<
}//End of class
