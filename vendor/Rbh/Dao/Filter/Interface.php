<?php
//%LICENCE_HEADER%

/**
 * $Id: User.php 520 2011-07-18 22:32:13Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/User.php $
 * $LastChangedDate: 2011-07-19 00:32:13 +0200 (mar., 19 juil. 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 520 $
 */

namespace Rbh\Dao\Filter;

use \Rbh\Dao\Filter\Op;

interface FilterInterface
{

	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Op::OP_*
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function andfind( $what, $where, $op=Op::OP_CONTAINS );
	
	/**
	 * 
	 * @param string $what	a word to find
	 * @param string $where	property name
	 * @param string $op	One of constant Op::OP_*
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function orfind( $what, $where, $op=Op::OP_CONTAINS );
	
	/**
	 * 
	 * @param integer	$int
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function page( $page, $limit );

	/**
	 * 
	 * @param string	$by
	 * @param string	$dir
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function sort( $by , $dir='ASC' );
	
	/**
	 * Get children of node
	 * 
	 * @param $path		string	Path as define in application
	 * @param $option	array	Options
	 * Options may be :
	 * 	level:	integer	The level after current node to query
	 * 	depth:	Match "depth" levels from level "level"
	 * 	pathAttributeName: string	Name of system attribute for path, default is 'path'.
	 * 	boolOp: string		Boolean operator, may be 'AND' or 'OR'
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function children( $path, $options );
	
	/**
	 * Get the ancestors of a node.
	 * 
	 * @param $path		string		Path as define in application
	 * @param $option	array		Options
	 * 
	 * Options may be :
	 * 	minLevel:	integer	Min level of ancestors to query. If level is negatif, calcul level from current node, else calcul from root node. Default is 0.
	 * 	maxLevel:	Max level to query. May be negatif. Default is level of current node.
	 * 	pathAttributeName: string	Name of system attribute for path, default is 'path'.
	 * 	boolOp: string		Boolean operator, may be 'AND' or 'OR'
	 * 
	 * If maxLevel = 0, max is unactivate.
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function ancestors( $path, $options );
	
	/**
	 * Add a sub-filter to filter with and condition.
	 * @param Rbplm_Dao_Filter_Interface
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function suband( $subFilter );
	
	/**
	 * Add a sub-filter to filter with or condition.
	 * @param Rbplm_Dao_Filter_Interface
	 * @return Rbplm_Dao_Filter_Interface
	 */
	public function subor( $subFilter );
	
	/**
	 * @return string
	 */
	public function __toString();
	
	/**
	 * @param array	$select
	 * @return Rbplm_Dao_Pg_Filter
	 */
	public function select( $select );
	
	/**
	 * Set or return status of pagination activation.
	 * If unactive, remove offset, limits and order.
	 * 
	 * @param boolean
	 * @return boolean
	 */
	public function paginationIsActive( $bool=null );
	
}//End of class
