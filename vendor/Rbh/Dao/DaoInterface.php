<?php
//%LICENCE_HEADER%


/**
 * $Id: Interface.php 818 2012-04-30 21:45:46Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Interface.php $
 * $LastChangedDate: 2012-04-30 23:45:46 +0200 (lun., 30 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 818 $
 */

namespace Rbh\Dao;
use Rbh\Dao\MappedInterface;

/**
 * @brief Interface for Dao to single object.
 * 
 *
 */
interface DaoInterface{

	/**
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param string|Rbplm_Dao_Pg_Filter		$filter [OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Rbplm_Dao_Pg_Filter.
	 * @param array					$options	Is array of options.
	 * 								Basic options may be :
	 * 									'lock'	=>boolean 	If true, lock the db records in accordance to the use db system. Default is false.
	 * 									'force'	=>boolean	If true, force to query db to reload links. Default is false.
	 * 									'expectedProperties' Array of property to load in $mapped. As a SELECT in SQL. Default is all.
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Interface
	 * 
	 */
	public function load( MappedInterface $mapped, $filter = null, array $options = array() );
	
	
	/**
	 * Load the properties from $array in the mapped object.
	 * $array is a mappe where key is name of property and value, property value.
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param array 					$array	Mappe of property name, property value to set in $mapped.
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Interface
	 */
	public static function loadFromArray( $mapped, array $array );
	
	
	/**
	 * Populate object from db datas with uid as input parameter.
	 *
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param string				$uid	Uuid of the $mapped object to load.
	 * @param array					$options	Is array of options.
	 * 								Basic options may be :
	 * 									'lock'	=>boolean 	If true, lock the db records in accordance to the use db system. Default is false.
	 * 									'force'	=>boolean	If true, force to query db to reload links. Default is false.
	 * 									'expectedProperties' Array of property to load in $mapped. As a SELECT in SQL. Default is all.
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Interface
	 * 
	 */
	public function loadFromUid( $mapped, $uid, $options = array() );
	
	
	/**
	 * Populate object from db datas with path as input parameter.
	 *
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param string				$path	Path of the $mapped object to load.
	 * @param array					$options	Is array of options.
	 * 								Basic options may be :
	 * 									'lock'	=>boolean 	If true, lock the db records in accordance to the use db system. Default is false.
	 * 									'force'	=>boolean	If true, force to query db to reload links. Default is false.
	 * 									'expectedProperties' Array of property to load in $mapped. As a SELECT in SQL. Default is all.
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Interface
	 * 
	 */
	public function loadFromPath($mapped, $path, $options = array() );
	
	
	/**
	 * Load links of the $mapped from db in accordance to $filter and return a Rbplm_Dao_List object.
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param string							$class Class name of links to load
	 * @param string|Rbplm_Dao_Pg_Filter		$filter [OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Rbplm_Dao_Pg_Filter.
	 * @param array					$options	Is array of options.
	 * 								Basic options may be :
	 * 									'lock'	=>boolean 	If true, lock the db records in accordance to the use db system. Default is false.
	 * 									'force'	=>boolean	If true, force to query db to reload links. Default is false.
	 * 									'expectedProperties' Array of property to load in $mapped. As a SELECT in SQL. Default is *.
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Interface
	 * 
	 */
	//public function loadLinks( MappedInterface $mapped, $class, $filter = null, $options = array() );
	
	
    /**
     * Count line from a filter
     *
	 * @param string|Rbplm_Dao_Pg_Filter		$filter [OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Rbplm_Dao_Pg_Filter.
     * @return integer
     */
    public function count($filter=null);
	
	/**
	 * To render permanent. Update or create the records.
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param array					$options	Is array of options.
	 * 								Basic options may be :
	 * 									'unlock'	=>boolean 	If true, unlock the db records in accordance to the use db system. Default is false.
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Interface
	 */
	public function save( MappedInterface $mapped, array $options = array() );
	
	
	/**
	 * Suppress current record in database
	 * 
	 * @param Rbplm_Dao_MappedInterface	$mapped
	 * @param boolean $withChilds	If true, suppress all childs and childs of childs
	 * @throws Rbplm_Sys_Exception
	 * @return Rbplm_Dao_Interface
	 */
	public function suppress(MappedInterface $mapped, $withChilds = false);
	
    /**
     * Getter for links. Return a list.
     *
     * @param Rbplm_Dao_MappedInterface
	 * @param string|Rbplm_Dao_Pg_Filter		$filter [OPTIONAL] Filter is a string in db system synthax to select the records or a instance of an object with type Rbplm_Dao_Pg_Filter.
     * @param array								Options map specific to this DAO.
     * @return Rbplm_Dao_ListInterface
     */
    //public function getLinksRecursively($mapped, $filter = null, $options = array() );
} //End of class
