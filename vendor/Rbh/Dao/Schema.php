<?php
namespace Rbh\Dao;

class Schema{
	protected $conn = null;
	public $sqlInScripts = array('sequence.sql', 'create.sql', 'insert.sql', 'alter.sql', 'foreignKey.sql', 'view.sql', 'proc.sql', 'trigger.sql');
	public $sqlScriptsPath;
	public $sqlCompiledScriptsPath;
	
	public function __construct($Conn)
	{
		$this->conn = $Conn;
		$this->sqlScriptsPath = __DIR__ . '/Schemas/pgsql/extracted/';
		$this->sqlCompiledScriptsPath = __DIR__ . '/Schemas/pgsql/compiled/';
	}

	/**
	 * @param string $RbhPath Path to Rbh librairy
	 */
	public function compileSchema($RbhPath)
	{
		$Extractor = new \Rbh\Dao\Schemas\SqlExtractor($this->sqlScriptsPath);
		$Extractor->parse($RbhPath);
		
		$sql = '';
		foreach( $this->sqlInScripts as $script){
			$sql .= file_get_contents($this->sqlScriptsPath .'/' . $script, true);
		}
		
		$ver = 'v1';
		$outFile = $this->sqlCompiledScriptsPath . '/pgsql.'.$ver.'.sql';
		file_put_contents($outFile, $sql);
		
		return $outFile;
	}
	
	public function createBdd()
	{
		$conn = $this->conn;
		//$compiledPath = $this->sqlCompiledScriptsPath;
		$compiledPath = $this->sqlScriptsPath;
		
		foreach( $this->sqlInScripts as $script){
			try{
				echo "Try to import $script" . CRLF;
				$sql = file_get_contents($compiledPath .'/'. $script, true);
				$conn->exec($sql);
			}
			catch(Exception $e){
				echo 'Unable to import '.$script . CRLF;
				echo $e->getMessage() . CRLF;
			}
		}
	}

	/**
	 * Create the tree root node in database
	 * @return unknown_type
	 */
	public function createRootNode()
	{
		$conn = $this->conn;
		$root = \Rbh\Org\Root::singleton()->init();
		$Dao = new \Rbh\Org\UnitDao( array(), $conn );
		$Dao->save($root);
	}

	/**
	 * Create the admin user and his group
	 * @return unknown_type
	 */
	public function createUsers()
	{
		$conn = $this->conn;
		$root = \Rbh\Org\Root::singleton();
		
		/*
		$adminUser = new \Rbh\People\User(array(
			'uid'=>\Rbh\People\User::SUPER_USER_UUID,
			'id'=>\Rbh\People\User::SUPER_USER_ID,
			'firstname'=>'admin',
			'lastname'=>'rbh',
		), $root);
		$adminUser->setLogin('admin');
		$adminUser->setLabel('admin');
		
		$anonymousUser = new \Rbh\People\User(array(
			'uid'=>\Rbh\People\User::ANONYMOUS_USER_UUID,
			'id'=>\Rbh\People\User::ANONYMOUS_USER_ID,
			'firstname'=>'anonymous',
			'lastname'=>'rbh',
		), $root);
		$anonymousUser->setLogin('anonymous');
		$anonymousUser->setLabel('anonymous');
		
		$adminGroup = new \Rbh\People\Group(array(
			'uid'=>\Rbh\People\Group::SUPER_GROUP_UUID,
			'name'=>'adminGroup',
			'label'=>'adminGroup',
			'description'=>'administrateur of system',
		), $root);
		
		$UserDao = new \Rbh\People\UserDao( array(), $conn );
		$GroupDao = new \Rbh\People\GroupDao( array(), $conn );
		
		try{
			$UserDao->save($adminUser);
		}
		catch(\Exception $e){
			$conn->rollBack();
			var_dump($e->getMessage()) ;
		}
		try{
			$UserDao->save($anonymousUser);
		}
		catch(\Exception $e){
			$conn->rollBack();
			var_dump($e->getMessage());
		}
		try{
			$GroupDao->save($adminGroup);
		}
		catch(\Exception $e){
			$conn->rollBack();
			var_dump($e->getMessage()) ;
		}
		*/
		
		/*
		//Create Ou
		$GrpOu = \Rbh\Org\Unit::init('Groups', $root);
		$GrpOu->setLabel('Groups');
		$GrpOu->ownerId = \Rbh\People\User::SUPER_USER_ID;
		
		$UserOu = \Rbh\Org\Unit::init('Users', $root);
		$UserOu->setLabel('Users');
		$UserOu->ownerId = \Rbh\People\User::SUPER_USER_ID;
		
		$OuDao = new \Rbh\Org\UnitDao( array(), $conn );
		
		$OuDao->save($GrpOu);
		$OuDao->save($UserOu);
		*/
		
		/*
		$adminUser->setParent($UserOu);
		$anonymousUser->setParent($UserOu);
		$adminGroup->setParent($GrpOu);
		
		$UserDao->save($adminUser);
		$UserDao->save($anonymousUser);
		$GroupDao->save($adminGroup);
		*/
	}

} //End of class

