<?php
//%LICENCE_HEADER%

/**
 * $Id: DocumentTest.php 814 2012-04-27 13:47:25Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Ged/DocumentTest.php $
 * $LastChangedDate: 2012-04-27 15:47:25 +0200 (ven., 27 avr. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 814 $
 */

namespace Rbh\Dao;

use \Rbh\Test\Test As Test;
use \Rbh\Dao\Connexion As Connexion;
use \Rbh\Sys\Exception As Exception;

/**
 * @brief Test class for Rbh_Material.
 * @include Rbh/MaterialTest.php
 *
 */
class ConnexionTest extends Test
{
	/**
	 * Test database connexion
	 */
	function Test_ConnexionsPdo()
	{
		try{
			$db = Connexion::get();
			$stmt = $db->prepare("INSERT INTO anyobject (uid, cid, name, label, owner) VALUES(?,?,?,?,?)");
			$stmt->execute(
				array(
					\Rbh\Uuid::newUid(),
					20,
					uniqId('_connect_'),
					uniqId(),
					\Rbh\People\User::SUPER_USER_ID,
				)
			);
		}
		catch(\Exception $e){
			//throw new \Exception( $e->getMessage() );
			var_dump($e->getMessage());die;
		}
		//echo "autocommit :" . $db->getAttribute(\PDO::ATTR_AUTOCOMMIT) . CRLF; //not sopported by postgresql
		echo "status :" . $db->getAttribute(\PDO::ATTR_CONNECTION_STATUS) .CRLF;
		echo "persistence :" . $db->getAttribute(\PDO::ATTR_PERSISTENT) .CRLF;
		echo "server info :" . $db->getAttribute(\PDO::ATTR_SERVER_INFO) .CRLF;
		echo "driver :" . $db->getAttribute(\PDO::ATTR_DRIVER_NAME) .CRLF;
		echo "TEST DATAS IN DB TO CLEAN, ID:" . $db->lastInsertId('anyobject_id_seq') .CRLF;
	}
}
