<?php
//%LICENCE_HEADER%

/**
 * $Id: Config.php 497 2011-06-25 21:39:07Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Sys/Config.php $
 * $LastChangedDate: 2011-06-25 23:39:07 +0200 (sam., 25 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 497 $
 */

namespace Rbh\Dao;


/**
 * @brief Factory for connexions resources.
 * Require a configuration that must be set before use:
 * @code
 * Rbplm_Dao_Connexion::setConfig($conf)
 * @endcode
 * 
 * $conf is a array with keys:
 * 	array($name=>array(
 * 		'adapter'=>mysql|mssql|postgresql|ldap|openldap|activedirectory
 * 		'params'=>specific parameters for each adapter
 * 	))
 * 	$name is a arbitrary name for connexion.
 * 	
 * 
 *  a named connexion is simply get with:
 *  @code
 *  $conn = Rbplm_Dao_Connexion::get($name);
 *  @endcode
 *  
 *  to list require params, see methods 
 *  Rbplm_Dao_Connexion::_connectPdo for mysql, mssql, postgresql adapters
 *  and 
 *  Rbplm_Dao_Connexion::_connectLdap for ldap, openldap and activedirectory adapters
 *  
 * 
 */
class Connexion {

    /**
     *
     * @var array
     */
    public static $config = false;

    /**
     * Associative array where key is name of classe that require the connexion
     * 
     * @var array		Collection of Rbplm_Dao_Interface
     */
    public static $dbs = array();
    
    /**
     * Name of default connexion to use.
     * @var string
     */
    public static $defaultConnName = 'default';

    /**
     * Get the config
     *
     * @return array
     */
    public static function getConfig() {
        return self::$config;
    } // End of method

    /**
     * Set the config
     *
     * @param array $config
     * @return void
     */
    public static function setConfig(array $config, $connName=null) {
    	if($connName<>null){
    		self::$config[$connName] = $config;
    	}
    	else{
    		self::$config = $config;
    	}
    } // End of method

    /**
     * Create Pdo object
     *
     * @param array $options
     * @param boolean $db_consult_only
     * @return Pdo
     */
    protected static function _connectPdo(array $params, $adapter, $db_consult_only = false) {
        /*Database connexion parameters*/
         $driver   	= $adapter;
         $host   	= $params['host'];
         $dbname    = $params['dbname'];
         $port    	= $params['port'];
         
        if($db_consult_only){ //read only access user
            $user   = $params['readonly']['username'];
            $pass   = $params['readonly']['password'];
        }else{ //Full access user
            $user   = $params['username'];
            $pass   = $params['password'];
        }
        
        try{
            $dsn = "$driver:host=$host;dbname=$dbname";
            if($port){
                $dsn .= ";port=$port";
            }
            if($driver=='mysql'){
                $dbh = new \PDO($dsn, $user, $pass, array(PDO::ATTR_PERSISTENT => true));
            }
            else{
                $dsn .= ';user='.$user.';password='.$pass;
                $dbh = new \PDO($dsn);
            }
            //$dbh->exec("SET CHARACTER SET utf8");
            $dbh->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION );
            //Conversion des valeurs NULL et cha�nes vides:
            $dbh->setAttribute( \PDO::ATTR_ORACLE_NULLS, \PDO::NULL_EMPTY_STRING );
            //Casse pour le nom de colonnes
            $dbh->setAttribute( \PDO::ATTR_CASE, \PDO::CASE_NATURAL );
        }catch(\PDOException $e){
        	print $e->getMessage();
        	print ( ' -- Dsn: ' . "$driver:host=$host;dbname=$dbname" );
        }
        return $dbh;
    } // End of method
    
    /**
     * Create Ldap connexion
     *
     * @param array $options
     * @param boolean $db_consult_only
     * @return 
     * @todo: implement this method
     */
    protected static function _connectLdap(array $options = null, $db_consult_only = false) {
    } // End of method
    
    /**
     * Get db abstract access object.
     * 
     * This method work as a singleton, factory and accessor.
     * Only if connexion is not existing, she will be created and stored in a internal registry, 
     * else the previous created connexion is returned.
     * 
     * Connexion params are extracted  from config set in rbplm. The config must define at least :
	 * 	array($name=>array(
	 * 		'adapter'=>mysql|mssql|postgresql|ldap|openldap|activedirectory
	 * 		'params'=>specific parameters for each adapter
	 * 	))
     *
     * @param string	name of connexion to use
     * @return PDO
     */
    public static function get($name = null){
    	if( !$name ){
    		$name = self::$defaultConnName;
    	}
        if ( !isset(self::$dbs[$name]) ) {
            $adapter = self::$config[$name]['adapter'];
            $params = self::$config[$name]['params'];
            switch($adapter){
                case 'activedirectory':
                case 'ldap':
                case 'openldap':
                    self::$dbs[$name] = self::_connectLdap($params);
                    break;
                case 'mysql':
                case 'mssql':
                case 'pgsql':
                    self::$dbs[$name] = self::_connectPdo($params, $adapter);
                    break;
                default:
                    //@TODO: throw a error
                    break;
            }
        }
        return self::$dbs[$name];
    }
    
    /**
     * 
     * Add a connexion to registry.
     * @param string $name
     * @param object $conn		A object used by DAO to access to datas. 
     * May be any type, because its just used by appropriate DAO.
     */
    public static function add($name, $conn){
        self::$dbs[$name] = $conn;
    }
    
} //End of class