<?php
//%LICENCE_HEADER%

/**
 * $Id: MappedInterface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/MappedInterface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

namespace Rbh\Dao;

/**
 * @brief A mapped object may be associate to a Dao.
 * 
 * A mapped object is object associate to a dao and is permanently saved.
 * So to render permanent a object, just implements this interface and create the Dao implementing Rbplm_Dao_Interface.
 * 
 */
interface MappedInterface
{

	/**
	 * Setter/Getter
	 * Return true if the object is loaded from database
	 * 
	 * @param boolean	$bool	Set load state
	 * @return boolean
	 */
	public function isLoaded( $bool = null );
	
} //End of class
