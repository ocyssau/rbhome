<?php
//%LICENCE_HEADER%


/**
 * $Id: Interface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Interface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

namespace Rbh\Dao;

use \Rbh\Dao\Connexion;
use \Rbh\Sys\Exception;
use \Rbh\Sys\Error;

/**
 * @brief Factory for Dao.
 * 
 * Factory to instanciate DAOs from a specified model classe name.
 * 
 * Examples:
 * Get the dao of buizness class Rbplm_Org_Unit
 * @code
 * $Dao = \Rbh\Dao\Factory::getDao( 'Rbplm_Org_Unit' );
 * //To get only class name
 * $DaoClassName = \Rbh\Dao\Factory::getDaoClass( 'Rbplm_Org_Unit' );
 * @endcode
 * 
 * Instanciate the appropriate Filter object:
 * @code
 * $Filter = \Rbh\Dao\Factory::getFilter( 'Rbplm_Org_Unit' );
 * //Equivalent to
 * $Filter = \Rbh\Dao\Factory::getDao( 'Rbplm_Org_Unit' )->newFilter();
 * @endcode
 * 
 * Instanciate the appropriate List object
 * @code
 * $List = \Rbh\Dao\Factory::getList( 'Rbplm_Org_Unit' );
 * //Equivalent to
 * $List = \Rbh\Dao\Factory::getDao( 'Rbplm_Org_Unit' )->newList();
 * @endcode
 *
 */
class Factory
{

	/**
     * Registry of instanciated DAO.
     * 
     * @var array	Array of \Rbh\Dao\Interface where key is buizness object class name.
     */
    private static $registry = array();
    
    /**
     * Map object class to Dao class
     * @var array
     */
    public static $map = array();
    
    /**
     *
     * @param array $config		Is array of map buizness object to Dao class to instanciate and connexion to use.
     * @see Factory::$_config
     *
     */
    public static function setConfig( array $config )
    {
        self::$map = $config;
    }
    
    /**
     * Return a dao object for the mapped object.
     * 
     * @param string	$class			Buizness object class name or object
     * @param string	$daoClass		[OPTIONAL] DAO class name. If not set, get the dao class name from config.
     * @return \Rbh\Dao\DaoInterface
     */
    public static function getDao( $class, $daoClass = null )
    {
    	if(is_object($class)){
    		$class = get_class($class);
    	}
		$class = trim($class, '\\');
    	//echo $class.CRLF;
    	if(isset(self::$map[$class])){
    		$daoClass = self::$map[$class];
    	}
    	else{
    		$daoClass = $class . 'Dao';
    	}
    	//echo $daoClass.CRLF;
    	
    	$i = trim($daoClass, '\\');
    	 
    	if( !isset(self::$registry[$i]) ){
	    	$dao = new $daoClass();
    		self::$registry[$i] = $dao;
    	}
    	return self::$registry[$i];
    }
    
    /**
     * Return the dao class name from config.
     * 
     * @param string	$class		Buizness object class name
     * @return string
     */
    public static function getDaoClass( $class )
    {
    	if(is_object($class)){
    		$class = get_class($class);
    	}
		$class = trim($class, '\\');
    	//echo $class.CRLF;
    	if(self::$map[$class]){
    		$daoClass = self::$map[$class];
    	}
    	else{
    		$daoClass = $class . 'Dao';
    	}
    	return trim($daoClass, '\\');;
    }
    
} //End of class