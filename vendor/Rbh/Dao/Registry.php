<?php
//%LICENCE_HEADER%


/**
 * $Id: Interface.php 425 2011-06-04 11:39:52Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/Dao/Interface.php $
 * $LastChangedDate: 2011-06-04 13:39:52 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 425 $
 */

namespace Rbh\Dao;

/**
 * @brief Generic storage class helps to manage global data.
 *
 */
class Registry extends \ArrayObject
{

    /**
     * Singleton pattern.
     * @var \Rbh\Dao\Registry
     */
    private static $singleton = null;

    /**
     * If false, add methode is unactive
     */
    public $isActive = true;
    
    /**
     * Constructs a parent ArrayObject with default
     * ARRAY_AS_PROPS to allow acces as an object
     *
     * @param array $array data array
     * @param integer $flags ArrayObject flags
     */
    public function __construct($array = array(), $flags = parent::ARRAY_AS_PROPS)
    {
        parent::__construct($array, $flags);
    }
    
    
    /**
     * Singleton pattern.
     * 
     * @return \Rbh\Dao\Registry
     */
    public static function singleton()
    {
    	if( !self::$singleton ){
    		self::$singleton = new self();
    	}
    	return self::$singleton;
    }
    
    /**
     *
     * @param \Rbh\AnyObject
     */
    public function add( $AnyObject )
    {
    	if($this->isActive){
    		return $this->offsetSet($AnyObject->id, $AnyObject);
    	}
    }
    
    /**
     * 
     * @return \Rbh\Dao\Registry
     */
    public function get($id)
    {
    	if( $this->offsetExists((int)$id)){
	    	return $this->offsetGet($id);
    	}
    	return null;
    }
    
    /**
     * 
     * @param integer		Id of object
     */
    public function exist( $id )
    {
    	return $this->offsetExists($id);
    }
	
    /**
     * 
     * @param \Rbh\AnyObject
     */
    public function reset( $id = null )
    {
    	if($id){
    		$this->offsetUnset($id);
    	}
    	else{
    		self::$singleton = new self();
    	}
    }
    
}
