<?php
//%LICENCE_HEADER%

namespace Rbh\Acl;

use Rbh\Dao\Pg as Dao;
use Rbh\Dao\MappedInterface;

/** SQL_SCRIPT>>

CREATE SEQUENCE acl_id_seq
START WITH 1
INCREMENT BY 1
NO MAXVALUE
NO MINVALUE
CACHE 1;

CREATE TABLE acl_rules (
id bigint NOT NULL,
resource_id uuid NOT NULL,
role_id uuid  NOT NULL,
privilege varchar(32)  NOT NULL,
rule varchar(32)  NOT NULL
);
<<*/

/** SQL_INSERT>>
 <<*/

/** SQL_ALTER>>
 ALTER TABLE acl_rules ALTER COLUMN id SET DEFAULT nextval('acl_id_seq'::regclass);
 ALTER TABLE acl_rules ADD UNIQUE (resource_id,role_id,privilege);
 CREATE INDEX INDEX_acl_rules_resource_id ON acl_rules USING btree (resource_id);
 CREATE INDEX INDEX_acl_rules_role_id ON acl_rules USING btree (role_id);
 CREATE INDEX INDEX_acl_rules_privilege ON acl_rules USING btree (privilege);
 CREATE INDEX INDEX_acl_rules_rule ON acl_rules USING btree (rule);
 <<*/

/** SQL_FKEY>>
 --- ALTER TABLE acl_rules ADD FOREIGN KEY (resource_id) REFERENCES anyobject (uid);
 <<*/

/** SQL_TRIGGER>>
 CREATE OR REPLACE FUNCTION rb_replace_aclrule(in_resource_id UUID, in_role_id UUID, in_privilege TEXT, in_rule TEXT) RETURNS VOID AS
 $$
 BEGIN
 LOOP
 UPDATE acl_rules SET rule=in_rule  WHERE resource_id=in_resource_id AND role_id=in_role_id AND privilege=in_privilege;
 IF found THEN
 RETURN;
 END IF;

 BEGIN
 INSERT INTO anyobject_links (resource_id,role_id,privilege,rule) VALUES (in_resource_id, in_role_id, in_privilege, in_rule);
 RETURN;
 EXCEPTION WHEN unique_violation THEN
 -- do nothing
 END;
 END LOOP;
 END;
 $$
 LANGUAGE plpgsql;



 -- http://wiki.postgresql.org/wiki/Return_more_than_one_row_of_data_from_PL/pgSQL_functions

 CREATE TYPE aclrule AS (resource_id uuid, role_id uuid, privilege varchar(32), rule  varchar(32), resource_path text, role_path text);
 CREATE OR REPLACE FUNCTION rb_get_aclrule(in_resource_path ltree, in_role_id UUID) RETURNS SETOF aclrule AS
 $$
 DECLARE
 aclrule aclrule%ROWTYPE;
 BEGIN
 FOR aclrule IN
 SELECT DISTINCT acl.resource_id, acl.role_id, acl.privilege, acl.rule, resource.uidpath AS resource_path, role.path AS role_path FROM
 (
 WITH RECURSIVE tree_role(lparent, lchild, data, depth, path, lindex) AS (
 SELECT lnk.related, lnk.lchild, lnk.data, 1, (lnk.lparent || '.' || lnk.lchild)::text, lnk.lindex
 FROM anyobject_links lnk
 WHERE lnk.lparent=in_role_id
 UNION ALL
 SELECT lnk.lparent, lnk.lchild, lnk.data, tr.depth + 1, (tr.path || '.' || lnk.lchild)::text, lnk.lindex
 FROM anyobject_links lnk, tree_role tr
 WHERE tr.lchild = lnk.lparent)
 SELECT DISTINCT tr.*,
 child.uid AS childuid, child.name AS childname, child.path AS childpath, child.cid AS childclassid,
 parent.uid AS parentuid, parent.name AS parentname, parent.path AS parentpath, parent.cid AS parentclassid
 FROM tree_role AS tr
 JOIN anyobject AS child ON child.uid = tr.lchild
 JOIN anyobject AS parent ON parent.uid = tr.lparent
 ORDER BY path ASC
 ) AS role
 JOIN acl_rules AS acl ON role.lchild = acl.role_id
 JOIN
 (
 SELECT uid, path, array_to_string(
 ARRAY(SELECT bcomp.uid FROM anyobject AS bcomp WHERE bcomp.path @> comp.path ORDER BY bcomp.path),
 '.') As uidpath
 FROM
 anyobject AS comp
 WHERE comp.path @> in_resource_path
 ) AS resource ON resource.uid = acl.resource_id
 LOOP
 RETURN NEXT aclrule;
 END LOOP;
 END
 $$
 LANGUAGE plpgsql;


 <<*/
/** TESTS>>
 SELECT path FROM anyobject WHERE uid='bd9b351b-adac-484e-8880-4e97bdc11292';
 SELECT path FROM anyobject WHERE uid='ef4346fa-f11f-43f6-a847-2d556ee774aa';
 SELECT * FROM rb_get_aclrule('RanchbePlm.4e6f304c5969a.4e6f304c596b0.4e6f304c596c2', 'ef4346fa-f11f-43f6-a847-2d556ee774aa');
 <<*/

/** SQL_VIEW>>
 CREATE OR REPLACE VIEW view_acl_resource_tree AS
 SELECT comp.name, comp.label, comp.path, comp.parent, acl.*
 FROM anyobject AS comp
 JOIN acl_rules AS acl ON comp.uid = acl.resource_id;
 <<*/

/** SQL_DROP>>
 DROP TABLE acl_rules;
 DROP FUNCTION rb_get_aclrule(in_resource_path ltree, in_role_id UUID);
 DROP TYPE aclrule;
 <<*/

/**
 * @brief Dao class for \Rbh\Acl\Acl
 *
 * See the examples: Rbh/Acl/AclTest.php
 *
 * @see \Rbh\Dao\Pg
 * @see \Rbh\Acl\AclTest
 *
 * About shema:
 *
 *
 *
 */
class AclDaoPg extends Dao
{

    /**
     *
     * @var string
     */
    public static $table = 'acl_rules';

    /**
     *
     * @var integer
     */
    protected static $classId = 9999;


    public static $sysToApp = array(
        'privilege'=>'privilege',
        'resource_id'=>'resourceId',
        'role_id'=>'roleId',
        'rule'=>'rule'
    );

    private $stdLoadSelect = 'privilege, resource_id, role_id, rule';

    /**
     *  save rules in database
     *
     * @param \Rbh\Acl\Acl	$mapped
     *
     * @throws \Rbh\Sys\Exception
     *
     */
    public function save(MappedInterface $mapped, $options = array())
    {

        $table = static::$table;

        $sql = "INSERT INTO $table (resource_id,role_id,privilege,rule)
                            VALUES (:resourceId,:roleId,:privilege,:rule)";
        
        $stmt = $this->connexion->prepare($sql);
        $cleanStmt = $this->connexion->prepare( "DELETE FROM $table WHERE role_id=:roleId AND resource_id=:resourceId" );

        foreach($mapped->getAllRules() as $resourceId=>$byRuleId)
        {
            foreach($byRuleId['byRoleId'] as $roleId=>$byPrivilegeId)
            {
                $cleanStmt->execute( array(':roleId'=>$roleId, ':resourceId'=>$resourceId) );
                foreach($byPrivilegeId['byPrivilegeId'] as $privilegeId=>$rule){
                    $bind = array(
                        ':resourceId'=>$resourceId,
                        ':roleId'=>$roleId,
                        ':privilege'=>$privilegeId,
                        ':rule'=>$rule['type']
                    );
                    $stmt->execute($bind);
                }
            }
        }
    } //End of method

    /**
     * Load the properties in the mapped object.
     *
     * @param \Rbh\Acl\Acl	$Acl
     * @param array $row	\PDO fetch result to load
     * @return void
     */
    public function loadFromArray( $Acl, array $row)
    {
        if( $row['rule'] == \Zend_Acl::TYPE_ALLOW ){
            $Acl->allow(\Rbh\Uuid::format($row['role_id']), \Rbh\Uuid::format($row['resource_id']), $row['privilege']);
        }
        if( $row['rule'] == \Zend_Acl::TYPE_DENY ){
            $Acl->deny(\Rbh\Uuid::format($row['role_id']), \Rbh\Uuid::format($row['resource_id']), $row['privilege']);
        }
    }


    /**
     * @see \Rbh\Dao\DaoInterface::load()
     *
     * @param \Rbh\Dao\MappedInterface	$mapped
     * @param string				$filter	[OPTIONAL] Filter is a string in db system synthax to select the records. As name='objectName' in SQL.
     * @param array					$options	Is array of options.
     * 								Basic options may be :
     * 									'lock'	=>boolean 	If true, lock the db records in accordance to the use db system. Default is false.
     * 									'force'	=>boolean	If true, force to query db to reload links. Default is false.
     * 									'expectedProperties' Array of property to load in $mapped. As a SELECT in SQL. Default is all.
     * @throws \Rbh\Sys\Exception
     * @return void
     *
     */
    public function load( \Rbh\Dao\MappedInterface $mapped, $filter = null, array $options = array() )
    {
        if ( $this->_isLoaded == false || $force == true ){
            $sql = "SELECT $this->_stdLoadSelect FROM $this->_vtable";
            if($filter){
                $sql .= ' WHERE ' . $filter;
            }
            $stmt = $this->connexion->prepare($sql);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            $stmt->execute();
            while( $row = $stmt->fetch() ){
                $this->loadFromArray($mapped, $row);
            }
        }
    } //End of method


    /**
     * Add role of current object and parent lchild roles objects to ACL.
     *
     * Create Role for current object and for each of his parents groups.
     * As it is a recusrsive method, create too Role for each parent groups of groups.
     * Add each Role to Acl and set correctly parent in accordance to groups links relations.
     *
     * @param $roleId
     * @return void
     */
    public function initRole($roleId, $stmt=null)
    {
        //get parents roles
        if(!$stmt){
            $sql="SELECT lparent FROM anyobject_links WHERE lchild=:child";
            $stmt = $this->connexion->prepare($sql);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        }

        $stmt->execute(array(':child'=>$roleId));
        $parents=array();
        while ($row = $stmt->fetch()) {
            $parents[]=$row['lparent'];
            $this->initRole($row['lparent'], $stmt);
        }
        $Acl = Acl::singleton();
        if (!$Acl->hasRole($roleId)){
            $Acl->addRole($roleId, $parents);
        }
    }

    /**
     * Init Acl resource of a \Rbh\AnyObject.
     *
     * @param $ressourceId
     * @return void
     */
    public function initResource($ressourceId, $stmt=null)
    {
        //get parents ressource
        if(!$stmt){
            $sql = "SELECT lparent FROM anyobject_links WHERE lchild=:child";
            $stmt = $this->connexion->prepare($sql);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        }

        $stmt->execute(array(':child'=>$ressourceId));
        $parents = array();
        while ($row = $stmt->fetch()) {
            $parents[] = $row['lparent'];
            $this->initResource($row['lparent'], $stmt);
        }
        $Acl = Acl::singleton();

        if( !$Acl->has( $ressourceId ) ){
            $Acl->addResource( $ressourceId, $parents );
        }
    }


    /**
     *
     * @param \Rbh\Dao\MappedInterface|string
     * @return \Rbh\Dao\Pg\DaoList
     */
    public function loadResourceRecursivelyFromPath( $path, $Acl )
    {
        $path = \Rbh\Dao\Pg::pathToSys($path);
        $sql = "SELECT comp.name, comp.label, comp.path, comp.parent, acl.*
        FROM anyobject AS comp
        JOIN acl_rules AS acl ON comp.uid = acl.resource_id
        WHERE path @> '$path';";
         
        $List = new \Rbh\Dao\Pg\DaoList( array(), $this->connexion );
        $List->loadFromSql($sql);

        foreach($List as $row){
            if( !$Acl->has( $row['resource_id'] ) ){
                $Acl->addResource( $row['resource_id'], $row['parent'] );
            }
            $this->loadFromArray($Acl, $row);
        }
        return $List;
    }

    /**
     * @param \Rbh\Dao\MappedInterface	$mapped
     * @param string				$filter
     * @param array				$options
     * @return void
     */
    public function loadInList( $List, $filter = null, $options = array() )
    {
        $sql = "SELECT $this->_stdLoadSelect FROM $this->_vtable";
        if($filter){
            $sql .= ' WHERE ' . $filter;
        }
        $stmt = $this->connexion->prepare($sql);
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $stmt->execute();
        $List->setStmt($stmt);
    } //End of method


    /**
     *
     * Return a list of acl for the role and the resource.
     *
     * @param string $RoleId		Uuid of the role.
     * @param string $ResourcePath 	Application path of the resource.
     * @param \Rbh\Acl\Acl $Acl 	[OPTIONAL] Acl instance to load in rule. If null return only a list. Default is null.
     * @return \Rbh\Dao\Pg\DaoList
     */
    public function loadAclFromRoleResource($RoleId, $ResourcePath, $Acl = null)
    {
        $ResourcePath = \Rbh\Dao\Pg::pathToSys($ResourcePath);
        $sql = "SELECT resource_id, role_id, privilege, rule, resource_path, role_path FROM rb_get_aclrule('$ResourcePath', '$RoleId');";

        $List = new \Rbh\Dao\Pg\DaoList( array(), $this->connexion );
        $List->loadFromSql($sql);

        if($Acl){
            /*Construct parent list*/
            foreach($List as $row){
                $resPath = explode('.', $row['resource_path']);
                $rolPath = explode('.', $row['role_path']);
                for($i=0;$i<count($resPath);$i++){
                    if( !$Acl->has( $resPath[$i] ) ){
                        if($i>0){
                            $Acl->addResource( $resPath[$i] );
                        }
                        else{
                            $Acl->addResource( $resPath[$i], $resPath[$i-1] );
                        }
                    }
                }

                for($i=0;$i<count($rolPath);$i++){
                    if ( !$Acl->hasRole( $rolPath[$i] ) ){
                        if($i>0){
                            $Acl->addRole( $rolPath[$i] );
                        }
                        else{
                            $Acl->addRole( $rolPath[$i], $rolPath[$i-1] );
                        }
                    }
                }
                $this->loadFromArray($Acl, $row);
            }
        }
        return $List;
    }


    /**
     * @see library/Rbh/Dao/\Rbh\Dao\DaoInterface#suppress($mapped)
     *
     */
    public function suppress(\Rbh\Dao\MappedInterface $mapped, $withChilds = false)
    {
        throw new \Rbh\Sys\Exception( \Rbh\Sys\Error::FUNCTION_IS_NOT_EXISTING, \Rbh\Sys\Error::ERROR, 'suppress');
    } //End of method


} //End of class
