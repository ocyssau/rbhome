<?php
//%LICENCE_HEADER%

namespace Rbh\Acl;

use Zend\Permissions\Acl;
use Zend\Permissions\Acl\Role;
use Zend\Permissions\Acl\Resource;

/**
 * @brief A rule apply to a resource/role pair.
 */
class Rule implements \Rbh\Dao\MappedInterface
{
	protected $_isLoaded;
	protected $_resourceId;
	protected $_roleId;
	protected $_privilege;
	protected $_rule;
	
	public function __construct( array $properties = null )
	{
		if(is_array($properties)){
			$this->_resourceId = $properties['resourceId'];
			$this->_roleId = $properties['roleId'];
			$this->_rule = $properties['rule'];
			$this->_privilege = $properties['privilege'];
		}
	}
	
	/**
	 * Setter
	 * @param string	$string
	 * @return void
	 */
	public function setRule($string)
	{
		$this->_rule = $string;
	}
	
	/**
	 * Getter
	 * @return string
	 */
	public function getRule()
	{
		return $this->_rule;
	}
	
	/**
	 * Setter
	 * @param string	$string
	 * @return void
	 */
	public function setPrivilege($string)
	{
		$this->_privilege = $string;
	}
	
	/**
	 * Getter
	 * @return string
	 */
	public function getPrivilege()
	{
		return $this->_privilege;
	}
	
	
	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setResourceId($uid)
	{
		$this->_resourceId = $uid;
	}
	
	/**
	 * Getter. 
	 * Return uuid.
	 * @return string
	 */
	public function getResourceId()
	{
		return $this->_resourceId;
	}
	
	/**
	 * Setter
	 * @param string	$uid uuid
	 * @return void
	 */
	public function setRoleId($uid)
	{
		$this->_roleId = $uid;
	}
	
	/**
	 * Getter.
	 * Return uuid.
	 * @return string
	 */
	public function getRoleId()
	{
		return $this->_roleId;
	}
	
	/**
	 * Implements \Rbh\Dao\MappedInterface.
	 * 
	 * @see library/Rbh/Dao/\Rbh\Dao\MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
	
} //End of class
