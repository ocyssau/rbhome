<?php
//%LICENCE_HEADER%

namespace Rbh\Acl;

use Zend\Permissions\Acl\Role;
use Zend\Permissions\Acl\Resource;
use Zend\Permissions\Acl\Acl as ZendPermAcl;
use Rbh\Dao\MappedInterface;

/**
 * @brief Class represent a acl.
 * 
 * Acl extends the \Zend_Acl. See the zend documentation:
 * http://framework.zend.com/manual/en/zend.acl.introduction.html
 * 
 * Acl is define by:
 * 	- resource: a object under access control
 * 	- role: a object asking access to resource
 *  - privilege: what is to do: see, create, suppress...
 *  - rule: ALLOW or DENY
 *  
 *  Acl is a tree where each resource inherit from other, role inherit from other.
 *  
 *  Acl implement singleton pattern and must be instanciate with singleton method:
 *  @code
 *  	$Acl = Acl::singleton();
 *  @endcode
 *  
 *  To re-init the ACL instance, you may use newSingleton:
 *  @code
 *  	$Acl1 = Acl::singleton();
 *  	$Acl2 = Acl::newSingleton();
 *  	assert( $Acl1 != $Acl2 );
 *  @endcode
 *  
 *	See the examples: Rbh/Acl/AclTest.php
 *
 *	@see AclTest
 *
 */
class Acl extends ZendPermAcl implements MappedInterface
{

	/**
	 * 
	 * @var \Rbh\Acl\Acl
	 */
	static protected $_instance;
	
	/**
	 * @var boolean
	 */
	protected $_isLoaded;
	
	/** 
	 * Singleton pattern
	 * 
	 * @return \Rbh\Acl\Acl
	 */
	public static function singleton()
	{
		if( !self::$_instance ){
			self::$_instance = new Acl();
		}
		return self::$_instance;
	} //End of method
	
	/** 
	 * Create a new singleton instance. The previous singleton is lost.
	 * 
	 * @return \Rbh\Acl\Acl
	 */
	public static function newSingleton()
	{
		self::$_instance = new Acl();
		return self::$_instance;
	} //End of method
	
	/**
	 * Implements \Rbh\Dao\MappedInterface.
	 * 
	 * @see library/Rbh/Dao/\Rbh\Dao\MappedInterface#isLoaded($bool)
	 * 
	 */
	public function isLoaded( $bool = null )
	{
		if( $bool === null ){
			return $this->_isLoaded;
		}
		else{
			return $this->_isLoaded = (boolean) $bool;
		}
	}
	
	/**
	 * Get rules
	 * Expose rules, require by DAOs
	 *
	 * @return Array
	 */
	public function getAllRules()
	{
	    return $this->rules['byResourceId'];
	} //End of method
	
} //End of class
