<?php
//%LICENCE_HEADER%

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rbh;

use Rbh\Link;
use Rbh\AnyObject;


/**
 * @brief Composite object of AnyObject and Link
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class AnyUsed extends Any
{
	/**
	 * @var Rbh\AnyObject
	 */
	public $useAny;

	/**
	 * @var Rbh\Link
	 */
	public $useLink;
	
	/**
	 * @param \Rbh\AnyObject $useAny
	 * @param \Rbh\Link $useLink
	 */
	public function __construct(\Rbh\AnyObject $useAny, \Rbh\Link $useLink=null)
	{
		$this->useAny  = $useAny;
		if(!$useLink){
			$this->useLink = new Link();
			$this->useLink->uid = \Rbh\Uuid::newUid();
			$this->useLink->childId = $this->useAny->id;
			$this->useLink->child = $this->useAny;
		}
		else{
			$this->useLink = $useLink;
		}
	}
	
	/**
	 * 
	 * @param \Rbh\AnyObject $Used
	 * @param \Rbh\AnyObject $By
	 */
	public static function newUsed(\Rbh\AnyObject $Used, \Rbh\AnyObject $By)
	{
		$hash = explode('\\', get_class($Used));
		$cname = '\Rbh\Material\Used'.end($hash);
		$o=new $cname($Used);
		$o->useLink->parent = $By;
		$o->useLink->parentId = $By->id;
		return $o;
	}
	
	/**
	 * 
	 * @param string $name
	 */
	public function __get($name){
		if(isset($this->useLink->$name)){
			return $this->useLink->$name;
		}
		else{
			return $this->useAny->$name;
		}
	}

	/**
	 * 
	 * @param string $name
	 * @param unknown_type $value
	 */
	public function __set($name, $value){
		if(isset($this->useLink->$name)){
			$this->useLink->$name=$value;
		}
		else{
			$this->useAny->$name=$value;
		}
	}

	/**
	 * 
	 * @param string $name
	 * @param unknown_type $argument
	 */
	public function __call($name, $argument){
		return call_user_method($name, $this->useLink, $argument);
	}
	
	/**
	 * @param string $name
	 */
	public function getData($name=null){
		return $this->useLink->data[$name];
	}
}
