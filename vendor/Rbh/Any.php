<?php
//%LICENCE_HEADER%

namespace Rbh;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Any
{
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 * @var string
	 */
	public $uid;
	
	
	public function __construct($properties=null){
		if(is_array($properties)){
			foreach($properties as $name=>$value){
				$this->$name = $value;
			}
		}
	}
	

	
	
	
	
	
	
	
	/**
	 * Generate a uniq identifier and set to object
	 *
	 * @return Any
	 */
	public function newUid()
	{
	    $this->_uid = Uuid::newUid();
	    return $this;
	} // End of method
	
	/**
	 * Get the uid
	 *
	 * @return String
	 */
	public function getUid()
	{
	    return $this->_uid;
	} //End of function
	
	/**
	 * Get the name
	 *
	 * @return String
	 */
	public function getName()
	{
	    return $this->_name;
	} //End of function
	
	/**
	 * Set the name
	 *
	 * @param string
	 * @return Any
	 */
	public function setName($string)
	{
	    $this->_name = $string;
	    Signal::emit($this, Signal::SIGNAL_POST_SETNAME);
	    return $this;
	} //End of function
	
	/**
	 * Get the label
	 *
	 * @return String
	 */
	public function getLabel()
	{
	    return $this->_label;
	} //End of function
	
	/**
	 * Set the label
	 *
	 * @param string
	 * @return Any
	 */
	public function setLabel($string, $format=true)
	{
	    if($format){
	        $string = str_replace ( ' ', '_', $string );
	        $string = str_replace ( '-', '_', $string );
	        $string = preg_replace ( '/[^0-9A-Za-z\_]/', '', $string );
	        $this->_label = $string;
	    }
	    else{
	        $this->_label = $string;
	    }
	    return $this;
	} //End of function
	
	/**
	 * Set the uid
	 *
	 * @param string
	 */
	public function setUid($uid)
	{
	    $this->_uid = Uuid::format($uid);
	    return $this;
	} //End of function
	
	/**
	 * @return 	string
	 */
	public function serialize()
	{
	    $properties = array();
	    foreach($this as $name=>$val){
	        if($name == '_plugins'){
	            continue;
	        }
	        if( !is_object($val) && !is_null($val) ){
	            $properties[$name] = $val;
	        }
	        else{
	            $this->$name = null;
	        }
	    }
	    return serialize($properties);
	}
	
	/**
	 * @return 	string
	 */
	public function xmlSerialize()
	{
	    $properties = array();
	    $SXml = new \SimpleXMLElement('<properties/>');
	    foreach( $this as $name=>$val ){
	        if($name == '_plugins'){
	            continue;
	        }
	        if( !is_object($val) && !is_null($val) ){
	            $name = trim($name, '_');
	            if( is_array($val) ){
	                $e = $SXml->addChild($name, '');
	                foreach($val as $keyj=>$valj){
	                    $e->addChild($keyj, $valj);
	                }
	            }
	            else{
	                $SXml->addChild($name, $val);
	            }
	        }
	    }
	    return $SXml->asXml();
	}
	
	/**
	 * @return 	string
	 */
	public function jsonSerialize()
	{
	    $properties = array();
	    foreach($this as $name=>$val){
	        if( !is_object($val) && !is_null($val) ){
	            $properties[$name] = $val;
	        }
	        else{
	            $this->$name = null;
	        }
	    }
	    return json_encode($properties);
	}
	
	/**
	 * @param string
	 * @return self
	 */
	public function unserialize( $serialized )
	{
	    foreach(unserialize( $serialized ) as $k=>$v){
	        $this->$k = $v;
	    }
	}
	
	/**
	 * Convert current object to array
	 * @return 	array
	 */
	public function toArray()
	{
	    $a = array();
	    foreach($this as $name=>$val){
	        if($name == '_plugins'){
	            continue;
	        }
	        $outname = trim($name, '_');
	        if( !is_object($val) && !is_null($val) ){
	            $a[$outname] = $val;
	        }
	    }
	    return $a;
	}
}
