<?php
//%LICENCE_HEADER%

namespace Rbh;

/** 
 * @brief Abstract generic type for all Rbplm classes.
 * 
 */
abstract class Rbh
{
	
	/**
	 * 
	 * @var Zend_Log
	 */
	protected static $logger;
	
	/**
	 * 
	 * Current version of Rbplm librairy
     * @constant String
	 */
    const VERSION = '%VERSION%';
    
    /**
     * 
     * Build number of Ranchbe
     * @constant String
     */
    const BUILD = '%BUILD%';
    
    /**
     * 
     * Copyright
     * @constant String
     */
    const COPYRIGHT = '&#169;%COPYRIGHT%';
    
    /**
     * Logger.
     * Create a new log message. If none logger is find, none exception is throws.
     * 
     * @return void
     */
    public static function log($message)
    {
    	if( Rbh::$logger ){
    		Rbh::$logger->log($message);
    	}
    }
    
    /**
     * Setter for the logger instance.
     * $logger may be a instance of Zend_Log but it is probably better 
     * to use a instance of Rbplm_Sys_Logger.
     * 
     * @param	Zend_Log	$logger
     * @return 	void
     */
    public static function setLogger( Zend_Log $logger )
    {
    	Rbh::$logger = $logger;
        return $this;
    }
    
    /**
     * Getter for the logger.
     * 
     * @return 	Zend_Log
     */
    public static function getLogger()
    {
    	return Rbh::$logger;
    }
    
	/**
	 * Return the full version and build identifier of the Rbplm Api.
	 * @return string
	 */
	public static function getApiFullVersion(){
		return Rbh::VERSION . '-' . Rbh::BUILD;
	}
	
	/**
	 * Return the version identifier of Rbplm librairy.
	 * @return string
	 */
	public static function getApiVersion(){
		return Rbh::VERSION;
	}
	
	/**
	 * Return the Rbplm Api copyright.
	 * @return string
	 */
	public static function getApiCopyright(){
		return Rbh::COPYRIGHT;
	}
    
} //End of class
