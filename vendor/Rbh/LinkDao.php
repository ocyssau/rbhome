<?php 
//%LICENCE_HEADER%

namespace Rbh;

use \Rbh\Dao\MappedInterface;
use \Rbh\Dao\Connexion;
use \Rbh\Sys\Exception As Exception;
use \Rbh\Sys\Error As Error;
use \Rbh\Signal As Signal;
use \Rbh\Rbh As Rbh;

class LinkDao
{
	/**
	 *
	 * @var \PDO
	 */
	protected $connexion;

	/**
	 * Table where write the basics properties common to all objects extends Rbplm_Sheet.
	 * @var string
	 */
	public $table = 'anyobject_links';

	/**
	 *
	 * @var unknown_type
	 */
	private $insertStmt;
	private $updateStmt;

	public function __construct( $conn = null )
	{
		/*
		 if( $conn ){
		$this->connexion = $conn;
		}
		else{
		$this->connexion = \Rbh\Dao\Connexion::get();
		}
		*/
		$this->connexion = \Rbh\Dao\Connexion::get();
	} //End of function


	/**
	 * $mapped \Rbh\Link or array of \Rbh\Link
	 *
	 */
	public function insert($mapped, array $select = null)
	{
		$table = $this->table;

		if(!$this->insertStmt){
			$sql = 'INSERT INTO '.$table.' (luid, lparent, lchild, lname, lindex, ldata)';
			$sql .= ' VALUES (:uid, :parentId, :childId, :name, :lindex, :data)';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->connexion->beginTransaction();
			if(is_array($mapped)){
				foreach($mapped as $map){
					$this->insertStmt->execute($map->bind());
				}
			}
			else{
				$this->insertStmt->execute($mapped->bind());
			}
			$this->connexion->commit();
		}
		catch(Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	} //End of function

	/**
	 */
	public function update($mapped, array $select = null )
	{
		$table = $this->table;

		if(!$this->updateStmt){
			$sql = 'UPDATE '.$table.' SET
			lparent=:parentId,
			lchild=:childId,
			lname=:name,
			ldata=:data,
			lindex=:lindex
			WHERE luid=:uid;';
			$this->updateStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->connexion->beginTransaction();
			if(is_array($mapped)){
				foreach($mapped as $map){
					$this->updateStmt->execute($map->bind());
				}
			}
			else{
				$this->updateStmt->execute($mapped->bind());
			}
			$this->connexion->commit();
		}
		catch(Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	} //End of function

	/**
	 */
	public function update2($filter, $properties )
	{
		$table = $this->table;
		
		//$sets=implode(array_keys($properties), ',');
		//$setv=implode(array_values($properties), ',');
		$set='';
		foreach($properties as $pn=>$pv){
			$set .= $pn.' = '.$pv .',';
		}
		$set=trim($set, ',');
		
		$sql = 'UPDATE '.$table.' SET '.$set.' WHERE '.$filter;
		//echo $sql;
		$this->connexion->exec($sql);
		return $this;
	} //End of function


	/**
	 *
	 */
	public function suppress($filter)
	{
		$table = $this->table;
		$sql = 'DELETE FROM '.$table . " WHERE $filter";
		$suppressStmt = $this->connexion->prepare($sql);
		$suppressStmt->execute();
		$this->connexion->commit();
		return $this;
	} //End of function

	/**
	 * Suppress all links without child or parent
	 */
	public function cleanOrpheanLinks()
	{
		$table = $this->table;
		$sql = "DELETE FROM $table as l WHERE l.lchild NOT IN(SELECT id FROM anyobject) OR l.lparent NOT IN (SELECT id FROM anyobject)";
		$stmt = $this->connexion->prepare($sql);
		$stmt->execute();
		return $this;
	}

	/**
	 *
	 */
	public static function loadFromArray(&$mapped, array $row, $fromApp = false)
	{
		$mapped->uid = $row['luid'];
		$mapped->name = $row['lname'];
		$mapped->parentId = $row['lparent'];
		$mapped->childId = $row['lchild'];
		$mapped->data = json_decode($row['ldata']);
		$mapped->path = $row['lpath'];
		$mapped->lindex = $row['lindex'];
		$mapped->level = $row['level'];
		return $mapped;
	} //End of function

	/**
	 *
	 */
	public function loadChildren(\Rbh\AnyObject $mapped)
	{
		if(!$this->loadChildrenStmt){
			$sql='SELECT * FROM anyobject_links WHERE lparent=:parentId';
			$this->loadChildrenStmt = $this->connexion->prepare($sql);
			$this->loadChildrenStmt->setFetchMode(\PDO::FETCH_ASSOC);
		}
		$this->loadChildrenStmt->execute(array(':parentId'=>$mapped->id));
		while($row = $this->loadChildrenStmt->fetch()){
			$lnk = new Link();
			$this->loadFromArray($lnk, $row);
			$mapped->links[$lnk->uid]=$lnk;
		}
		return $mapped;
	} //End of function


	/**
	 * Factory method for instanciate List object.
	 *
	 * @throws \Rbh\Sys\Exception
	 * @return Rbplm_Dao_Pg_List
	 */
	public function newList()
	{
		$list=new \Rbh\Dao\Pg\DaoList();
		$list->table = $this->table;
		return $list;
	} //End of method


} //End of class
