<?php

namespace Rbh\Measure;

/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category  Zend
 * @package   Zend_Measure
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 * @version   $Id: Abstract.php 23775 2011-03-01 17:25:24Z ralph $
 */

/**
 * Abstract class for all measurements
 *
 * @category   Zend
 * @package    Zend_Measure
 * @subpackage Rbh\Measure\Abstract
 * @copyright  Copyright (c) 2005-2011 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
abstract class AbstractMeasure
{
    /**
     * Plain value in standard unit
     *
     * @var string $_value
     */
    protected $_value;

    /**
     * Original type for this unit
     *
     * @var string $_type
     */
    protected $_type;

    /**
     * Unit types for this measurement
     */
    protected $_units = array();

    /**
     * Rbh\Measure\Abstract is an abstract class for the different measurement types
     *
     * @param  mixed       $value  Value as string, integer, real or float
     * @param  int         $unit   OPTIONAL a measure type f.e. Rbh\Measure\Length::METER
     * @throws Rbh\Measure\Exception
     */
    public function __construct($value, $unit = null)
    {
        if ($unit === null) {
            $unit = $this->_units['STANDARD'];
        }

        if (isset($this->_units[$unit]) === false) {
            throw new Exception("Type ($unit) is unknown");
        }
        
        $this->_value = $value;
        $this->_type = $unit;
    }

    /**
     * Returns the internal value
     *
     * @return integer|string
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Set a new value
     *
     * @param  integer|string      $value   Value as string, integer, real or float
     * @param  string              $unit    OPTIONAL A measure type f.e. Rbh\Measure\Length::METER
     * @throws Rbh\Measure\Exception
     * @return Rbh\Measure\Abstract
     */
    public function setValue($value, $unit = null)
    {
        $unit = $this->_units['STANDARD'];

        if (empty($this->_units[$unit])) {
            throw new Exception("Type ($unit) is unknown");
        }

        $this->_value = $value;
        $this->setUnit($unit);
        return $this;
    }

    /**
     * Returns the original type
     *
     * @return type
     */
    public function getUnit()
    {
        return $this->_type;
    }

    /**
     * Set a new type, and convert the value
     *
     * @param  string $unit New type to set
     * @throws Rbh\Measure\Exception
     * @return Rbh\Measure\Abstract
     */
    public function setUnit($unit)
    {
        if (empty($this->_units[$unit])) {
            throw new Exception("Type ($unit) is unknown");
        }

        if (empty($this->_type)) {
            $this->_type = $unit;
        }
        else {
            // Convert to standard value
            $value = $this->_value;
            if (is_array($this->_units[$this->getUnit()][0])) {
                foreach ($this->_units[$this->getUnit()][0] as $key => $found) {
                    switch ($key) {
                        case "/":
                            if ($found != 0) {
                                $value = bcdiv($value, $found, 25);
                            }
                            break;
                        case "+":
                            $value = bcadd($value, $found, 25);
                            break;
                        case "-":
                            $value = bcsub($value, $found, 25);
                            break;
                        default:
                            $value = bcmul($value, $found, 25);
                            break;
                    }
                }
            } 
            else {
                $value = bcmul($value, $this->_units[$this->getUnit()][0], 25);
            }

            // Convert to expected value
            if (is_array($this->_units[$unit][0])) {
                foreach (array_reverse($this->_units[$unit][0]) as $key => $found) {
                    switch ($key) {
                        case "/":
                            $value = bcmul($value, $found, 25);
                            break;
                        case "+":
                            $value = bcsub($value, $found, 25);
                            break;
                        case "-":
                            $value = bcadd($value, $found, 25);
                            break;
                        default:
                            if ($found != 0) {
                                $value = bcdiv($value, $found, 25);
                            }
                            break;
                    }
                }
            } else {
                $value = bcdiv($value, $this->_units[$unit][0], 25);
            }

            $slength = strlen($value);
            $length  = 0;
            for($i = 1; $i <= $slength; ++$i) {
                if ($value[$slength - $i] != '0') {
                    $length = 26 - $i;
                    break;
                }
            }

            $this->_value = round($value, $length);
            $this->_type  = $unit;
        }
        return $this;
    }

    /**
     * Compare if the value and type is equal
     *
     * @param  Rbh\Measure\Abstract $object object to compare
     * @return boolean
     */
    public function equals($object)
    {
        if ((string) $object == $this->toString()) {
            return true;
        }

        return false;
    }

    /**
     * Returns a string representation
     *
     * @param  integer            $round  (Optional) Runds the value to an given exception
     * @param  string|Zend_Locale $locale (Optional) Locale to set for the number
     * @return string
     */
    public function toString($round = -1, $locale = null)
    {
        if ($locale === null) {
            $locale = $this->_locale;
        }
        return $this->getValue($round, $locale) . ' ' . $this->_units[$this->getUnit()][1];
    }

    /**
     * Returns a string representation
     *
     * @return string
     */
    public function __toString()
    {
    	return $this->getValue() . ' ' . $this->_units[$this->getUnit()][1];
    }

    /**
     * Returns the conversion list
     *
     * @return array
     */
    public function getConversionList()
    {
        return $this->_units;
    }

    /**
     * Alias function for setUnit returning the converted unit
     *
     * @param  string             $unit   Constant Type
     * @param  integer            $round  (Optional) Rounds the value to a given precision
     * @return string
     */
    public function convertTo($unit, $round = 2)
    {
        $this->setUnit($unit);
        return $this->toString($round, $locale);
    }

    /**
     * Adds an unit to another one
     *
     * @param  Rbh\Measure\Abstract $object object of same unit type
     * @return Rbh\Measure\Abstract
     */
    public function add($object)
    {
        $object->setUnit($this->getUnit());
        $value  = $this->getValue(-1) + $object->getValue(-1);

        $this->setValue($value, $this->getUnit(), $this->_locale);
        return $this;
    }

    /**
     * Substracts an unit from another one
     *
     * @param  Rbh\Measure\Abstract $object object of same unit type
     * @return Rbh\Measure\Abstract
     */
    public function sub($object)
    {
        $object->setUnit($this->getUnit());
        $value  = $this->getValue(-1) - $object->getValue(-1);

        $this->setValue($value, $this->getUnit(), $this->_locale);
        return $this;
    }

    /**
     * Compares two units
     *
     * @param  Rbh\Measure\Abstract $object object of same unit type
     * @return boolean
     */
    public function compare($object)
    {
        $object->setUnit($this->getUnit());
        $value  = $this->getValue(-1) - $object->getValue(-1);

        if ($value < 0) {
            return -1;
        } else if ($value > 0) {
            return 1;
        }

        return 0;
    }
}
