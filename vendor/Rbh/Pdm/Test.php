<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm;

use Rbh\Pdm\Effectivity;
use Rbh\Pdm\Product;
use Rbh\Pdm\Configuration;
use Rbh\Measure;
use Rbh\Pdm\Usage;

/**
 * Test class for Pdm module
 */
class Test extends \Rbh\Test\Test
{
	/**
	 * @access protected
	 */
	protected $object;


	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
	}


	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}

	
	public function Test_PdmTest()
	{
		$parentOu = new \Rbh\Org\Unit( array('name'=>'myParentOu'), \Rbh\Org\Root::singleton() );
		
		$BaseProduct001 = new Product( array('name'=>'BaseProduct001', 'description'=>'base product'), $parentOu );
		$ProductVersion001 = $BaseProduct001->newVersion( array('name'=>'ProductVersion001', 'description'=>'init'), $parentOu );
		assert( $ProductVersion001 instanceof Product\Version );
		assert( $ProductVersion001->name == 'ProductVersion001');
		assert( $ProductVersion001->description == 'init');
		
		assert( $BaseProduct001->getVersions() instanceof \Rbh\Link\Collection );
		assert( $BaseProduct001->getVersions()->getByIndex(0) === $ProductVersion001 );
		assert( $BaseProduct001->lastVersionId == 1 );
		
		$PhysicalProperties = $ProductVersion001->getPhysicalProperties();
		$PhysicalProperties->setVolume( 20, Measure\Volume::CUBIC_METER );
		$PhysicalProperties->setWeight( 30, Measure\Weight::KILOGRAM );
		$PhysicalProperties->setGravityCenter( 10, 10, 10, Measure\Length::MILLIMETER );
		$PhysicalProperties->setWetSurface( 30, Measure\Area::SQUARE_MILLIMETER );
		assert( $PhysicalProperties instanceof Product\PhysicalProperties );
		
		$ProductVersion001Instance = new Product\Instance( array('name'=>'ProductVersion001Instance.1'), $parentOu, $ProductVersion001 );
		$ProductVersion001Instance->getUsage()->setQuantity(4);
		$Position2 = $ProductVersion001Instance->getPosition();
		$Position2[9] = -120;
		$Position2[10] = 150;
		$Position2[11] = 200;
		$ProductVersion001Instance->nomenclature = 11;
		
		//assert( $ProductVersion001Instance->getPath() == '/RanchbePlm/myParentOu/ProductVersion001Instance1' );
		//assert( $ProductVersion001Instance->getPath(false, '_name') == '/RanchbePlm/myParentOu/ProductVersion001Instance.1' );
		assert( $ProductVersion001Instance->nomenclature == 11 );
		assert( $ProductVersion001Instance->getPosition()->offsetGet(9) === -120 );
		assert( $ProductVersion001Instance->getPosition()->offsetGet(10) === 150 );
		assert( $ProductVersion001Instance->getPosition()->offsetGet(11) === 200 );
		assert( $ProductVersion001Instance->getUsage()->getQuantity() == 4 );
		assert( $ProductVersion001Instance->getUsage()->getType() == Usage::TYPE_QUANTIFIED );
		assert( $ProductVersion001Instance->getUsage()->getUnit() == Usage::UNIT_SCALAR );
	}
	
	
	/**
	 * Use case create a Product and a Product\Version and a Product\Instance of this version in a OrganizationalUnit
	 * and put it in a Root Product\Version.
	 */
	public function Test_PdmTutoriel()
	{
		$parentOu = new \Rbh\Org\Unit( array('name'=>'myParentOu'), \Rbh\Org\Root::singleton() );

		/*
		 * Create a basic product definition of a wheel
		 */
		$Wheel = new Product( array(), $KKCarProductContext );
		$Wheel->setName( 'wheel' );
		$Wheel->description = '15pouce wheel alu for kekes';

		/*
		 * Create a version for wheel
		 */
		$Wheel001 = $Wheel->newVersion( 'wheel001', $KKCarProductContext );
		$Wheel001->description = 'Initial version';

		/*
		 * Affect mechanicals properties to wheel
		 */
		$PhysicalProperties = $Wheel001->getPhysicalProperties();
		$PhysicalProperties->setVolume( 20, Measure\Volume::CUBIC_METER );
		$PhysicalProperties->setWeight( 30, Measure\Weight::KILOGRAM );
		$PhysicalProperties->setGravityCenter( 10, 10, 10, Measure\Length::MILLIMETER );
		$PhysicalProperties->setWetSurface( 30, Measure\Area::SQUARE_MILLIMETER );

		/*
		 * Affect a material to wheel
		 */
		$Material = new Product\Material(array('name'=>'AU4G'), $parentOu);
		$Material->setDensity(2450, Measure\Density::GRAM_PER_CUBIC_METER);
		$Wheel001->setMaterial($Material);

		/*
		 * create a car sheet metal body
		 */
		$SheetMetalBody = new Product( array('name'=>'sheetMetalBody', 'description'=>'The car body'), $KKCarProductContext );
		$SheetMetalBody001 = $SheetMetalBody->newVersion( 'sheetMetalBody001', $KKCarProductContext );
		$SheetMetalBody001->description = 'Initial version';
		$SheetMetalBody001->setMaterial($Material);
		$PhysicalProperties = $SheetMetalBody001->getPhysicalProperties();
		$PhysicalProperties->setVolume( 3, Measure\Volume::CUBIC_METER );
		$PhysicalProperties->setWeight( 400, Measure\Weight::KILOGRAM );
		$PhysicalProperties->setGravityCenter( 100, 0, 300, Measure\Length::MILLIMETER );
		$PhysicalProperties->setWetSurface( 30000, Measure\Area::SQUARE_MILLIMETER );

		/*
		 * Create a steering wheel
		 */
		$SteeringW = new Product( array('name'=>'steering_wheel', 'description'=>'The car steering wheel'), $KKCarProductContext );
		$SteeringW001 = $SteeringW->newVersion( 'steeringw001', $KKCarProductContext );
		$SteeringW001->description = 'The car steering wheel version 1';
		
		
		/*
		 * Create steering column
		 */
		$SteeringColumn = new Product( array('name'=>'steering_column', 'description'=>'The car steering column'), $KKCarProductContext );
		$SteeringColumn001 = $SteeringColumn->newVersion( array('name'=>'steeringc001', 'description'=>'The car steering column version 1'), $KKCarProductContext );
		
		
		/*
		 * Create chassis
		 */
		$Chassis = new Product( array('name'=>'chassis', 'description'=>'The car chassis'), $KKCarProductContext );
		$Chassis001 = $Chassis->newVersion( array('name'=>'chassis001', 'description'=>'The car chassis version 1'), $KKCarProductContext );
		
		/*
		 * create seats
		 */
		$Seat = new Product( array('name'=>'seats', 'description'=>'The car seats'), $KKCarProductContext );
		$Seat001 = $Seat->newVersion( array('name'=>'seats001', 'description'=>'The car seats version 1'), $KKCarProductContext );
		
		/*
		 * create engine option 1
		 */
		$Engine950cc = new Product( array('name'=>'engine950cc', 'description'=>'The car engine of 950cc'), $KKCarProductContext );
		$Engine950cc001 = $Seat->newVersion( array('name'=>'engine950cc001', 'description'=>'The car engine of 950cc version 1'), $KKCarProductContext );
		
		/*
		 * create engine option 2
		 */
		$Engine1200cc = new Product( array('name'=>'engine1200cc', 'description'=>'The car engine of 1200cc'), $KKCarProductContext );
		$Engine1200cc001 = $Seat->newVersion( array('name'=>'engine1200cc001', 'description'=>'The car engine 1200cc version 1'), $KKCarProductContext );
		
		/*
		 * create engine option 3
		 */
		$Engine1600cc = new Product( array('name'=>'engine1600ccV6', 'description'=>'The car engine of 1600cc V6'), $KKCarProductContext );
		$Engine1600cc001 = $Seat->newVersion( array('name'=>'engine1600ccV6', 'description'=>'The car engine 1600cc V6 version 1'), $KKCarProductContext );
		
		/*
		 * Create a root product for car
		 */
		$Car = new Product( array('name'=>'car'), $KKCarProductContext );
		$Car001 = $Car->newVersion( 'car001', $KKCarProductContext );
		$Car001->description = 'A car with 4 wheels and a body for keke ';
		
		/*Create a dashboard*/
		$Dashboard = new Product( array('name'=>'dashboard', 'description'=>'The car dashboard'), $KKCarProductContext );
		$Dashboard001 = $Dashboard->newVersion( array('name'=>'dashboard001', 'description'=>'The car engine dashboard 1'), $KKCarProductContext );

		/*
		 * get versions collection of a product
		 */
		$Versions = $Wheel->getVersions();
		
		
		/*
		 * Now create this structure:
		 * 
		 * Car001/
		 * 		Chassis001/
		 * 				Wheels001
		 * 				SteeringColumns001
		 * 				Engine001
		 * 		Body001/
		 * 				SheetMetalBody001
		 * 				Seats
		 * 				Dashboard
		 * 
		 */
		

		$Car001Instance = new Product\Instance( array('name'=>$Car001->getName() . '.1'), $ProductDefinitionContext );
		
		/*Create chassis instance*/
		$Chassis001Instance = new Product\Instance( array('name'=>$Chassis001->getName() . '.1'), $Car001Instance, $Chassis001 );
		
		/*
		 * Create instance for wheels
		 */
		$Wheel001Instance = new Product\Instance( array('name'=>$Wheel001->getName() . '.1'), $Chassis001Instance );
		$Wheel001Instance->setProduct($Wheel001);
		$Wheel001Instance->getUsage()->setQuantity(4);
		$Position2 = $Wheel001Instance->getPosition();
		$Position2[9] = -120;
		$Position2[10] = 150;
		$Position2[11] = 200;
		$Wheel001Instance->nomenclature = 11;
		$Wheel001Instance->description = 'Roues arrieres';
		
		
		/*Create a empty instance to group steering assembly*/
		$SteeringAssyInstance = new Product\Instance( array('name'=>'steeringAssy.1'), $Chassis001Instance );
		
		/*Steering column instance*/
		$Steering001ColumnInstance = new Product\Instance( array('name'=>$SteeringColumn001->getName() . '.1'), $SteeringAssyInstance, $SteeringColumn001 );
		
		/*Create instance for steering*/
		$Steering001WInstance = new Product\Instance( array('name'=>$SteeringW001->getName() . '.1', 'nomenclature'=>12), $SteeringAssyInstance, $SteeringW001 );
		
		/*Engine 950cc*/
		$Engine950cc001Instance = new Product\Instance( array('name'=>$Engine950cc001->getName() . '.1'), $Chassis001Instance, $Engine950cc001 );
		
		/*Engine 1600cc V6*/
		$Engine1600cc001Instance = new Product\Instance( array('name'=>$Engine1600cc001->getName() . '.1'), $Chassis001Instance, $Engine1600cc001 );
		
		/* Create body instance
		 * This is a little special because it has not assiociated Product\Version.
		 */
		$Body001Instance = new Product\Instance( array('name'=>'body001.1'), $Car001Instance );
		
		/*Sheet metal body*/
		$SheetMetalBody001Instance = new Product\Instance( array('name'=>$SheetMetalBody001->getName() . '.1'), $Body001Instance, $Body001 );

		/*Seats*/
		$Seat001Instance = new Product\Instance( array('name'=>$Seat001->getName() . '.1'), $Body001Instance, $Seat001 );
		
		/*Dashboard*/
		$Dashboard001Instance = new Product\Instance( array('name'=>$Dashboard001->getName() . '.1'), $Body001Instance, $Dashboard001 );
		
		
		/*Just for fun, display the tree*/
		/*
		$ChildrenIterator = new \RecursiveIteratorIterator( $ProductDefinitionContext->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		echo $node->getPath() . "\n";
    	}
		echo '--------------------------------------------------------------------' . "\n";
		*/
		
		/*
		 * We may too explore the concept.
		 * We can see that there are 3 engines for this car.
		 * We must apply a configuration to select the engine.
		 */
		/*
		$ChildrenIterator = new \RecursiveIteratorIterator( $KKCarProductContext->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
		echo '--------------------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if(is_a($node, 'Product\Version')){
	    		echo $node->getPath() .' (' . get_class($node) . ") \n";
    		}
    	}
		echo '--------------------------------------------------------------------' . "\n";
		*/
    	
		/*
		 * CONFIGURATION
		 * 
		 */
	}
	
	
	/**
     * Example of traduction of STEP shema into Rbh;
	 */
	public function Test_Steptutorial()
	{
		
		$parentOu = new \Rbh\Org\Unit( array('name'=>'myParentOu'), \Rbh\Org\Root::singleton() );
		
		/* 
		 * Product context is just a OU for products
		 * #8=APPLICATION_CONTEXT('');
		 * #9=PRODUCT_CONTEXT('', #8, '');
		 */
		 /* Base product
		  * #10=PRODUCT('PC-0023', 'PC system', $, (#9));
		  */
		 $product10 = new Product( array('number'=>'PC-0023', 'name'=>'PC system'), $pContext9 );
		/*Product Version. Put in OU $pContext9, same that the base product, but may be in other place
		 * #11=PRODUCT_DEFINITION_FORMATION('D', 'description of PC-0023,D', #10);
		 */
		 $productVersion11 = $product10->newVersion('D', $pContext9);
		 $productVersion11->description = 'description of PC-0023,D';
		 /* @todo Ignore category for the moment but must be implement in futur versions.
		 * #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10, #18, #20, #21, #22, #23));
		 */
		  
		 /*  Create other base products and versions.
		 * #18=PRODUCT('MB-0013', 'Mainboard', $, (#9));
		 * #19=PRODUCT_DEFINITION_FORMATION('F', 'description of MB-0013,F', #18);
		 * #20=PRODUCT('PSU-0009', 'Power supply unit', 'Power supply unit 220V', (#9));
		 * #21=PRODUCT('PSU-0011', 'Power supply unit', 'Power supply unit 110V', (#9));
		 * #22=PRODUCT('PR-0133', 'CPU', 'Pentium II 233', (#9));
		 * #23=PRODUCT('PR-0146', 'CPU', 'Pentium II 266', (#9));
		 * #24=PRODUCT_DEFINITION_FORMATION('A', 'description of PSU-0009,A', #20);
		 * #25=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0009,B', #20);
		 * #26=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0011,B', #21);
		 * #27=PRODUCT_DEFINITION_FORMATION('A', 'description of PR-0133,A', #22);
		 * #28=PRODUCT_DEFINITION_FORMATION('C', 'description of PR-0146,C', #23);
		 */
		 $product18 = new Product( array('number'=>'MB-0013', 'name'=>'Mainboard'), $pContext9 );
		 $productVersion19 = $product18->newVersion('F', $pContext9);
		 $productVersion19->description = 'description of MB-0013,F';
		 
		 $product20 = new Product( array('number'=>'PSU-0009','name'=>'Power supply unit', 'description'=>'Power supply unit 220V'), $pContext9 );
		 $productVersion24 = $product20->newVersion('A', $pContext9);
		 $productVersion25 = $product20->newVersion('B', $pContext9);
		 
		 $product21 = new Product( array('number'=>'PSU-0011','name'=>'Power supply unit', 'description'=>'Power supply unit 110V'), $pContext9 );
		 $productVersion26 = $product21->newVersion('B', $pContext9);
		 
		 $product22 = new Product( array('number'=>'PR-0133','name'=>'CPU', 'description'=>'Pentium II 233'), $pContext9 );
		 $productVersion27 = $product22->newVersion('B', $pContext9);
		 
		 $product23 = new Product( array('number'=>'PR-0146','name'=>'CPU', 'description'=>'Pentium II 266'), $pContext9 );
		 $productVersion28 = $product23->newVersion('A', $pContext9);
		 
		 
		 /* Define the context for apply a view
		 * #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
		 */
		 
		 /* Create instance for product versions
		 * #29=PRODUCT_DEFINITION('pc_v1', 'design view on PC-0023,D', #11, #12);
		 * #30=PRODUCT_DEFINITION('mb_v1', 'design view on MB-0013,F', #19, #12);
		 * #31=PRODUCT_DEFINITION('psu1A_v1', 'design view on PSU-0009,A', #24, #12);
		 * #33=PRODUCT_DEFINITION('psu1B_v1', 'design view on PSU-0009,B', #25, #12);
		 * #34=PRODUCT_DEFINITION('psua_v1', 'design view on PSU-0011,B', #26, #12);
		 * #35=PRODUCT_DEFINITION('pr1_v1', 'design view on PR-0133,A', #27, #12);
		 * #36=PRODUCT_DEFINITION('pr2_v1', 'design view on PR-0146,C', #28, #12);
		 * #37=NEXT_ASSEMBLY_USAGE_OCCURRENCE('mb-u1', 'single instance usage', $, #29, #30, $);
		 * #38=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u1', 'single instance usage', $, #29, #31, $);
		 * #39=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u2', 'single instance usage', $, #29, #33, $);
		 * #40=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u3', 'single instance usage', $, #29, #34, $);
		 * #41=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u1', 'single instance usage', $, #30, #35, $);
		 * #42=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u2', 'single instance usage', $, #30, #36, $);
		 */
		
		//#29=PRODUCT_DEFINITION('pc_v1', 'design view on PC-0023,D', #11, #12);
		$ProductInstance29 = new Product\Instance( array('name'=>'pc_v1', 'description'=>'design view on PC-0023,D') );
		$ProductInstance29->setProduct($productVersion11);
		
		//#30=PRODUCT_DEFINITION('mb_v1', 'design view on MB-0013,F', #19, #12);
		$ProductInstance30 = new Product\Instance( array('name'=>'mb_v1', 'description'=>'design view on MB-0013,F') );
		$ProductInstance30->setProduct($productVersion19);
		
		//#31=PRODUCT_DEFINITION('psu1A_v1', 'design view on PSU-0009,A', #24, #12);
		$ProductInstance31 = new Product\Instance( array('name'=>'psu1A_v1', 'description'=>'design view on PSU-0009,A') );
		$ProductInstance31->setProduct($productVersion24);
		
		//#33=PRODUCT_DEFINITION('psu1B_v1', 'design view on PSU-0009,B', #25, #12);
		$ProductInstance33 = new Product\Instance( array('name'=>'psu1B_v1', 'description'=>'design view on PSU-0009,B') );
		$ProductInstance33->setProduct($productVersion25);
		
		//#34=PRODUCT_DEFINITION('psua_v1', 'design view on PSU-0011,B', #26, #12);
		$ProductInstance34 = new Product\Instance( array('name'=>'psua_v1', 'description'=>'design view on PSU-0011,B') );
		$ProductInstance34->setProduct($productVersion26);
		
		//#35=PRODUCT_DEFINITION('pr1_v1', 'design view on PR-0133,A', #27, #12);
		$ProductInstance35 = new Product\Instance( array('name'=>'pr1_v1', 'description'=>'design view on PR-0133,A') );
		$ProductInstance35->setProduct($productVersion27);
		
		//#36=PRODUCT_DEFINITION('pr2_v1', 'design view on PR-0146,C', #28, #12);
		$ProductInstance36 = new Product\Instance( array('name'=>'pr2_v1', 'description'=>'design view on PR-0146,C') );
		$ProductInstance36->setProduct($productVersion28);
		
		/*
		 * Set the relations
		 * #37=NEXT_ASSEMBLY_USAGE_OCCURRENCE('mb-u1', 'single instance usage', $, #29, #30, $);
		 * #38=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u1', 'single instance usage', $, #29, #31, $);
		 * #39=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u2', 'single instance usage', $, #29, #33, $);
		 * #40=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u3', 'single instance usage', $, #29, #34, $);
		 * #41=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u1', 'single instance usage', $, #30, #35, $);
		 * #42=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u2', 'single instance usage', $, #30, #36, $);
		 */
		$ProductInstance30->setParent($ProductInstance29);
		$ProductInstance31->setParent($ProductInstance29);
		$ProductInstance33->setParent($ProductInstance29);
		$ProductInstance34->setParent($ProductInstance29);
		$ProductInstance35->setParent($ProductInstance30);
		$ProductInstance36->setParent($ProductInstance30);
		
    	echo 'Display all parent until root of a instance--------------------------------------------------------' . "\n";
    	/*
    	$Iterator = new \ParentIterator($ProductInstance34);
    	foreach($Iterator as $node){
    		var_dump( $node->getPath(), $Iterator->key(), get_class($node) );
    	}
    	
    	echo 'Iterate all tree from $parentOu--------------------------------------------------------' . "\n";
		$ChildrenIterator = new \RecursiveIteratorIterator( $parentOu->getChild() , \RecursiveIteratorIterator::SELF_FIRST);
    	foreach($ChildrenIterator as $node){
    		echo 'NodePath:' . $node->getPath() . "\n";
    		echo 'Level:' . $ChildrenIterator->getDepth() . "\n";
    		echo 'Class:' . get_class($node) . "\n";
    		echo '----------------------------------' . "\n";
    		//var_dump( $node->getPath(), $ChildrenIterator->getDepth(), get_class($node) );
    	}
    	
    	echo 'Iterate all tree from $parentOu but display only instances--------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if( is_a( $node , 'Product\Instance') ){
    			var_dump( $node->getPath(), $ChildrenIterator->key() );
    		}
    	}
    	
    	echo 'Iterate all tree from $parentOu but display only product version--------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if( is_a( $node , 'Product\Version') ){
    			var_dump( $node->getPath(), $ChildrenIterator->key() );
    		}
    	}
    	
    	
    	echo 'Iterate all tree from $parentOu but display only product base--------------------------------------------------------' . "\n";
    	foreach($ChildrenIterator as $node){
    		if( is_a( $node , 'Product') ){
    			var_dump( $node->getPath(), $ChildrenIterator->key() );
    		}
    	}
    	
    	echo 'End of iterations display--------------------------------------------------------' . "\n";
    	*/
    	
	}
	
	

}


