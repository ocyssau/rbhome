<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm;


/**
 * $Id: Usage.php 435 2011-06-05 21:50:17Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbh/Pdm/Usage.php $
 * $LastChangedDate: 2011-06-05 23:50:17 +0200 (dim., 05 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 435 $
 */


/**
 * @brief Usage define quantity and type of use to apply to a product.
 *
 */
class Usage
{
	/**
	 * Promissory usage.
	 * A promissory_usage_occurrence the intention to use the constituent in an assembly.
	 * It may also be used when product structure is not completely defined.
	 * 
	 */
	const TYPE_PROMISSORY = 1;

	/**
	 * Quantified usage.
	 */
	const TYPE_QUANTIFIED = 2;
	
	/**
	 * 
	 */
	const UNIT_SCALAR = 'SCALAR';
	

	/**
	 * One of the constante self::TYPE_*
	 * @var integer
	 */
	protected $type;

	/**
	 * @var float
	 */
	protected $_quantity;

	/**
	 * @var string
	 */
	protected $_unit;
	
	
	/**
	 * 
	 * @param integer	One of constant self::TYPE_*
	 */
	public function __construct($type = self::TYPE_PROMISSORY)
	{
		$this->type = $type;
	} //End of class
	
	
	/**
	 * Set quantity and switch type to self::TYPE_QUANTIFIED
	 * @param float $quantity
	 * @param string $unit
	 */
	public function setQuantity( $quantity, $unit = self::UNIT_SCALAR )
	{
		$this->type = self::TYPE_QUANTIFIED;
		$this->_quantity = $quantity;
		$this->_unit = $unit;
	} //End of class
	
	
	/**
	 * @return float
	 */
	public function getQuantity()
	{
		return $this->_quantity;
	} //End of class
	
	
	/**
	 * @return string
	 */
	public function getUnit()
	{
		return $this->_unit;
	} //End of class
	
	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	} //End of class
	
	

}//End of class

