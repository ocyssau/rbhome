<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm\Product;

/**
 * @brief Material definition.
 * 
 * Example and tests: Rbh/Pdm/Test.php
 * 
 * @verbatim
 * @endverbatim
 */
class Material extends \Rbh\Pdm\PdmAbstract
{

	/**
	 * @var float
	 */
	protected $_density;
	
	
	/**
	 * @var \Rbh\People\User_Collection
	 */
	protected $_suppliers;
	
	
	/**
	 * @var \Rbh\Ged\Document\Version_Collection
	 */
	protected $_technicalDocuments;
	
	
	/**
	 * Constructor
	 *
	 * @param array	$properties
	 */
	public function __construct( array $properties = null, $parent = null )
	{
		parent::__construct($properties, $parent);
	} //End of function
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setDensity($value, $unit = \Rbh\Measure\Density::STANDARD)
	{
		if( $unit != \Rbh\Measure\Density::STANDARD ){
			$unite = new \Rbh\Measure\Density($value, $unit);
			$this->_density = $unite->convertTo(\Rbh\Measure\Density::STANDARD);
		}
		else{
			$this->_density = $value;
		}
	} //End of function
	
	
	/**
	 * Getter
	 * @return float
	 */
	public function getDensity()
	{
		return $this->_density;
	} //End of function
	
	
	/**
	 * 
	 * @return \Rbh\People\User_Collection
	 */
	public function getSuppliers()
	{
		if( !$this->_suppliers ){
			$this->_suppliers = new \Rbh\People\User_Collection();
		}
		return $this->_suppliers;
	}
	
	
	/**
	 * 
	 * @return \Rbh\Ged\Document\Version_Collection
	 */
	public function getTechnicalDocuments()
	{
		if( !$this->_technicalDocuments ){
			$this->_technicalDocuments = new \Rbh\Ged\Document\Version_Collection();
		}
		return $this->_technicalDocuments;
	}
	
	
}//End of class
