<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm\Product;

use Rbh\Measure;

/**
 * @brief Physicals properties of a product.
 * 
 * @todo voir comment etendre et ajouter des propriétés à la volé.
 * 
 * 
 * Example and tests: Rbh/Pdm/Test.php
 * 
 * @verbatim
 * @property-read float $weight
 * @property-read float $volume
 * @property-read float $wetSurface
 * @property-read float $density
 * @property-read array $gravityCenter
 * @property-read array $inertia
 * @endverbatim
 * 
 * 
 *
 */
class PhysicalProperties
{
	
	/**
	 * Implements \Rbh\LinkableInterface
	 * 
	 * @var string uuid
	 */
	protected $_uid;
	
	
	/**
	 * Implements \Rbh\LinkableInterface
	 * 
	 * @var string
	 */
	protected $_name;
	
	
	/**
	 * Unit grammes
	 * @var float
	 */
	protected $weight;
	
	/**
	 * Unit m3
	 * @var float
	 */
	protected $volume;
	
	/**
	 * Unit m2
	 * @var float
	 */
	protected $wetSurface;
	
	/**
	 * Unit gramme/m3
	 * @var float
	 */
	protected $density;
	
	/**
	 * Units m
	 * @var array
	 */
	protected $gravityCenter;
	
	/**
	 * Unit m4
	 * @var array
	 */
	protected $inertia;
	
	/**
	 * 
	 * @param array $properties
	 * @return void
	 */
	public function __construct(array $properties)
	{
		if( $properties['uid'] ){
			$this->_uid = $properties['uid'];
		}
		else{
			$this->_uid = \Rbh\Uuid::newUid();
		}
		if( $properties['name'] ){
			$this->_name = $properties['name'];
		}
		else{
			$this->_name = $this->_uid;
		}
	}
	
	/**
	 * Getter.
	 * Implements \Rbh\LinkableInterface.
	 * @return string uuid
	 */
	public function getUid()
	{
		return $this->_uid;
	}
	
	
	/**
	 * Getter.
	 * Implements \Rbh\LinkableInterface.
	 * @return string
	 */
	public function getName()
	{
		return $this->_name;
	}
	
	
	/**
	 */
	public function __get($name)
	{
		return $this->$name;
	}
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setWeight($value, $unit = Measure\Weight::STANDARD)
	{
		if( $unit != Measure\Weight::STANDARD ){
			$unite = new Measure\Weight($value, $unit);
			$this->weight = $unite->convertTo(Measure\Weight::STANDARD);
		}
		else{
			$this->weight = $value;
		}
	}
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setDensity($value, $unit = Measure\Density::STANDARD)
	{
		if( $unit != Measure\Density::STANDARD ){
			$unite = new Measure\Density($value, $unit);
			$this->density = $unite->convertTo(Measure\Density::STANDARD);
		}
		else{
			$this->density = $value;
		}
	}
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setVolume($value, $unit = Measure\Volume::STANDARD)
	{
		if( $unit != Measure\Volume::STANDARD ){
			$unite = new Measure\Volume($value, $unit);
			$this->volume = $unite->convertTo(Measure\Volume::STANDARD);
		}
		else{
			$this->volume = $value;
		}
	}
	
	
	/**
	 * Setter
	 * @param float $value
	 * @param string $unit
	 */
	public function setWetSurface($value, $unit = Measure\Area::STANDARD)
	{
		if( $unit != Measure\Area::STANDARD ){
			$unite = new Measure\Area($value, $unit);
			$this->wetSurface = $unite->convertTo(Measure\Area::STANDARD);
		}
		else{
			$this->wetSurface = $value;
		}
	}
	
	
	/**
	 * Setter
	 * @param float $x
	 * @param float $y
	 * @param float $z
	 * @param string $unit
	 */
	public function setGravityCenter( $x, $y, $z, $unit = Measure\Length::STANDARD )
	{
		if( $unit != Measure\Length::STANDARD ){
			$xunite = new Measure\Length($x, $unit);
			$x = $xunite->convertTo(Measure\Length::STANDARD);
			
			$yunite = new Measure\Length($y, $unit);
			$y = $xunite->convertTo(Measure\Length::STANDARD);
			
			$zunite = new Measure\Length($z, $unit);
			$z = $xunite->convertTo(Measure\Length::STANDARD);
		}
		
		$this->gravityCenter = new \SplFixedArray(3);
		$this->gravityCenter[0] = $x;
		$this->gravityCenter[1] = $y;
		$this->gravityCenter[2] = $z;
	}

	
	/**
	 * Setter
	 * @param float $x
	 * @param float $y
	 * @param float $z
	 * @param string $unit
	 */
	public function setIntertiaCenter($x, $y, $z, $unit = 'mm4')
	{
		if( $unit != Measure\Length::STANDARD ){
			$xunite = new Measure\Length($x, $unit);
			$x = $xunite->convertTo(Measure\Length::STANDARD);
			
			$yunite = new Measure\Length($y, $unit);
			$y = $xunite->convertTo(Measure\Length::STANDARD);
			
			$zunite = new Measure\Length($z, $unit);
			$z = $xunite->convertTo(Measure\Length::STANDARD);
		}
		
		$this->intertia = new \SplFixedArray(3);
		$this->intertia[0] = $x;
		$this->intertia[1] = $y;
		$this->intertia[2] = $z;
	}
	
}//End of class
