<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm\Product;

use Rbh\Pdm;
use Rbh\Sys\Exception;
use Rbh\Sys\Error;

/**
 * @brief This class implement the product_definition_formation and product_definition_formation_with_specified_source entities of STEP PDM SHEMA.
 *
 * The product_definition_formation entity represents the identification of a specific version of the base product identification. 
 * A particular product_definition_formation is always related to exactly one product.
 * 
 * Each instance of product is required to have an associated instance of product_definition_formation. 
 * A single product entity may have more than one associated product_definition_formation. 
 * The set of these product_definition_formation entities represents the revision history of the product. 
 * 
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#Product_definition
 * 
 * 
 * Product definition formation with specified source
 * The product_definition_formation_with_specified_source entity is a subtype of the entity product_definition_formation. 
 * This entity adds the attribute make_or_buy to indicate the source of the version - either 'made' or 'bought'. 
 * This entity is never used in the context of 'Document as Product', but it may be used in 'Part as Product' for compatibility 
 * with AP203. However, this make-versus-buy distinction can be ambiguous in the context of exchange - going down a supply chain, 
 * the sender is often 'bought' while the receiver is 'made', while in the other direction, the sender is 'made' and the receiver 
 * 'bought'. Because of this ambiguity the use of this entity is not generally recommended.
 *  
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#product_definition_formation_with_specified_source
 * 
 * 
 * Example and tests: Rbh/Pdm/Test.php
 * 
 * @verbatim
 * @property string $version
 * @property string $documentId
 * @property string $productId
 * @endverbatim
 * 
 * @verbatim
 * STEP EXAMPLE
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_description', #40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #20, ); 
 * @endverbatim
 * 
 */
class Version extends Pdm\PdmAbstract
{

	/**
	 * Part version identification.
	 * @var string uuid
	 */
	public $version;

	/**
	 * Document uuid associate to this product.
	 * @var string uuid
	 */
	public $documentId;
	
	/**
	 * Document associate to this product.
	 * @var \Rbh\Ged\Document
	 */
	protected $_document;
	

	/**
	 * The base product uuid.
	 * @var string uuid
	 */
	public $productId;

	/**
	 * The base product for this version.
	 * @var \Rbh\Pdm\Product
	 */
	protected $_ofProduct;
	
	/**
	 * @var \Rbh\Pdm\Product\PhysicalProperties
	 */
	protected $_PhysicalProperties;
	
	/**
	 * @var \Rbh\Pdm\Product\Material
	 */
	protected $_material;
	
	/**
	 * The make_or_buy attribute contains the source information.
	 * @var string
	 */
	protected $_makeOrBuy;
	
	
	/**
	 * Constructor
	 * @see \Rbh\AnyObject::__construct()
	 *
	 * @param array|string			$properties
	 * @param \Rbh\Org\Unit		$parent
	 */
	public function __construct( $properties = null, $parent = null )
	{
		parent::__construct($properties, $parent);
		if( !$this->number ){
			$this->number = $this->_uid;
		}
	} //End of function	
	

	/**
	 *
	 * @param \Rbh\Ged\Document $document
	 */
	public function setDocument(\Rbh\Ged\Document &$document)
	{
		$this->documentId = $document->getUid();
		$this->_document = $document;
	} //End of method
	

	/**
	 * Getter
	 * @throws \Rbh\Sys\Exception
	 * @return \Rbh\Ged\Document
	 */
	public function getDocument()
	{
		if( !$this->_document ){
			throw new Exception('PROPERTY_%0%_IS_NOT_SET', Error::ERROR, array('_document'));
		}
		return $this->_document;
	} //End of method
	

	/**
	 * Setter for the base product of this version
	 * @param \Rbh\Pdm\Product $product
	 */
	public function setBase(\Rbh\Pdm\Product &$product)
	{
		$this->productId = $product->getUid();
		$this->_ofProduct = $product;
	} //End of method
	

	/**
	 * Getter of the base product of this version
	 * @throws \Rbh\Sys\Exception
	 * @return \Rbh\Pdm\Product
	 */
	public function getBase()
	{
		if( !$this->_ofProduct ){
			throw new Exception('PROPERTY_%0%_IS_NOT_SET', Error::ERROR, array('_product'));
		}
		return $this->_ofProduct;
	} //End of method
	
	
	/**
	 * 
	 * @return \Rbh\Pdm\Product\Material
	 * @throws \Rbh\Sys\Exception
	 */
	public function getMaterial()
	{
		if( !$this->_material ){
			throw new Exception('PROPERTY_%0%_IS_NOT_SET', Error::ERROR, array('_material'));
		}
    	return $this->_material;
	} //End of method
	
	
	/**
	 * 
	 * @param \Rbh\Pdm\Product\Material $material
	 * @return void
	 */
	public function setMaterial( $material )
	{
		$this->_material = $material;
		$this->getLinks()->add( $this->_material );
	} //End of method
	
	
	/**
	 * 
	 * @return \Rbh\Pdm\Product\PhysicalProperties
	 */
	public function getPhysicalProperties()
	{
		if( !$this->_PhysicalProperties ){
			$this->_PhysicalProperties = new Pdm\Product\PhysicalProperties(array('name'=>'PhysicalProperties', 'uid'=>$this->getUid() ) );
			$this->getLinks()->add( $this->_PhysicalProperties );
		}
    	return $this->_PhysicalProperties;
	} //End of method
	

}//End of class
