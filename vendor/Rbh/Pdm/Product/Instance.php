<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm\Product;

use Rbh\Pdm;
use Rbh\Sys\Exception;
use Rbh\Sys\Error;


/*
 TABLE DEFINITION :
 DROP TABLE `pdm_product_version`;
 DROP TABLE `pdm_product_version_seq`;

 CREATE TABLE `pdm_product_version` (
 `pv_id` int(11) NOT NULL,
 `of_product_id` int(11) NOT NULL,
 `version_id` int(11) NOT NULL,
 `pv_description` varchar(512) NULL,
 `document_id` int(11) NULL,
 `document_space` varchar(16) NULL,
 PRIMARY KEY  (`pv_id`),
 UNIQUE KEY `pdm_product_version_uniq1` (`of_product_id`,`version_id`),
 KEY `INDEX_pdm_product_version_1` (`of_product_id`),
 KEY `INDEX_pdm_product_version_2` (`version_id`),
 KEY `INDEX_pdm_product_version_3` (`pv_description`)
 );
 */


/**
 * @brief This class implement the product_definition and product_usage entities of STEP PDM SHEMA.
 *
 * The product_definition entity represents the identification of a particular view 
 * on a version of the product base identification relevant for the requirements of
 * particular life-cycle stages and application domains. View may be based on 
 * application domain and/or life-cycle stage (e.g., design, manufacturing). A view collects
 * product data for a specific task. It is possible to have many product_definition views
 * for a part/version combination.
 * 
 * The product_definition entity enables the establishment of many important relationships. 
 * Product_definition is the central element to link product information to parts, 
 * e.g., assembly structure, properties (including shape), and external descriptions of the product via documents.
 * 
 * The use of product_definition entities is not strictly required by rules in the PDM Schema, but it is strongly recommended. 
 * All product_definition_formation entities should always have at least one associated product_definition, 
 * except in the case of supplied product identification and version history information. 
 * 
 * See http://www.wikistep.org/index.php/PDM-UG:_Product_Master_Identification#Product_definition
 * 
 * @verbatim
 * @property string $nomenclature
 * @endverbatim
 * 
 * 
 * @verbatim
 * STEP EXAMPLE 1
 * #30 = PRODUCT_CONTEXT('', #20, '');
 * #40 = PRODUCT('part_id', 'part_name', 'part_description', (#30));
 * #60 = PRODUCT_DEFINITION_FORMATION('pversion_id','pversion_description', #40);
 * #80 = PRODUCT_DEFINITION('view_id', 'view_name', #60, #90);
 * #90 = PRODUCT_DEFINITION_CONTEXT('part definition', #20, ); 
 * 
 * 
 * STEP EXAMPLE 2
 * /* primary application context and life cycle stage
 * #10 = APPLICATION_CONTEXT('mechanical design');
 * #220 = PRODUCT_CONTEXT(, #10, );
 * #230 = PRODUCT_DEFINITION_CONTEXT('part definition', #10, 'design');
 * 
 * /* optional AP214 specific characterization for assembly definition
 * #340 = PRODUCT_DEFINITION_CONTEXT_ROLE('part definition type', $);
 * #350 = PRODUCT_DEFINITION_CONTEXT('assembly definition', #10, );
 * #520 = PRODUCT_DEFINITION_CONTEXT_ASSOCIATION(#460, #350, #340);
 * 
 * /* type discriminator for part as product
 * #100 = PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#380, #440));
 * 
 * /* part master for component definition
 * #360 = PRODUCT_RELATED_PRODUCT_CATEGORY('detail', $, (#380));
 * #380 = PRODUCT('g1', 'gasket', $, (#220)); /*base
 * #390 = PRODUCT_DEFINITION_FORMATION('A',,#380); /*version
 * #400 = PRODUCT_DEFINITION('gv1', 'design view on gasket', #390, #230); /*instance
 * 
 * /* part master for assembly definition
 * #420 = PRODUCT_RELATED_PRODUCT_CATEGORY('assembly', $, (#440));
 * #440 = PRODUCT('s1', 'sleeve assembly', $, (#220)); /*base
 * #450 = PRODUCT_DEFINITION_FORMATION('A',,#440); /*version
 * #460 = PRODUCT_DEFINITION('sv1', 'design view on sleeve assembly', #450, #230); /*instance
 * 
 * /* single usage occurrence of component defn within assembly defn
 * #530 = NEXT_ASSEMBLY_USAGE_OCCURRENCE('gu1', 'single instance usage', 'gasket usage 1', #460, #400, $); /*instance parent-child relation
 * @endverbatim
 *
 *
 * A instance is uniq for a product
 * 
 * In Rbh implementation Instance has relation with Pdm\Usage to define type and properties of the use.
 * Type may be PROMISSORY, or QUANTIFIED. Properties of usage are the quantity and unit of this quantity.
 * 
 * Instance bring too the position of the instance in 3D space. Position is a simple \SplFixedArray with 12 entries.
 * The first 3 entries define the U axis.
 * 3 next the V axis.
 * 3 next the W axis.
 * and 3 last a translation vector from absolute origin.
 * 
 * 
 *
 *
 */
class Instance extends Pdm\PdmAbstract
{

	/**
	 * The related product version uuid.
	 * @var string uuid
	 */
	public $pvId;

	/**
	 * The related product version.
	 * STEP: The formation attribute links to the product_definition_formation of which this represents a view. 
	 * 
	 * @var Pdm\Product_Version
	 */
	protected $_product;

	/**
	 * @var Pdm\Usage
	 */
	protected $_usage;

	/**
	 * @var \SplFixedArray
	 */
	protected $_position;

	/**
	 * Identifiant for nomenclature.
	 * @var string
	 */
	public $nomenclature;
	
	/**
	 * Context id
	 * @var string uuid
	 */
	public $ctId;
	
	/**
	 * which view of the product the particular represents
	 * @var Pdm\Product\Definition\Context
	 */
	protected $_context;
	

	/**
	 * Constructor
	 *
	 * @param array|string	$prop
	 * @param Instance $parent
	 * @param Pdm\Product_Version	 $relatedProduct
	 */
	public function __construct( array $properties = null, $parent = null, $relatedProduct=null )
	{
		parent::__construct($properties, $parent);
		
		if( !$this->number ){
			$this->number = $this->_uid;
		}
		
		if($relatedProduct){
			$this->setProduct($relatedProduct);
		}
	} //End of function


	/**
	 * Setter for the related product version
	 * @param Pdm\Product\Version $product
	 */
	public function setProduct(Pdm\Product\Version &$product)
	{
		$this->pvId = $product->getUid();
		$this->_product = $product;
	} //End of method


	/**
	 * Getter of the base product of this version
	 * @throws \Rbh\Sys\Exception
	 * @return Pdm\Product
	 */
	public function getProduct()
	{
		if( !$this->_product ){
			throw new Exception('PROPERTY_%0%_IS_NOT_SET', Error::ERROR, array('_product'));
		}
		return $this->_product;
	} //End of method


	
	/**
	 * Getter
	 * @throws \Rbh\Sys\Exception
	 * @return Pdm\Usage
	 */
	public function getUsage()
	{
		if( !$this->_usage ){
			//require_once('Rbh/Pdm/Usage.php');
			$this->_usage = new Pdm\Usage();
		}
		return $this->_usage;
	} //End of method

	
	/**
	 * Getter for position.
	 * Position is a array with 12 dimensions where 3 first defined coordinate of the U axis
	 * of triedre, 3 next the V axis, 3 next the W axis, and 3 last the translation vector :
	 *  - index 0,1,2, U axis:
	 *	- Ux
	 *	- Uy
	 *	- Uz
	 *  - index 3,4,5, V axis:
	 *	- Vx
	 *	- Vy
	 *	- Vz
	 *  - index 6,7,8, W axis:
	 *	- Wx
	 *	- Wy
	 *	- Wz
	 *  - index 9,10,11, Translation vector:
	 *	- Tx
	 *	- Ty
	 *	- Tz
	 *
	 * @throws \Rbh\Sys\Exception
	 * @return \SplFixedArray	12 dimensions fixed array
	 */
	public function getPosition()
	{
		if( !$this->_position ){
			$this->_position = new \SplFixedArray(12);
			$this->_position[0] = 1; //Ux
			$this->_position[1] = 0; //Uy
			$this->_position[2] = 0; //Uz
			$this->_position[3] = 0; //Vx
			$this->_position[4] = 1; //Vy
			$this->_position[5] = 0; //Vz
			$this->_position[6] = 0; //Wx
			$this->_position[7] = 0; //Wy
			$this->_position[8] = 1; //Wz
			$this->_position[9] = 0; //Tx
			$this->_position[10] = 0; //Ty
			$this->_position[11] = 0; //Tz
		}
		return $this->_position;
	} //End of method
	
	
	/**
	 * Setter
	 * @param Pdm\Product_Definition_Context $context
	 */
	public function setContext(Pdm\Product\Definition\Context &$context)
	{
		$this->ctId = $context->getUid();
		$this->_context = $context;
	}//End of method
	

	/**
	 * Getter
	 * @throws \Rbh\Sys\Exception
	 * @return Pdm\Product_Definition_Context
	 */
	public function getContext()
	{
		if( !$this->_context ){
			throw new Exception('PROPERTY_%0%_IS_NOT_SET', Error::ERROR, array('_context'));
		}
		return $this->_context;
	}//End of method

}//End of class
