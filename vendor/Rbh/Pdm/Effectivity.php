<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm;


/**
 * $Id: Effectivity.php 428 2011-06-04 13:34:16Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOn\SplObjectStorage/rbPlmOc/library/Rbh/Pdm/Effectivity.php $
 * $LastChangedDate: 2011-06-04 15:34:16 +0200 (sam., 04 juin 2011) $
 * $LastChangedBy: olivierc $
 * $Rev: 428 $
 */

/*
 TABLE DEFINITION :
 DROP TABLE `pdm_effectivity`;
 DROP TABLE `pdm_effectivity_seq`;
 CREATE TABLE `pdm_effectivity` (
 `effectivity_id` int(11) NOT NULL,
 `number` varchar(128) NULL,
 `name` varchar(128) NULL,
 `description` varchar(512) NULL,
 `usage_id` int(11) NULL,
 `usage_type` int(11) NULL,
 `cd_id` int(11) NULL,
 `start_date` int(11) NULL,
 `end_date` int(11) NULL,
 `lot_id` int(11) NULL,
 `lot_size` int(11) NULL,
 `snum_start_id` int(11) NULL,
 `snum_end_id` int(11) NULL,
 PRIMARY KEY  (`effectivity_id`),
 UNIQUE KEY `UNIQ_pdm_effectivity_1` (`number`),
 KEY `INDEX_pdm_effectivity_1` (`name`),
 KEY `INDEX_pdm_effectivity_2` (`description`),
 KEY `INDEX_pdm_effectivity_3` (`usage_id`),
 KEY `INDEX_pdm_effectivity_4` (`cd_id`),
 KEY `INDEX_pdm_effectivity_5` (`start_date`),
 KEY `INDEX_pdm_effectivity_6` (`end_date`),
 KEY `INDEX_pdm_effectivity_7` (`lot_id`),
 KEY `INDEX_pdm_effectivity_8` (`snum_start_id`),
 KEY `INDEX_pdm_effectivity_9` (`snum_end_id`)
 );
 */

/**
 * @brief Abstract class for effectivity definition. This class implement the effectivity entity of STEP PDM SHEMA.
 * 
 * The effectivity entity supports the specification of a domain of applicability
 * for product data. Effectivity is a generic concept defining the valid use of
 * the product data to which the effectivity is assigned.
 * 
 * NOTE - The assignment of effectivities is often done during the approval
 * process, i.e, when releasing product data for the next stage of the
 * development process, it gets effectivity information assigned to define when
 * and in which context these product data may be used.
 * 
 * Within the PDM Schema, there are two areas identified for the usage of the
 * effectivity concept: configuration effectivity and general validity period
 * effectivity. These two areas are reflected by two orthogonal subtype
 * structures defined for the effectivity entity.
 * 
 * One subtype structure contains the effectivity subtypes dated_effectivity,
 * lot_effectivity, serial_numbered_effectivity, and
 * time_interval_based_effectivity, restricting the domain of applicability of
 * the associated product data to a date range, a particular lot or serial number
 * range, or to a time interval respectively. There is a ONEOF constraint between
 * these subtypes, i.e., an effectivity can be, at most, one of dated_effectivity,
 * lot_effectivity, serial_numbered_effectivity, and
 * time_interval_based_effectivity.
 * 
 * The other subtype structure contains product_definition_effectivity as subtype
 * of effectivity, and configuration_effectivity as subtype of
 * product_definiton_effectivity. The subtypes in the second structure are
 * restricted to apply to a particular usage occurrence of a constituent part in
 * some higher-level assembly.
 * 
 * 
 * See http://www.wikistep.org/index.php/PDM-UG:_Configuration_Composition_Management#Effectivity
 * 
 * 
 * 
 * @verbatim
 * STEP EXAMPLE
 * #100=PRODUCT_CONCEPT_CONTEXT('pcc_name1', #1, '');
 * #1=APPLICATION_CONTEXT('');
 * #2=PRODUCT_CONCEPT('PC-M01', 'PC model name1', 'PC system', #100);
 * #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
 * #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
 * #8=APPLICATION_CONTEXT('');
 * #9=PRODUCT_CONTEXT('', #8, '');
 * #10=PRODUCT('PC-0023', 'PC system', $, (#9));
 * #11=PRODUCT_DEFINITION_FORMATION('D', 'description of PC-0023,D', #10);
 * #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
 * #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10, #18, #20, #21, #22, #23));
 * #15=CONFIGURATION_DESIGN(#5, #11);
 * #16=CONFIGURATION_DESIGN(#6, #11);
 * #18=PRODUCT('MB-0013', 'Mainboard', $, (#9));
 * #19=PRODUCT_DEFINITION_FORMATION('F', 'description of MB-0013,F', #18);
 * #20=PRODUCT('PSU-0009', 'Power supply unit', 'Power supply unit 220V', (#9));
 * #21=PRODUCT('PSU-0011', 'Power supply unit', 'Power supply unit 110V', (#9));
 * #22=PRODUCT('PR-0133', 'CPU', 'Pentium II 233', (#9));
 * #23=PRODUCT('PR-0146', 'CPU', 'Pentium II 266', (#9));
 * #24=PRODUCT_DEFINITION_FORMATION('A', 'description of PSU-0009,A', #20);
 * #25=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0009,B', #20);
 * #26=PRODUCT_DEFINITION_FORMATION('B', 'description of PSU-0011,B', #21);
 * #27=PRODUCT_DEFINITION_FORMATION('A', 'description of PR-0133,A', #22);
 * #28=PRODUCT_DEFINITION_FORMATION('C', 'description of PR-0146,C', #23);
 * #29=PRODUCT_DEFINITION('pc_v1', 'design view on PC-0023,D', #11, #12);
 * #30=PRODUCT_DEFINITION('mb_v1', 'design view on MB-0013,F', #19, #12);
 * #31=PRODUCT_DEFINITION('psu1A_v1', 'design view on PSU-0009,A', #24, #12);
 * #33=PRODUCT_DEFINITION('psu1B_v1', 'design view on PSU-0009,B', #25, #12);
 * #34=PRODUCT_DEFINITION('psua_v1', 'design view on PSU-0011,B', #26, #12);
 * #35=PRODUCT_DEFINITION('pr1_v1', 'design view on PR-0133,A', #27, #12);
 * #36=PRODUCT_DEFINITION('pr2_v1', 'design view on PR-0146,C', #28, #12);
 * #37=NEXT_ASSEMBLY_USAGE_OCCURRENCE('mb-u1', 'single instance usage', $, #29, #30, $);
 * #38=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u1', 'single instance usage', $, #29, #31, $);
 * #39=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u2', 'single instance usage', $, #29, #33, $);
 * #40=NEXT_ASSEMBLY_USAGE_OCCURRENCE('psu-u3', 'single instance usage', $, #29, #34, $);
 * #41=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u1', 'single instance usage', $, #30, #35, $);
 * #42=NEXT_ASSEMBLY_USAGE_OCCURRENCE('cpu-u2', 'single instance usage', $, #30, #36, $);
 * #43=(CONFIGURATION_EFFECTIVITY(#15)
 *      DATED_EFFECTIVITY(#970, #910)
 *     EFFECTIVITY('')
 *      PRODUCT_DEFINITION_EFFECTIVITY(#38)
 *     );
 * #45=(CONFIGURATION_EFFECTIVITY(#15)
 *      DATED_EFFECTIVITY($, #1010)
 *      EFFECTIVITY('')
 *      PRODUCT_DEFINITION_EFFECTIVITY(#39)
 *     );
 * #46=(CONFIGURATION_EFFECTIVITY(#16)
 *      EFFECTIVITY('')
 *      PRODUCT_DEFINITION_EFFECTIVITY(#40)
 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000567', $)
 *    );
 * #47=(CONFIGURATION_EFFECTIVITY(#15)
 *      DATED_EFFECTIVITY($, #1160)
 *      EFFECTIVITY('')
 *      PRODUCT_DEFINITION_EFFECTIVITY(#41)
 *    );
 * #48=(CONFIGURATION_EFFECTIVITY(#16)
 *      EFFECTIVITY('')
 *      PRODUCT_DEFINITION_EFFECTIVITY(#41)
 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000345', 'PS253-000976')
 *     );
 * #49=(CONFIGURATION_EFFECTIVITY(#16)
 *      EFFECTIVITY('')
 *      PRODUCT_DEFINITION_EFFECTIVITY(#42)
 *      SERIAL_NUMBERED_EFFECTIVITY('PS253-000977', $)
 *     );
 * #910=DATE_AND_TIME(#920, #930);
 * #920=CALENDAR_DATE(2000, 1, 7);
 * #930=LOCAL_TIME(0, 0, 0., #940);
 * #940=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
 * #970=DATE_AND_TIME(#980, #990);
 * #980=CALENDAR_DATE(1999, 31, 3);
 * #990=LOCAL_TIME(0, 0, 0., #1000);
 * #1000=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
 * #1010=DATE_AND_TIME(#1020, #1030);
 * #1020=CALENDAR_DATE(1999, 1, 4);
 * #1030=LOCAL_TIME(0, 0, 0., #1031);
 * #1031=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
 * #1110=DATE_AND_TIME(#1120, #1130);
 * #1120=CALENDAR_DATE(2000, 15, 7);
 * #1130=LOCAL_TIME(0, 0, 0., #1140);
 * #1140=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.);
 * #1160=DATE_AND_TIME(#1170, #1180);
 * #1170=CALENDAR_DATE(2000, 1, 10);
 * #1180=LOCAL_TIME(0, 0, 0., #1190);
 * #1190=COORDINATED_UNIVERSAL_TIME_OFFSET(1, $, .AHEAD.); * 
 * @endverbatim
 * 
 * 
 * 
 * 
 * 
*/
abstract class Effectivity
{
	
	/**
	 * @var integer
	 */
	const EFFECTIVITY_TYPE_DATED = 1;
	
	/**
	 * @var integer
	 */
	const EFFECTIVITY_TYPE_LOT = 2;
	
	/**
	 * @var integer
	 */
	const EFFECTIVITY_TYPE_NUMBERED = 3;
	
	
	/**
	 * 
	 * @return string
	 */
	public abstract function getType();
	
	
	//public abstract function check($what);
	
	
}//End of class

