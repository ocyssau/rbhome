<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm;

use Rbh\AnyObject;
use Rbh\Dao\MappedInterface;
use Rbh\Link\Collection as LinkCollection;
use Rbh\Sys\Exception;
use Rbh\Sys\Error;


/**
 * @brief Provide generic methods and properties for all classes of pdm.
 *
 * @verbatim
 * @property string $number
 * @property string $description
 * @endverbatim
 */
abstract class PdmAbstract extends AnyObject implements MappedInterface
{
	
	/**
	 * Implements \Rbh\Dao\MappedInterface.
	 * True if object is loaded from db.
	 * 
	 * @var boolean
	 */
	protected $_isLoaded = false;

	/**
	 *
	 * @var string uuid
	 */
	public $owner = null;

	/**
	 * 
	 * @var string
	 */
	public $number;
	
	/**
	 * 
	 * @var string
	 */
	public $description;
	
	/**
	 * Collection of all links with this object.
	 * Current object is call related, links are call link.
	 *
	 * @var \Rbplm\Model\Collection
	 */
	protected $_links;
	
	/**
	 * Collection of all links with this object.
	 * Current object is call related, links are call link.
	 * @var \Rbplm\AnyObject
	 */
	protected $_parent;
	public $parentId;
	
	/**
	 * Constructor
	 *
	 * @param array	$properties
	 */
	public function __construct( $properties = null, \Rbh\AnyObject $parent = null )
	{
		parent::__construct($properties, $parent);
		
		if( !$this->owner ){
			$this->owner = \Rbh\People\CurrentUser::get()->getUid();
		}
	} //End of function
	
	public function getPath()
	{
	}
	
	public function setPath()
	{
	}
	
	public function getChild()
	{
	}
	
	public function setChild()
	{
	}
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\Model\CompositComponentInterface#getParent()
	 * @return anyobject
	 */
	public function getParent()
	{
	    return $this->_parent;
	} //End of method
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\Model\CompositComponentInterface#setParent($object)
	 * @param \Rbplm\Model\CompositComponentInterface $object
	 * @return void
	 */
	public function setParent(AnyObject $object)
	{
	    $this->parentId = $object->getUid();
	    $this->_parent = $object;
	    return $this;
	} //End of method
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\LinkRelatedInterface#getLinks()
	 * @return LinkCollection
	 */
	public function getLinks()
	{
	    if( !$this->_links ){
	        $this->_links = new LinkCollection( array('name'=>'Links') );
	    }
	    return $this->_links;
	} //End of method
	
	/**
	 * @see library/Rbplm/Model/\Rbplm\LinkRelatedInterface#hasLinks()
	 * @return boolean
	 */
	public function hasLinks()
	{
	    if( !$this->_links ){
	        return false;
	    }
	    else if( $this->_links->count() == 0){
	        return false;
	    }
	    else{
	        return true;
	    }
	} //End of method
	
	
}//End of class
