<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm\Configuration;

use Rbh\Sys\Exception;
use Rbh\Sys\Error;

/**
 * @brief This class implement the configuration_item entity of STEP PDM SHEMA.
 * 
 * The configuration design entity represents an association between a
 * configuration_item identifying a particular product configuration and a
 * product design intended to implement that item. Thus, a configuration_design
 * entity represents the association of a configuration_item with a
 * product_definition or product_definition_formation to specify that the
 * corresponding design is a solution for a given configuration_item. A given
 * design may be the design for different configuration_items belonging to the
 * same or even different product_concepts.
 * 
 * The configuration_design entity may have, at most, one name associated with it
 * through the entity name_attribute. If a name is associated, the
 * configuration_design is referenced as the named_item by a name_attribute where
 * the name is stored in the attribute attribute_value. It is not recommended to
 * instantiate this additional value.
 * 
 * The configuration_design entity may have a description associated with it
 * through the entity description_attribute. If a description is associated, the
 * configuration_design is referenced as the described_item by a
 * description_attribute where the description is stored in the attribute
 * attribute_value. It is not recommended to instantiate this additional value.
 * Attributes
 * 
 * The configuration attribute specifies the configuration_item, i.e., the
 * product configuration that the associated design implements.
 * 
 * The design attribute specifies the product_definition or product_definition_formation
 * that is a candidate for use in manufacturing actual units of a
 * configuration_item.
 * 
 * @verbatim
 * @property string $ciId
 * @property string $pvId
 * @endverbatim
 * 
 * @verbatim
 * STEP EXAMPLE:
 * #100=PRODUCT_CONCEPT_CONTEXT('pcc_name1', #1, '');
 * #1=APPLICATION_CONTEXT();
 * #2=PRODUCT_CONCEPT('PC-M01', 'PC model name1', 'PC system', #100);
 * #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
 * #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
 * #7=CONFIGURATION_ITEM('PC-Conf3', 'High End Config US', 'High end PC system for US', #2, $);
 * #8=APPLICATION_CONTEXT('');
 * #9=PRODUCT_CONTEXT('', #8, '');
 * #10=PRODUCT('PC-0023', 'PC system', $, (#9));
 * #11=PRODUCT_DEFINITION_FORMATION('D', 'housing redesigned', #10);
 * #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
 * #13=PRODUCT_DEFINITION('pc_v1', 'design view on base PC', #11, #12);
 * #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10));
 * #15=CONFIGURATION_DESIGN(#5, #11);
 * #16=CONFIGURATION_DESIGN(#6, #11);
 * #17=CONFIGURATION_DESIGN(#7, #11); * 
 * @endverbatim
 */
class Design extends \Rbh\AnyObject
{

	/**
	 * Product definition formation id that is a design for the associated configuration item.
	 *
	 * @var string
	 */
	protected $pvId;
	
	/**
	 * The design attribute specifies the product_definition or product_definition_formation 
	 * 	that is a candidate for use in manufacturing actual units of a configuration_item.
	 * 
	 * The design attribute allows specifying a product_definition or product_definition_formation as 
	 * the design for the associated configuration_item. 
	 * Both AP203 and AP214 do not foresee product_definitions to be specified as the design in a configuration_design. 
	 * It is therefore recommended to only reference product_definition_formations from the configuration_design. 
	 * 
	 * @var \Rbh\Pdm\Product\Version | \Rbh\Pdm\Product\Instance
	 */
	protected $_design;
	
	/**
	 * specifies the configuration_item, i.e., the product configuration that
	 * the associated design implements
	 *
	 * @var \Rbh\Pdm\Configuration\Item
	 */
	//protected $_configurationItem;
	
	/**
	 * Configuration item id for which a design is specified.
	 *
	 * @var string
	 */
	//protected $ciId;
	
	
	/**
	 * @param array $properties		array('name'=>'string')
	 * @param \Rbh\Pdm\Configuration\Item 	$ci
	 * @param \Rbh\Pdm\Product\Version|\Rbh\Pdm\Product\Instance 	$product
	 * @return void
	 */
	public function __construct($properties, $ci=null, $product=null)
	{
		parent::__construct($properties, $ci);
		/*
		if($ci){
			$this->setConfigurationItem($ci);
		}
		*/
		if($product){
			$this->setDesign($product);
		}
	}
	

	/**
	 * Setter
	 * 
	 * @param \Rbh\Pdm\Product\Version|\Rbh\Pdm\Product\Instance $product
	 * @return void
	 */
	public function setDesign(&$product)
	{
		if (( $product instanceof \Rbh\Pdm\Product\Version)||($product instanceof \Rbh\Pdm\Product\Instance )){
			$this->_design = $product;
			$this->pvId = $product->getUid();
		}
		else{
			throw new Exception('BAD_PARAMETER_TYPE', Error::WARNING, '_product must be a \Rbh\Pdm\Product\Version or \Rbh\Pdm\Product\Instance');
		}
	}//End of method


	/**
	 *
	 * Getter
	 * 
	 * @return \Rbh\Pdm\Product\Version|\Rbh\Pdm\Product\Instance
	 */
	public function getDesign()
	{
		if( !$this->_product ){
			throw new Exception('PROPERTY_%0%_IS_NOT_SET', Error::ERROR, array('_design'));
		}
		return $this->_design;
	}//End of method


	/**
	 * Setter
	 * 
	 * @param \Rbh\Pdm\Configuration\Item $ci
	 * @return void
	 */
	/*
	public function setConfigurationItem( \Rbh\Pdm\Configuration\Item &$ci ){
		$this->_configurationItem = $ci;
		$this->ciId = $ci->getUid();
	}//End of method
	*/

	/**
	 * Getter
	 * 
	 * @return \Rbh\Pdm\Configuration\Item
	 */
	/*
	public function getConfigurationItem(){
		if( !$this->_configurationItem ){
			throw new \Rbh\Sys\Exception('PROPERTY_%0%_IS_NOT_SET', \Rbh\Sys\Error::ERROR, array('_configurationItem'));
		}
		return $this->_configurationItem;
	}//End of method
	*/
}//End of class


