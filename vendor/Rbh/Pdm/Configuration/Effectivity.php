<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm\Configuration;


/** 
 * @brief This class implement the configuration_effectivity, product_definition_effectivity entity of STEP PDM SHEMA.
 *
 * The entity configuration_effectivity is a subtype of
 * product_definition_effectivity that contains information about the planned
 * usage of components in a product configuration. It defines the valid use of a
 * particular product_definition occurrence at a certain position in the assembly
 * structure for a specified product configuration.
 *
 * The configuration_effectivity entity allows the association of the appropriate
 * versions of constituent parts intended to be used at the defined position in
 * the assembly structure to build a configuration_item. In the context of the
 * PDM Schema, the configuration_effectivity always relates to a particular unit
 * of manufacture of the end items of the given configuration_item. The three
 * ways to instantiate configuration_effectivity are:
 *
 * * serial_numbered_effectivity, where the configuration_effectivity defines the
 * valid use of constituent part occurrences for a serial number range of
 * instances of a product configuration to be manufactured;
 * * dated_effectivity, where the configuration_effectivity defines the valid use
 * of constituent part occurrences for a time range based on dates when instances
 * of the product configuration are manufactured;
 * * lot_effectivity, where the configuration_effectivity defines the valid use
 * of constituent part occurrences for a given lot of instances of a product
 * configuration to be manufactured.
 *
 *
 * The particular usage of the constituent part the effectivity applies to is
 * defined as product_definition_usage referenced by the usage attribute. The
 * product_definition_usage should be instantiated as an assembly_component_usage
 * or one of its subtypes (see Product concept). The attribute
 * related_product_definition specifies the definition of a constituent part, and
 * the attribute relating_ product_definition specifies the definition of the
 * assembly (or sub-assembly) in which the constituent may be used. This assembly
 * definition is part of the design that implements the configuration_item for
 * which the configuration_effectivity applies, i.e., it either is a part
 * definition of the product_definition_formation defining the ('complete')
 * design for the configuration_item, or it is an occurrence in the product
 * structure of that complete design, i.e., is related to that in a tree of
 * assembly_component_usage instances.
 * 
 * See http://www.wikistep.org/index.php/PDM-UG:_Configuration_Composition_Management#Configuration_effectivity_2
 * 
 * 
 * 
 * The product_definition_effectivity entity is a subtype of effectivity. 
 * It applies to a particular occurrence of a component or sub-assembly part as used within an assembly. 
 * It is the identification of a valid use of a particular product_definition in the context of its participation in a given product_definition_usage. 
 * The effectivity applies to a product_definition that is referenced as the related_product_definition by the product_definition_relationship 
 * associated to the product_definition_effectivity through the usage attribute. 
 * 
 * See http://www.wikistep.org/index.php/PDM-UG:_Configuration_Composition_Management#Product_definition_effectivity
 * 
 * 
 * 
 * Example and tests: Rbh/Pdm/Test.php
 * 
 */
class Effectivity extends \Rbh\Pdm\PdmAbstract
{

	/**
	 * Value is one of constant \Rbh\Pdm\Effectivity::EFFECTIVITY_TYPE_*
	 * 
	 * @var integer
	 */
	protected $_type;
	
	/**
	 * reference to the associated product_definition_relationship.
	 * @var \Rbh\Pdm\Product\Instance
	 */
	protected $_usage;
	
	
	/**
	 * The configuration attribute identifies the configuration_design that
	 * specifies the associated configuration_item for which the
	 * configuration_effectivity applies, and the corresponding
	 * product_definition_formation which defines the design that implements the
	 * configuration_item and thus specifies the upper most node ('entry point') in
	 * its product structure.
	 * 
	 * Reference to the configuration_design specifying the configuration_item
	 * identifies the context in which the effectivity applies.
	 * 
	 * @var \Rbh\Pdm\Configuration\Design
	 */
	protected $_design;

	/** 
	 * Reference to the \Rbh\Pdm\Effectivity subtype dated, lot or snumbered.
	 * 
	 * @var \Rbh\Pdm\Effectivity
	 */
	protected $_effectivity;


	/**
	 * Constructor
	 *
	 * @param array	$properties
	 * @param \Rbh\AnyObject			$parent
	 * @param \Rbh\Pdm\Effectivity			$effectivity
	 * @param \Rbh\Pdm\Product\Instance	$usage
	 */
	public function __construct( $properties = null, $parent = null, $effectivity = null, $usage = null )
	{
		parent::__construct($properties, $parent);
		
		if($effectivity){
			$this->setEffectivity($effectivity);
		}
		
		if($usage){
			$this->setUsage($usage);
		}
		
	} //End of function
	
	

	/**
	 * Setter
	 * @param \Rbh\Pdm\Configuration\Design $design
	 */
	public function setDesign(\Rbh\Pdm\Configuration\Design &$design)
	{
		$this->_design = $design;
	}//End of method


	/**
	 * 
	 * Enter description here ...
	 * @throws \Rbh\Sys\Exception
	 * @return \Rbh\Pdm\Configuration\Design
	 */
	public function getDesign()
	{
		if( !$this->_design ){
			throw new \Rbh\Sys\Exception('PROPERTY_%0%_IS_NOT_SET', \Rbh\Sys\Error::ERROR, array('_design'));
		}
		return $this->_design;
	}//End of method
	
	
	/**
	 * Setter
	 * @param \Rbh\Pdm\Effectivity $effectivity
	 */
	public function setEffectivity(\Rbh\Pdm\Effectivity &$effectivity)
	{
		$this->_effectivity = $effectivity;
		$this->_type = $effectivity->getType();
	}//End of method
	

	/**
	 * Getter
	 * @throws \Rbh\Sys\Exception
	 * @return \Rbh\Pdm\Effectivity_Effectivity
	 */
	public function getEffectivity()
	{
		if( !$this->_effectivity ){
			throw new \Rbh\Sys\Exception('PROPERTY_%0%_IS_NOT_SET', \Rbh\Sys\Error::ERROR, array('_effectivity'));
		}
		return $this->_effectivity;
	}//End of method
	
	
	/**
	 * Setter
	 * @param \Rbh\Pdm\Product\Instance $usage
	 */
	public function setUsage(\Rbh\Pdm\Product\Instance &$usage)
	{
		$this->_usage = $usage;
	}//End of method
	

	/**
	 * Getter
	 * @throws \Rbh\Sys\Exception
	 * @return \Rbh\Pdm\Product\Instance
	 */
	public function getUsage()
	{
		if( !$this->_usage ){
			throw new \Rbh\Sys\Exception('PROPERTY_%0%_IS_NOT_SET', \Rbh\Sys\Error::ERROR, array('_usage'));
		}
		return $this->_usage;
	}//End of method
	
	
	
	/**
	 * @see library/Rbh/Pdm/\Rbh\Pdm\Effectivity#getType()
	 */
	public function getType()
	{
		return $this->_type;
	}

}//End of class
