<?php
//%LICENCE_HEADER%

namespace Rbh\Pdm\Configuration;


/*
 TABLE DEFINITION :
 CREATE TABLE `pdm_configuration_item` (
 `ci_id` int(11) NOT NULL,
 `number` varchar(128) NULL,
 `name` varchar(128) NULL,
 `description` varchar(512) NULL,
 `concept_id` int(11) NOT NULL,
 `purpose` varchar(512) NULL,
 PRIMARY KEY  (`ci_id`),
 UNIQUE KEY  `UNIQ_pdm_configuration_item_1` (`number`),
 INDEX `INDEX_pdm_configuration_item_1` (`name`),
 INDEX `INDEX_pdm_configuration_item_2` (`concept_id`),
 INDEX `INDEX_pdm_configuration_item_3` (`purpose`)
 );
 */


/**
 * @brief This class implement the configuration_item entity of STEP PDM SHEMA.
 *
 * The configuration_item entity is a key concept to support explicit
 * configuration management. It represents the identification of a particular
 * configuration, i.e., variation of a product_concept. A configuration_item is
 * defined with respect to the product concept, i.e., the class of similar
 * products of which it is a member.
 *
 * The configuration_item defines a manufacturable end item, or something that is
 * conceived and expected as such. The association between a configuration_item
 * and a corresponding part design is established using a configuration_design.
 * The valid use of component parts for planned units of manufacturing of a
 * particular configuration_item may be specified using configuration effectivity
 *
 * NOTE - Depending on the type of products, and the organization that defines
 * and sells the products, a product_concept may directly correspond to a
 * particular part design, which means that only one variation, i.e., one
 * configuration_item exists for that particular product_concept.
 *
 * NOTE - ISO 10303-44 allows the configuration_item to be a variation of a
 * product concept, or of any of its discrete portions that is treated as a
 * single unit in the configuration management process. According to this
 * definition, it is possible to associate a configuration_item describing a
 * particular configuration of, for example, an engine to a product concept
 * describing a class of cars. This usage is not recommended.
 * 
 * See http://www.wikistep.org/index.php/Product_Concept_Configuration_Identification#Configuration_item
 * 
 * @verbatim
 * @property string $purpose
 * @property string $conceptId
 * @endverbatim
 * 
 * 
 * @verbatim
 * STEP EXAMPLE:
 * #100=PRODUCT_CONCEPT_CONTEXT('pcc_name1', #1, '');
 * #1=APPLICATION_CONTEXT();
 * #2=PRODUCT_CONCEPT('PC-M01', 'PC model name1', 'PC system', #100);
 * #5=CONFIGURATION_ITEM('PC-Conf1', 'Base Config Europe', 'PC system standard configuration for Europe', #2, $);
 * #6=CONFIGURATION_ITEM('PC-Conf2', 'Base Config US', 'PC system standard configuration for US', #2, $);
 * #7=CONFIGURATION_ITEM('PC-Conf3', 'High End Config US', 'High end PC system for US', #2, $);
 * #8=APPLICATION_CONTEXT('');
 * #9=PRODUCT_CONTEXT('', #8, '');
 * #10=PRODUCT('PC-0023', 'PC system', $, (#9));
 * #11=PRODUCT_DEFINITION_FORMATION('D', 'housing redesigned', #10);
 * #12=PRODUCT_DEFINITION_CONTEXT('part definition', #8, 'design');
 * #13=PRODUCT_DEFINITION('pc_v1', 'design view on base PC', #11, #12);
 * #14=PRODUCT_RELATED_PRODUCT_CATEGORY('part', $, (#10));
 * #15=CONFIGURATION_DESIGN(#5, #11);
 * #16=CONFIGURATION_DESIGN(#6, #11);
 * #17=CONFIGURATION_DESIGN(#7, #11); * 
 * @endverbatim
 */
class Item extends \Rbh\Pdm\PdmAbstract{
	
	/**
	 * The item_concept attribute specifies the product_concept of which the configuration_item is a member, i.e., of which it is a configuration. 
	 * 
	 * @var \Rbh\Pdm\Product\Concept
	 */
	protected $_concept;
	
	/**
	 * Id of \Rbh\Pdm\Product\Concept
	 * @var string
	 */
	protected $conceptId;
	
	/**
	 * Descriptive label providing a reason to create the item_concept.
	 * @var string
	 */
	protected $purpose;

	
	/**
	 * @param array 	$properties		array(name=>string, name=>string, description=>string, conceptId=>string, purpose=>string )
	 * @return void
	 */
	public function __construct($properties, $parent=null)
	{
		parent::__construct($properties, $parent);
	}
	
	
	/**
	 * Setter
	 * 
	 * @param \Rbh\Pdm\Product\Concept $concept
	 */
	public function setConcept(\Rbh\Pdm\Product\Concept &$concept){
		$this->conceptId = $concept->getUid();
		$this->_concept = $concept;
	}//End of method
	
	
	/**
	 * Getter
	 * 
	 * @retutn \Rbh\Pdm\Product\Concept
	 */
	public function getConcept(){
		if( !$this->_concept ){
			throw new \Rbh\Sys\Exception('PROPERTY_%0%_IS_NOT_SET', \Rbh\Sys\Error::ERROR, array('_concept'));
		}
		return $this->_concept;
	}//End of method

}//End of class
