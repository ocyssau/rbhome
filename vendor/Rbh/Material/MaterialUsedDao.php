<?php
//%LICENCE_HEADER%

/** SQL_SCRIPT>>
 CREATE TABLE usedmaterial(
 volume real,
 weight real,
 length real,
 quantity real,
 surface real,
 inputunit character(2),
 cid integer
 ) INHERITS (anyobject_links);
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (5010, 'Rbh\Material\UsedMaterial', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5011, 'Rbh\Material\UsedBulk', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5013, 'Rbh\Material\UsedDiscrete', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5014, 'Rbh\Material\UsedLineic', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5015, 'Rbh\Material\UsedSurfacic', 'usedmaterial');
 INSERT INTO classes (id, name, tablename) VALUES (5060, 'Rbh\Material\UsedRecipe', 'usedmaterial');
<<*/

/** SQL_ALTER>>
ALTER TABLE usedmaterial ADD UNIQUE (lparent, lchild);
ALTER TABLE usedmaterial ALTER COLUMN cid SET DEFAULT 5010;
CREATE INDEX INDEX_usedmaterial_parent ON usedmaterial USING btree (lparent);
CREATE INDEX INDEX_usedmaterial_child ON usedmaterial USING btree (lchild);
ALTER TABLE usedmaterial ADD PRIMARY KEY (luid);
<<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

namespace Rbh\Material;
use Rbh\AnyObject;
use Rbh\LinkDao;

/**
 * @brief Dao class
 *
 * @see Rb\Dao\Pg
 *
 */
class MaterialUsedDao extends LinkDao
{

	/**
	 *
	 * @var string
	 */
	var $table = 'usedmaterial';

	/**
	 * @var integer
	 */
	var $classId = 5010;

	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'volume'=>'volume',
		'weight'=>'weight',
		'length'=>'length',
		'quantity'=>'quantity',
		'surface'=>'surface',
		'inputunit'=>'inputUnit'
	);

	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbh\Material\Material	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbh\Material\Material
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		LinkDao::loadFromArray($mapped, $row, $fromApp);
		$mapped->volume = $row['volume'];
		$mapped->weight = $row['weight'];
		$mapped->length = $row['length'];
		$mapped->quantity = $row['quantity'];
		$mapped->surface = $row['surface'];
		$mapped->inputUnit = $row['inputunit'];
		return $mapped;
	} //End of function

	/**
	 * $mapped \Rbh\Link or array of \Rbh\Link
	 *
	 */
	public function insert($mapped, array $select = null)
	{
		$table = $this->table;
		
		if(!$this->insertStmt){
			$sql = 'INSERT INTO '.  $table.' (luid, lparent, lchild, lname, lindex, volume, weight, length, quantity, surface, inputunit, cid)';
			$sql .= ' VALUES (:uid, :parentId, :childId, :name, :lindex, :volume, :weight, :length, :quantity, :surface, :inputUnit, :cid)';
			$this->insertStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->connexion->beginTransaction();
			if(is_array($mapped)){
				foreach($mapped as $map){
					$map->classId = $this->classId;
					$this->insertStmt->execute($map->bind());
				}
			}
			else{
				$mapped->classId = $this->classId;
				$this->insertStmt->execute($mapped->bind());
			}
			$this->connexion->commit();
		}
		catch(Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	} //End of function

	/**
	 */
	public function update($mapped, array $select = null )
	{
		$table = $this->table;

		if(!$this->updateStmt){
			$set = array(
				'lparent=:parentId',
				'lchild=:childId',
				'lname=:name',
				'lindex=:lindex',
				'volume=:volume',
				'weight=:weight',
				'length=:length',
				'quantity=:quantity',
				'surface=:surface',
				'inputunit=:inputUnit',
				'cid=:cid'
			);
			$sql = 'UPDATE '.$table.' SET ' . implode(',', $set) . ' WHERE luid=:uid;';
			$this->updateStmt = $this->connexion->prepare($sql);
		}

		try{
			$this->connexion->beginTransaction();
			if(is_array($mapped)){
				foreach($mapped as $map){
					$map->classId = $this->classId;
					$this->updateStmt->execute($map->bind());
				}
			}
			else{
				$mapped->classId = $this->classId;
				$this->updateStmt->execute($mapped->bind());
			}
			$this->connexion->commit();
		}
		catch(Exception $e){
			$this->connexion->rollBack();
			throw $e;
		}
		return $this;
	} //End of function


	/**
	 * @param Rbh\AnyObject   $mapped
	 * @return void
	 * @throws Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$bind = array(
			':volume'=>(float)$mapped->volume,
			':weight'=>(float)$mapped->weight,
			':length'=>(float)$mapped->length,
			':quantity'=>(float)$mapped->quantity,
			':surface'=>(float)$mapped->surface,
			':inputUnit'=>$mapped->inputUnit,
		);
		if( $mapped->id > 0 ){
			$this->_update($mapped, $bind);
		}
		else{
			$this->_insert($mapped, $bind);
		}
	}
} //End of class

