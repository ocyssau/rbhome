<?php
//%LICENCE_HEADER%

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rbh\Material;

use Rbh\AnyObject;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Material extends AnyObject
{
	const TYPE_MATERIAL = 5000;
	const TYPE_BULK = 5001;
	const TYPE_COMPOSIT = 5002;
	const TYPE_DISCRETE = 5003;
	const TYPE_LINEIC = 5004;
	const TYPE_SURFACIC = 5005;
	const TYPE_RECIPE = 5050;
	
	/**@var literals**/
	public $density;
	public $thLambda;
	public $thSpecificHeat;
	public $classId = self::TYPE_MATERIAL;
	public $inputUnit = ''; //m, m2, m3, kg, u

	/**
	 * @var Rbh\Material\Material
	 */
	public $material;
	public $materialId;

	/**
	 * @var Rbh\Material\Material
	 */
	public $primaryMaterial;
	public $primaryMaterialId;

	/**
	 * @var
	 */
	public $purchased;

	/**
	 * @var Rbh\Parameter\Collection
	 */
	public $parameters;

	/**
	 * @var Rbh\People\Supplier
	 */
	public $builder;

	/**
	 * @var Rbh\People\Supplier
	 */
	public $supplier;
	public $supplierId;

	/**
	 * @var Rbh\Pack\Pack
	 */
	public $pack;
	public $packId;

	/**
	 * @return Rbh\Pack\Pack
	 */
	public function getPackaging()
	{
		if($this->pack === null){
			return $this->pack = new \Rbh\Pack\Lot();
		}
		else{
			return $this->pack;
		}
	}

}
