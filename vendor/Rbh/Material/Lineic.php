<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use \Rbh\Material\Material as Material;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Lineic extends Material
{
	public $section;
	public $length;
	public $type = Material::TYPE_LINEIC;
}
