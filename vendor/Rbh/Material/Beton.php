<?php
//%LICENCE_HEADER%

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rbh\Material;

use Rbh\AnyObject;
use Rbh\Material\Material;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Beton extends Material
{
	public function prepare($recipe, $volume)
	{
		$this->volume=$volume;
		$this->density=$recipe->density;
		$this->weight=$this->density*$this->volume;
		
		$lnk = \Rbh\Link::init(array(
			'parentId'=>$this->id, 
			'childId'=>$recipe->id, 
			'parent'=>$this, 
			'child'=>$recipe,
			'rule'=>\Rbh\Link::RULE_NONE));
		$this->links[$lnk->uid] = $lnk;
		
		$factor = $volume/$recipe->volume;
		
		foreach($recipe->links as $rlnk){
			$lnk=clone($rlnk);
			$lnk->parent = $this;
			$lnk->parentId = $this->id;
			$lnk->uid = \Rbh\Uuid::newUid();
			$lnk->data['volume']=$rlnk->data['volume']*$factor;
			$lnk->data['weight']=$rlnk->data['weight']*$factor;
			$this->links[$lnk->child->uid] = $lnk;
		}
	}
}
