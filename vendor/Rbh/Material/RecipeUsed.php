<?php
//%LICENCE_HEADER%

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rbh\Material;

use Rbh\AnyObject;
use Rbh\Material\MaterialUsed as MaterialUsed;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class RecipeUsed extends MaterialUsed
{
	/**@var literals**/
	public $volume=-1; //inherited from parent
	public $weight=-1; //kg of final mixture
	public $type = UsedMaterial::TYPE_USEDRECIPE;
	public $inputUnit = ''; //m, m2, m3, kg, u
	
	/**
	 * @param $volume volume en m3
	 */
	public function prepare($volume)
	{
	}
	
	/**
	 *
	 */
	public function getInheritedVolume(){
		if( (is_a($this->parent, '\Rbh\Build\Surface')
				||is_a($this->parent, '\Rbh\Material\Material')
		) && $this->volume = -1
		){
			$this->volume = $this->parent->getInheritedVolume();
		}
		return $this->volume;
	}
}
