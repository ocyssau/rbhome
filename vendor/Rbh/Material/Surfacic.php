<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use \Rbh\Material\Material as Material;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Surfacic extends Material
{
	public $thickness;
	public $length;
	public $width;
	public $surface;
	public $section;
	public $type = Material::TYPE_SURFACIC;
}

