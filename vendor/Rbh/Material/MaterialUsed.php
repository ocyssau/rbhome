<?php
//%LICENCE_HEADER%

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rbh\Material;

use Rbh\Link;
use Rbh\AnyUsed;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class MaterialUsed extends AnyUsed
{
	const TYPE_USEDMATERIAL = 5010;
	const TYPE_USEDBULK = 5011;
	const TYPE_USEDCOMPOSIT = 5012;
	const TYPE_USEDDISCRETE = 5013;
	const TYPE_USEDLINEIC = 50014;
	const TYPE_USEDSURFACIC = 5015;
	const TYPE_USEDRECIPE = 5060;
	
	/**@var literals**/
	public $volume; //inherited from parent
	public $weight;
	public $inputUnit = ''; //m, m2, m3, kg, u
	public $classId = self::TYPE_USEDMATERIAL;
	
	/**
	 *
	 */
	public function getWeight(){
		$this->getInheritedVolume();
		$this->useLink->data['weight'] = $this->useLink->data['volume'] * $this->useAny->density;
		return $this->useLink->data['weight'];
	}
	
	/**
	 *
	 */
	public function getInheritedVolume(){
		$parent = $this->useLink->parent;
		if( (is_a($parent, '\Rbh\Build\Surface')
			) && $this->volume = -1
		){
			$this->volume = $parent->getInheritedVolume();
		}
		elseif( is_a($parent, '\Rbh\Material\Recipe')
			 && $this->volume = -1
		){
		}
		return $this->volume;
	}
}
