<?php
//%LICENCE_HEADER%

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Rbh\Material;

use Rbh\AnyObject;
use \Rbh\Material\Material;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Recipe extends AnyObject
{
	const FINAL_MAT = 0; 
	const INTER_MAT = 1;
	
	/**@var literals**/
	public $density=0; //of final mixture
	public $volume=1; //m3 of finale mixture
	public $weight=0; //kg of final mixture
	
	public $classId = Material::TYPE_RECIPE;
	
	/**
	 * 
	 * @param \Rbh\Material\Material $material
	 * @param string $name
	 * @param real $qty
	 * @param string $unit
	 * @param integer $type = Recipe::FINAL | Recipe::INTER
	 */
	public function addIngredient(\Rbh\Material\Material $material, $name, $qty, $unit, $type=0)
	{
		switch($unit) {
			case 'kg':
				$data['volume'] = $qty / $material->density;
				$data['weight'] = $qty;
				$data['inputUnit'] = 'kg';
				break;
			case 'm3':
				$data['volume'] = $qty;
				$data['weight'] = $qty * $material->density;
				$data['inputUnit'] = 'm3';
				break;
			case 'm2':
				break;
			case 'm':
				break;
		}
		$lnk=\Rbh\Link::init(array('child'=>$material, 'childId'=>$material->id, 'name'=>$name, 'data'=>$data, 'rule'=>\Rbh\Link::RULE_ING));
		$lnk->child = $material;
		$lnk->parent = $this;
		$lnk->parentId = $this->id;
		$this->links[$material->uid] = $lnk;
		return $lnk;
	}
	
	/**
	 */
	public function prepare()
	{
		$this->weight=0;
		foreach($this->links as $lnk){
			$this->weight = $this->weight + ($lnk->data['weight']);
		}
		$this->density = ($this->weight / $this->volume);
	}
}
