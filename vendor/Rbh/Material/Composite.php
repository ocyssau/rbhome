<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use \Rbh\Material\Material as Material;

/**
* @brief
*
* @see Rbh/MaterialTest.php
* @version $Rev: 808 $
* @license GPL-V3: Rbh/licence.txt
*/
abstract class Composite extends AnyObject
{
	/**
	 * array
	 */
	protected $materials;
	
	public $type = Material::TYPE_COMPOSIT;
	
	
	/**
	 * @param Rbh_Material_Market
	 * @param real proportion de matiere
	 * @param string	proportion par vol, weight
	 */
	public function add(Rbh\Material\Material $Material, $qty, $unit)
	{
		$this->materials[$Material->getUid()]=array(
				'mat'=>$material,
				'qty'=>$qty,
				'unit'=>$unit
				);
	}
	
	/**
	 * Quantite total de matiere composite
	 * @param real
	 */
	public function setQuantity(real $qty)
	{
	}
	
	
	/**
	 * @param string
	 */
	public function getByName(string $name)
	{
		foreach($this->$materials as $mat){
			if($mat->getName() == $name){
				return $mat;
			}
		}
	}
	
	/**
	 * @param string
	 */
	public function getByUid(string $uid)
	{
		if(isset($this->$materials[$uid])){
			return $this->$materials[$uid];
		}
	}
}
