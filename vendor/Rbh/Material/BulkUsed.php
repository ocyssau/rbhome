<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use Rbh\Material\MaterialUsed as MaterialUsed;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class BulkUsed extends MaterialUsed
{
	public $type = UsedMaterial::TYPE_USEDBULK;
}
