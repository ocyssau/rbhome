<?php
//%LICENCE_HEADER%

/** SQL_SCRIPT>>
 CREATE TABLE materialrecipe(
 ) INHERITS (material);
 <<*/

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (5050, 'Rbh\Material\Recipe', 'materialrecipe');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE materialrecipe ADD PRIMARY KEY (id);
 ALTER TABLE materialrecipe ALTER COLUMN cid SET DEFAULT 5050;
 CREATE INDEX INDEX_materialrecipe_owner ON materialrecipe USING btree (owner);
 CREATE INDEX INDEX_materialrecipe_uid ON materialrecipe USING btree (uid);
 CREATE INDEX INDEX_materialrecipe_name ON materialrecipe USING btree (name);
 CREATE INDEX INDEX_materialrecipe_label ON materialrecipe USING btree (label);
 CREATE INDEX INDEX_materialrecipe_path ON materialrecipe USING btree (path);
 <<*/

/** SQL_FKEY>>
 ALTER TABLE materialrecipe ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_materialrecipe AFTER INSERT OR UPDATE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_materialrecipe AFTER DELETE
 ON materialrecipe FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

namespace Rbh\Material;
use Rbh\AnyObject;
use Rbh\Dao\Pg\Dao;

/**
 * @brief Dao class
 *
 * @see Rb\Dao\Pg
 *
 */
class RecipeDao extends Dao
{
	
	/**
	 *
	 * @var string
	 */
	var $table = 'material';
	
	/**
	 * @var integer
	 */
	var $classId = 5050;
	
	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'density'=>'density',
		'volume'=>'volume',
		'weight'=>'weight',
		'thlambda'=>'thLambda',
		'thspecificheat'=>'thSpecificHeat'
		);
	
	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbh\Material\Recipe	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbh\Material\Recipe
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		$mapped->type = $row['cid'];
		$mapped->density = $row['density'];
		$mapped->volume = $row['volume'];
		$mapped->weight = $row['weight'];
		$mapped->thLambda = $row['thlambda'];
		$mapped->thSpecificHeat = $row['thspecificheat'];
		return $mapped;
	} //End of function
	
	/**
	 * @param Rbplm_Org_Unit   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		//$this->classId = $mapped->classId;
		
		$bind = array(
			':density'=>$mapped->density,
			':volume'=>$mapped->volume,
			':weight'=>$mapped->weight,
			':thLambda'=>$mapped->thLambda,
			':thSpecificHeat'=>$mapped->thSpecificHeat,
		);
		if( $mapped->id > 0 ){
			$this->_update($mapped, $bind);
		}
		else{
			$this->_insert($mapped, $bind);
			// @todo : change and optimize
		}
	}
	
	/**
	 * Load the properties in the mapped object.
	 *
	 * @param Rbplm_Org_Unit	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public function loadMaterials( $mapped )
	{
		$table = 'material';
		$select = '*';
		$filter = 'parent='.$mapped->id;
		$sql = "SELECT $select FROM $table WHERE $filter";
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute();
		$Dao = new \Rbh\Material\MaterialDao();
		while( $row = $stmt->fetch() ){
			$usedMaterial = new \Rbh\Material\Material();
			$Dao->loadFromArray($usedMaterial, $row);
			$mapped->materials[] = $usedMaterial;
		}
		return $this;
	} //End of function
	
	
} //End of class

