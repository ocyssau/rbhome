<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use \Rbh\Test\Test;
use \Rbh\Material\Material;
use \Rbh\Material\UsedMaterial;
use \Rbh\Dao\Factory as DaoFactory;

/**
 * @brief Test class for Rbh_Material.
 * @include Rbh/MaterialTest.php
 * 
 */
class MaterialTest extends Test
{
	public function Test_Constructor()
	{
		$monProjet = \Rbh\Org\Root::singleton();
		$Material1 = Material::init('ciment');
		$Material1->density = 1000;
		$Material2 = Material::init('sablesec');
		$Material2->density = 2000;
		$Material3 = Material::init('gravier');
		$Material3->density = 4000;
	}
}
