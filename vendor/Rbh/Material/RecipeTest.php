<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use \Rbh\Test\Test;
use \Rbh\Material\Recipe;
use \Rbh\Material\Material;
use Rbh\Dao\Pg\Loader AS Loader;
use Rbh\Dao\Factory AS DaoFactory;

/**
 * @brief Test class for Rbh_Material.
 * @include Rbh/MaterialTest.php
 *
 */
class RecipeTest extends Test
{
	/**
	 *
	 */
	public static function recursifChildren($obj, $lnk=null, $level=1)
	{
		echo str_repeat('.', $level) . $obj->name  .' - '. get_class($obj);
		echo ' -- ID:'.$obj->id . '--';
		if(is_a($obj,'\Rbh\Material\Recipe')){
			echo ' density='.$obj->density.'Kg/m3';
			if($lnk && is_object($lnk->data)){
				echo ' used weight='.$lnk->data->weight.'Kg';
				echo ' used volume='.$lnk->data->volume.'m3';
			}
			else{
				echo ' volume='.$obj->volume.'m3';
				echo ' weight='.$obj->weight.'kg';
			}
			//echo ' used weight='.$lnk->data['weight'].'Kg';
			//echo ' used volume='.$lnk->data['volume'].'m3';
			//echo ' volume='.$obj->getInheritedVolume().'m3';
		}
		elseif(is_a($obj,'\Rbh\Material\Material')){
			echo ' density='.$obj->density.'Kg/m3';
			if($lnk && is_object($lnk->data)){
				echo ' used weight='.$lnk->data->weight.'Kg';
				echo ' used volume='.$lnk->data->volume.'m3';
			}
			//echo ' volume='.$obj->getInheritedVolume().'m3';
			//echo ' weight='.$obj->getWeight().'Kg';
		}
		elseif(is_a($obj,'\Rbh\Build\Surface')){
			echo ' Volume='.$obj->getInheritedVolume().'m3';
			echo ' Surface='.$obj->getInheritedSurface().'m2';
			echo ' Thickness='.$obj->getInheritedThickness().'m';
		}
		echo  CRLF;
		
		if(is_array($obj->links)){
			foreach($obj->links as $lnk){
				self::recursifChildren($lnk->child, $lnk, $level+1);
			}
		}
	}
	
	public function Test_Constructor()
	{
		$Recipe = Recipe::init('beton1');
		$Material1 = Material::init('ciment');
		$Material1->density = 1000;
		$Material2 = Material::init('sablesec');
		$Material2->density = 2000;
		$Material3 = Material::init('gravier');
		$Material3->density = 4000;
		
		$Recipe->volume = 1;
		$Recipe->addIngredient($Material1, 'liant', 250, 'kg');
		$Recipe->addIngredient($Material2, 'granulat04', 0.5, 'm3');
		$Recipe->addIngredient($Material3, 'granulat30', 0.5, 'm3');
		$Recipe->prepare();
		
		foreach($Recipe->links as $lnk){
			if(is_a($lnk->child, '\Rbh\Material\Material')){
				$material = $lnk->child;
				var_dump($lnk->child->name, $lnk->data['weight'], $lnk->data['volume']);
			}
		}
		
		//var_dump($Recipe->links[$Material1->uid]);
		assert($Recipe->links[$Material1->uid]->name == 'liant');
		assert($Recipe->links[$Material1->uid]->child->name == 'ciment');
		assert($Recipe->links[$Material2->uid]->child->name == 'sablesec');
		assert($Recipe->links[$Material2->uid]->data['volume'] == 0.5);
		assert($Recipe->links[$Material2->uid]->data['weight'] == 1000);
		assert($Recipe->links[$Material3->uid]->child->name == 'gravier');
		assert($Recipe->links[$Material3->uid]->data['volume'] == 0.5);
		assert($Recipe->links[$Material3->uid]->data['weight'] == 2000);
		
		$Beton = \Rbh\Material\Beton::init('monbeton1');
		$Beton->prepare($Recipe,10);
		assert($Beton->volume = 10);
		//var_dump($Beton->links[$Material1->uid]->data['weight']);
		//var_dump($Recipe->links[$Material1->uid]->data['weight']);
		assert($Beton->weight = $Recipe->weight*10);
		assert($Beton->links[$Material1->uid]->data['weight'] == $Recipe->links[$Material1->uid]->data['weight']*10);
		assert($Beton->links[$Material2->uid]->data['weight'] == $Recipe->links[$Material2->uid]->data['weight']*10);
		assert($Beton->links[$Material3->uid]->data['weight'] == $Recipe->links[$Material3->uid]->data['weight']*10);
	}

	public function Test_DaoSave()
	{
		$RecipeDao = DaoFactory::getDao('\Rbh\Material\Recipe');
		$MaterialDao = DaoFactory::getDao('\Rbh\Material\Material');
		$LinkDao = DaoFactory::getDao('\Rbh\Link');
		$RecipeDao->newList()->suppress('name LIKE \'recipetest_recipe%\'');
		$MaterialDao->newList()->suppress('name LIKE \'recipetest_ing%\'');
		
		$Recipe = Recipe::init('recipetest_recipe1');
		$Material1 = Material::init('recipetest_ing1');
		$Material2 = Material::init('recipetest_ing2');
		$Material3 = Material::init('recipetest_ing3');
		$Material1->density=1000;
		$Material2->density=2000;
		$Material3->density=3000;
		$MaterialDao->save($Material1);
		$MaterialDao->save($Material2);
		$MaterialDao->save($Material3);
		$Recipe->addIngredient($Material1, 'liant', 250, 'kg');
		$Recipe->addIngredient($Material2, 'granulat04', 0.5, 'm3');
		$Recipe->addIngredient($Material3, 'granulat30', 0.5, 'm3');
		$Recipe->prepare();
		$RecipeDao->save($Recipe);
		$this->recursifChildren($Recipe);
	}
	
	public function Test_DaoLoad()
	{
		$RecipeDao = DaoFactory::getDao('\Rbh\Material\Recipe');
		$LinkDao = DaoFactory::getDao('\Rbh\Link');
		
		$Recipe = $RecipeDao->load(new Recipe(), "name='recipetest_recipe1'");
		$LinkDao->loadChildren($Recipe);
		
		Loader::init();
		//Loader::loadChildren($Recipe);
		
		foreach($Recipe->links as $lnk){
			$class = Loader::getClassFromId($lnk->childId);
			$lnk->child = Loader::load($lnk->childId, $class);
		}
		
		$this->recursifChildren($Recipe);
	}
}
