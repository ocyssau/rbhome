<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use \Rbh\Material\UsedMaterial as UsedMaterial;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class SurfacicUsed extends MaterialUsed
{
	public $thickness;
	public $length;
	public $width;
	public $surface;
	public $type = Material::TYPE_USEDSURFACIC;
}

