<?php
//%LICENCE_HEADER%

/** SQL_SCRIPT>>
CREATE TABLE material(
	density real,
	volume real,
	weight real,
	thlambda real,
	thSpecificHeat real,
	material bigint,
	inputunit character(2)
) INHERITS (anyobject);
 <<*/

//ALTER TABLE material ADD COLUMN material bigint;

/** SQL_INSERT>>
INSERT INTO classes (id, name, tablename) VALUES (5000, 'Rbh\Material\Material', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5001, 'Rbh\Material\Bulk', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5002, 'Rbh\Material\Composite', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5003, 'Rbh\Material\Discrete', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5004, 'Rbh\Material\Lineic', 'material');
INSERT INTO classes (id, name, tablename) VALUES (5005, 'Rbh\Material\Surfacic', 'material');
 <<*/

/** SQL_ALTER>>
 ALTER TABLE material ADD PRIMARY KEY (id);
 ALTER TABLE material ALTER COLUMN cid SET DEFAULT 5000;
 CREATE INDEX INDEX_material_owner ON material USING btree (owner);
 CREATE INDEX INDEX_material_uid ON material USING btree (uid);
 CREATE INDEX INDEX_material_name ON material USING btree (name);
 CREATE INDEX INDEX_material_label ON material USING btree (label);
 CREATE INDEX INDEX_material_path ON material USING btree (path);
 <<*/

/** SQL_FKEY>>
 ALTER TABLE material ADD FOREIGN KEY (owner) REFERENCES people_user (id) ON UPDATE CASCADE ON DELETE SET NULL;
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_material AFTER INSERT OR UPDATE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 
 CREATE TRIGGER trig02_material AFTER DELETE
 ON material FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL_VIEW>>
 <<*/

/** SQL_DROP>>
 <<*/

namespace Rbh\Material;
use Rbh\AnyObject;
use Rbh\Dao\Pg\Dao;

/**
 * @brief Dao class
 *
 * @see Rb\Dao\Pg
 *
 */
class MaterialDao extends Dao
{
	
	/**
	 *
	 * @var string
	 */
	var $table = 'material';
	
	/**
	 * @var integer
	 */
	var $classId = 5000;
	
	/**
	 * @var array
	 */
	public static $sysToApp = array(
		'density'=>'density',
		'volume'=>'volume',
		'weight'=>'weight',
		'thlambda'=>'thLambda',
		'thspecificheat'=>'thSpecificHeat',
		'material'=>'primaryMaterialId',
		'inputunit'=>'inputUnit'
		);
	
	/**
	 * Load the properties in the mapped object.
	 *
	 * @param \Rbh\Material\Material	$mapped
	 * @param array $row			\PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return \Rbh\Material\Material
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		$mapped->density = $row['density'];
		$mapped->volume = $row['volume'];
		$mapped->weight = $row['weight'];
		$mapped->thLambda = $row['thlambda'];
		$mapped->thSpecificHeat = $row['thspecificheat'];
		$mapped->primaryMaterialId = $row['material'];
		$mapped->inputUnit = $row['inputunit'];
		$mapped->classId = $row['cid'];
		return $mapped;
	} //End of function
	
	/**
	 * @param Rbplm_Org_Unit   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		//$this->classId = $mapped->classId;
		if(is_object($mapped->primaryMaterial)){
			$mapped->primaryMaterialId = $mapped->primaryMaterial->id;
		}
		$bind = array(
			':density'=>(float)$mapped->density,
			':volume'=>(float)$mapped->volume,
			':weight'=>(float)$mapped->weight,
			':thLambda'=>(float)$mapped->thLambda,
			':thSpecificHeat'=>(float)$mapped->thSpecificHeat,
			':primaryMaterialId'=>(int)$mapped->primaryMaterialId,
			':inputUnit'=>$mapped->inputUnit,
		);
		if( $mapped->id > 0 ){
			$this->_update($mapped, $bind);
		}
		else{
			$this->_insert($mapped, $bind);
		}
	}
} //End of class

