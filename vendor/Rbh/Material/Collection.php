<?php
//%LICENCE_HEADER%

namespace Rbh\Material;

use Rb\Model\Collection;

/**
 * @brief
*
* @see Rbh/MaterialTest.php
* @version $Rev: 808 $
* @license GPL-V3: Rbh/licence.txt
*/
class Collection extends Collection
{
	public function newUsed($usedMaterial)
	{
		$class = get_class($usedMaterial);
		$instance = '';
		$umat = new $class($usedMaterial->name & '.' & $instance, $this);
		$umat->material = $usedMaterial;
		return $umat;
	}
	
	public function newMaterial($name, $class)
	{
		$umat = new $class($name, $this);
		return $umat;
	}
}
