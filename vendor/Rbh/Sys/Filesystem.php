<?php
//%LICENCE_HEADER%

namespace Rbh\Sys;

use Rbh\Sys\Exception as Exception;
use Rbh\Sys\Error as Error;

/**
 * @brief Filesystem on the server
 *
 */
class Filesystem
{
	
	/**
	 * Array of paths where rbplm is authorized to write.
	 * @var array
	 */
	static private $_authorized_dir = array ();
	
	/**
	 * Activate or not write limitations.
	 * @var boolean
	 */
	static private $_limit_dir_activate = true;
	
	/**
	 * Active/unactive the protection against write in not authorized directories
	 */
	static public function isSecure($bool)
	{
		if( $bool === null ){
			return self::$_limit_dir_activate;
		}
		else{
			return self::$_limit_dir_activate = (boolean) $bool;
		}
	} //End of function
	
	/**
	 *
	 * @param string	$path
	 * @return void
	 */
	static public function addAuthorized($path)
	{
		$path = (string) $path;
		self::$_authorized_dir[] = $path;
	} //End of function

	/**
	 * Check if the request file is in a authorized directory.
	 * Return true if it is authorized else return false.
	 *
	 *
	 * @param string	$path	Fullpath to test
	 * @return boolean
	 */
	static function limitDir($path)
	{
		if (self::$_limit_dir_activate == false){
			return true;
		}
		
		//".." , "//" "/./" forbidden in the path
		if (strpos ( "$path", '..' )) {
			throw new Exception('characters .. forbidden in path');
		}
		if (strpos ( "$path", '//' )) {
			throw new Exception('characters // forbidden in path');
		}
		if (strpos ( "$path", '/./' )) {
			throw new Exception('characters /./ forbidden in path');
		}
		$path = str_replace('\\', '/', $path);

		//Check that the path is in a authorized directory
		foreach ( self::$_authorized_dir as $motif ) {
			if (empty ( $motif )){
				continue;
			}
			$motif = str_replace('\\', '/', $motif);
			$motif = ltrim ( $motif, './' );
			$path = ltrim ( $path, './' );
			if (rtrim ( $path, '/' ) == rtrim ( $motif, '/' )) {
				return false;
			} // Check that the system directories are not directly manipulated
				
			if (strpos ( $path, $motif ) === 0){
				return true; // Check that the file is in a directory of the system
			}
		} //End of foreach
		
		throw new Exception('LIMIT_ACCESS_VIOLATION', Error::WARNING, $path);
		return false;
	} //End of function


	/**
	 * Generation of uniq identifiant
	 *
	 * @return strting
	 */
	static function uniq_id()
	{
		return md5 ( uniqid ( rand () ) );
	}


	/**
	 * Encode path
	 */
	static function encodePath($path)
	{
		$path = str_replace ( '/', '%2F', $path );
		$path = str_replace ( '.', '%2E', $path );
		$path = str_replace ( ':', '%3A', $path );
		$path = str_replace ( '\\', '%2F', $path );
		return $path;
	}

	
	/**
	 * Decode path
	 */
	static function decodePath($path)
	{
		$path = str_replace ( '%2F', '/', $path );
		$path = str_replace ( '%2E', '.', $path );
		$path = str_replace ( '%3A', ':', $path );
		return $path;
	}

} //End of class





