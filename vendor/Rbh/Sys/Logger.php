<?php
//%LICENCE_HEADER%

namespace Rbh\Sys;

/**
 * @brief Logger for Rbplm. Inherit from Zend_Log and add singleton pattern.
 * Singleton instanciate logger and set a Zend_Log_Writer_Syslog writer.
 * In futur version, singleton may add other functionalities to logger.
 * 
 * Consult Zend_Log documentation to learn more about Logger:
 * @see http://framework.zend.com/manual/en/zend.log.html
 */
class Logger extends \Zend\Log\Logger
{
	
	/**
	 * 
	 * @var Rbplm_Sys_Logger
	 */
	private static $instance;
	
    const EMERG   = 0;  // Emergency: system is unusable
    const ALERT   = 1;  // Alert: action must be taken immediately
    const CRIT    = 2;  // Critical: critical conditions
    const ERR     = 3;  // Error: error conditions
    const WARN    = 4;  // Warning: warning conditions
    const NOTICE  = 5;  // Notice: normal but significant condition
    const INFO    = 6;  // Informational: informational messages
    const DEBUG   = 7;  // Debug: debug messages
	
	
    /**
     * Singleton method.
     * Add the writer syslog.
     * 
     * @return Rbplm_Sys_Logger
     */
	public static function singleton(){
		if (self::$instance === null) {
			self::$instance = new Logger();
			$writer = new Zend\Log\Writer\Syslog();
			$writer->setApplicationName('Rbplm');
			self::$instance->addWriter($writer);
		}
		return self::$instance;
	}

	
	/**
	 * @see Zend/Zend_Log#log($message, $priority, $extras)
	 */
	public function log($message, $priority = Zend\Log::INFO, $extras = null){
		return parent::log($message, $priority, $extras);
	}

} //End of class
