<?php
//%LICENCE_HEADER%

namespace Rbh\Sys;

use Rbh\Sys\Exception as Exception;
use Rbh\Sys\Error as Error;
use Rbh\Sys\Filesystem;
use Rbh\SysLogger;

/**
 * @brief Directory on filesystem
 *
 */
class Directory extends Filesystem
{

	/**
	 *
	 */
	static public $default_trash_dir = '/tmp';


	/**
	 * A function to copy files from one Directory to another one,
	 * including subdirectories and nonexisting or newer files.
	 *
	 * @param String $srcdir
	 * @param String $dstdir
	 * @param Bool $verbose
	 * @param Bool $initial
	 * @param Bool $recursive
	 * @param Integer $mode
	 * @return Bool
	 */
	static function copy($srcdir, $dstdir, $verbose = false, $initial=true, $recursive=true, $mode=0755)
	{
		if (!Filesystem::LimitDir($srcdir)){
			throw new Exception('NO_ACCESS_TO_%path%', Error::ERROR, array('path'=>$srcdir));
		}
		
		if(!is_dir($dstdir)){
			mkdir($dstdir, $mode, true);
		}
		if($curdir = opendir($srcdir)) {
			while($file = readdir($curdir)) {
				if($file != '.' && $file != '..') {
					$srcfile = "$srcdir" . '/' . "$file";
					$dstfile = "$dstdir" . '/' . "$file";
					if(is_file("$srcfile")) {
						if($verbose) echo "Copying '$srcfile' to '$dstfile'...";
						if(copy("$srcfile", "$dstfile")) {
							touch("$dstfile", filemtime("$srcfile"));
							if($verbose) echo "OK\n";
						}
						else {
							print "Error: File '$srcfile' could not be copied!\n"; return false;
						}
					}
					else{
						if(is_dir("$srcfile") && $recursive){
							self::dircopy("$srcfile", "$dstfile", $verbose, false);
						}
					}
				}
			}
			closedir($curdir);
		}
		return true;
	}//End of function

	/**
	 * Create a new Directory recursivly
	 *
	 * @param 	String 		$path
	 * @param 	Integer 	$mode
	 * @param 	Boolean 	$secure, if true, check that Directory to create is in a authorized Directory
	 * @param 	Boolean 	$test_mode
	 * @return 	Boolean
	 */
	static function create($path, $mode=0755, $secure=true, $test_mode=false)
	{
		if($secure){
			if (!Filesystem::LimitDir($path)){
				throw new Exception('NO_ACCESS_TO_%path%', Error::ERROR, array('path'=>$path));
			}
		}

		if( file_exists($path) ){
			throw new Exception('%path%_IS_EXISTING', Error::ERROR, array('path'=>$path));
		}

		//Le "mode" ne fonctionne pas sous windows, ce qui oblige a faire un
		//chmod par la suite.
		//bool mkdir ( string pathname [, int mode [, bool recursive [, resource context]]] )
		if(!is_dir($path)){
			if(mkdir($path, $mode, true)){
				if (! chmod ($path, $mode) ) { //!!chmod ne fonctionne pas avec les fichiers distants!!
					Logger::singleton()->log('Chmod impossible on ' . $path);
					return true;
				}
				Logger::singleton()->log('Create directory ' . $path);
				return true;
			}
			else{
				throw new Exception('CREATE_FAILED_OF_%path%', Error::ERROR, array('path'=>$path));
			}
		}
		else{
			Logger::singleton()->log('Directory '. $path .'is yet existing ');
			return true;
		}
	}//End of function
	
	/**
	 * Remove files from one Directory to another one, including subdirectories and nonexisting or newer files.
	 *   This function is a adaptation from the script of "....." visible in PHP help site.
	 *
	 * @param String $path
	 * @param Bool $recursive
	 * @param Bool $verbose
	 * @param Bool $test_mode
	 * @return Bool
	 *
	 */
	static function delete($path, $recursive=false, $verbose = false)
	{
		if (!Filesystem::LimitDir($path)) {
			print 'removeDir error : you have no right access on file '. $path;
			return false;
		}
		
		// Add trailing slash to $path if one is not there
		if (substr($path, -1, 1) != "/"){
			$path .= "/";
		}
		foreach (glob($path . "*") as $file){
			if (is_file($file) === TRUE){
				// Remove each file in this Directory
				if (!unlink($file)){
					echo "failed to removed File: " . $file . "<br>";
					return false;
				}
				else{ 
					if($verbose) echo "Removed File: " . $file . "<br>";
				}
			}
			else if (is_dir($file) === true && $recursive === true){
				// If this Directory contains a Subdirectory and if recursivity is requiered, run this Function on it
				if (!self::removeDir($file, $recursive, $verbose)){
					if($verbose) echo "failed to removed File: " . $file . "<br>";
					return false;
				}
			}
		}

		// Remove Directory once Files have been removed (If Exists)
		if(is_dir($path)){
			if(@rmdir($path)){
				if($verbose) echo "<br>Removed Directory: " . $path . "<br><br>";
				return true;
			}
		}
		return false;
	}//End of function

	/**
	 * Remove files from one Directory to another one, including subdirectories and put in a trash.
	 *
	 * @param String $dir
	 * @param Bool $verbose
	 * @param Bool $recursive
	 * @return Bool
	 */
	static function putDirInTrash($dir, $verbose = false, $recursive=false)
	{
		$trashDir = self::$default_trash_dir;
		
		//Check if directories exists and are writable
		if (!is_dir($dir) || !is_dir($trashDir) || !is_writable($trashDir)) {
			print ' error : '. "$dir" . ' don\'t exist or is not writable <br />';
			print ' or    : '. "$trashDir" . ' don\'t exist or is not writable <br />';
			return false;
		}

		if( $test_mode) return true;

		//Add original path to name of dstfile
		$oriPath = self::encodePath(dirname($dir));
		
		//Check if there is not conflict name in trash dir else rename it
		$dstdir = $trashDir .'/'. "$oriPath" .'%&_'. basename($dir);
		$i = 0;
		if(is_dir($dstdir)){
			//Rename destination dir if exist
			$Renamedstdir = $dstdir;
			while(is_dir($Renamedstdir)){
				$Renamedstdir = $dstdir . '(' . "$i" . ')';
				$i ++;
			}
			$dstdir = $Renamedstdir;
		}
		
		//Copy dir to trash
		if (!self::dircopy($dir , $dstdir, false, false, true, 0755 )){ //copy dir to dirtrash
			print 'error copying dir '. $dir . ' to Trash: '. $dstdir . ' <br />';
			return false;
		}else{ //Suppress original dir
			if (!self::removeDir($dir, $recursive, $verbose)){
				return false;
			}
		}
		return true;

	}//End of function

	
	/**
	 *
	 * @param string $path
	 * @param string $regex regular expression to verified by file name
	 * @return RecursiveDirectoryIterator
	 */
	static function getIterator($path, $regex = false)
	{
		if(!$this->iterator){
			if($regex){
				$this->iterator = new RegexIterator(new RecursiveDirectoryIterator($path), '/'.$regex.'/i', RegexIterator::GET_MATCH);
			}
			else{
				$this->iterator = new RecursiveDirectoryIterator($path);
			}
		}
		return $this->iterator;
	}//End of method


	/**
	 * This method can be used to get files in directory.
	 * Return a array of files property if no errors, else return false.
	 *
	 * @param String $path
	 * @param Filesystem_Filter	$Filter
	 * @return Array|false
	 */
	public static function getDatas($path = '', $Filter = null)
	{
		$c = $this->getCollectionDatas();
		$r = array();
		foreach($c as $f){
			$r[] = $f->getProperties();
		}
		return $r;
	}//End of method

	/**
	 * This method can be used to get files in directory.
	 * Return a collectio of Rbplm_Sys_Datatype_Interface objects if no errors, else return false.
	 *
	 * @param String $path
	 * @param Filesystem_Filter	$Filter
	 * @return Array|false
	 */
	public static function getCollectionDatas($path = '', $Filter = null)
	{
		if( empty($path) ){
			throw new Exception(Error::INVALID_PATH, Error::ERROR, $path);
		}

		$sortBy = 'name';
		$sortOrder = 'ASC';
		$limit = 0;
		$offset = 0;

		if(is_a($Filter, 'Filesystem_Filter')){
			$regex = $Filter->toRegex();
			$sortBy = $Filter->getSortBy();
			$sortOrder = $Filter->getSortOrder();
			$limit = $Filter->getLimit();
			$offset = $Filter->getOffset();
		}

		try{
			$it = new DirectoryIterator($path);
			$return = array();
			$count = 0;
			foreach ($it as $file) {
				if( $it->isDot() ){
					continue;
				}
				if( $regex ){
					if( !preg_match('/'.$regex.'/i', $file->getFilename() ) ){
						continue;
					}
				}
				if( $offset && $count < $offset){
					$count++;
					continue;
				}
				$odata = Rbplm_Sys_Fsdata::_dataFactory( $file->getPath() . '/' . $file->getFilename() );
				if( $odata ){
					$key = call_user_method('get' . ucfirst($sortBy), $odata) . '.' . $count; //key used to sort
					$return[$key] = $odata;
					$count++;
				}
				if( $limit && $count > $limit){
					break;
				}
			}
		}
		catch(Exception $e){
			throw new Exception(Error::INVALID_PATH, Error::ERROR, $path);
		}

		if($sortOrder == 'DESC') {
			krsort($return);
		}
		else if($sortOrder == 'ASC'){
			ksort($return);
		}

		return array_values($return);
	}//End of method


	/**
	 * Alternative (peu performante) a disk_total_space qui ne comprend pas les chemins
	 * relatifs (sur windows)
	 * source du scripts : http://fr3.php.net/manual/fr/function.disk-total-space.php
	 *
	 * @param String $path
	 * @return Integer
	 */
	static function dskspace($path)
	{
		$s = filesize($path);
		$size = $s;
		if (is_dir($path)){
			$dh = opendir($path);
			while (($file = readdir($dh)) !== false)
				if ($file != "." and $file != "..")
				$size += self::dskspace($path."/".$file);
			closedir($dh);
		}
		return $size;
	}//End of method

	/** Méthode qui permet de décompresser un fichier zip $file dans un répertoire de destination $path
	 *  et qui retourne un tableau contenant la liste des fichiers extraits
	 *  Si $effacer_zip est égal à true, on efface le fichier zip d'origine $file
	 */
	function unzip($file, $path='', $effacer_zip=false)
	{
		$tab_liste_fichiers = array(); //Initialisation

		$zip = zip_open($file);

		if ($zip)
		{
			while ($zip_entry = zip_read($zip)) //Pour chaque fichier contenu dans le fichier zip
			{
				if (zip_entry_filesize($zip_entry) > 0)
				{
					$complete_path = $path.dirname(zip_entry_name($zip_entry));

					/*On supprime les éventuels caractères spéciaux et majuscules*/
					$nom_fichier = zip_entry_name($zip_entry);
					$nom_fichier = strtr($nom_fichier,"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ","AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");
					$nom_fichier = strtolower($nom_fichier);
					$nom_fichier = ereg_replace('[^a-zA-Z0-9.]','-',$nom_fichier);

					/*On ajoute le nom du fichier dans le tableau*/
					array_push($tab_liste_fichiers,$nom_fichier);

					$complete_name = $path.$nom_fichier; //Nom et chemin de destination

					if(!file_exists($complete_path))
					{
						$tmp = '';
						foreach(explode('/',$complete_path) AS $k)
						{
							$tmp .= $k.'/';

							if(!file_exists($tmp))
							{
								mkdir($tmp, 0755);
							}
						}
					}

					/*On extrait le fichier*/
					if (zip_entry_open($zip, $zip_entry, "r"))
					{
						$fd = fopen($complete_name, 'w');

						fwrite($fd, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));

						fclose($fd);
						zip_entry_close($zip_entry);
					}
				}
			}

			zip_close($zip);

			/*On efface éventuellement le fichier zip d'origine*/
			if ($effacer_zip === true)
				unlink($file);
		}

		return $tab_liste_fichiers;
	}

}



