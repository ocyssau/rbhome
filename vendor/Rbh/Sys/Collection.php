<?php
//%LICENCE_HEADER%

namespace Rbh\Sys;

/**
 * @brief Collection of \Rbplm\LinkableInterface object.
 * 
 * See Php \SplObjectStorage documentation:
 * @link http://php.net/manual/en/class.splobjectstorage.php
 * 
 * Collection implements RecursiveIterator interface and may be iterate on all COMPOSITE OBJECTS tree.
 * Collection implements \Rbplm\LinkableInterface to be link to a \Rbplm\AnyObject.
 * Each item of this collection must be a \Rbplm\LinkableInterface object.
 * 
 * A collection is a \Rbplm\Model\CompositComponentInterface, so it may be add to a other collection and be integrated to COMPOSIT OBJECTS tree.
 * 
 * Example and tests: Rbplm/Model/CollectionTest.php
 * 
 * @see \Rbplm\Model\CompositComponentInterface
 * @see \Rbplm\LinkableInterface
 * 
 * @link http://php.net/manual/en/class.recursiveiterator.php
 * 
 */
class Collection extends \SplObjectStorage implements \RecursiveIterator
{
	
	/**
	 * 
	 * @var string
	 */
	protected $_uid;
	
	/**
	 * 
	 * @var string
	 */
	protected $_name;
		
	/**
	 * Implement design pattern COMPOSITE OBJECT.
	 * @var Collection
	 */
	protected $_children;
	
	/**
	 * 
	 * @var CompositComponentInterface
	 */
	protected $_parent;
	
	/**
	 * Constructor.
	 * 
	 * @param array	$properties
	 * @param CompositComponentInterface $parent
	 */
	public function __construct( array $properties = null, \Rbh\AnyObject $parent = null )
	{
	    if(is_array($properties)){
	        foreach($properties as $name=>$value){
	            $this->$name = $value;
	        }
	    }
	    if($parent){
	        $this->setParent($parent);
	    }
	}
	
	
	/**
	 * Implements RecursiveIterator.
	 * Do not confuse with CompositComponentInterface::getChild
	 * 
	 * @return Collection | false
	 */
	public function getChildren()
	{
	    if( $this->current() instanceof CompositComponentInterface ){
			return $this->current()->getChild();
		}
		else{
			return false;
		}
	}
	
	
	/**
	 * 
	 * Implements RecursiveIterator.
	 * Do not confuse with CompositComponentInterface::hasChild
	 * 
	 * @return boolean
	 */
	public function hasChildren ()
	{
		if( $this->current() instanceof CompositComponentInterface ){
			return $this->current()->hasChild();
		}
		else{
			return false;
		}
	}
	
	
	/**
	 *
	 * Setter for children collection
	 * @param Collection $collection
     * @return Collection
	 */
	public function setChild(Collection $collection)
	{
		$this->_children = $collection;
		return $this;
	} //End of method
	
	
	/**
	 * Getter for the children collection.
	 * @return Collection
	 */
	public function getChild()
	{
		if( !$this->_children ){
			$this->_children = new Collection( array('name'=>get_class($this) . '_Children'), $this );
		}
		return $this->_children;
	} //End of method
	
	
	/**
	 * Return true if element has childrens
	 * @return boolean
	 */
	public function hasChild()
	{
		if( !$this->_children ){
			return false;
		}
		else if( $this->_children->count() == 0){
			return false;
		}
		else{
			return true;
		}
	} //End of method
	
	
	/**
	 * Add a object to collection.
	 * Alias for attach.
	 * 
	 * @param LinkableInterface
	 * @param mixed			Additionals datas to associate to object in collection
	 * @return Collection
	 */
	public function add($object, $data = null)
	{
		parent::attach($object, $data);
		return $this;
	}
	
	
	/**
	 * Overload of \SplObjectStorage::attach
	 */
	/*
	public function attach($object, $data)
	{
		$this->_indexUid[ $object->getUid() ] = $object;
		$this->_indexName[ $object->getName() ] = $object;
		parent::attach($object, $data);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::detach
	 */
	/*
	public function detach($object)
	{
		unset( $this->_indexUid[ $object->getUid() ] );
		unset( $this->_indexName[ $object->getName() ] );
		return parent::detach($object);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::offsetUnset
	 */
	/*
	public function offsetUnset($object)
	{
		$this->detach($object);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::offsetSet
	 */
	/*
	public function offsetSet($object, $data = null)
	{
		$this->attach($object, $data);
	}
	*/
	
	/**
	 * Overload of \SplObjectStorage::removeAll
	 */
	/*
	public function removeAll($storage)
	{
		$this->_indexName = array();
		$this->_indexUid = array();
		parent::removeAll($storage);
		foreach($this as $obj){
			$this->_indexName[$obj->getName()] = $obj;
			$this->_indexUid[$obj->getUid()] = $obj;
		}
	}
	*/
	
	/**
	 * Get item object of collection from his uid.
	 * 
	 * @param string $uid
	 * @return CompositComponentInterface | false
	 */
	public function getByUid($uid)
	{
		foreach($this as $current){
			if( $current->getUid() == $uid ){
				return $current;
				break;
			}
		}
		
		/* 3x plus lent:
		$this->rewind();
		while( $this->valid() ){
			if( $this->current()->getUid() == $uid ){
				return $this->current();
				break;
			}
			$this->next();
		}
		*/
	}
	
	
	/**
	 * Get item object of collection from his name.
	 * 
	 * @param string $name
	 * @return CompositComponentInterface | void
	 */
	public function getByName($name)
	{
		foreach($this as $current){
			if( $current->getName() == $name ){
				return $current;
				break;
			}
		}
		return;
		/* 3x Plus lent:
		$this->rewind();
		while( $this->valid() ){
			if( $this->current()->getName() == $name ){
				return $this->current();
				break;
			}
			$this->next();
		}
		*/
	}
	
	
	/**
	 * Get item object of collection from his index.
	 * 
	 * @param string $name
	 * @return CompositComponentInterface | false
	 */
	public function getByIndex($index)
	{
		$this->rewind();
		while($this->key() != $index){
			$this->next();
		}
		return $this->current();
	}
	
	/**
	 * @see CompositComponentInterface#getParent()
	 */
    public function getParent()
    {
    	return $this->_parent;
    }
    
	
    /**
     * @see CompositComponentInterface#setParent($object)
     * @return Collection
     */
    public function setParent($object)
    {
    	$this->_parent = $object;
		return $this;
    }
    
	/**
	 * @see LinkableInterface#getUid()
	 */
	function getUid()
	{
		return $this->_uid;
	}
	
	
	/**
	 * @see LinkableInterface#getName()
	 */
	function getName()
	{
		return $this->_name;
	}
	
	
	/**
	 * @param 	string	$name
	 * @return void
	 */
	function setName($name)
	{
		$this->_name = $name;
	}
    
}
