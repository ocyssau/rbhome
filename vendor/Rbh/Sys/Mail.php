<?php
//%LICENCE_HEADER%

namespace Rbh\Sys;
use Zend\Mail\Message as Message;
use Zend\Mail\Transport\Smtp as Transport;
use Zend\Mail\Transport\SmtpOptions;

/** 
 *
 */
class Mail extends Message
{
	
	protected static $_transport;
	
	/**
	 *
	 *	 array(
	 *	 'name'              => 'localhost.localdomain',
	 *	 'host'              => '127.0.0.1',
	 *	 'connection_class'  => 'login',
	 *	 'connection_config' => array(
	 *							 'username' => 'user',
	 *							 'password' => 'pass',
	 *							 ),
	 *	 );
	 */
	public static function initTransport($options, $type='smtp'){
		$transport = new Transport();
		$transport->setOptions(new SmtpOptions($options));
		self::$_transport = $transport;
	}

} //End of class
