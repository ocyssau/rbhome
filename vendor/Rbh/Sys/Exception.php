<?php
//%LICENCE_HEADER%


namespace Rbh\Sys;

/**
 * @brief Exception for Rb.
 * Args of constructor: $message, $code=null, $args=null
 *
 */
class Exception extends \Exception{
	
    /**
     * Additionals arguments to put in message
     * 
     * @var array
     */
    public $args = array();
    
    /**
     * @var string
     */
    protected $code = null;
    
    function __construct($message, $code=null, $args=null){
    	/* Replace %words% in message by values defines in $args
    	 */
    	$code = $message;
    	$this->code = $code;
    	
    	if(is_array($args)){
	    	foreach($args as $key=>$val){
	    		$message = str_replace('%'.$key.'%', $val, $message);
	    	}
    	}
    	else if(is_string($args)){
    		$message = str_replace('%0%', $args, $message);
    	}
    	
    	/*
        $args = func_get_args();
        if( isset($args[2]) ){
            $this->args = array_slice($args, 2);
        }
    	*/
		parent::__construct($message);
    }
}
