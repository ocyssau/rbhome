<?php
namespace Rbh\Parameter;


class Formula
{
	protected $_parent;
	protected $_leftExpression;
	protected $_rightExpression;
	protected $_tokens=array();
	protected $_contextStack = array();
	protected $_tree = null;
	
	const T_NUMBER = 1;
	const T_OPERATOR = 2;
	const T_SCOPE_OPEN = 3;
	const T_SCOPE_CLOSE = 4;
	const T_SIN_SCOPE_OPEN = 5;
	const T_COS_SCOPE_OPEN = 6;
	const T_TAN_SCOPE_OPEN = 7;
	const T_SQRT_SCOPE_OPEN = 8;
	const T_VARIABLE = 9;
	
	/**
	 * 
	 */
	public function __construct($expression, $parent)
	{
		$expression = preg_split('.=.',$expression);
		$this->_leftExpression = $expression[0];
		$this->_rightExpression = $expression[1];
		
		$this->_parent=$parent;
		
	}
	
	/**
	 * 
	 * @param string $expression
	 * @return string
	 */
	public function evaluate()
	{
		$this->_contextStack = array();
		$this->_tree = null;
		
		$this->_tokenize()->_parse();

		if ( ! $this->_tree ) {
		    throw new \Exception();
		}
		
		$value=$this->_tree->evaluate();
		$this->getParameterFromName($this->_leftExpression)->setValue($value);
		
		return $value;
	}


	/**
	 * this function does some simple syntax cleaning:
	 * - removes all spaces
	 * - replaces '**' by '^'
	 * then it runs a regex to split the contents into tokens. the set
	 * of possible tokens in this case is predefined to numbers (ints of floats)
	 * math operators (*, -, +, /, **, ^) and parentheses.
	 */
	protected function _tokenize()
	{
		$expression = $this->_rightExpression;
		$expression = str_replace(array("\n","\r","\t"," "), '', $expression);
		$expression = str_replace('**', '^', $expression);
		$expression = str_ireplace('pi()', (string)PI(), $expression);
		$this->_tokens = preg_split(
			'@([\d\.]+)|(sin\(|cos\(|tan\(|sqrt\(|\+|\-|\*|/|\^|\(|\))|([A-Za-z\.]+)@',
			$expression,
			null,
			PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
		);
		return $this;
	}

	/**
	 * this is the the loop that transforms the tokens array into
	 * a tree structure.
	 */
	protected function _parse()
	{
		// this is the global scope which will contain the entire tree
		$this->_contextStack[] = new Scope($this);

		// get the last context model from the context stack, and have it handle the next token
		foreach ( $this->_tokens as $token ) {
			$this->_handleToken( $token, end($this->_contextStack) );
		}
		$this->_tree = array_pop( $this->_contextStack );

		return $this;
	}

	
	/**
	 * handle the next token from the tokenized list. example actions
	 * on a token would be to add it to the current context expression list,
	 * to push a new context on the the context stack, or pop a context off the
	 * stack.
	 */
	protected function _handleToken( $token, $Context )
	{
	    $type = null;
		
	    if ( in_array( $token, array('*','/','+','-','^') ) ) $type = self::T_OPERATOR;
	    if ( $token === ')' ) $type = self::T_SCOPE_CLOSE;
	    if ( $token === '(' ) $type = self::T_SCOPE_OPEN;
	    if ( $token === 'sin(' ) $type = self::T_SIN_SCOPE_OPEN;
	    if ( $token === 'cos(' ) $type = self::T_COS_SCOPE_OPEN;
	    if ( $token === 'tan(' ) $type = self::T_TAN_SCOPE_OPEN;
	    if ( $token === 'sqrt(' ) $type = self::T_SQRT_SCOPE_OPEN;
		
	    if ( is_null( $type ) ) {
	        if ( is_numeric( $token ) ) {
	            $type = self::T_NUMBER;
	            $token = (float)$token;
	        }
	        else if( is_string($token) ){
	            $type = self::T_VARIABLE;
	        }
	    }
		
	    switch ( $type ) {
	        case self::T_NUMBER:
	        case self::T_OPERATOR:
	            $Context->addOperation($token);
	            break;
	        case self::T_SCOPE_OPEN:
	        	$this->_contextStack[] = new Scope($this);
	            break;
            case self::T_SCOPE_CLOSE:
            	//get to n-1 context, and add operation
                $scopeOperation = array_pop( $this->_contextStack );
                $newContext = end($this->_contextStack);
                if ( is_null( $scopeOperation ) || ( ! $newContext ) ) {
                    # this means there are more closing parentheses than openning
                    throw new \Exception();
                }
                $newContext->addOperation( $scopeOperation );
                break;
	        case self::T_SIN_SCOPE_OPEN:
	        	$this->_contextStack[] = new SineScope($this);
	            break;
	        case self::T_COS_SCOPE_OPEN:
	        	$this->_contextStack[] = new CosineScope($this);
	            break;
	        case self::T_TAN_SCOPE_OPEN:
	        	$this->_contextStack[] = new TangentScope($this);
	            break;
	        case self::T_SQRT_SCOPE_OPEN:
	        	$this->_contextStack[] = new SqrtScope($this);
	            break;
	        case self::T_VARIABLE:
	        	$value=$this->getParameterFromName($token)->getValue();
	        	$Context->addOperation($value);
	        	break;
	        default:
	            throw new \Exception($token);
	            break;
	    }
	}
	
	/**
	 * 
	 * @param string $paramName
	 * @return Parameter
	 */
	public function getParameterFromName($paramName)
	{
		$component=$this->_parent;
		$plitted = explode('.', $paramName);
		$c=count($plitted);
		for($i=0;$i<($c-1);$i++){
			$component=$component->getComponent($plitted[$i]);
		}
		return $component->getParam($plitted[$i]);
	}
}

class Scope
{
	protected $_builder = null;
	protected $_operations = array();

	/**
	 * 
	 * @param Resolver $builder
	 * @throws \Exception
	 */
	public function __construct($builder)
	{
		$this->_builder = $builder;
	}
	
	/**
	 * 
	 */
	public function __toString() 
	{
	}

	/**
	 * 
	 * @param Scope $operation
	 */
	public function addOperation( $operation )
	{
		$this->_operations[] = $operation;
	}

	/**
	 * order of operations:
	 * - parentheses, these should all ready be executed before this method is called
	 * - exponents, first order
	 * - mult/divi, second order
	 * - addi/subt, third order
	 */
	protected function _expressionLoop( &$operationList ) 
	{
		
		while ( list( $i, $op ) = each ( $operationList ) )
		{
			//if $op is not a operator, its a member
			if ( !in_array( (string)$op, array('^','*','/','+','-') ) ) continue;
			
			//members left and right
			$left =  isset( $operationList[ $i - 1 ] ) ? (float)$operationList[ $i - 1 ] : null;
			$right = isset( $operationList[ $i + 1 ] ) ? (float)$operationList[ $i + 1 ] : null;
			
			//BUG PHP: in_array alway true if array contains 0 value
			$first = ( in_array('^', $operationList, true) );
			$second = ( in_array('*', $operationList, true ) || in_array('/', $operationList, true ) );
			$third = ( in_array('-', $operationList, true ) || in_array('+', $operationList, true ) );

			$remove = true;
			if ( $first ) {
				switch( $op ) {
					case '^': 
						$operationList[ $i ] = pow( (float)$left, (float)$right ); 
						break;
					default: 
						$remove = false; 
						break;
				}
			}
			elseif ( $second ) {
				switch ( $op ) {
					case '*': 
						$operationList[ $i ] = (float)($left * $right);
						break;
					case '/':
						$operationList[ $i ] = (float)($left / $right); 
						break;
					default: 
						$remove = false; 
						break;
				}
			}
			elseif ( $third ) {
				
				switch ( $op ) {
					case '+': 
						$operationList[ $i ] = (float)($left + $right); 
						break;
					case '-': 
						$operationList[ $i ] = (float)($left - $right); 
						break;
					default: 
						$remove = false; 
						break;
				}
			}
			if ( $remove ) {
				unset( $operationList[ $i + 1 ], $operationList[ $i - 1 ] );
				$operationList = array_values( $operationList );
			}
		}
				
		if ( count( $operationList ) === 1 ) return end( $operationList );
		
		return false;
	}

	# order of operations:
	# - sub scopes first
	# - multiplication, division
	# - addition, subtraction
	# evaluating all the sub scopes (recursivly):
	public function evaluate() 
	{
		foreach ( $this->_operations as $i => $operation ) {
			if ( is_object( $operation ) ) {
				$this->_operations[ $i ] = $operation->evaluate();
			}
		}

		$operationList = $this->_operations;

		while ( true ) {
			$operationCheck = $operationList;
			$result = $this->_expressionLoop( $operationList );
			if ( $result !== false ) return $result;
			if ( $operationCheck === $operationList ) {
				break;
			} 
			else {
				$operationList = array_values($operationList);
			}
		}
		throw new \Exception('failed... here');
	}
}

class SineScope extends Scope
{
	public function evaluate() 
	{
		return sin( deg2rad( parent::evaluate() ) );
	}
}

class CosineScope extends Scope
{
    public function evaluate()
    {
        return cos( deg2rad( parent::evaluate() ) );
    }
}

class TangentScope extends Scope
{
    public function evaluate()
    {
        return tan( deg2rad( parent::evaluate() ) );
    }
}

class SqrtScope extends Scope
{
	public function evaluate() 
	{
		return sqrt( parent::evaluate() );
	}
}
