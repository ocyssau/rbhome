<?php
//%LICENCE_HEADER%

namespace Rbh\Parameter;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Length extends Parameter
{
	/**
	 * @var string
	 */
	protected $_unit='m';
}
