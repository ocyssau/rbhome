<?php
//%LICENCE_HEADER%

namespace Rbh\Parameter;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Parameter
{
	/**
	 * @var string
	 */
	protected $_value;
	
	/**
	 * @var string
	 */
	protected $_unit;
	
	public function __construct($value, $unit=STANDARD)
	{
		$this->_value = $value;
		$this->_unit = $unit;
	}
	
	public function getValue()
	{
		return $this->_value;
	}
	
	public function setValue($value)
	{
	    $this->_value=$value;
	    return $this;
	}
	
	public function getUnit()
	{
	    return $this->_unit;
	}
}
