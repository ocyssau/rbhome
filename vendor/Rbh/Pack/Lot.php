<?php
//%LICENCE_HEADER%

namespace Rbh\Pack;

/**
 * @brief
 *
 * @see Rbh/MaterialTest.php
 * @version $Rev: 808 $
 * @license GPL-V3: Rbh/licence.txt
 */
class Lot extends Pack
{
	/**
	 * quantity in lot
	 * @var real
	 */
	protected $quantity;
	
	/**
	 * Unity of quantity in lot
	 * @var String
	 */
	protected $unit;
	
	/**
	 * Sales unit
	 * @var String
	 */
	protected $salesUnit;
}
