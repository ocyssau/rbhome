<?php
namespace Rbh;

class Sender extends \Rbplm\Any
{

	public function init()
	{
		\Rbplm\Signal::emit($this, 'init', 'arg2', 'arg3');
	}
	
	public function nothing($arg)
	{
		//var_dump($arg);
	}
	
	public function callback($arg, $arg2, $arg3)
	{
		var_dump( $arg, $arg2, $arg3 );
	}
}


class SignalTest extends \Rbplm\Test\Test
{

	protected function setUp()
	{
	}

	function Test_Tutorial()
	{
		$s = new \Rbh\Sender();
		\Rbplm\Signal::connect($s, 'init', array($s, 'callback'));
		$s->init();
		die;
	}

}

