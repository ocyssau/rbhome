<?php
//%LICENCE_HEADER%


/** SQL_SCRIPT>>
 CREATE TABLE people_group(
 is_active boolean,
 description varchar(255)
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (120, 'Rbh\People\Group', 'people_group');
 INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, is_active, description)
 VALUES (15, '99999999-9999-9999-9999-00000000cdef', 120, 'admins', 'admins', 90, 10, 'Rbh.Groups.admins', true, 'rbh administrators');
 INSERT INTO people_group (id, uid, cid, name, label, parent, owner, path, is_active, description)
 VALUES (16, '99999999-9999-9999-9999-00000000ffff', 120, 'anonymous', 'anonymous', 90, 10, 'Rbh.Groups.anonymous', true, 'anonymous users');
<<*/

/** SQL_ALTER>>
 ALTER TABLE people_group ADD PRIMARY KEY (id);
ALTER TABLE people_group ADD UNIQUE (uid);
ALTER TABLE people_group ADD UNIQUE (path);
ALTER TABLE people_group ALTER COLUMN cid SET DEFAULT 120;
CREATE INDEX INDEX_people_group_uid ON people_group USING btree (uid);
CREATE INDEX INDEX_people_group_name ON people_group USING btree (name);
CREATE INDEX INDEX_people_group_label ON people_group USING btree (label);
CREATE INDEX INDEX_people_group_class_id ON people_user USING btree (cid);
CREATE INDEX INDEX_people_group_path ON people_group USING btree (path);
<<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_people_group AFTER INSERT OR UPDATE
ON people_group FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_update_path();
CREATE TRIGGER trig02_people_group AFTER DELETE
ON people_group FOR EACH ROW
EXECUTE PROCEDURE rb_trig_anyobject_delete();

<<*/

/** SQL_VIEW>>
<<*/

namespace Rbh\People;

use Rbh\Dao\Pg\Dao As Dao;
use Rbh\People\Group As Group;
use Rbh\People\User As User;
use Rbh\People\UserDao As UserDao;
use Rbh\Dao\Pg\DaoList as DaoList;
use Rbh\Dao\Registry as Registry;

/**
 * @brief Dao class for Rbh\People\Group
 *
 * See the examples: Rbh/People/GroupTest.php
 *
 * @see Rbh\Dao\Pg
 * @see Rbh\People\GroupTest
 *
 */
class GroupDao extends Dao
{
	/**
	 *
	 * @var string
	 */
	var $table = 'people_group';

	/**
	 * @var integer
	 */
	var $classId = 120;

	public static $sysToApp = array('is_active'=>'isActive', 'description'=>'description');

	/**
	 * Load the properties in the mapped object.
	 *
	 * @param Rbplm_People_Group	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return Rbplm_People_GroupDaoPg
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->isActive($row['isActive']);
		}
		else{
			$mapped->isActive($row['is_active']);
		}
		$mapped->description = $row['description'];
		return $mapped;
	} //End of function


	/**
	 * @param Rbplm_People_Group   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$mapped->classId = $this->classId;
		$bind = array(
			':isActive'=>(integer) $mapped->isActive(),
			':description'=>$mapped->description
		);
		if( $mapped->id > 0 ){
			$this->_update($mapped, $bind);
		}
		else{
			$this->_insert($mapped, $bind);
		}
	}//End of function

	/**
	 * Getter for groups. Return a list.
	 *
	 * @param \Rbh\People\Group
	 * @return array
	 */
	public function getGroups($mapped, $level=100)
	{
		$sql="
		WITH RECURSIVE treegraph(luid, lparent, lchild, lname, lindex, ldata, level, lpath) AS (
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, ('0.'||l.lparent||'.'||l.lchild)
			FROM anyobject_links l
			WHERE l.lparent = :id
			UNION ALL
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, tg.level + 1, (tg.lpath || '.'|| l.lchild)
			FROM anyobject_links l, treegraph tg
			WHERE tg.lchild = l.lparent AND tg.level < :level
		)
		SELECT
			tg.luid, tg.lparent, tg.lchild, tg.lname, tg.lindex, tg.ldata, tg.level, tg.lpath
			,child.*
		FROM treegraph AS tg
		JOIN people_group AS child ON child.id = tg.lchild
		ORDER BY tg.lpath ASC
		";
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute(array(':id'=>$mapped->id, ':level'=>$level));
		//echo $sql;
		
		while($row=$stmt->fetch()){
			//var_dump($row);
			$Registry = Registry::singleton();
			if($Registry->exist($row['uid'])){
				$groups[] = $Registry->get($row['uid']);
			}
			else{
				$g = GroupDao::loadFromArray(new Group(), $row);
				$Registry->add($g);
				$groups[] = $g;
			}
		}
		return $groups;
	}//End of function


	/**
	 *
	 * @param \Rbh\People\Group
	 * @return array
	 */
	public function getUsers($mapped, $level=100)
	{
		$sql="
		WITH RECURSIVE treegraph(luid, lparent, lchild, lname, lindex, ldata, level, lpath) AS (
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, ('0.'||l.lparent||'.'||l.lchild)
			FROM anyobject_links l
			WHERE l.lparent = :id
			UNION ALL
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, tg.level + 1, (tg.lpath || '.'|| l.lchild)
			FROM anyobject_links l, treegraph tg
			WHERE tg.lchild = l.lparent AND tg.level < :level
		)
		SELECT
			tg.luid, tg.lparent, tg.lchild, tg.lname, tg.lindex, tg.ldata, tg.level, tg.lpath
			,child.*
		FROM treegraph AS tg
		JOIN people_user AS child ON child.id = tg.lchild
		ORDER BY tg.lpath ASC
		";
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute(array(':id'=>$mapped->id, ':level'=>$level));
		//echo $sql;
		
		while($row=$stmt->fetch()){
			//var_dump($row);
			$Registry = Registry::singleton();
			if($Registry->exist($row['uid'])){
				$users[] = $Registry->get($row['uid']);
			}
			else{
				$u = UserDao::loadFromArray(new User(), $row);
				$Registry->add($u);
				$users[] = $u;
			}
		}
		return $users;
	}//End of function

} //End of class

