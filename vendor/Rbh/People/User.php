<?php
//%LICENCE_HEADER%

namespace Rbh\People;

use Rbh\People\AbstractPeople As AbstractPeople;

/**
 * @brief user definition.
 * 
 * Example and tests: Rbplm/People/UserTest.php
 *
 */
class User extends AbstractPeople
{
	const SUPER_USER_UUID 		= '99999999-9999-9999-9999-00000000abcd';
	const ANONYMOUS_USER_UUID 	= '99999999-9999-9999-9999-99999999ffff';
	const SUPER_USER_ID 		= 10;
	const ANONYMOUS_USER_ID 	= 11;
	
    /**
     * @var boolean
     */
    public $isActive = null;

    /**
     * @var timestamp
     */
    public $lastLogin = null;

    /**
     * @var string
     */
    public $login = null;

    /**
     * @var string
     */
    public $firstname = null;

    /**
     * @var
     */
    public $lastname = null;

    /**
     * @var
     */
    public $password = null;

    /**
     * @var
     */
    public $mail = null;

    /**
     * @var Rbh\People\User\Wildspace
     */
    protected $wildspace = null;

    /**
     * @var string
     */
    public $wildspacePath = null;

    /**
     * @var Rbplm_People_User_Preference
     */
    protected $preference = null;

    /**
     * @var Rbplm_People_User
     */
    public static $currentUser = null;

    /**
     * Collection of Rbplm_People_Group
     *
     * @var Rbplm_Model_LinkCollection
     */
    protected $groups = null;
	
    /**
     * @param string $name
     * @param \Rbh\AnyObject $parent
     * @return \Rbh\People\User
     */
    public static function init($name="", \Rbh\AnyObject $parent=null)
	{
		$obj = parent::init($name, $parent);
		$obj->_login = $obj->name;
		$obj->isLeaf = true;
		$obj->isActive = true;
		return $obj;
	}
	
    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isActive($bool = null)
    {
    	if( is_bool($bool) ){
    		return $this->isActive = $bool;
    	}
    	else{
    		return $this->isActive;
    	}
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
	 * Set the User name
	 * 
     * @param string $Login
     * @return void
     */
    public function setLogin($login)
    {
		$this->login = trim($login);
    }
    
    /**
     * @return
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param unknown $Password
     * @return void
     */
    public function setPassword($Password)
    {
        $this->password = trim($Password);
        return $this;
    }
	
    /**
	 * Get the wildspace object of the current User
     * @return Rbplm_People_User_Wildspace
     */
    public function getWildspace()
    {
        if( !$this->wildspace ){
        	$this->wildspace = new Rbplm_People_User_Wildspace($this);
        }
        return $this->wildspace;
    }
    
    /**
     * @return Rbplm_People_User_Preference
     */
    public function getPreference()
    {
        if( !$this->preference ){
			$this->preference = new Rbh\People\User\Preference($this->uid);
        }
        return $this->preference;
    }
	
    /**
	 * Get all groups of the current User
     * @return Rbplm_People_Group
     */
    public function getGroups()
    {
        if( !$this->groups ){
        	$this->groups = new \Rbh\Collection();
        }
        return $this->groups;
    }
	
	/** 
	 * Set the current user. You can too directly call getCurrentUser if session are operate.
	 * 
	 * @param Rbh\People\User $user
	 * @return Rbh\People\User
	 */
	public static function setCurrentUser($user)
	{
		return self::$currentUser = $user;
        return $this;
	} //End of method
	
}//End of class
