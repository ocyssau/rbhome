<?php
//%LICENCE_HEADER%

namespace Rbh\People;
use \Rbh\AnyObject as AnyObject;

/**
 * @brief Base class for all people objects.
 * 
 * @verbatim
 * @endverbatim
 *
 */
abstract class AbstractPeople extends AnyObject
{
} //End of class
