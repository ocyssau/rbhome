<?php
//%LICENCE_HEADER%

/**
 * $Id: Wildspace.php 760 2012-01-30 23:28:28Z olivierc $
 * $HeadURL: http://194.79.142.38:1080/svn/rbplmpre/branches/CollectionOnSplObjectStorage/rbPlmOc/library/Rbplm/People/User/Wildspace.php $
 * $LastChangedDate: 2012-01-31 00:28:28 +0100 (mar., 31 janv. 2012) $
 * $LastChangedBy: olivierc $
 * $Rev: 760 $
 */

require_once('Rbplm/Sys/Directory.php');


/**
 * @brief Wildspace is the user working directory.
 *
 * Wildspace directory must be writable by the user.
 * The files to store in ranchbe must be copy in the Wildspace before.
 * 
 * Example and tests: Rbplm/People/UserTest.php
 * 
 */
class Rbplm_People_User_Wildspace extends Rbplm_Sys_Directory {

	/**
	 * The owner of this wildspace
	 * @var Rbplm_People_User
	 */
	protected $owner;
	
	/**
	 * @var string
	 */
	protected $currentUserName;
	
	/**
	 * 
	 * @var string	uuid
	 */
	protected $currentUserId;
	
	/**
	 * Path to wildspace
	 * @var unknown_type
	 */
	protected $path;
	
	/**
	 * 
	 * @var string
	 */
	public static $basePath;
	
	/**
	 *
	 * @param Rbplm_People_User	$user
	 * @param String			$path
	 * @return void
	 */
	function __construct(Rbplm_People_User &$user, $path = null)
	{
		$this->currentUserName = $user->getName ();
		$this->currentUserId = $user->getUid ();
		
		$this->owner = $user;
		
		if(!$path){
			$basePath = self::$basePath;
			//construct the path to the personnal working directory for the current user.
			if (! empty ( $this->currentUserName )) {
				$this->path = $basePath . '/' . $this->currentUserName;
			} else {
				$this->path = $basePath . '/' . 'anonymous';
			}
		}else{
			$this->path = $path;
		}
		
		if (! is_dir ( $this->path )){
			if (! mkdir ( $this->path, 0775, true )) {
				throw new Rbplm_Sys_Exception('CAN_CREATE_PATH', Rbplm_Sys_Error::ERROR, $this->path);
			}
		}

		$this->path = realpath($this->path);
	} //End of method

	
	/** Return the path of the Wildspace
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	} //End of method


	/**
	 * Set the path of the Wildspace
	 *
	 * @param string $path
	 */
	function setPath($path)
	{
		if ( !is_dir ( $path ) ){
			if ( !mkdir ( $path, 0775, true ) ) {
				throw new Rbplm_Sys_Exception('CAN_CREATE_PATH', Rbplm_Sys_Error::ERROR, $path);
			}
		}
		$this->path = realpath($path);
        return $this;
	} //End of method

	
	/**
	 * Suppress a file or directory from the Wildspace.
	 *
	 * @param string $dname name of the data to suppress.
	 * @return boolean
	 */
	function suppressData($dname)
	{
		$path = $this->path . '/' . $dname;
		$odata = Rbplm_Sys_Fsdata::_dataFactory ( $path );
		if($odata){
			return $odata->putInTrash ();
		}
		else{
			throw new Rbplm_Sys_Exception(Rbplm_Sys_Error::FILE_NOT_FOUND, Rbplm_Sys_Error::ERROR, $path);
		}
	} //End of method


	/**
	 * Copy a upload file in Wildspace.
	 *	Return true or false.
	 *
	 * Take the parameters of the uploaded file(from array $file) and create the file in the wildpspace.
	 * @param array	$file :
	 * $file[name](string) name of the uploaded file
	 * $file[type](string) mime type of the file
	 * $file[tmp_name](string) temp name of the file on the server
	 * $file[error](integer) error code if error occured during transfert(0=no error)
	 * $file[size](integer) size of the file in octets
	 * 
	 * @param boolean $replace if true and if the file exist in the Wildspace, it will be replaced by the uploaded file.
	 *
	 * @return Rbplm_Sys_Datatype_File
	 */
	function uploadFile($file, $replace = false)
	{
		return Rbplm_Sys_Datatype_File::upload ( $file, $replace, $this->path );
	} //End of method


}//End of class

