<?php
//%LICENCE_HEADER%

namespace Rbh\People;

use Rbh\People\AbstractPeople As AbstractPeople;

/**
 * @brief Group definition.
 *
 */
class Group extends AbstractPeople
{
	const SUPER_GROUP_UUID = '99999999-9999-9999-9999-00000000cdef';
	const ANONYMOUS_GROUP_UUID = '99999999-9999-9999-9999-00000000ffff';
	const SUPER_GROUP_ID 		= 15;
	const ANONYMOUS_GROUP_ID 	= 16;
	
	/**
	 *
	 * @var string
	 */
	public $description;
	
	/**
	 *
	 * @var boolean
	 */
	protected $isActive = false;
	
	/**
	 *
	 * @var Rbh\Collection
	 */
	protected $groups;
	
	
	/**
	 *
	 * @var Rbh\Collection
	 */
	protected $users;
	
	
    /**
     * @param boolean $bool
     * @return boolean
     */
    public function isActive($bool = null)
    {
    	if( is_bool($bool) ){
    		return $this->isActive = $bool;
    	}
    	else{
    		return $this->isActive;
    	}
    }

    /**
     * @return \Rbh\Collection
     */
    public function getGroups()
    {
    	if( !$this->groups ){
    		$this->groups = new \Rbh\Collection();
    	}
    	return $this->groups;
    }

    /**
     * @return Rbh\People\User
     */
    public function getUsers()
    {
    	if( !$this->users ){
    		$this->users = new \Rbh\Collection();
    	}
    	return $this->users;
    }
	
} //End of class
