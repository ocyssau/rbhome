<?php
//%LICENCE_HEADER%

namespace Rbh\People;
use \Rbh\People\User;
use \Rbh\Sys\Exception;
use \Rbh\Sys\Error;


/**
 * @brief Current connected user.
 * 
 * Example and tests: Rbh/People/UserTest.php
 */
class CurrentUser extends User
{
	
	/**
	 * Singleton pattern implementation
	 * 
	 * @var CurrentUser
	 */
	protected static $_instance;
	
	/**
	 * 
	 * Constructor must not be directly call, use singleton method instead.
	 */
	function __construct(){
		throw new Exception('CLASS_CAN_NOT_BE_INSTANCIATE', Error::ERROR, get_class($this));
	} //End of method
	
	
	/**
	 * Set and get user object with current User datas
	 * 
	 * this method return always the same instance of class \Rbh\People\User. Its a singlton method
	 * so you can call it many time in your code without overload server.
	 * 
	 * @todo : revoir avec serialisation dans la session de l'objet
	 */
	public static function get(){
		if( !self::$_instance ){
			$storage = \Zend_Auth::getInstance()->getStorage()->read(); //Get info from session
			if(!$storage) {
				$storage['user_id']  = 2147483647; //(int 11) voir les entier php et les limitations de taille en fonction de l'OS
				$storage['user_name']= 'anonymous';
			}
			self::$_instance = new User();
			self::$_instance->login = $storage['user_name'];
			self::$_instance->mail = $storage['email'];
		}
		return self::$_instance;
	}
	
	/** 
	 * Set the current user instance.
	 * 
	 * @param \Rbh\People\User $user
	 * @return void
	 */
	public static function set(\Rbh\People\User $user)
	{
		return self::$_instance = $user;
	}//End of method
	
}//End of class
