<?php
//%LICENCE_HEADER%

namespace Rbh\People;

use Rbh\Test\Test as Test;
use Rbh\People\User as User;
use Rbh\People\Group as Group;
use Rbh\People\UserDao as UserDao;
use Rbh\People\GroupDao as GroupDao;
use \Rbh\Dao\Factory As DaoFactory;
use \Rbh\Dao\Pg\Loader As Loader;
use \Rbh\Dao\Pg\ListDao As DaoList;


/**
 * @brief Test class
 * 
 */
class PeopleTest extends Test
{
	/**
	 */
	protected function setUp()
	{
		\Rbh\Dao\Pg\Loader::init();
		$GroupDao = new GroupDao();
		$UserDao = new UserDao();
		$GroupDao->newList()->suppress('name LIKE \'PeopleTest_%\'');
		$UserDao->newList()->suppress('name LIKE \'PeopleTest_%\'');
		$LinkDao = new \Rbh\LinkDao();
		$LinkDao->cleanOrpheanLinks();
	}
	
	/**
	 */
	protected function tearDown()
	{
	}
	

	protected static function _displayGroupTree($group){
		/*Walk along the groups tree relation with a iterator*/
		$it = new \RecursiveIteratorIterator( $group->getGroups(), \RecursiveIteratorIterator::SELF_FIRST );
		$it->setMaxDepth(100);
		echo $group->name . CRLF;
		foreach($it as $node){
			echo str_repeat('  ', $it->getDepth()+1) . $node->name . CRLF;
		}
	}

	/**
	 *
	 * @param unknown_type $Parent
	 * @param unknown_type $LnkName
	 * @return unknown
	 */
	private static function getLinkByChildName($Parent, $LnkChildName){
		foreach($Parent->links as $lnk){
			if ($lnk->child->name == $LnkChildName){
				return $lnk->child;
			}
		}
	}
	
	/**
	 * 
	 */
	public function Test_GroupUserConstructor()
	{
		$group1 = Group::init('PeopleTest_grp1');
		$group1->description = 'groupe 1';
		$group1->isActive(true);
		
		$group2 = Group::init('PeopleTest_grp2');
		$group2->description = 'groupe 2';
		$group2->isActive(true);
		
		$group3 = Group::init('PeopleTest_grp3');
		$group3->description = 'groupe 3';
		$group3->isActive(true);
		
		$user1 = User::init('PeopleTest_user1');
		$user1->setLogin('PeopleTest_User1');
		
		$group1->addLink($group2);
		$group1->addLink($group3);
		$group2->addLink($user1);
		
		return $group1;
	}
	
	/**
	 * 
	 */
	public function Test_DaoGroupUserSave()
	{
		$group1 = $this->Test_GroupUserConstructor();
		$group2 = self::getLinkByChildName($group1, 'PeopleTest_grp2');
		$group3 = self::getLinkByChildName($group1, 'PeopleTest_grp3');
		$user1 = self::getLinkByChildName($group2, 'PeopleTest_user1');
		
		$GroupDao = new GroupDao();
		$UserDao = new UserDao();
		
		$UserDao->save($user1);
		$GroupDao->save($group3);
		$GroupDao->save($group2);
		$GroupDao->save($group1);
	}
	
	/**
	 * 
	 */
	public function Test_DaoGroupUserLoadUpdate()
	{
		$GroupDao = new GroupDao();
		$UserDao = new UserDao();
		
		$group1 = $GroupDao->loadFromName(new Group(), 'PeopleTest_grp1');
		assert($group1->name == 'PeopleTest_grp1');
		$group2 = $GroupDao->loadFromName(new Group(), 'PeopleTest_grp2');
		assert($group2->name == 'PeopleTest_grp2');
		$user1 = $UserDao->loadFromName(new User(), 'PeopleTest_user1');
		assert($user1->name == 'PeopleTest_user1');
		
		//Get groups of user
		$groups = $UserDao->getGroups($user1, $level=10);
		foreach ($groups as $grp){
			var_dump($grp->name);
		}
		assert($groups[0]->uid==$group2->uid);
		assert($groups[1]->uid==$group1->uid);
		
		$groups = $UserDao->getGroups($user1, $level=1);
		assert( count($groups) == 1 );
		
		//Get users of group
		$users = $GroupDao->getUsers($group1);
		foreach ($users as $u){
			var_dump($u->login);
		}
		assert($users[0]->uid==$user1->uid);
	}
	
	/**
	 * 
	 */
	public function Test_DaoGroupUserAuth()
	{
	}
	
	/**
	 * 
	 */
	public function Test_DaoGroupUserDelete()
	{
	}
	
}
