<?php
//%LICENCE_HEADER%


namespace Rbh\People;

use Rbh\Dao\Pg\Dao As Dao;
use Rbh\People\User As User;
use Rbh\People\Group As Group;
use Rbh\People\GroupDao As GroupDao;
use Rbh\Dao\Pg\DaoList as DaoList;
use Rbh\Dao\Registry as Registry;


/** SQL_SCRIPT>>
 CREATE TABLE people_user(
 is_active boolean,
 last_login integer,
 login varchar(255) NOT NULL,
 firstname varchar(255),
 lastname varchar(255),
 password varchar(255),
 mail varchar(255),
 wildspace varchar(255)
 ) INHERITS (anyobject);
 <<*/

/** SQL_INSERT>>
 INSERT INTO classes (id, name, tablename) VALUES (110, 'Rbh\People\User', 'people_user');
 INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, is_active, last_login, login, firstname, lastname, password, mail, wildspace)
 VALUES (10, '99999999-9999-9999-9999-00000000abcd', 110, 'admin', 'admin', 91, 10, 'Rbh.Users.admin', true, NULL, 'admin', 'rbh administrator', 'administrator', NULL, NULL, NULL);
 INSERT INTO people_user (id, uid, cid, name, label, parent, owner, path, is_active, last_login, login, firstname, lastname, password, mail, wildspace)
 VALUES (11, '99999999-9999-9999-9999-99999999ffff', 110, 'anybody', 'anybody', 91, 10, 'Rbh.Users.anybody', true, NULL, 'anybody', 'anonymous user', 'anybody', NULL, NULL, NULL);
 <<*/

/** SQL_ALTER>>
 ALTER TABLE people_user ADD PRIMARY KEY (id);
 ALTER TABLE people_user ADD UNIQUE (login);
 ALTER TABLE people_user ADD UNIQUE (uid);
 ALTER TABLE people_user ADD UNIQUE (path);
 ALTER TABLE people_user ALTER COLUMN cid SET DEFAULT 110;
 CREATE INDEX INDEX_people_user_login ON people_user USING btree (login);
 CREATE INDEX INDEX_people_user_uid ON people_user USING btree (uid);
 CREATE INDEX INDEX_people_user_name ON people_user USING btree (name);
 CREATE INDEX INDEX_people_user_label ON people_user USING btree (label);
 CREATE INDEX INDEX_people_user_class_id ON people_user USING btree (cid);
 CREATE INDEX INDEX_people_user_path ON people_user USING btree (path);
 <<*/

/** SQL_FKEY>>
 <<*/

/** SQL_TRIGGER>>
 CREATE TRIGGER trig01_people_user AFTER INSERT OR UPDATE
 ON people_user FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_update_path();
 CREATE TRIGGER trig02_people_user AFTER DELETE
 ON people_user FOR EACH ROW
 EXECUTE PROCEDURE rb_trig_anyobject_delete();
 <<*/

/** SQL//CANCEL//_VIEW>>
 CREATE OR REPLACE VIEW view_people_user_links AS
 SELECT l.related AS lrelated, l.data AS ldata, l.lindex AS lindex, r.*
 FROM people_user AS r
 JOIN anyobject_links AS l ON r.id = l.linked;
 <<*/

/** SQL_DROP>>
 <<*/


/**
 * @brief Dao class for Rbplm_People_User
 *
 * See the examples: Rbplm/People/UserTest.php
 *
 * @see Rbplm_Dao_Pg
 * @see Rbplm_People_UserTest
 *
 */
class UserDao extends Dao
{

	/**
	 *
	 * @var string
	 */
	var $table = 'people_user';

	/**
	 *
	 * @var integer
	 */
	var $classId = 110;

	public static $sysToApp = array('is_active'=>'isActive', 'last_login'=>'lastLogin', 'login'=>'login', 'firstname'=>'firstname', 'lastname'=>'lastname', 'password'=>'password', 'mail'=>'mail', 'wildspace'=>'wildspacePath');

	/**
	 * Load the properties in the mapped object.
	 *
	 * @param Rbplm_People_User	$mapped
	 * @param array $row			PDO fetch result to load
	 * @param boolean	$fromApp	If true, assume that keys $row are name of properties as set in buizness model, else are set as in persitence system.
	 * @return void
	 */
	public static function loadFromArray( $mapped, array $row, $fromApp = false )
	{
		Dao::loadFromArray($mapped, $row, $fromApp);
		if($fromApp){
			$mapped->isActive($row['isActive']);
			$mapped->lastLogin = $row['lastLogin'];
			$mapped->setLogin($row['login']);
			$mapped->firstname = $row['firstname'];
			$mapped->lastname = $row['lastname'];
			$mapped->setPassword($row['password']);
			$mapped->mail = $row['mail'];
			$mapped->wildspacePath = $row['wildspacePath'];
		}
		else{
			$mapped->isActive($row['is_active']);
			$mapped->lastLogin = $row['last_login'];
			$mapped->setLogin($row['login']);
			$mapped->firstname = $row['firstname'];
			$mapped->lastname = $row['lastname'];
			$mapped->setPassword($row['password']);
			$mapped->mail = $row['mail'];
			$mapped->wildspacePath = $row['wildspace'];
		}
		return $mapped;
	} //End of function


	/**
	 * @param Rbplm_People_User   $mapped
	 * @return void
	 * @throws Rbplm_Sys_Exception
	 *
	 */
	protected function _saveObject($mapped)
	{
		$mapped->classId = $this->classId;
		$bind = array(
			':isActive'=>(integer) $mapped->isActive(),
			':lastLogin'=>$mapped->lastLogin,
			':login'=>$mapped->getLogin(),
			':firstname'=>$mapped->firstname,
			':lastname'=>$mapped->lastname,
			':password'=>$mapped->getPassword(),
			':mail'=>$mapped->mail,
			':wildspacePath'=>$mapped->wildspacePath
		);
		if( $mapped->id > 0 ){
			$this->_update($mapped, $bind);
		}
		else{
			$this->_insert($mapped, $bind);
		}
	} //End of function

	/**
	 * Getter for groups. Return a list.
	 *
	 * @param Rbplm_Dao_MappedInterface
	 * @return Rbplm_People_UserDaoPg
	 */
	public function getGroups($mapped, $level=1000)
	{
		$sql="
		WITH RECURSIVE treegraph(luid, lparent, lchild, lname, lindex, ldata, level, lpath) AS (
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, 1, ('0.'||l.lparent||'.'||l.lchild)
			FROM anyobject_links l
			WHERE l.lchild = :id
			UNION ALL
			SELECT l.luid, l.lparent, l.lchild, l.lname, l.lindex, l.ldata, tg.level + 1, (tg.lpath || '.'|| l.lchild)
			FROM anyobject_links l, treegraph tg
			WHERE tg.lparent = l.lchild AND tg.level < :level
		)
		SELECT
			tg.luid, tg.lparent, tg.lchild, tg.lname, tg.lindex, tg.ldata, tg.level, tg.lpath
			,parent.*
		FROM treegraph AS tg
		JOIN people_group AS parent ON parent.id = tg.lparent
		ORDER BY tg.lpath ASC
		";
		
		$stmt = $this->connexion->prepare($sql);
		$stmt->setFetchMode(\PDO::FETCH_ASSOC);
		$stmt->execute(array(':id'=>$mapped->id, ':level'=>$level));
		//echo $sql;
		
		$Registry = Registry::singleton();
		
		while($row=$stmt->fetch()){
			if($Registry->exist($row['uid'])){
				$groups[] = $Registry->get($row['uid']);
			}
			else{
				$g = GroupDao::loadFromArray(new Group(), $row);
				$Registry->add($g);
				$groups[] = $g;
			}
			//$groups[] = GroupDao::loadFromArray(new Group(), $row);
		}
		return $groups;
	} //End of function

} //End of class

