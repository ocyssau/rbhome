<?php
//%LICENCE_HEADER%


namespace Rbh\People;

use Rbh\Test\Test as Test;
use Rbh\People\User as User;

/**
 * @brief Test class for User
 * 
 * @include Rbplm/People/UserTest.php
 * 
 */
class UserTest extends Test
{
	/**
	 * @var    User
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		/*Init the loader*/
		\Rbh\Dao\Pg\Loader::init();
	}

	
	/**
	 * 
	 */
	function _Test_Preference(){
		$user = User::init( uniqid('tester'));
		$user->login = uniqid('USERPREFSTEST');
		
		$prefs = $user->getPreference();
		
		foreach($prefs->getPreferences() as $key=>$value ){
			assert( true );
		}
		
		/*
		 * initialy, all is set to default
		 */
		assert( $prefs->lang == 'default' );
		
		$prefs->css_sheet = 'perso/mycss.css';
		$prefs->lang = 'fr';
		assert( $prefs->lang == 'fr' );
		
		/*
		 * If not enable user prefs, return always default
		 */
		$prefs->isEnable(false);
		assert( $prefs->lang == 'default' );
	}
	
	/**
	 * 
	 * @return unknown_type
	 */
	function Test_GroupAssociation(){
		$user = User::init( uniqid('tester'));
		
		//Get the group collection.
		$Groups = $user->getGroups();
		assert( $Groups->count() == 0 );
		
		$group0 = Group::init( 'group0' );
		$group1 = Group::init( 'group1' );
		$group2 = Group::init( 'group2' );
		
		$Groups->addLink($group0);
		$Groups->addLink($group1);
		$Groups->addLink($group2);
		
		//assert($group0 === $Groups->getItem(0));
		//assert($group0 === $Groups->getItem(0));
		//assert($group0 === $Groups->getItem('group0'));
		//assert($group2 === $Groups->getItem('group2'));
	}
	
	/**
	 */
	function Test_Dao()
	{
		$user = User::init( uniqid('tester'));
		$user->setLogin( uniqid('USERTEST') );
		
		/*Save object*/
		$UserDao = new UserDao();
		$UserDao->save( $user );
		
		/*Load in a new object*/
		$object = new User();
		$UserDao->loadFromUid( $object, $user->uid );
		assert( $object->getName() == $user->getName() );
		//assert( \Rbh\Uuid::compare($object->getParent()->getUid(), $this->object->getParent()->getUid()) );
		
		/*Create some groups*/
		$group0 = Group::init(uniqid('group0'));
		$group1 = Group::init(uniqid('group1'));
		$group2 = Group::init(uniqid('group2'));
		
		$GroupDao = new GroupDao();
		$GroupDao->save($group0);
		$GroupDao->save($group1);
		$GroupDao->save($group2);
		
		/*Assign groups to user and save*/
		$Groups = $user->getGroups();
		$Groups->addLink($group0);
		$Groups->addLink($group1);
		$Groups->addLink($group2);
		$UserDao->save($user);
		
		$uid = $user->uid;
		
		/*Reload groups links in a list*/
		$List = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links') );
		$List->setConnexion( \Rbh\Dao\Connexion::get() );
		$List->load("lrelated='$uid' ORDER BY lindex ASC");
		
		/*Convert list to collection*/
		$collection = new Rbplm_Model_Collection();
		$List->loadInCollection($collection);
		
		/*Test if list is conform*/
		$i = 0;
		foreach($List as $entry){
//			var_dump( $entry['name'] , $Groups->getByIndex($i)->getName() );
			assert( $entry['name'] == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		/*Test if collection is conform*/
		$i = 0;
		foreach($collection as $current){
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			assert( get_class($current) == 'Rbplm_People_Group' );
			$i++;
		}
		
		
		/*In other style with helpers methods of DAO*/
		$this->setUp();
		$UserDao->loadFromUid($this->object, $uid);
		$UserDao->loadGroups($this->object, true);
		assert( $this->object->getGroups()->getByIndex(0)->getName() == $group0->getName() );
		
		/*
		 * Test user preferences DAO
		 */
		$PrefDao = new User_PreferenceDaoPg();
		$PrefDao->setConnexion( \Rbh\Dao\Connexion::get() );
		$prefs = $this->object->getPreference();
		
		$prefs->css_sheet = 'perso/mycss.css';
		$prefs->lang = 'fr';
		$prefs->isEnable(true);
		$PrefDao->save($prefs);
		
		$prefs2 = new User_Preference( $prefs->getOwner() );
		$PrefDao->loadFromOwner($prefs2, $prefs2->getOwner() );

//var_dump( $prefs2->lang , $prefs->lang );
//var_dump( $prefs2->getPreferences() );

		assert($prefs2->lang == $prefs->lang);
		assert($prefs2->css_sheet == $prefs->css_sheet);
		
		/*
		 * Test update preference
		 */
		$prefs->lang = 'en';
		$PrefDao->save($prefs);
		
		$prefs2 = new User_Preference( $prefs->getOwner() );
		$PrefDao->loadFromOwner($prefs2, $prefs2->getOwner() );
		
//var_dump( $prefs2->lang , $prefs->lang );
		assert($prefs2->lang == $prefs->lang);
		
		return;
		
		/*
		 * *************************************************************************************
		 * And with Rbplm_Model_CollectionListBridge:
		 * *************************************************************************************
		 */
		//Load list in collection
		$object = new User();
		$Collection = $object->getLinks();
		$CollectionDao = new Rbplm_Model_CollectionListBridge($Collection, $List);
		
		
		//test if dao collection is conform
		$i=0;
		foreach($CollectionDao as $key=>$current){
			var_dump( $key, $current->getName() );
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		
		//test if collection is conform
		$i=0;
		foreach($Collection as $key=>$current){
			var_dump( $key, $current->getName() );
			assert( $current->getName() == $Groups->getByIndex($i)->getName() );
			$i++;
		}
		
		assert($Collection->count() == 3);
	}
}


