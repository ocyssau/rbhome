<?php
//%LICENCE_HEADER%

namespace Rbh\People;

use \Rbh\Test\Test as Test;
use \Rbh\People\Group as Group;
use \Rbh\People\GroupDao as GroupDao;

/**
 * @brief Test class for Group
 * 
 * @include Rbplm/People/GroupTest.php
 */
class GroupTest extends Test
{
	/**
	 * @var    \Rbh\People\Group
	 * @access protected
	 */
	protected $object;
	

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 *
	 * @access protected
	 */
	protected function setUp()
	{
		$this->object = Group::init( 'gtester' );
	}

	
	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 *
	 * @access protected
	 */
	protected function tearDown()
	{
	}
	
	
	/**
	 */
	function Test_Dao(){
		$myGroup = Group::init( uniqid('GROUPTEST') );
		$myGroup->description = 'descriptionOfgtester';
		$myGroup->isActive(true);
		
		$Dao = new GroupDao();
		$Dao->save( $myGroup );
		
		$loadedGroup = new Group();
		$Dao->loadFromUid( $loadedGroup, $myGroup->uid );
		assert( $loadedGroup->name ==  $myGroup->name );
		assert( \Rbh\Uuid::compare($loadedGroup->parent->uid, $myGroup->gparent>uid ) );
		
		/*
		 * Create a group tree.
		 * $myGroup
		 * 		|$group0
		 * 		|$group1
		 * 		|$group2
		 * 			|$group3
		 */
		$group0 = Group::init( uniqid('group0_') );
		$group1 = Group::init( uniqid('group0_') );
		$group2 = Group::init( uniqid('group0_') );
		$group3 = Group::init( uniqid('group0_') );
		/*
		$myGroup->getGroups()->addItem($group0);
		$myGroup->getGroups()->addItem($group1);
		$myGroup->getGroups()->addItem($group2);
		$group2->getGroups()->addItem($group3);
		*/
		$myGroup->addLink($group0);
		$myGroup->addLink($group1);
		$myGroup->addLink($group2);
		$myGroup->addLink($group3);
		
		$Dao->save($group0);
		$Dao->save($group1);
		$Dao->save($group2);
		$Dao->save($group3);
		$Dao->save($myGroup);
		
		echo 'Names of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->name, $group1->name, $group2->name, $group3->name );
		echo 'Uids of groups group0, group1, group2, group3' . CRLF;
		var_dump($group0->uid, $group1->uid, $group2->uid, $group3->uid );
		
		/*Walk along the groups tree relation with a iterator*/
		self::_displayGroupTree($myGroup);
		
		$uid = $myGroup->uid;
		$group0Name = $group0->name;
		
		unset($group0);
		unset($group1);
		unset($group2);
		unset($group3);
		
		$this->setUp();
		$myGroup = $this->object;
		
		$Dao->loadFromUid($myGroup, $uid);
		self::_displayGroupTree($myGroup);
		
		/*
		$GroupList = new Rbplm_Dao_Pg_List( array('table'=>'view_people_group_links'), Rbplm_Dao_Connexion::get() );
		$GroupList->loadInCollection($myGroup->getGroups(), "lrelated='$uid' ORDER BY lindex ASC");
		
		self::_displayGroupTree($myGroup);
		
		var_dump( $myGroup->getGroups()->getByIndex(0)->name, $group0Name, $myGroup->uid );
		assert( $myGroup->getGroups()->getByIndex(0)->name == $group0Name );
		
		//In other style with helpers methods of DAO
		$this->setUp();
		$Dao->loadFromUid($myGroup, $uid);
		$Dao->getGroups($myGroup, true);
		assert( $myGroup->getGroups()->getByIndex(0)->name == $group0Name );
		*/
		
		/*
		 * Load groups, and all sub-groups until infinite depth
		 */
		$List = $Dao->loadGroupsRecursively($myGroup);
		foreach( $List as $row ){
			var_dump($row);
		}
		self::_displayGroupTree($myGroup);
	}
	
	protected static function _displayGroupTree( $group ){
		/*Walk along the groups tree relation with a iterator*/
    	$it = new \RecursiveIteratorIterator( $group->getGroups(), \RecursiveIteratorIterator::SELF_FIRST );
    	$it->setMaxDepth(100);
    	echo $group->name . CRLF;
    	foreach($it as $node){
    		echo str_repeat('  ', $it->getDepth()+1) . $node->name . CRLF;
    	}
	}
}
