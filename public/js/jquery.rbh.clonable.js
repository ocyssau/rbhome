/*
 * jQuery Clonable v1.0
 * http://www.icietla.net
 *
 * Copyright 2012, Christophe Lombard
 * Libre d'utilisation sous licence MIT (MIT license).
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Aout 2012
 * Modifie pour rbhome.
 * Attention: le parent est directement selectionné, ce qui interdit l'insertion de div entre le ul et le li
 * 
 */
 
(function($) {
   
   var Clonable= function(element, options){

		var settings = $.extend({}, $.fn.clonable.defaults, options);

		var clone = function (event) {
			event.preventDefault();
			
			//var original=$(event.target).prev();
			var original=$($(event.target).prev().children()[0]);
			var parentUl=original.parent();
			//var original=$(event.target).prev().children();
			var count=Number(parentUl.attr("data-count"));
			console.log('Count of items in list:' + count);
			
			// Copie
			var copie = original.clone(true);
			
			// Modification
			var tags = copie.find('input').add( copie.find('textarea') ).add( copie.find('label') ).add( copie.find('select') );
			
			//console.log(tags);
			tags.each(function(index) {
				// Pour les tags <select>
				$(this).find('option').each(function(index, element) {
					if (index == 0) { $(this).attr('selected','selected'); }
					else { $(this).removeAttr('selected'); }
				});
				
				// Pour les checkbox
				$(this).removeAttr('checked');
				
				// Pour les input type text
				if ($(this).attr('value')) {
					$(this).attr('value','');
				}
				
				var attributs=Array("id","name", "for");
				// Incrémente les attributs des inputs
				for ( var i=0; i< attributs.length; i++) {
					var attr=$(this).attr( attributs[i] );
					if (attr) {
						var attrArray=$(this).attr(attributs[i]).split("_");
						if (attrArray.length > 0) {
							//var newAttr=attrArray[0]+"_"+(Number(attrArray[1])+1);
							var newAttr=attrArray[0]+"_"+(count+1);
							$(this).attr(attributs[i],newAttr);
						}
					}
				}
			});
			
			// Insertion
			//copie.insertAfter(original).hide().fadeIn();
			parentUl.append(copie);
			
			// Compteur
			//var count=$(this).attr("data-count");
			/*
			if (count == 0) {
				// Affiche le bouton supprimer si c'est le 1er clic
				$('.deClone_btn'+original.attr("data-clonable-id")).show();
			}
			*/
			// Incrémente le compteur
			//$(this).attr("data-count", Number(count)+1 );
			parentUl.attr("data-count", Number(count)+1 );
			
			options.afterclone(event, $(this));
		}
		
		var deClone = function (event) {
			event.preventDefault();
			//$(this).parent().fadeOut("fast", function() { $(this).remove(); } );
			/*
			var boutonClone=$(this).prev();
			if (boutonClone.attr("data-count") > 0 ) {
				boutonClone.prev().fadeOut("fast", function() { $(this).remove(); } );
				boutonClone.attr("data-count", Number(boutonClone.attr("data-count"))-1);
			}
			if (boutonClone.attr("data-count") == 0 ) { $(this).hide(); }
			*/
			var parentUl=$(this).parent().parent();
			var count=parentUl.attr("data-count");
			console.log('Count of items in list:' + count);
			if (count > 1 ) {
				$(this).parent().fadeOut("fast", function() { $(this).remove(); } );
				parentUl.attr("data-count", Number(parentUl.attr("data-count"))-1);
			}
			//if (parentUl.attr("data-count") == 0 ) { $(this).hide(); }
		}
		
		element.attr("data-count", element.children("li").length);
		
		// Incrémente le nombre d'instances dans la page
		$.icietla.Clonable.indice+=1;
		
		element.addClass("clonable");
		element.attr("data-clonable-id",$.icietla.Clonable.indice);
		
		// Ajoute le bouton aux tableaux
		//$('<a href="#" data-count=0 class="clone_btn clone_btn'+$.icietla.Clonable.indice+'">'+settings.plusTxt+'</a> <a href="#" class="deClone_btn deClone_btn'+$.icietla.Clonable.indice+'">'+settings.moinsTxt+'</a> ').insertAfter( element );
		//$('.deClone_btn').hide();
		//$('<a href="#" data-count=0 class="clone_btn clone_btn'+$.icietla.Clonable.indice+'">'+settings.plusTxt+'</a> <a href="#" class="deClone_btn deClone_btn'+$.icietla.Clonable.indice+'">'+settings.moinsTxt+'</a> ').insertAfter( element );
		//$('<a class="clone_btn" href="#"></a>').insertAfter( element );
		
		// Evenement au click des boutons		
		//$('.clone_btn'+$.icietla.Clonable.indice).on('click', clone );
		//$('.deClone_btn'+$.icietla.Clonable.indice).on('click', deClone );
		$('.clone_btn').on('click', clone );
		$('.deClone_btn').on('click', deClone );
   }
   
   $.fn.clonable=function(options) {
		return this.each(function(key, value) {
			// Object global
			if ( ! $.icietla ) { $.icietla={}; }
			if ( ! $.icietla.Clonable ) {
				$.icietla.Clonable = {
					indice : 0
				};
			}
			// GO
			var instance = new Clonable($(this), options);
		});
    };
	
	$.fn.clonable.defaults = {
      "plusTxt": "Plus",
	   "moinsTxt": "Moins"
    };
	
})(jQuery);