<?php 
    return array(
    	'Rbh\Build\SurfaceComplex'=>'Rbh\Build\SurfaceDao',
    	'Rbh\Build\SurfaceStack'=>'Rbh\Build\SurfaceDao',
    	'Rbh\Build\SurfaceUsed'=>'Rbh\Build\SurfaceDao',
    	'Rbh\Material\Surfacic'=>'Rbh\Material\MaterialDao',
    	'Rbh\Material\Lineic'=>'Rbh\Material\MaterialDao',
    	'Rbh\Material\Discrete'=>'Rbh\Material\MaterialDao',
    	'Rbh\Material\Composite'=>'Rbh\Material\MaterialDao',
    	'Rbh\Material\Bulk'=>'Rbh\Material\MaterialDao',
    	'Rbh\Material\Beton'=>'Rbh\Material\MaterialDao',
    );
