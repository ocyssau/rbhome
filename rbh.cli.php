#!/usr/bin/php
<?php

namespace rbh\cli;

use Rbh\Dao\Connexion As Connexion;

use Rbh\Dao\Factory As DaoFactory;
use Rbh\Dao\Schemas\SqlExtractor;
use Rbh\Dao\Pg\Loader;
use Rbh\Dao\Pg\ClassDao;

use Rbh\Sys\Exception;
use Rbh\Sys\Error;
use Rbh\People\User;
use Rbh\People\CurrentUser;

use Rbh\AnyObject;
use Rbh\Org;


/**
 *
 */
function optionh()
{
    $display = 'Utilisation :

    php tests.php
    [-c <class>]
    [-m <method>]
    [-a]
    [--nodao,]
    [--croot,]
    [-i]
    [-lf]
    [-lc]
    [--doctype]
    [-help, -h, -?]

    Avec:
    -c <class>
    : Le nom de la classe de test a executer. Il peut y avoir plusieur option -c

    -m <method>
    : La methode a executer, ou si omis, execute tous les tests de la classe.

    -a
    : Execute tous les tests, annule l\'option -c

    -t
    : Execute mes tests, annule l\'option -c

    -p
    : Active le profiling des tests

    --nodao
    : Ignore les methodes testDao*

    --croot
    : Creer le noeud root

    -i
    : initialise les données de test.

    --initdb
    : initialise les données de test.

    --lf
    : Affiche la liste des functions de test disponibles

    --lc
    : Affiche la liste des classes de test disponibles

    -s
    : Extrait les scripts sql depuis les classes

    -h
    : Affiche cette aide.

    Examples:
    Execute tous les tests de la classe \Rbh\AnyObjectTest et Rbh\Org\Test:
    php tests.php -c \Rbh\AnyObjectTest -c Rbh\Org\Test

    Execute la methode \Rbh\AnyObjectTest::testDao
    php tests.php -c \Rbh\AnyObjectTest -m testDao

    Avec les options -help, -h vous obtiendrez cette aide

    -t : Execute la function test <fonction> parmis les fonctions suivantes :';
    echo $display;
    displaylisttest();
}

function displaylisttest()
{
    echo $display='

    ';
}


/**
 *
 */
function run()
{
    chdir(__DIR__);
    define('CRLF', "\n");
    
    error_reporting( E_ALL ^ E_DEPRECATED ^ E_NOTICE );
    ini_set ( 'display_errors', 1 );
    ini_set ( 'display_startup_errors', 1 );

    ini_set('xdebug.collect_assignments', 1);
    ini_set('xdebug.collect_params', 10);
    ini_set('xdebug.collect_return', 10);
    ini_set('xdebug.collect_vars', 10);
    ini_set('xdebug.var_display_max_children', 1000);
    ini_set('xdebug.var_display_max_data', 1000);
    ini_set('xdebug.var_display_max_depth', 100);

    $loader = include 'vendor/autoload.php';
    $loader->set('Rbh', realpath('./vendor/'));
    $loader->set('Rba', realpath('./vendor/'));

    /*
     ASSERT_ACTIVE 	assert.active 	1 	active l'évaluation de la fonction assert()
    ASSERT_WARNING 	assert.warning 	1 	génére une alerte PHP pour chaque assertion fausse
    ASSERT_BAIL 	assert.bail 	0 	termine l'exécution en cas d'assertion fausse
    ASSERT_QUIET_EVAL 	assert.quiet_eval 	0 	désactive le rapport d'erreur durant l'évaluation d'une assertion
    ASSERT_CALLBACK 	assert.callback 	(NULL) 	fonction de rappel utilisateur, pour le traitement des assertions fausses
    */
    assert_options( ASSERT_ACTIVE, 1);
    assert_options( ASSERT_WARNING, 1 );
    assert_options( ASSERT_BAIL, 0 );
    //assert_options( ASSERT_CALLBACK , 'myAssertCallback');

    /*
     ob_start();
    phpinfo();
    file_put_contents('.phpinfos.out.txt', ob_get_contents());
    */

    echo 'include path: ' . get_include_path ()  . PHP_EOL;

    $zver=new \Zend\Version\Version();
    echo 'ZEND VERSION : ' . $zver::VERSION . "\n";

    //For pdtEclipse debugger, set the env variable "arg1", "arg2" ... "argn" in tab env of debugger configuration.
    $i=1;
    while($ENV['arg'.$i]){
        $argv[$i] = $ENV['arg'.$i];
        $i++;
    }

    $shortopts = '';
    $shortopts .= "c:"; // La classe <class> de test a lancer
    $shortopts .= "m:"; // La methode <method> a executer
    $shortopts .= "a";  // Execute tous les tests
    $shortopts .= "h";  // affiche l'aide
    $shortopts .= "t:"; // call test function <function>
    $shortopts .= "i"; //initialise les données de test
    $shortopts .= "s"; //Extrait les scripts sql depuis les classes
    $shortopts .= "p"; //Active le profiling des tests


    $longopts  = array(
        'initdb',
        'nodao',
        'croot',
        'lc', //Affiche la liste des classes de test disponibles
        'lf:',//Affiche la liste des methode de la <classe> de test
    );

    $norun=array('nodao','m', 'p');

    $options = getopt($shortopts, $longopts);

    foreach(array_keys($options) as $o){
        if(!in_array($o, $norun)){
            call_user_func(__NAMESPACE__.'\option'.$o, $options);
        }
    }
    return;
}


/**
 * execute la fonction test spécifiée
 */
function optiont($options)
{
    $suffix = $options['t'];
    if($suffix==''){
        $suffix='my';
    }
    call_user_func('test_'.$suffix, array());
}

/**
 * execute tous les test
 */
function optiona($options)
{
    boot();

    $Test=new \Rbh\Test\Test();

    if ( isset($options['nodao']) ) {
        $Test->withDao(false);
    }
    else{
        $Test->withDao(true);
    }

    if ( isset($options['p']) ) {
        $Test->withProfiling(true);
    }
    else{
        $Test->withProfiling(false);
    }

    $libPath=__DIR__.'/vendor/Rbh';
    return $Test->runAllClasses($libPath);
}

/**
 * liste les fonctions de la classe spécifiée
 */
function optionlf($options)
{
    echo 'List of test function:' .CRLF;
    $class = $options['lf'];

    $methods = get_class_methods($class);
    foreach($methods as $method){
        $t = explode('_', $method);
        if($t[0] == 'Test'){
            echo $method . CRLF;
        }
    }
    return;
}

/**
 * liste les classes de tests
 */
function optionlc($options)
{
    echo 'List of test class:' . CRLF;
    $Test = new \Rbh\Test\Test();
    $libPath = __DIR__.'/vendor/Rbh';
    var_dump($libPath);
    foreach($Test->getTestsFiles($libPath) as $path){
        $class = substr( $path, strlen($libPath) );
        $class = trim($class, '/');
        $class = trim($class, '.php');
        $class = str_replace('/', '\\', $class);
        if($class == 'Test\Test'){
            continue;
        }
        $class = '\Rbh\\'.$class;
        echo $class . CRLF;
    }
    return;
}

/**
 * create root node
 */
function optioncroot($options)
{
    //$Schema = new \Rbh\Dao\Schema( Connexion::get() );
    //$Schema->createUsers();
    //$Schema->createRootNode();
    echo "please, import data directly from SQL scripts" . PHP_EOL;
}

/**
 * Initialise la base de données
 */
function optioninitdb($options)
{
    boot();

    $RbhLibPath = realpath(__DIR__);
    $Schema = new \Rbh\Dao\Schema( Connexion::get() );
    $Schema->sqlInScripts=array(
        'sequence.sql',
        'create.sql',
        'insert.sql',
    );
    $sqlCompiledFile=$Schema->compileSchema($RbhLibPath);
    $Schema->createBdd(file_get_contents($sqlCompiledFile));
}

/**
 * Execute les test de la classe ou des classes spécifiées
 */
function optionc($options)
{
    boot();

    $method = $options['m'];

    if ( isset($options['nodao']) ) {
        $withDao = false;
    }
    else{
        $withDao = true;
    }

    if ( isset($options['p']) ) {
        $withProfiling=true;
    }
    else{
        $withProfiling=false;
    }

    if(is_array($options['c'])){
        foreach($options['c'] as $class){
            \Rbh\Test\Test::runOne($class, $method, $withDao, $continue=false, $withProfiling);
        }
    }
    else{
        \Rbh\Test\Test::runOne($options['c'], $method, $withDao, $continue=false, $withProfiling);
    }
}

/**
 * Extrait les scripts SQL depuis les classes DAO
 */
function options($options)
{
    $RbhPath = realpath(__DIR__.'/vendor/Rbh');
    $Extractor = new SqlExtractor($RbhPath.'/Dao/Schemas/pgsql/extracted');
    $Extractor->parse($RbhPath);
    echo 'See result in ' . $Extractor->resultPath . CRLF;
}

/**
 * Initialize
 */
function boot()
{
    //Bootstrap
    $config = include(__DIR__.'/config/autoload/local.php');

    Connexion::setConfig($config['rbh']['db']);
    DaoFactory::setConfig(include(__DIR__.'/config/objectdaomap.config.php'));

    Loader::$connexion = Connexion::get();
    //ClassDao::singleton()->connexion = Connexion::get();

    $User = new User();
    DaoFactory::getDao($User)->loadFromName($User, 'admin');
    CurrentUser::set($User);
}

/**
 *
 */
function test_my()
{
    //initialize
    boot();

    $Test = new \Rbh\Test\Test();
    $Test->profiling = true;
    $Test->loop = 1;
    $Test->withDao(true);
    //$Test->add('\Rbh\Ged\Docfile\VersionTest');
    $Test->add('\Rbh\Ged\Document\VersionTest');

    $Test->run();
}

/**
 *
 */
function test_date()
{
    $tz = new DateTimeZone('Europe/Paris');
    $date = new DateTime('now',$tz);
    echo $date->format('Y-m-d H:i:s') . "\n";
    echo $date->getTimestamp() . "\n";

    $date = new DateTime('05 oct 2050',$tz);
    echo $date->format('Y-m-d') . "\n";
    echo $date->getTimestamp() . "\n";

    $date = (new datetime())->setTimeStamp(time());
    echo $date->format('Y-m-d') . "\n";
}

function test_exception()
{
    try{
        throw new \Exception('Sys exception message avec un %mot1%');
    }
    catch(\Rbh\Sys\Exception $e){
        var_dump($e->getMessage());
        var_dump($e->getCode());
    }
    catch(\Exception $e){
    }

    try{
        throw new \Rbh\Sys\Exception('Sys exception message avec un %mot1%', 5000, array('mot1'=>'remplacepar'));
    }
    catch(\Rbh\Sys\Exception $e){
        var_dump($e->getMessage());
        var_dump($e->getCode());
    }
    catch(\Rbh\Sys\Exception $e){
        var_dump($e->getMessage());
    }

    try{
        throw new \Rbh\Sys\Exception('Sys exception message avec un %mot1%', 5000, array('mot1'=>'remplacepar'));
    }
    catch(\Exception $e){
        echo 'attrape SysException depuis \Exception'.CRLF;
        var_dump($e->getMessage());
        var_dump($e->getCode());
    }


    try{
        throw new \Rbh\Dao\Exception('Sys exception message avec un %mot1%', 5000, array('mot1'=>'remplacepar'));
    }
    catch(\Rbh\Sys\Exception $e){
        echo 'attrape DaoException depuis SysException'.CRLF;
        var_dump($e->getMessage());
        var_dump($e->getCode());
    }
}

run();
