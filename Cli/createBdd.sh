#!/bin/bash
# Create new database for postgres ubuntu
sudo -u postgres createuser -P -e --createdb --createrole ranchbe

sudo -u postgres dropdb --interactive rbplm_test
sudo -u postgres dropdb --interactive rbplm_dev
sudo -u postgres dropdb --interactive rbplm_prod

sudo -u postgres createdb rbplm_test --owner ranchbe
sudo -u postgres createdb rbplm_dev --owner ranchbe
sudo -u postgres createdb rbplm_prod --owner ranchbe

sudo -u postgres psql -d rbplm_dev -f /usr/share/postgresql/8.4/contrib/ltree.sql
sudo -u postgres psql -d rbplm_test -f /usr/share/postgresql/8.4/contrib/ltree.sql
sudo -u postgres psql -d rbplm_prod -f /usr/share/postgresql/8.4/contrib/ltree.sql

php initBdd.php -e testing -n
php initBdd.php -e testing --initadm
php initBdd.php -e testing --croot
php initBdd.php -e testing --doctype

php initBdd.php -e development -n
php initBdd.php -e development --initadm
php initBdd.php -e development --croot
php initBdd.php -e development --doctype

php initBdd.php -e production -n
php initBdd.php -e production --initadm
php initBdd.php -e production --croot
php initBdd.php -e production --doctype


