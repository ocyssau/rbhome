#!/usr/bin/php
<?php

function cli_daoGenerator_display_help(){
		$display = 'Utilisation :
			  -c <class> -f <toFile> -t <templateFile> -m <model>
			
			  -c <class> Le nom de la classe dont il faut generer la DAO
			  -f <toFile> OPTIONEL Le chemin complet vers le fichier résultat. Defaut est "./tmp/Dao".
			  -t <templateFile> OPTIONEL Le chemin complet vers le fichier template. Defaut est "LIB_PATH/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl".
			  -m <model> OPTIONEL Le chemin complet vers le fichier model. Defaut est "LIB_PATH/Rbplm/Dao/Schemas/pgsql/MODL.csv".
			  
			  Avec les options -help, -h vous obtiendrez cette aide.';
		echo $display;
}

//For pdtEclipse debugger, set the env variable "arg1", "arg2" ... "argn" in tab env of debugger configuration.
$i=1;
if( isset($_ENV['arg'.$i]) ){
	while( $_ENV['arg'.$i] ){
		$argv[$i] = $_ENV['arg'.$i];
		$i++;
	}
}

$shortopts = '';
$shortopts .= "c:";  // <class>
$shortopts .= "f:"; // <toFile>
$shortopts .= "t:"; // <templateFile>
$shortopts .= "m:"; // <model>
$shortopts .= "h:"; // <help>

$options = getopt($shortopts);

if( isset($options['h']) ){
	cli_daoGenerator_display_help();
	die;
}

error_reporting(E_ALL ^ E_NOTICE);
require_once('boot2.php');
echo 'include path: ' . get_include_path ()  . "\n";

require_once('Rbplm/Dao/Pg/Generator.php');
require_once('Rbplm/Sys/Meta/Loader/Csv.php');
require_once('Rbplm/Sys/Meta/Model.php');

$class = $options['c'];
if(!$class){
	echo "l'option -c est obligatoire, vous devez spécifier la classe model" . CRLF;
	cli_daoGenerator_display_help();
	die;
}

if( isset($options['f']) ){
	$toFile = $options['f'];
}
else{
	$toFile = getcwd() . '/tmp/Dao';
}

if( isset($options['t']) ){
	$templateFile = $options['t'];
}
else{
	$templateFile = LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/daoTemplate.tpl';
}

if( isset($options['m']) ){
	$modelFile = $options['m'];
}
else{
	$modelFile = LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/MODL.csv';
}

$Loader = new Rbplm_Sys_Meta_Loader_Csv( array('filename'=>$modelFile) );
$Model = new Rbplm_Sys_Meta_Model();
$Loader->load($Model);

$Generator = new Rbplm_Dao_Pg_Generator();
$Generator->generate($class, $toFile, $templateFile, $Model);
