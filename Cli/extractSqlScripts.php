#!/usr/bin/php
<?php

putenv('APPLICATION_ENV=testing');
require_once('../application/configs/boot.php');
rbinit_autoloader(false);
rbinit_includepath();

rbinit_cli();
rbinit_api();
rbinit_config();
rbinit_logger();

Zend_Session::start();
//echo 'Lib path: ' . RBPLM_LIB_PATH . CRLF;
//echo 'include path: ' . get_include_path ()  . CRLF;

require_once('./class/SqlScripts.php');

function cli_extractSqlScripts_display_help(){
		$display = 'Utilisation :
			  -f <toFile>
			
			  -f <toFile> OPTIONEL Le chemin complet vers le fichier résultat. Defaut est "./tmp/Dao".
			  Avec les options -help, -h vous obtiendrez cette aide.';
		echo $display;
}

/* CLI */

$shortopts = '';
$shortopts .= "f:"; // <toFile>
$shortopts .= "h:"; // <help>

$options = getopt($shortopts);
if( isset($options['h']) ){
	cli_extractSqlScripts_display_help();
	die;
}
if( isset($options['f']) ){
	$toFile = $options['f'];
}
else{
	$toFile = RBPLM_LIB_PATH . '/Rbplm/Dao/Schemas/pgsql/compiled';
}


$Extractor = new rbplmCli_SqlScripts();
if($toFile){
	$Extractor->resultPath = $toFile;
}
$Extractor->parse(RBPLM_LIB_PATH . '/Rbplm');
echo 'See result in ' . $Extractor->resultPath . CRLF;

require_once('./class/Pgsql/Bddschema.php');
$Schema = new rbplmCli_Pgsql_Bddschema( Rbplm_Sys_Config::getConfig() );

$out = $Schema->createShema();
echo "Schema is generate in file " . $out . CRLF;



