<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
	'router' => array(
		'routes' => array(
			'calculator' => array(
				'type'    => 'Segment',
				'options' => array(
					'route' => '/calculator[/][:action]',
					'defaults' => array(
						'controller' => 'Calculator\Controller\Calculator',
						'action'     => 'index',
					),
				),
			),
			'material' => array(
				'type'    => 'Segment',
				'options' => array(
					'route' => '/material[/][:action][/:id]',
					'defaults' => array(
						'controller' => 'Calculator\Controller\Material',
						'action'     => 'index',
					),
				),
			),
			'projet' => array(
					'type'    => 'Segment',
					'options' => array(
							'route' => '/projet[/][:action][/:id]',
							'defaults' => array(
									'controller' => 'Calculator\Controller\Projet',
									'action'     => 'index',
							),
					),
			),
			'recipe' => array(
					'type'    => 'Segment',
					'options' => array(
							'route' => '/recette[/][:action][/:id]',
							'defaults' => array(
									'controller' => 'Calculator\Controller\Recipe',
									'action'     => 'index',
							),
					),
			),
			'build' => array(
					'type'    => 'Segment',
					'options' => array(
							'route' => '/build[/][:action][/:id]',
							'defaults' => array(
									'controller' => 'Calculator\Controller\Buildsurface',
									'action'     => 'index',
							),
					),
			),
		),
	),
	'controllers' => array(
		'invokables' => array(
			'Calculator\Controller\Calculator' => 'Calculator\Controller\CalculatorController',
			'Calculator\Controller\Material' => 'Calculator\Controller\MaterialController',
			'Calculator\Controller\Recipe' => 'Calculator\Controller\RecipeController',
			'Calculator\Controller\Buildsurface' => 'Calculator\Controller\BuildsurfaceController',
			'Calculator\Controller\Projet' => 'Calculator\Controller\ProjetController',
			),
	),
    'view_manager' => array(
        'template_path_stack' => array(
            'calculator' => __DIR__ . '/../view',
        ),
    ),
);