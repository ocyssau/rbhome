<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Calculator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Calculator\Form\BuildsurfaceForm;
use Calculator\Form\BuildsurfacelayerForm;
use Calculator\Form\PaginatorForm;
use Calculator\Form\StdFilterForm;

use \Rbh\Build as Build;
use \Rbh\Dao\Connexion;
use \Rbh\Dao\Factory as DaoFactory;
use \Rbh\Dao\Pg\DaoList as DaoList;
use \Rbh\Dao\Pg\Loader as DaoLoader;
use \Rbh\Dao\Pg\ClassDao as ClassDao;

class BuildsurfaceController extends AbstractActionController
{
	public function indexAction()
	{
		$List = new DaoList();
		$List->table='build_surface';
		$request = $this->getRequest();
		
		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($List->countAll(""));
		$paginator->setData($request->getPost());
		$paginator->prepare();
		
		$filter = new StdFilterForm();
		$filter->setData($request->getPost());
		$filter->prepare();
		($filter->where)? $where=$filter->where : $where='cid>1999';
		
		$List->load("$where ORDER BY id ASC LIMIT $paginator->limit OFFSET $paginator->offset");

		return array(
			'paginator' => $paginator,
			'filter'=>$filter,
			'List'=>$List,
			'cConverter'=>ClassDao::singleton()->load()
		);
	}

	/**
	 *
	 */
	public function addAction()
	{
		$form = new BuildsurfaceForm();
		$form->get('submit')->setValue('Add');
			
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->prepare();
			$form->setInputFilter( $form->prepareFilters() );
			$form->setData($request->getPost());

			if ($form->isValid()) {
				$Model = Build\Surface::init();
				$form->save($Model);
				$Dao = new Build\SurfaceDao(array(), Connexion::get());
				$Dao->save($Model);
				return $this->redirect()->toRoute('build');
			}
		}
		return array('form' => $form);
	}

	/**
	 *
	 */
	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}
		
		try {
			$Dao = new Build\SurfaceDao(array(), Connexion::get());
			$Model = $Dao->load(new Build\Surface(), 'id='.$id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}
		
		$form = new BuildsurfaceForm();
		$form->get('submit')->setAttribute('value', 'Save');
		$form->bind($Model);
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter( $form->prepareFilters() );
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$form->save($Model);
				$Dao->save($Model);
				return $this->redirect()->toRoute('build');
			}
		}
		$view = new ViewModel();
		$view->setTemplate('calculator/buildsurface/add');
		$view->id=$id;
		$view->form=$form;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}
			
		try {
			$Dao = new Build\SurfaceDao(array(), Connexion::get());
			$Model = $Dao->load(new Build\Surface(), 'id='.$id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}
			
		$Dao->suppress($Model);
		return $this->redirect()->toRoute('build', array(
			'action' => 'index'
		));
	}

	/**
	 *
	 */
	public function addlayerAction()
	{
		$request = $this->getRequest();

		$parentSurfaceId = (int) $this->params()->fromRoute('id', 0);
		if (!$parentSurfaceId) {
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}

		//DaoLoader::init();
		//$parentSurface = DaoLoader::load($parentSurfaceId,DaoLoader::getClassFromId($parentSurfaceId));

		try {
			$SurfaceDao = new Build\SurfaceDao();
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}

		$formLayer = new BuildsurfacelayerForm();
		$formLayer->get('submit')->setValue('Save');

		if ($request->isPost()) {
			$formLayer->prepare();
			$formLayer->setData($request->getPost());

			if ($formLayer->isValid()) {
				$LinkDao = new \Rbh\LinkDao();
				$datas = $request->getPost();
				//var_dump($datas);die;

				//clean layers and recreate
				$beforelist = explode('_', $datas['beforelist']);
				$afterlist = array();
				
				//var_dump($datas);die;

				$i=1;
				while( isset($datas['name_'.$i]) ){
					$layid = $datas['layid_'.$i];
					//echo $layid;
					//Create
					if($layid == 0){
						$name = $datas['name_'.$i];
						$materialId = $datas['material_'.$i];
						$thickness = $datas['thickness_'.$i];
						$surface = $datas['surface_'.$i];
						$layOffset = $datas['layoffset_'.$i];

						$Layer = Build\Surface::init($name);
						$Layer->thickness = $thickness;
						$Layer->surface = $surface;
						$Layer->layOffset = $layOffset;
						
						//create relation with material
						$SurfaceDao->save($Layer);
						//var_dump($Layer->id, $Dao->table);
						$lnk1 = \Rbh\Link::init(array('parentId'=>$Layer->id, 'childId'=>$materialId));
						$lnk2 = \Rbh\Link::init(array('parentId'=>$parentSurfaceId, 'childId'=>$Layer->id, 'lindex'=>$layOffset));
						$LinkDao->insert(array($lnk1, $lnk2));
					}
					//Update
					else{
						$afterlist[] = $layid;
						$materialId = $datas['material_'.$i];
						$lnkUid = $datas['lnkuid_'.$i];
						$matLnkUid = $datas['matlnk_'.$i];
						($datas['layOffset_'.$i]) ? $layOffset=$datas['layOffset_'.$i]: $layOffset=0;
						
						$Layer=$SurfaceDao->loadFromId(new Build\Surface(), $layid);
						$Layer->name=$datas['name_'.$i];
						$Layer->thickness=$datas['thickness_'.$i];
						$Layer->surface=$datas['surface_'.$i];
						$Layer->layOffset=$datas['layOffset_'.$i];
						$Layer->description = 'changed';
						$SurfaceDao->save($Layer);
						
						//link surface->layer
						$LinkDao->update2("luid='$lnkUid'", array('lindex'=>$layOffset));
						
						//link layer->material
						//var_dump($lnkUid, $layid, $materialId);die;
						$LinkDao->update2("luid='$matLnkUid'", array('lchild'=>$materialId));
						
						/*
						$properties = array(
							'id'=>$datas['layid_'.$i],
							'name'=>$datas['name_'.$i],
							'thickness'=>$datas['thickness_'.$i],
							'surface'=>$datas['surface_'.$i],
							'layOffset'=>$datas['layoffset_'.$i],
						);
						*/
						//$SurfaceDao->save($Layer, array('name','thickness','surface','layOffset'));
						//$lnk1 = new \Rbh\Link(array('parentId'=>$datas['layid_'.$i], 'childId'=>$materialId));
					}
					$i++;
				}
				//Suppress
				$List = $SurfaceDao->newList();
				foreach( array_diff($beforelist, $afterlist) as $val ){
					//$List->suppress('id='.$val);
				}
				return $this->redirect()->toRoute('build');
			}
		}
		else{
			DaoLoader::init();
			$surface1 = $SurfaceDao->load(new Build\Surface(), 'id='.$parentSurfaceId);
			DaoLoader::loadChildren($surface1, 2);
			//$List = $SurfaceDao->newList();
			//$sql = 'SELECT * FROM anyobject_links AS l JOIN '.$SurfaceDao->table.' AS a ON l.lchild=a.id WHERE l.lparent='.$parentSurfaceId.' ORDER BY lindex ASC';
			//$List->loadFromSql($sql);
			//$List->loadTree($parentSurfaceId);
			//$formLayer->bind($List);
			$formLayer->bindFromObject($surface1);
		}
		return array('formLayer'=>$formLayer);
	}

	/**
	 *
	 * @return \Zend\Http\Response
	 */
	public function reportAction()
	{
		$request = $this->getRequest();
			
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}
			
		$Dao = new Build\SurfaceDao(array(), Connexion::get());
			
		$Model = new Build\Surface();
		$Dao->load($Model, 'id='.$id);
		
		try {
			$List = $Dao->newList();
			//$List->load('parent='.$id. ' ORDER BY layoffset ASC');
			$sql = 'SELECT surface.id as sid,surface.name as sname,surface.parent as sparent, material.name as mname
					FROM build_surface AS surface
					LEFT OUTER JOIN material AS material ON surface.material=material.id
					ORDER BY surface.layoffset ASC';
			$List->loadFromSql($sql);
		}
		catch (\Exception $ex){
			return $this->redirect()->toRoute('build', array(
				'action' => 'index'
			));
		}
		$View = new ViewModel();
		$View->List = $List;
		
		foreach($List as $item){
		}
		
		return $View;
			
		//calculate surface,volumes on each surface
		//get density of material
		//calculate weight
	}
}
