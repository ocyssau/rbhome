<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Calculator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Rbh\Material as Material;
use Calculator\Form\MaterialForm;
use Calculator\Form\PaginatorForm;
use Calculator\Form\StdFilterForm;
use \Rbh\Dao\Connexion;
use \Rbh\Dao\Factory;
use \Rbh\Dao\Pg\DaoList;
use \Rbh\Dao\Pg\Loader;
use \Rbh\Dao\Pg\ClassDao as ClassDao;

class MaterialController extends AbstractActionController
{
	public function indexAction()
	{
		$List = new DaoList();
		$List->table='material';
		$request = $this->getRequest();
		
		$paginator = new PaginatorForm();
		$paginator->setMaxLimit($List->countAll("cid > 0 AND cid < 5050"));
		$paginator->setData($request->getPost());
		$paginator->prepare();

		$filter = new StdFilterForm();
		$filter->setData($request->getPost());
		$filter->key = 'name';
		$filter->passThrough=true;
		$filter->prepare();
		if($filter->where){
			$where = ' AND '.$filter->where;
		}
		else{
			$where=null;
		}
		
		$List->load("cid > 0 AND cid < 5051 $where ORDER BY id ASC LIMIT $paginator->limit OFFSET $paginator->offset");

		return array(
			'paginator' => $paginator,
			'filter'=>$filter,
			'List'=>$List,
			'cConverter'=>ClassDao::singleton()->load()
		);
	}

	public function addAction()
	{
		$form = new MaterialForm();
		$form->get('submit')->setValue('Add');
		//$form->setAttribute('action', $this->url('material', array('action' => 'edit')));

		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter( $form->prepareFilters() );
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$type = $form->get('type')->getValue();
				switch ($type){
					case Material\Material::TYPE_BULK:
						$material = Material\Bulk::init();
						break;
					case Material\Material::TYPE_COMPOSIT:
						$material = Material\Composite::init();
						break;
					case Material\Material::TYPE_DISCRETE:
						$material = Material\Discrete::init();
						break;
					case Material\Material::TYPE_LINEIC:
						$material = Material\Lineic::init();
						break;
					case Material\Material::TYPE_SURFACIC:
						$material = Material\Surfacic::init();
						break;
				}
				$form->save($material);
				$Dao = Factory::getDao($material)->save($material);
				return $this->redirect()->toRoute('material');
			}
		}
		return array('form' => $form);
	}

	/**
	 *
	 */
	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('material', array(
				'action' => 'index'
			));
		}
			
		try {
			$Dao = new Material\MaterialDao();
			$Model = $Dao->load(new Material\Material(), 'id='.$id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('material', array(
				'action' => 'index'
			));
		}

		$form = new MaterialForm();
		$form->get('submit')->setAttribute('value', 'Save');
		$form->bind($Model);

		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter( $form->prepareFilters() );
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$form->save($Model);
				$Dao->save($Model);
				return $this->redirect()->toRoute('material');
			}
		}
		$view = new ViewModel();
		$view->setTemplate('calculator/material/add');
		$view->id=$id;
		$view->form=$form;
		return $view;
	}

	/**
	 *
	 */
	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('material', array(
				'action' => 'index'
			));
		}
			
		try {
			$Dao = new Material\MaterialDao(array(), Connexion::get());
			$Model = $Dao->load(new Material\Material(), 'id='.$id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('material', array(
				'action' => 'index'
			));
		}
			
		$Dao->suppress($Model);
		return $this->redirect()->toRoute('material');
	}

}