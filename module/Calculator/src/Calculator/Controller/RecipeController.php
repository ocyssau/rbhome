<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Calculator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Rbh\Material;
use \Rbh\Material\Recipe;
use Calculator\Form\RecipeForm;
use Calculator\Form\RecipeIngredientForm;
use \Rbh\Dao\Connexion;
use \Rbh\Dao\Factory;

class RecipeController extends AbstractActionController
{
    public function indexAction()
    {
    	$View = new ViewModel();
    	$List = new \Rbh\Dao\Pg\DaoList(array('table'=>'materialrecipe'), Connexion::get());
    	$sql = 'SELECT recipe.id, recipe.name,recipe.density,recipe.material, mat.name AS material
			    	FROM materialrecipe AS recipe
			    	LEFT OUTER JOIN anyobject_links AS link ON recipe.id = link.parent
			    	LEFT OUTER JOIN material AS mat ON link.child=mat.id
    				ORDER BY recipe.id ASC';
    	$List->loadFromSql($sql);
    	$View->List = $List;
    	return $View;
    }
    
    public function addAction()
    {
    	$form = new RecipeForm();
    	$form->get('submit')->setValue('Add');
    	//$form->setAttribute('action', $this->url('material', array('action' => 'edit')));
    	
    	$request = $this->getRequest();
    	if ($request->isPost()) {
			//$form->setInputFilter( $form->prepareFilters() );
            $form->setData($request->getPost());
    		if ($form->isValid()) {
    			$recipe = Recipe::init();
    			$form->save($recipe);
				$Dao = new Material\RecipeDao(array(), Connexion::get());
				$Dao->save($recipe);
    			return $this->redirect()->toRoute('recipe');
    		}
    	}
    	return array('form' => $form);
    }
    
    /**
     * 
     */
    public function editAction()
    {
    	$id = (int) $this->params()->fromRoute('id', 0);
    	if (!$id) {
    		return $this->redirect()->toRoute('recipe', array(
				'action' => 'index'
    		));
    	}
    	
    	try {
    		$Dao = new Material\RecipeDao(array(), Connexion::get());
    		$Model = $Dao->load(new Recipe(), 'id='.$id);
    	}
    	catch (\Exception $ex) {
    		return $this->redirect()->toRoute('recipe', array(
    				'action' => 'index'
    		));
    	}
    	
    	$form = new RecipeForm();
    	$form->get('submit')->setAttribute('value', 'Save');
    	$form->bind($Model);
    	
    	$request = $this->getRequest();
    	if ($request->isPost()) {
			//$form->setInputFilter( $form->prepareFilters() );
    		$form->setData($request->getPost());
    		if ($form->isValid()) {
    			$form->save($Model);
    			$Dao->save($Model);
    			return $this->redirect()->toRoute('recipe');
    		}
    	}
    	$view = new ViewModel();
    	$view->setTemplate('calculator/recipe/add');
    	$view->id=$id;
    	$view->form=$form;
    	return $view;
    }
    
    /**
     *
     */
    public function deleteAction()
    {
    	$id = (int) $this->params()->fromRoute('id', 0);
    	if (!$id) {
    		return $this->redirect()->toRoute('recipe', array(
    				'action' => 'index'
    		));
    	}
    	
    	try {
    		$Dao = new Material\RecipeDao(array(), Connexion::get());
    		$Model = $Dao->load(new Recipe(), 'id='.$id);
    	}
    	catch (\Exception $ex) {
    		return $this->redirect()->toRoute('recipe', array(
    				'action' => 'index'
    		));
    	}
    	
    	$Dao->suppress($Model);
    	return $this->redirect()->toRoute('recipe');
    }
    
    /**
     *
     */
    public function addingredientAction()
    {
    	$request = $this->getRequest();
    	
    	$id = (int) $this->params()->fromRoute('id', 0);
    	if (!$id) {
    		return $this->redirect()->toRoute('recipe', array(
    				'action' => 'index'
    		));
    	}
    	
    	$List = Factory::getDao('\Rbh\Material\Material')->newList();
    	$List->withLinks = true;
    	$List->load('parent='.$id);
    	
    	$form = new RecipeIngredientForm();
    	$form->get('submit')->setValue('Save');
    	$form->get('id')->setValue($id);
    	
    	if ($request->isPost()) {
    		$form->prepare();
    		$form->setData($request->getPost());
    		
    		if ($form->isValid()) {
    			$datas = $request->getPost();
		    	
    			//clean and recreate
    			$beforelist = explode('_', $datas['beforelist']);
    			$afterlist = array();
    			
    			$Recipe = Factory::getDao('Rbh\Material\Recipe')->load(new Recipe(), 'id='.$id);
    			
    			//var_dump($datas);die;
    			$i=1;
    			while( isset($datas['name_'.$i]) ){
    				$matid = (int)$datas['matid_'.$i]; //ingredientId
    				//Create
    				if($matid == 0){
    					$name = $datas['name_'.$i];
    					$materialId = (int)$datas['material_'.$i];
    					$qty = (float) $datas['qty_'.$i];
    					$unit = $datas['unit_'.$i];

    					$Material = Factory::getDao('\Rbh\Material\Material')->load(new Material\Material(), 'id='.$materialId);
    					$Recipe->addIngredient($Material, $name, $qty, $unit);
    					
    					$toSave=true;
    				}
    				//Update
    				else{
    					$afterlist[] = $matid;
    					$Ingredient = Factory::getDao('\Rbh\Material\Material')->load(new Material\Material(), 'id='.$matid);
    					$Ingredient->name = $datas['name_'.$i];
    					$Ingredient->materialId = (int)$datas['material_'.$i];
    					$Ingredient->qty = (float)$datas['qty_'.$i];
    					$Ingredient->inputUnit = $datas['unit_'.$i];
    					
    					Factory::getDao('\Rbh\Material\Material')->save($Ingredient, array('name','materialId','thickness','surface','layOffset', 'inputUnit'));
    				}
    				$i++;
    			}
    			
    			//save
    			if($toSave == true){
    				$Recipe->prepare();
    				foreach($Recipe->materials as $material){
    					Factory::getDao('\Rbh\Material\Material')->save($material);
    				}
    			}
    			
    			//Suppress
    			foreach( array_diff($beforelist, $afterlist) as $val ){
    				$List->suppress('id='.$val);
    			}
    			 
    			return $this->redirect()->toRoute('recipe');
    		}
    	}
    	else{
    		$form->bind($List);
    	}
    	 
    	return array('form' => $form);
    }
    
}