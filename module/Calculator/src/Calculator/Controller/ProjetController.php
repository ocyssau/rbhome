<?php
/**
 * Zend Framework (http://framework.zend.com/)
*
* @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
* @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
* @license   http://framework.zend.com/license/new-bsd New BSD License
*/

namespace Calculator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Rbh\Project as Project;
use Calculator\Form\ProjetForm;

class ProjetController extends AbstractActionController
{
	public function indexAction()
	{
    	$View = new ViewModel();
    	$List = new \Rbh\Dao\Pg\DaoList(array('table'=>'project'), \Rbh\Dao\Connexion::get());
    	$List->load();
    	$View->List = $List;
    	return $View;
	}

	public function addAction()
	{
		$form = new ProjetForm();
		$form->get('submit')->setValue('Add');
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->prepare();
			$form->setInputFilter( $form->prepareFilters() );
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$Model = Project\Project::init();
				$form->save($Model);
				$Dao = new Project\ProjectDao(array(), \Rbh\Dao\Connexion::get());
				$Dao->save($Model);
				return $this->redirect()->toRoute('projet');
			}
		}
		return array('form' => $form);
	}
	
	
	/**
	 *
	 */
	public function editAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('projet', array(
					'action' => 'index'
			));
		}
		 
		try {
			$Dao = new Project\ProjectDao(array(), \Rbh\Dao\Connexion::get());
			$Model = $Dao->load(new Project\Project(), 'id='.$id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('projet', array(
					'action' => 'index'
			));
		}
		
		$form = new ProjetForm();
		$form->get('submit')->setAttribute('value', 'Save');
		$form->bind($Model);
		 
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setInputFilter( $form->prepareFilters() );
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$form->save($Model);
				$Dao->save($Model);
				return $this->redirect()->toRoute('projet');
			}
		}
		$view = new ViewModel();
		$view->setTemplate('calculator/projet/add');
		$view->id=$id;
		$view->form=$form;
		return $view;
	}
	
	/**
	 *
	 */
	public function deleteAction()
	{
		$id = (int) $this->params()->fromRoute('id', 0);
		if (!$id) {
			return $this->redirect()->toRoute('projet', array(
					'action' => 'index'
			));
		}
		
		try {
			$Dao = new Project\ProjectDao(array(), \Rbh\Dao\Connexion::get());
			$Model = $Dao->load(new Project\Project(), 'id='.$id);
		}
		catch (\Exception $ex) {
			return $this->redirect()->toRoute('projet', array(
					'action' => 'index'
			));
		}
		 
		$Dao->suppress($Model);
		return $this->redirect()->toRoute('projet', array(
				'action' => 'index'
		));
	}
	
}