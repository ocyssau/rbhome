<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Calculator\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Calculator\Form\AuthenticateForm as AuthenticateForm;
use Calculator\Form\CreateuserForm as CreateuserForm;
use Zend\Authentication\AuthenticationService;
use Calculator\Model\AuthAdapter;

class CalculatorController extends AbstractActionController
{
    public function indexAction()
    {
    }
    
    /**
     * 
     * @return multitype:\Calculator\Form\AuthenticateForm
     */
    public function authenticateAction()
    {
    	$form = new AuthenticateForm();
    	$form->get('submit')->setValue('Connect');
    	 
    	$request = $this->getRequest();
    	if ($request->isPost()) {
			$form->setInputFilter( $form->prepareFilters() );
            $form->setData($request->getPost());
    		if ($form->isValid()) {
    			//return $this->redirect()->toRoute('material');
    			$auth = new AuthenticationService();
    			$authAdapter = new \Calculator\Model\AuthAdapter(\Rbh\Dao\Connexion::get());
    			$authAdapter->identity = $this->getRequest()->getPost('username');
    			$authAdapter->password = $this->getRequest()->getPost('password');
    			
    			$result = $auth->authenticate($authAdapter);
    			
    			if (!$result->isValid()) {
    				// Authentication failed; print the reasons why
    				foreach ($result->getMessages() as $message) {
    					echo "$message\n";
    				}
    			}
    			else {
    				// Authentication succeeded; the identity ($username) is stored in the session
    				// $result->getIdentity() === $auth->getIdentity()
    				// $result->getIdentity() === $username
    			}
    		}
    	}
    	return array('form' => $form);
    }
    
    /**
     * 
     * @return \Zend\Http\Response|multitype:\Calculator\Form\CreateuserForm
     */
    public function createuserAction()
    {
    	$form = new CreateuserForm();
    	$form->get('submit')->setValue('Save');
    	
    	$request = $this->getRequest();
    	if ($request->isPost()) {
    		$form->setInputFilter($form->prepareFilters());
    		$form->setData($request->getPost());
    		if ($form->isValid()) {
    			$User = \Rbh\People\User::init();
    			$User->parentId=91;
    			$form->save($User);
    			$Dao = new \Rbh\People\UserDao(array(), \Rbh\Dao\Connexion::get());
    			$Dao->save($User);
    			return $this->redirect()->toRoute('calculator');
    		}
    	}
    	return array('form' => $form);
    }
    
    /**
     *
     * @return \Zend\Http\Response|multitype:\Calculator\Form\CreateuserForm
     */
    public function confirmuserAction()
    {
    	$request = $this->getRequest();
    	$token = $request->getParam('token');
    	$login = $request->getParam('login');
    	
    	$Dao = new \Rbh\People\UserDao(array(), \Rbh\Dao\Connexion::get());
    	$User = $Dao->load(new \Rbh\People\User(), "login='$login'");
    	
    	if($User->uid == $token){
    		$User->isActive(true);
    		$Dao->save($User);
    	}
    	else{
    		\Rbh\Sys\Mail::initTransport(
    			array(
    				'name'=>'localhost.localdomain',
    				'host'              => '127.0.0.1',
    				'connection_class'  => 'login',
    				'connection_config' => array(
    					'username' => 'user',
    					'password' => 'pass',
    				)
    			));
    		$mail = new \Rbh\Sys\Mail();
    		$mail->setBody('');
    		$mail->setFrom('noreply@example.org', 'Rbh');
    		$mail->addTo($User->mail, $User->firstname .' '. $User->lastname);
    		$mail->setSubject('TestSubject');
    	}
    }
}
