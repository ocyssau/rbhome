<?php
namespace Calculator\Model;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result as AuthenticationResult;

class AuthAdapter implements AdapterInterface
{
	public $identity;
	public $password;
	public $currentUser;
	
	public function __construct( $conn = null )
	{
		if( $conn ){
			$this->connexion = $conn;
		}
	} //End of function
	
	/**
	 * (non-PHPdoc)
	 * @see Zend\Authentication\Adapter.AdapterInterface::authenticate()
	 */
	public function authenticate()
	{
		$login   = $this->identity;
		
		$Dao = new \Rbh\People\UserDao(array(), $this->connexion);
		$filter = "login='$login'";
		
		try{
			$this->currentUser = $Dao->load(new \Rbh\People\User(), $filter);
		}
		catch(\Exception $e){
			$exception = AuthenticationResult::FAILURE_IDENTITY_NOT_FOUND;
			throw new \Exception($exception);
		}
		if($this->currentUser->password != md5($this->password)){
			$exception = AuthenticationResult::FAILURE_CREDENTIAL_INVALID;
			throw new \Exception($exception);
		}
		if($this->currentUser->isActive == false){
			$exception = AuthenticationResult::FAILURE_UNCATEGORIZED;
			throw new \Exception($exception);
		}
		$this->currentUser->lastLogin = now();
		$Dao->save($this->currentUser);
	}
}
