<?php
namespace Calculator\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use \Rbh\Dao\Connexion;

use \Rbh\Dao\Factory as DaoFactory;
use \Rbh\Dao\Pg\DaoList as DaoList;
use \Rbh\Dao\Pg\Loader as DaoLoader;
use \Rbh\Dao\Pg\ClassDao as ClassDao;

class BuildsurfacelayerForm extends Form
{
	private $materials;

	/**
	 *
	 * @param unknown_type $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('buildsurfacelayer');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		$this->add(array(
				'name' => 'beforelist',
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Go',
						'id' => 'submitbutton',
				),
		));
		//$this->addRow(1);
	}
	
	public function addRow($index){
		$this->add(array(
				'name' => 'lnkuid_'.$index,
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'layid_'.$index,
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'layoffset_'.$index,
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'name_'.$index,
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Name',
				),
		));
		$this->add(array(
				'type'  => 'Select',
				'name' => 'material_'.$index,
				'attributes' => array(
				),
				'options' => array(
						'label' => 'Materiaux',
						'value_options' => $this->_getMaterials(),
				),
		));
		$this->add(array(
				'name' => 'matlnk_'.$index,
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'type'  => 'Number',
				'name' => 'thickness_'.$index,
				'attributes' => array(
				),
				'options' => array(
						'label' => 'Thickness',
				),
		));
		$this->add(array(
				'type'  => 'Number',
				'name' => 'surface_'.$index,
				'attributes' => array(
				),
				'options' => array(
						'label' => 'Surface',
				),
		));
	}

	/**
	 *
	 */
	public function prepareFilters(){
		if (!isset($this->inputFilter)) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();

			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'name',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::bind()
	 */
	public function bind($List, $flags = 17)
	{
		$beforelist = $this->get('beforelist');
		$ba=array();
		
		if($List->count() > 0){
			foreach( $List as $item){
				$index=$List->key()+1;
				$this->addRow($index);
				$this->get('lnkuid_'.$index)->setValue($item['luid']);
				$this->get('layid_'.$index)->setValue($item['id']);
				$this->get('layoffset_'.$index)->setValue($item['layoffset']);
				$this->get('name_'.$index)->setValue($item['name']);
				$this->get('material_'.$index)->setValue($item['material']);
				$this->get('surface_'.$index)->setValue($item['surface']);
				$this->get('thickness_'.$index)->setValue($item['thickness']);
				$ba[] = $item['id'];
			}
			$beforelist->setValue( implode('_',$ba) );
		}
		else{
			$this->addRow(1);
			$beforelist->setValue( 0 );
		}
		//$this->get('id')->setValue($object->id);
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::bind()
	 */
	public function bindFromObject($Any)
	{
		$beforelist = $this->get('beforelist');
		$ba=array();
		
		$index=0;
		foreach( $Any->links as $lnk){
			$child=$lnk->child;
			if(is_a($child, '\Rbh\Build\Surface')){
				$materialId=null;
				foreach($child->links as $clnk){
					if(is_a($clnk->child, '\Rbh\Material\Material')){
						$materialId=$clnk->childId;
						$materialLnk=$clnk->uid;
						break;
					}
				}
				$index = $index+1;
				$this->addRow($index);
				$this->get('lnkuid_'.$index)->setValue($lnk->uid);
				$this->get('layid_'.$index)->setValue($child->id);
				$this->get('layoffset_'.$index)->setValue($lnk->lindex);
				$this->get('name_'.$index)->setValue($child->name);
				$this->get('material_'.$index)->setValue($materialId);
				$this->get('matlnk_'.$index)->setValue($materialLnk);
				$this->get('surface_'.$index)->setValue($child->surface);
				$this->get('thickness_'.$index)->setValue($child->thickness);
				$ba[] = $child->id;
			}
		}
		$beforelist->setValue( implode('_',$ba) );
	}
	

	/**
	 *
	 * @param DaoList $List
	 */
	public function save($object)
	{
		/*
		$data = $this->getData();
		(isset($data['name'])) ? $object->name=$data['name'] : null;
		(isset($data['material'])) ? $object->material=$data['material'] : null;
		(isset($data['surface'])) ? $object->surface=$data['surface'] : null;
		(isset($data['thickness'])) ? $object->thickness=$data['thickness'] : null;
		*/
	}
	
	private function _getMaterials() {
		if( !$this->materials ){
			$List = new DaoList();
			$List->table='material';
			$List->load("cid > 0 AND cid < 5051 ORDER BY id ASC");
			$this->materials = array();
			foreach($List as $mat){
				$this->materials[$mat['id']] = $mat['name'];
			}
		}
		return $this->materials;
	}
	
}


