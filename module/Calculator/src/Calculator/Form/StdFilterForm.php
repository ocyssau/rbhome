<?php
namespace Calculator\Form;

use Zend\Form\Form;

class StdFilterForm extends Form //implements InputFilterAwareInterface
{
	public $where;
	public $passThrough = true;
	public $key = 'name';
	
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('stdfilter');
		$this->setAttribute('id', 'stdfilter');
		$this->setAttribute('method', 'post');

		$this->add(array(
			'name' => 'stdfilter-searchInput',
			'type'  => 'Zend\Form\Element\Text',
			'options' => array(
				'label' => '',
			),
			'attributes' => array(
				'onChange'=>'',
			)
		));

		$this->add(array(
			'type'  => 'Zend\Form\Element\Submit',
			'name' => 'stdfilter-submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Find',
				'id' => 'stdfilter-submit',
			),
				
		));
	}
	
	public function prepare(){
		parent::prepare();
		$search=$this->get('stdfilter-searchInput')->getValue();
		if($search){
			if($this->passThrough){
				$search = str_replace('*','%',$search);
				$this->where= $this->key . " LIKE '%$search%'";
			}
			else{
				$search = str_replace('*','%',$search);
				$this->where= $this->key . " LIKE '$search'";
			}
		}
	}
	
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= $view->formRow($this->get('stdfilter-searchInput'));
		$html .= $view->formRow($this->get('stdfilter-submit'));
		$html .= $view->form()->closeTag();
		return $html;
	}
	
}
