<?php
namespace Calculator\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use \Rbh\Material\Material;

class MaterialForm extends Form //implements InputFilterAwareInterface
{
	protected $inputFilter;
	
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('material');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		$this->add(array(
			'name' => 'name',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Name',
			),
		));
		$this->add(array(
			'name' => 'density',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'number',
			),
			'options' => array(
				'label' => 'Density',
			),
		));
		$this->add(array(
			'name' => 'thLambda',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'number',
			),
			'options' => array(
				'label' => 'Lambda',
			),
		));
		$this->add(array(
			'name' => 'thSpecificHeat',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'number',
			),
			'options' => array(
				'label' => 'Specific heat',
			),
			'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
			),
			'validators' => array(
					array(
							'name'    => 'Float',
					),
			),
		));
		$this->add(array(
			'name' => 'type',
			'type'  => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => 'Type',
			),
			'attributes' => array(
				'options'=>array(
					Material::TYPE_LINEIC=>'lineic',
					Material::TYPE_BULK=>'bulk',
					Material::TYPE_SURFACIC=>'surfacic',
					Material::TYPE_DISCRETE=>'discret',
					Material::TYPE_RECIPE=>'recette',
				),
				'value' => Material::TYPE_BULK,
			)
		));
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Go',
				'id' => 'submitbutton',
			),
		));
	}
	
	public function prepareFilters(){
		if (!isset($this->inputFilter)) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();
			
			$inputFilter->add($factory->createInput(array(
					'name'     => 'id',
					'required' => true,
					'filters'  => array(
							array('name' => 'Int'),
					),
			)));
			
			$inputFilter->add($factory->createInput(array(
					'name'     => 'name',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
			
			$inputFilter->add($factory->createInput(array(
					'name'     => 'density',
					'required' => true,
					'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
					),
					'validators' => array(
						array(
							'name'    => 'Float',
						),
					),
			)));
			
			$inputFilter->add($factory->createInput(array(
					'name'     => 'thSpecificHeat',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'Float',
							),
					),
			)));
				
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'thLambda',
				'required' => true,
				'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				),
				'validators' => array(
						array(
								'name'    => 'Float',
						),
				),
			)));
				
			$this->inputFilter = $inputFilter;
		}
		
		return $this->inputFilter;
	}
	
	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->id);
		$this->get('name')->setValue($object->name);
		$this->get('density')->setValue($object->density);
		$this->get('thLambda')->setValue($object->thLambda);
		$this->get('thSpecificHeat')->setValue($object->thSpecificHeat);
		$this->get('type')->setValue($object->classId);
	}
	
	public function save($object)
	{
		$data = $this->getData();
		(isset($data['type'])) ? $object->classId=(int)$data['type'] : null;
		(isset($data['name'])) ? $object->name=$data['name'] : null;
		(isset($data['density'])) ? $object->density=(float)$data['density'] : null;
		(isset($data['thLambda'])) ? $object->thLambda=(float)$data['thLambda'] : null;
		(isset($data['thSpecificHeat'])) ? $object->thSpecificHeat=(float)$data['thSpecificHeat'] : null;
		//(isset($data['volume'])) ? $object->volume=(float)$data['volume'] : null;
		//(isset($data['weight'])) ? $object->weight=(float)$data['weight'] : null;
	}
	
	
	
}
