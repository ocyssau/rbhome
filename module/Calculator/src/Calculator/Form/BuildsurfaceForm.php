<?php
namespace Calculator\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BuildsurfaceForm extends Form
{
	
	/**
	 * 
	 * @param unknown_type $name
	 */
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('buildsurface');
		$this->setAttribute('method', 'post');
		$this->add(array(
				'name' => 'id',
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'name',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Name',
				),
		));
		$this->add(array(
				'name' => 'description',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Description',
				),
		));
		$this->add(array(
				'name' => 'azimut',
				'attributes' => array(
						'type'  => 'number',
				),
				'options' => array(
						'label' => 'Azimut',
				),
				'filters'  => array(
						array('name' => 'StripTags'),
						array('name' => 'StringTrim'),
				),
				'validators' => array(
						array(
								'name'    => 'Float',
						),
				),
		));
		$this->add(array(
				'name' => 'surface',
				'attributes' => array(
						'type'  => 'number',
				),
				'options' => array(
						'label' => 'Surface',
				),
		));
		$this->add(array(
				'name' => 'inclinaison',
				'attributes' => array(
						'type'  => 'number',
				),
				'options' => array(
						'label' => 'Inclinaison',
				),
		));
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Go',
						'id' => 'submitbutton',
				),
		));
	}
	
	/**
	 * 
	 */
	public function prepareFilters(){
		if (!isset($this->inputFilter)) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();
				
			$inputFilter->add($factory->createInput(array(
					'name'     => 'id',
					'required' => true,
					'filters'  => array(
							array('name' => 'Int'),
					),
			)));
	
			$inputFilter->add($factory->createInput(array(
					'name'     => 'name',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
			$this->inputFilter = $inputFilter;
		}
	
		return $this->inputFilter;
	}
	
	/**
	 * (non-PHPdoc)
	 * @see Zend\Form.Form::bind()
	 */
	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->id);
		$this->get('name')->setValue($object->name);
		$this->get('description')->setValue($object->description);
		$this->get('surface')->setValue($object->surface);
		$this->get('azimut')->setValue($object->azimut);
		$this->get('inclinaison')->setValue($object->inclinaison);
	}
	
	/**
	 * 
	 * @param unknown_type $object
	 */
	public function save($object)
	{
		$data = $this->getData();
		(isset($data['name'])) ? $object->name=$data['name'] : null;
		(isset($data['description'])) ? $object->description=$data['description'] : null;
		(isset($data['surface'])) ? $object->surface=$data['surface'] : null;
		(isset($data['azimut'])) ? $object->azimut=$data['azimut'] : null;
		(isset($data['inclinaison'])) ? $object->inclinaison=$data['inclinaison'] : null;
	}
	
	
}


