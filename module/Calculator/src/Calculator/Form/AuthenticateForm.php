<?php
namespace Calculator\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class AuthenticateForm extends Form
{
	protected $inputFilter;

	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('authenticate');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'username',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
					'type'  => 'text',
			),
			'options' => array(
					'label' => 'Name',
			),
		));
		$this->add(array(
				'name' => 'password',
				'type'  => 'Zend\Form\Element\Password',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Password',
				),
		));
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Go',
						'id' => 'submitbutton',
				),
		));
	}

	public function prepareFilters(){
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();
				
			$inputFilter->add($factory->createInput(array(
					'name'     => 'username',
					'required' => true,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
			
			$inputFilter->add($factory->createInput(array(
					'name'     => 'password',
					'required' => false,
					'filters'  => array(
							array('name' => 'StripTags'),
							array('name' => 'StringTrim'),
					),
					'validators' => array(
							array(
									'name'    => 'StringLength',
									'options' => array(
											'encoding' => 'UTF-8',
											'min'      => 1,
											'max'      => 100,
									),
							),
					),
			)));
				
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}
	
	public function bind($object, $flags = 17)
	{
		$this->get('username')->setValue($object->name);
		$this->get('password')->setValue($object->description);
	}
	
	public function save($object)
	{
		$data = $this->getData();
		(isset($data['username'])) ? $object->username=$data['username'] : null;
		(isset($data['password'])) ? $object->password=$data['password'] : null;
	}

}
