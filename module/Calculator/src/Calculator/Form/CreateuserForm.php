<?php
namespace Calculator\Form;

use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreateuserForm extends Form //implements InputFilterAwareInterface
{
	protected $inputFilter;

	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('material');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		$this->add(array(
			'name' => 'firstname',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'First name',
			),
		));
		$this->add(array(
			'name' => 'lastname',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Last name',
			),
		));
		$this->add(array(
			'name' => 'mail',
			'type'  => 'Zend\Form\Element',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Mail',
			),
		));
		$this->add(array(
			'name' => 'password',
			'type'  => 'Zend\Form\Element\Password',
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Password',
			),
		));

		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Go',
				'id' => 'submitbutton',
			),
		));
	}

	public function prepareFilters(){
		if (!isset($this->inputFilter)) {
			$inputFilter = new InputFilter();
			$factory     = new InputFactory();
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'id',
				'required' => true,
				'filters'  => array(
					array('name' => 'Int'),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'firstname',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));
			
			$inputFilter->add($factory->createInput(array(
				'name'     => 'lastname',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));
				
			$inputFilter->add($factory->createInput(array(
				'name'     => 'mail',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));

			$inputFilter->add($factory->createInput(array(
				'name'     => 'password',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 100,
						),
					),
				),
			)));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->id);
		$this->get('firstname')->setValue($object->firstname);
		$this->get('lastname')->setValue($object->lastname);
		$this->get('mail')->setValue($object->mail);
		$this->get('password')->setValue($object->password);
	}
	
	public function save($object)
	{
		$data = $this->getData();
		(isset($data['firstname'])) ? $object->firstname=$data['firstname'] : null;
		(isset($data['lastname'])) ? $object->lastname=$data['lastname'] : null;
		if (isset($data['mail'])) {
			$object->mail  = $data['mail'];
			$object->login = $data['mail'];
			$object->name  = $data['mail'];
		}
		(isset($data['password'])) ? $object->password=md5($data['password']) : null;
	}

}
