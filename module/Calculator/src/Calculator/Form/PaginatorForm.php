<?php
namespace Calculator\Form;

use Zend\Form\Form;

class PaginatorForm extends Form //implements InputFilterAwareInterface
{
	public $maxLimit = 100;
	public $limit = 50;
	public $page = 1;
	public $maxPage = 2;
	public $offset = 0;
	
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('paginator');
		$this->setAttribute('id', 'paginator');
		$this->setAttribute('method', 'post');

		$this->add(array(
			'name' => 'paginator-limit',
			'type'  => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => 'Result per page',
			),
			'attributes' => array(
				'onChange'=>'$(\'#paginator\').submit();',
				'options'=>array(
					10=>10,
					20=>20,
					50=>50,
					100=>100,
					1000=>1000
				),
				'value' => 50,
			)
		));

		$this->add(array(
			'name' => 'paginator-page',
			'type'  => 'Zend\Form\Element\Select',
			'options' => array(
				'label' => 'Page',
			),
			'attributes' => array(
				'id'=>'paginator-page',
				'onChange'=>"$('#paginator').submit();",
				'options'=>array(
					1=>1,
				),
				'value' => 1,
			)
		));
		
		$this->add(array(
				'name' => 'paginator-next',
				'type'  => 'Zend\Form\Element\Button',
				'options' => array(
						'label' => '>',
						'value' => 'next',
				),
				'attributes' => array(
						'onClick'=>"var page=$('#paginator-page').val();page++;$('#paginator-page').val(page);$('#paginator').submit();",
				)
		));
		
		$this->add(array(
				'name' => 'paginator-prev',
				'type'  => 'Zend\Form\Element\Button',
				'options' => array(
						'label' => '<',
						'value' => 'prev',
				),
				'attributes' => array(
						'onClick'=>"var page=$('#paginator-page').val();page--;$('#paginator-page').val(page);$('#paginator').submit();",
				)
		));
	}
	
	public function setMaxLimit($maxLimit)
	{
		$this->maxLimit = $maxLimit;
	}

	public function prepare()
	{
		parent::prepare();
		$pPageElmt = $this->get('paginator-page');
		
		$this->page = $pPageElmt->getValue();
		$this->limit = $this->get('paginator-limit')->getValue();
		if(!$this->limit){
			$this->limit=50;
		}
		$this->maxPage = ceil($this->maxLimit / $this->limit);
		
		$options = array(1=>1);
		for($i=2;$i<=$this->maxPage;$i++){
			$options[$i]=$i;
		}
		$pPageElmt->setValueOptions($options);
		
		$this->offset = ($this->page-1)*$this->limit;
	}
	
	public function render($view)
	{
		$html = $view->form()->openTag($this);
		$html .= $view->formRow($this->get('paginator-limit'));
		$html .= $view->formRow($this->get('paginator-prev'));
		$html .= $view->formRow($this->get('paginator-page'));
		$html .= $view->formRow($this->get('paginator-next'));
		$html .= $view->form()->closeTag();
		return $html;
	}
	
}
