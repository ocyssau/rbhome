<?php
namespace Calculator\Form;

use Zend\Form\Form;
use \Rbh\Dao\Connexion;
use \Rbh\Material;
use \Rbh\Material\Recipe;


class RecipeIngredientForm extends Form
{
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('recipeIngredient');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'id',
			'attributes' => array(
				'type'  => 'hidden',
			),
		));
		$this->add(array(
			'name' => 'beforelist',
			'attributes' => array(
					'type'  => 'hidden',
			),
		));
		$this->add(array(
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Go',
				'id' => 'submitbutton',
			),
		));
	}
	
	public function addRow($index)
	{
		$this->add(array(
				'name' => 'matid_'.$index,
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'name_'.$index,
				'type'  => 'Zend\Form\Element\Text',
				'attributes' => array(
				),
				'options' => array(
						'label' => 'Name',
				),
		));
		$this->add(array(
				'name' => 'qty_'.$index,
				'type'  => 'Zend\Form\Element\Number',
				'attributes' => array(
				),
				'options' => array(
						'label' => 'Quantité',
				),
		));
		$this->add(array(
				'type'  => 'Select',
				'name' => 'unit_'.$index,
				'attributes' => array(
				),
				'options' => array(
						'label' => 'Unité',
						'value_options' => array(
								'm3' => 'm3',
								'kg' => 'Kg',
						),
				),
		));
		$this->add(array(
				'type'  => 'Select',
				'name' => 'material_'.$index,
				'attributes' => array(
				),
				'options' => array(
						'label' => 'Materiaux',
						'value_options' => $this->_getMaterials(),
				),
		));
	}

	public function bind($List, $flags = 17)
	{
		$beforelist = $this->get('beforelist');
		$ba=array();
		
		if($List->count() > 0){
			foreach( $List as $item){
				$index=$List->key()+1;
				$this->addRow($index);
				$this->get('matid_'.$index)->setValue($item['id']);
				$this->get('name_'.$index)->setValue($item['name']);
				if($item['inputunit'] == 'm3'){
					$this->get('qty_'.$index)->setValue($item['volume']);
					$this->get('unit_'.$index)->setValue('m3');
				}
				else{
					$this->get('qty_'.$index)->setValue($item['weight']);
					$this->get('unit_'.$index)->setValue('kg');
				}
				$this->get('material_'.$index)->setValue($item['material']);
				$ba[] = $item['id'];
			}
			$beforelist->setValue( implode('_',$ba) );
		}
		else{
			$this->addRow(1);
			$beforelist->setValue( 0 );
		}
	}
	
	public function save()
	{
		$data = $_REQUEST;
		$id = $data['id'];
		
		$RecipeDao = new \Rbh\Material\RecipeDao(array(), Connexion::get());
		$MaterialDao = new \Rbh\Material\MaterialDao(array(), Connexion::get());
		
		$Recipe = $RecipeDao->load(new Recipe(), 'id='.$id);
		
		$i=1;
		while(isset($data['name_'.$i])){
			$name = $data['name_'.$i];
			$materialId = (int) $data['material_'.$i];
			$qty = (float) $data['qty_'.$i];
			$unit = $data['unit_'.$i];
			$Material = $MaterialDao->load(new Material\Material(), 'id='.$materialId);
			$Recipe->addIngredient($Material, $name, $qty, $unit);
			$i++;
		}
		
		$Recipe->prepare();
		$RecipeDao->save($Recipe);
		foreach($Recipe->materials as $mat){
			$MaterialDao->save($mat);
		}
	}
	
	private function _getMaterials() {
		$List = new \Rbh\Dao\Pg\DaoList(array('table'=>'material'), Connexion::get());
		$List->load();
		$ret = array();
		foreach($List as $mat){
			$ret[$mat['id']] = $mat['name']; 
		}
		return $ret;
	}

}
