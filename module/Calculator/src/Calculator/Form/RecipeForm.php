<?php
namespace Calculator\Form;

use Zend\Form\Form;
use \Rbh\Dao\Connexion;
use \Rbh\Material;
use \Rbh\Material\Recipe;

class RecipeForm extends Form
{
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('material');
		$this->setAttribute('method', 'post');
		$this->add(array(
				'name' => 'id',
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'name',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Name',
				),
		));
		$this->add(array(
				'name' => 'volume',
				'attributes' => array(
						'type'  => 'number',
				),
				'options' => array(
						'label' => 'Volume',
				),
		));
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Go',
						'id' => 'submitbutton',
				),
		));
	}
	
	public function bind($object, $flags = 17)
	{
		$this->get('id')->setValue($object->id);
		$this->get('name')->setValue($object->name);
		$this->get('volume')->setValue($object->volume);
		//$this->get('density')->setValue($object->density);
		//$this->get('thLambda')->setValue($object->thLambda);
		//$this->get('thSpecificHeat')->setValue($object->thSpecificHeat);
		//$this->get('type')->setValue($object->type);
	}
	
	public function save($object)
	{
		$data = $this->getData();
		(isset($data['name'])) ? $object->name=$data['name'] : null;
		(isset($data['volume'])) ? $object->volume=$data['volume'] : null;
		//(isset($data['type'])) ? $object->type=$data['type'] : null;
		//(isset($data['density'])) ? $object->density=$data['density'] : null;
		//(isset($data['thLambda'])) ? $object->thLambda=$data['thLambda'] : null;
		//(isset($data['thSpecificHeat'])) ? $object->thSpecificHeat=$data['thSpecificHeat'] : null;
		//(isset($data['weight'])) ? $object->weight=$data['weight'] : null;
	}
	
	
}


