<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
	public function onBootstrap(MvcEvent $e)
	{
		$eventManager        = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);
	}

	public function getConfig()
	{
		return include __DIR__ . '/config/module.config.php';
	}

	public function getAutoloaderConfig()
	{
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}

	public function getControllerConfig()
	{
		return array(
			'factories' => array(
				'Application\Controller\Index' => function(ControllerManager $cm) {
				},
			),
		);
	}
	
	public function __getServiceConfig()
	{
		return array(
			'factories'=>array(
				'SanAuth\Model\MyAuthStorage' => function($sm){
					return new \SanAuth\Model\MyAuthStorage('zf_tutorial');
				},
				'AuthService' => function($sm) {
					//My assumption, you've alredy set dbAdapter
					//and has users table with columns : user_name and pass_word
					//that password hashed with md5
					$dbAdapter           = $sm->get('Zend\Db\Adapter\Adapter');
					$dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter,
												'users','user_name','pass_word', 'MD5(?)');
					$authService = new AuthenticationService();
					$authService->setAdapter($dbTableAuthAdapter);
					$authService->setStorage($sm->get('SanAuth\Model\MyAuthStorage'));
					return $authService;
				},
			),
		);
	}

	public function getServiceConfig()
	{
		return array(
			'abstract_factories' => array(),
			'aliases' => array(),
			'factories' => array(
				'Logger' => 'Application\Service\LoggerFactory',
				'AuthService'=>'Application\Service\AuthFactory'
			),
			'invokables' => array(),
			'services' => array(),
			'shared' => array(),
		);
	}

}
