<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Rbh\Material;
use Application\Form\MaterialForm;

use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class IndexController extends AbstractActionController
{
	public function indexAction()
	{
		if($this->getServiceLocator()->get('AuthService')->hasIdentity() == false){
			return $this->redirect()->toRoute('auth');
		}
		
		$acl = new Acl();
		$acl->addRole(new Role('guest'))
			->addRole(new Role('member'))
			->addRole(new Role('admin'));
		$parents = array('guest', 'member', 'admin');
		$acl->addRole(new Role('someUser'), $parents);
		$acl->addResource(new Resource('someResource'));
		$acl->deny('guest', 'someResource');
		$acl->allow('member', 'someResource');
		echo $acl->isAllowed('someUser', 'someResource') ? 'allowed' : 'denied';		
		
		
		$config = $this->getServiceLocator()->get('Configuration');
		$this->config = isset($config['rbhome']) ? $config['rbhome'] : array();
		var_dump($this->config);
		
		$Logger = $this->getServiceLocator()->get('Logger');
		var_dump($Logger);
		
		return new ViewModel();
	}
	
	public function threejsAction()
	{
	}
	
}
