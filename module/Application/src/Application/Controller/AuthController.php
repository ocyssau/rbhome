<?php
//module/SanAuth/src/SanAuth/Controller/AuthController.php
//namespace SanAuth\Controller;

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Form\Annotation\AnnotationBuilder;
use Zend\View\Model\ViewModel;

use Application\Model\User as AuthUser;
use Application\Form\Auth as AuthForm;

class AuthController extends AbstractActionController
{
	protected $form;
	protected $storage;
	protected $authservice;
	
	/**
	 * 
	 */
	public function getForm()
	{
		if (! $this->form) {
			$this->form = new AuthForm();
		}
		return $this->form;
	}
	
	/**
	 * 
	 */
	public function getAuthService()
	{
	    if (! $this->authservice) {
	        $this->authservice = $this->getServiceLocator()->get('AuthService');
	    }
	    return $this->authservice;
	}
	
	/**
	 * 
	 */
	public function loginAction()
	{
		//if already login, redirect to success page
		if ($this->getAuthService()->hasIdentity()){
			return $this->redirect()->toRoute('success');
		}
		
		$form = $this->getForm();
		return array(
            'form'      => $form,
            'messages'  => $this->flashmessenger()->getMessages()
		);
	}

	/**
	 * 
	 */
	public function authenticateAction()
	{
		$form       = $this->getForm();
		$redirect = 'auth';

		//$identity = $result->getIdentity();
		$request = $this->getRequest();
		
		if ($request->isPost()){
			$form->setData($request->getPost());
			if ($form->isValid()){
				$username=$request->getPost('username');
				$password=$request->getPost('password');
				
				//check authentication...
				$auth = $this->getAuthService();
				$auth->getAdapter()->setUsername($username)->setPassword($password);
				
				$result = $auth->authenticate();
				foreach($result->getMessages() as $message)
				{
					//save message temporary into flashmessenger
					$this->flashmessenger()->addMessage($message);
				}
				
				if ($result->isValid()) {
					$redirect = 'home';
					//check if it has rememberMe :
					if ($request->getPost('rememberme') == 1 ) {
						$this->getSessionStorage()->setRememberMe(1);
						$this->getAuthService()->setStorage($this->getSessionStorage());
					}
					$auth->getStorage()->write($username);
				}
			}
		}
		
		return $this->redirect()->toRoute($redirect);
	}
	
	/**
	 * 
	 */
	public function logoutAction()
	{
		$auth = $this->getServiceLocator()->get('AuthService');
		$auth->clearIdentity();
		
		$this->flashmessenger()->addMessage("You've been logged out");
		return $this->redirect()->toRoute('auth');
	}
}
