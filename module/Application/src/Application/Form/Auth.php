<?php

namespace Application\Form;

use Zend\Form\Form;

class Auth extends Form
{
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('auth');
		$this->setAttribute('method', 'post');

		$this->add(array(
			'name' => 'username',
			'type'=>'Zend\Form\Element\Text',
			'required'=>true,
			'filters' => array(
					array('name' => 'Zend\Filter\StripTags'),
			),
			'attributes' => array(
				'type'  => 'text',
			),
			'options' => array(
				'label' => 'Name',
			),
		));
		$this->add(array(
			'name' => 'password',
			'type'=>'Zend\Form\Element\Password',
			'required'=>true,
			'filters' => array(
				array('name' => 'Zend\Filter\StripTags'),
			),
			'attributes' => array(
				'type'  => 'Password',
			),
			'options' => array(
				'label' => 'Password',
			),
		));
		$this->add(array(
				'name' => 'rememberme',
				'type'=>'Zend\Form\Element\Checkbox',
				'options' => array(
						'label' => 'Remember Me ?:',
				),
		));
		$this->add(array(
			'type'  => 'Zend\Form\Element\Submit',
			'name' => 'submit',
			'attributes' => array(
				'type'  => 'submit',
				'value' => 'Submit',
				'id' => 'submitbutton',
			),
		));
	}
}