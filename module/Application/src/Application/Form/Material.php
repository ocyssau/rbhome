<?php

namespace Application\Form;

use Zend\Form\Form;

class MaterialForm extends Form
{
	public function __construct($name = null)
	{
		// we want to ignore the name passed
		parent::__construct('material');
		$this->setAttribute('method', 'post');
		$this->add(array(
				'name' => 'id',
				'attributes' => array(
						'type'  => 'hidden',
				),
		));
		$this->add(array(
				'name' => 'name',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Name',
				),
		));
		$this->add(array(
				'name' => 'density',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Density',
				),
		));
		$this->add(array(
				'name' => 'type',
				'attributes' => array(
						'type'  => 'text',
				),
				'options' => array(
						'label' => 'Type',
				),
		));
		$this->add(array(
				'name' => 'submit',
				'attributes' => array(
						'type'  => 'submit',
						'value' => 'Go',
						'id' => 'submitbutton',
				),
		));
	}
}