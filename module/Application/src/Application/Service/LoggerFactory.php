<?php

namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoggerFactory implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$logger = new \Zend\Log\Logger();
		$writer = new \Zend\Log\Writer\Syslog();
		$writer->setApplicationName('Rbh');
		$logger->addWriter($writer);
		return $logger;
	}
}
