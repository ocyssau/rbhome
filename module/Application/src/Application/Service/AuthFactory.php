<?php

namespace Application\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\Authentication\Adapter\Digest as AuthAdapter;
use Zend\Authentication\AuthenticationService;

class AuthFactory implements FactoryInterface
{
	public function createService(ServiceLocatorInterface $serviceLocator)
	{
		$auth = new AuthenticationService();
		
		$filename = './config/digest.password';
		$realm = 'test area';
		//echo md5($username.':'.$realm.':'.$password);
		
		$auth->setAdapter(new AuthAdapter($filename,$realm));
		
		return $auth;
	}
}
