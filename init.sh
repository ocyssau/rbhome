#!/bin/bash
sudo -u postgres dropdb --interactive rbh
sudo -u postgres dropdb --interactive rbh-test
sudo -u postgres createuser -P -e --createrole rbh
sudo -u postgres createdb rbh --owner rbh
sudo -u postgres createdb rbh-test --owner rbh
sudo -u postgres psql -d rbh <<EOF
\x
CREATE LANGUAGE plpgsql;
CREATE EXTENSION ltree;
EOF
sudo -u postgres psql -d rbh-test <<EOF
\x
CREATE LANGUAGE plpgsql;
CREATE EXTENSION ltree;
EOF
php ./vendor/Rbh/test.cli.php --initdb

curl -sS https://getcomposer.org/installer | php

