<?php

namespace ComposerScript;

use Composer\Script\Event;

class Installer
{
	public static function postUpdate(Event $event)
	{
		$composer = $event->getComposer();

		if(!is_dir('./public/js/bootstrap')){
			mkdir('./public/js/bootstrap');
		}
		if(!is_dir('./public/css/bootstrap')){
			mkdir('./public/css/bootstrap');
		}
		if(!is_dir('./public/fonts')){
			mkdir('./public/fonts');
		}

		$Dir = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/css');
		foreach($Dir as $file){
			if($Dir->isFile()){
				copy($Dir->getPathname(), './public/css/bootstrap/'.$Dir->getFileName());
			}
		}

		$Dir = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/js');
		foreach($Dir as $file){
			if($Dir->isFile()){
				copy($Dir->getPathname(), './public/js/bootstrap/'.$Dir->getFileName());
			}
		}

		$Dir = new \DirectoryIterator('./vendor/twitter/bootstrap/dist/fonts');
		foreach($Dir as $file){
			if($Dir->isFile()){
				copy($Dir->getPathname(), './public/fonts/'.$Dir->getFileName());
			}
		}
	}

	public static function postPackageInstall(Event $event)
	{
		$installedPackage = $event->getOperation()->getPackage();
	}

	public static function warmCache(Event $event)
	{
		// make cache toasty
	}
}
